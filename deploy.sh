#/bin/bash
echo "building docker image"
docker build --pull -f Dockerfile.prod -t git.uni-due.de:6666/commoop/commoop-web .

echo "pushing docker image"
docker push git.uni-due.de:6666/commoop/commoop-web

echo "deploying docker image"
ssh barkmin@app.cs.uni-due.de << EOF
 cd /data2/docker/commoop
 docker-compose pull
 docker-compose up -d --no-deps --build web
EOF

echo "deployed"
