import * as trace from './trace';
import * as parsonsPuzzle from './parsonsPuzzle';
import * as highlight from './highlight';
import * as fillInTheBlank from './fillInTheBlank';
import * as common from './common';

export default {
  trace: trace,
  highlight: highlight,
  'parsons-puzzle': parsonsPuzzle,
  'fill-in-the-blank': fillInTheBlank,
  common: common
};
