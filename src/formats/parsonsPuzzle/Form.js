import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import { List, Map } from 'immutable';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import ArrowUpwardIcon from '@material-ui/icons/ArrowUpward';
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';
import LockIcon from '@material-ui/icons/Lock';
import LinkIcon from '@material-ui/icons/Link';
import Typography from '@material-ui/core/Typography/Typography';

import MarkdownEditor from '../../admin/components/MarkdownEditor';
import PureTextField from '../../common/components/PureTextField';

const styles = theme => ({
  fragment: {
    display: 'flex',
    width: '100%'
  },
  sourceEditor: {
    flexGrow: 1
  },
  actions: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center'
  },
  groupId: {
    width: 40
  }
});

class Form extends React.Component {
  static propTypes = {
    privateData: PropTypes.instanceOf(Map).isRequired,
    publicData: PropTypes.instanceOf(Map).isRequired,
    onPublicChange: PropTypes.func.isRequired,
    onPrivateChange: PropTypes.func.isRequired
  };

  addFragment = () => {
    const { onPublicChange, publicData } = this.props;
    const fragment = Map({
      type: 'default',
      source: ''
    });
    onPublicChange(
      publicData.update(
        'fragments',
        fragments =>
          fragments ? fragments.push(fragment) : new List([fragment])
      )
    );
  };

  onSourceChange = (value, fragmentId) => {
    const { publicData, onPublicChange } = this.props;
    onPublicChange(
      publicData.updateIn(['fragments', fragmentId], fragment =>
        fragment.set('source', value)
      )
    );
  };

  onToggleFixed = fragmentId => {
    const { publicData, onPublicChange } = this.props;
    onPublicChange(
      publicData.updateIn(['fragments', fragmentId], fragment =>
        fragment.set(
          'type',
          fragment.get('type') !== 'fixed' ? 'fixed' : 'default'
        )
      )
    );
  };

  onToggleGroup = fragmentId => {
    const { publicData, onPublicChange } = this.props;
    onPublicChange(
      publicData.updateIn(['fragments', fragmentId], fragment =>
        fragment
          .set(
            'type',
            fragment.get('type') !== 'grouped' ? 'grouped' : 'default'
          )
          .delete('group_id')
      )
    );
  };

  onGroupIdChange = (e, fragmentId) => {
    const { publicData, onPublicChange } = this.props;
    const groupId = e.currentTarget.value;
    onPublicChange(
      publicData.updateIn(['fragments', fragmentId], fragment =>
        fragment.set('group_id', groupId)
      )
    );
  };

  onDelete = fragmentId => {
    const { publicData, onPublicChange } = this.props;
    onPublicChange(publicData.deleteIn(['fragments', fragmentId]));
  };

  moveUp = fragmentId => {
    const { publicData, onPublicChange } = this.props;
    const fragments = publicData.get('fragments');

    onPublicChange(
      publicData.set(
        'fragments',
        fragments
          .delete(fragmentId)
          .insert(fragmentId - 1, fragments.get(fragmentId))
      )
    );
  };
  moveDown = fragmentId => {
    const { publicData, onPublicChange } = this.props;
    const fragments = publicData.get('fragments');

    onPublicChange(
      publicData.set(
        'fragments',
        fragments
          .delete(fragmentId)
          .insert(fragmentId + 1, fragments.get(fragmentId))
      )
    );
  };

  render() {
    const { classes, publicData } = this.props;
    const fragments = publicData.get('fragments', new List([]));
    return (
      <div>
        <Typography>Fragments</Typography>
        {fragments.map((fragment, i) => (
          <div className={classes.fragment} key={i}>
            <div className={classes.actions}>
              <IconButton onClick={() => this.onDelete(i)}>
                <DeleteIcon />
              </IconButton>
              <div>
                <IconButton onClick={() => this.moveUp(i)}>
                  <ArrowUpwardIcon />
                </IconButton>
                <IconButton onClick={() => this.moveDown(i)}>
                  <ArrowDownwardIcon />
                </IconButton>
              </div>
              <div>
                {fragment.get('type') !== 'grouped' && (
                  <IconButton
                    color={
                      fragment.get('type') === 'fixed' ? 'primary' : 'default'
                    }
                    onClick={() => this.onToggleFixed(i)}
                  >
                    <LockIcon />
                  </IconButton>
                )}
                {fragment.get('type') !== 'fixed' && (
                  <IconButton
                    color={
                      fragment.get('type') === 'grouped' ? 'primary' : 'default'
                    }
                    onClick={() => this.onToggleGroup(i)}
                  >
                    <LinkIcon />
                  </IconButton>
                )}
                {fragment.get('type') === 'grouped' && (
                  <PureTextField
                    label="Id"
                    className={classes.groupId}
                    type="number"
                    value={fragment.get('group_id')}
                    onChange={e => this.onGroupIdChange(e, i)}
                  />
                )}
              </div>
            </div>
            <div className={classes.sourceEditor}>
              <MarkdownEditor
                value={fragment.get('source')}
                height="120px"
                onChange={value => this.onSourceChange(value, i)}
              />
            </div>
          </div>
        ))}
        <Button variant="contained" onClick={this.addFragment}>
          Add
        </Button>
      </div>
    );
  }
}

export default withStyles(styles)(Form);
