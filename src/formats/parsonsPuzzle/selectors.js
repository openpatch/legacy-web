/**
 * Select the pure solution without ui properties.
 */

export default state => state.delete('activeFragment');
