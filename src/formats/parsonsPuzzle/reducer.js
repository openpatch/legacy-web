import { fromJS } from 'immutable';

import {
  INIT_FRAGMENTS,
  MOVE_FROM_SOURCE_TO_USER,
  MOVE_FROM_USER_TO_SOURCE,
  MOVE_WITHIN_USER
} from './constants';

const initialState = fromJS({
  userFragments: [],
  sourceFragments: []
});

function ParsonsPuzzleReducer(state = initialState, action) {
  switch (action.type) {
    case INIT_FRAGMENTS:
      return state
        .set('userFragments', fromJS(action.userFragments))
        .set('sourceFragments', fromJS(action.sourceFragments));
    case MOVE_FROM_SOURCE_TO_USER:
      return state
        .update('userFragments', fragments =>
          fragments.insert(
            action.targetId,
            state.getIn(['sourceFragments', action.sourceId])
          )
        )
        .deleteIn(['sourceFragments', action.sourceId]);
    case MOVE_FROM_USER_TO_SOURCE:
      return state
        .update('sourceFragments', fragments =>
          fragments.insert(
            action.targetId,
            state.getIn(['userFragments', action.sourceId])
          )
        )
        .deleteIn(['userFragments', action.sourceId]);
    case MOVE_WITHIN_USER:
      const fragment = state.getIn(['userFragments', action.sourceId]);
      return state
        .deleteIn(['userFragments', action.sourceId])
        .update('userFragments', fragments =>
          fragments.insert(action.targetId, fragment)
        );
    default:
      return state;
  }
}

export default ParsonsPuzzleReducer;
