import React from 'react';
import { DragDropContext, Droppable } from 'react-beautiful-dnd';
import _isEmpty from 'lodash/isEmpty';
import _isEqual from 'lodash/isEqual';
import _find from 'lodash/find';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography/Typography';

import FragmentDnD from './components/FragmentDnD';
import {
  initFragments,
  moveFromSourceToUser,
  moveFromUserToSource,
  moveWithinUser
} from './actions';

const styles = theme => ({
  container: {
    display: 'flex'
  },
  source: {
    width: 500,
    minHeight: 500,
    padding: theme.spacing.unit
  },
  user: {
    width: 500,
    minHeight: 500,
    padding: theme.spacing.unit
  }
});

class ParsonsPuzzle extends React.Component {
  static defaultProps = {
    sourceFragments: [],
    userFragments: [],
    content: {
      fragments: []
    }
  };

  constructor(props) {
    super(props);
    let userFragments = [];
    let sourceFragments = [];
    const { fragments } = props.content;
    if (fragments) {
      fragments.forEach((fragment, i) => {
        if (fragment.type === 'fixed') {
          userFragments.push({ ...fragment, id: i });
        } else {
          sourceFragments.push({ ...fragment, id: i });
        }
      });
    }
    if (_isEmpty(props.sourceFragments) && _isEmpty(props.userFragments)) {
      props.dispatch(initFragments(userFragments, sourceFragments));
    }
  }

  shouldComponentUpdate(nextProps) {
    return !_isEqual(nextProps, this.props);
  }

  onDragEnd = result => {
    const { destination, source } = result;
    if (destination === null) return;

    if (destination.droppableId === 'user' && source.droppableId === 'source') {
      this.props.dispatch(
        moveFromSourceToUser(source.index, destination.index)
      );
    } else if (
      destination.droppableId === 'source' &&
      source.droppableId === 'source'
    ) {
      return;
    } else if (
      destination.droppableId === 'source' &&
      source.droppableId === 'user'
    ) {
      this.props.dispatch(
        moveFromUserToSource(source.index, destination.index)
      );
    } else if (
      destination.droppableId === 'user' &&
      source.droppableId === 'user'
    ) {
      this.props.dispatch(moveWithinUser(source.index, destination.index));
    }
  };

  render() {
    const { classes, userFragments, sourceFragments } = this.props;
    return (
      <div className={classes.container}>
        <DragDropContext onDragEnd={this.onDragEnd}>
          <Droppable droppableId="source">
            {(provided, snapshot) => (
              <div ref={provided.innerRef} {...provided.droppableProps}>
                <Paper className={classes.source}>
                  <Typography>Drag from here</Typography>
                  {sourceFragments.map((sourceFragment, index) => (
                    <FragmentDnD
                      key={sourceFragment.id}
                      index={index}
                      {...sourceFragment}
                      isDragDisabled={
                        sourceFragment.type === 'grouped' &&
                        undefined !==
                          _find(
                            userFragments,
                            userFragment =>
                              userFragment.group_id === sourceFragment.group_id
                          )
                      }
                    />
                  ))}
                  {provided.placeholder}
                </Paper>
              </div>
            )}
          </Droppable>
          <Droppable droppableId="user">
            {(provided, snapshot) => (
              <div ref={provided.innerRef} {...provided.droppableProps}>
                <Paper className={classes.user}>
                  <Typography>Drop here</Typography>
                  {userFragments.map((userFragment, index) => (
                    <FragmentDnD
                      key={userFragment.id}
                      index={index}
                      {...userFragment}
                      isDragDisabled={userFragment.type === 'fixed'}
                    />
                  ))}
                  {provided.placeholder}
                </Paper>
              </div>
            )}
          </Droppable>
        </DragDropContext>
      </div>
    );
  }
}

export default withStyles(styles)(ParsonsPuzzle);
