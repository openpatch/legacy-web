import {
  INIT_FRAGMENTS,
  MOVE_FROM_SOURCE_TO_USER,
  MOVE_FROM_USER_TO_SOURCE,
  MOVE_WITHIN_USER
} from './constants';

export function moveFromSourceToUser(sourceId, targetId) {
  return {
    type: MOVE_FROM_SOURCE_TO_USER,
    sourceId,
    targetId
  };
}

export function moveFromUserToSource(sourceId, targetId) {
  return {
    type: MOVE_FROM_USER_TO_SOURCE,
    sourceId,
    targetId
  };
}

export function moveWithinUser(sourceId, targetId) {
  return {
    type: MOVE_WITHIN_USER,
    sourceId,
    targetId
  };
}

export function initFragments(userFragments, sourceFragments) {
  return {
    type: INIT_FRAGMENTS,
    userFragments,
    sourceFragments
  };
}
