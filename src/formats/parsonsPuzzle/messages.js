/**
 * For using json files for translations.
 * @see https://github.com/yahoo/react-intl
 */
import { defineMessages } from 'react-intl';

/**
 * Define all strings, which will be used in this format here. Then use
 * <FormattedMessages {...messages.example} /> to use the translatable message.
 * 
 * Every message should have an id, which should be prefixed with
 * commoop.formats.ParsonsPuzzle.{someUniqueIdentifier}
 */
export default defineMessages({
  formatName: {
    id: 'commoop.formats.ParsonsPuzzle.formatName',
    defaultMessage: 'Parsons Puzzle'
  }
});
