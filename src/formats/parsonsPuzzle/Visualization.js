/**
 *
 * Visualization
 *
 */

import React from 'react';
import { Map, List } from 'immutable';
import Graph from 'react-graph-vis';
import { withStyles } from '@material-ui/core/styles';

const accept = 'green';
const start = 'orange';
const end = 'red';

const styles = theme => ({
  container: {
    border: '1px #000 solid',
    height: 600,
    margin: theme.spacing.unit,
    '& .vis-tooltip': {
      zIndex: 999,
      position: 'absolute',
      backgroundColor: 'white',
      padding: theme.spacing.unit,
      border: '1px #EAEAEA solid'
    }
  },
  legend: {
    display: 'flex',
    justifyContent: 'center'
  },
  startNode: {
    display: 'inline-block',
    backgroundColor: start,
    width: 10,
    height: 10,
    borderRadius: '50%',
    marginLeft: theme.spacing.unit
  },
  endNode: {
    display: 'inline-block',
    backgroundColor: end,
    width: 10,
    height: 10,
    borderRadius: '50%',
    marginLeft: theme.spacing.unit
  },
  acceptNode: {
    display: 'inline-block',
    backgroundColor: accept,
    width: 10,
    height: 10,
    borderRadius: '50%',
    marginLeft: theme.spacing.unit
  }
});

class Visualization extends React.Component {
  render() {
    const { classes, data } = this.props;
    const graph = data.get('graph');
    if (!graph) {
      return null;
    }

    const edges = graph.get('edges', new List([]));
    const nodes = graph.get('nodes', new Map({}));

    const visEdges = edges
      .map(edge => ({
        from: edge.get('source'),
        to: edge.get('target'),
        value: edge.get('payload'),
        label: `${edge.get('payload')}`,
        color: {
          color: 'lightskyblue',
          inherit: false
        }
      }))
      .toJS();
    const visNodes = nodes
      .map((node, key) => ({
        id: key,
        color:
          node.get('type') === 'start'
            ? start
            : node.get('type') === 'accept'
              ? accept
              : node.get('type') === 'end'
                ? end
                : null,
        value: node.get('payload'),
        label: `${node.get('payload')}`,
        title: node.get('html')
      }))
      .toList()
      .toJS();
    const options = {
      edges: {
        arrows: {
          to: {
            enabled: true,
            scaleFactor: 0.5,
            type: 'arrow'
          }
        },
        scaling: {
          max: 10
        }
      },
      nodes: {
        shape: 'box',
        scaling: {
          label: {
            enabled: true
          }
        }
      },
      layout: {
        randomSeed: 12345
      }
    };
    return (
      <React.Fragment>
        <div className={classes.legend}>
          <div>
            <span className={classes.startNode} /> Start
          </div>
          <div>
            <span className={classes.endNode} /> End
          </div>
          <div>
            <span className={classes.acceptNode} /> Accept
          </div>
        </div>
        <div className={classes.container}>
          <Graph
            graph={{ edges: visEdges, nodes: visNodes }}
            options={options}
          />
        </div>
      </React.Fragment>
    );
  }
}

export default withStyles(styles)(Visualization);
