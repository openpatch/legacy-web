import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import LockIcon from '@material-ui/icons/Lock';
import LinkIcon from '@material-ui/icons/Link';

import Markdown from '../../../common/components/Markdown';

const styles = theme => ({
  fragment: {
    userSelect: 'none',
    display: 'flex',
    padding: theme.spacing.unit,
    border: '2px dotted lightgrey',
    borderRadius: theme.spacing.unit,
    fontFamily: 'Roboto',
    justifyContent: 'space-between',
    '& .hljs': {
      background: 'none'
    }
  },
  container: {
    padding: theme.spacing.unit
  },
  grouped: {
    fontFamily: 'Roboto',
    color: '#696969',
    display: 'flex',
    alignItems: 'center',
    fontWeight: 'bold'
  },
  typeIcon: {
    width: 16,
    height: 16,
    marginLeft: 4,
    color: '#AEAEAE'
  }
});

class Fragment extends React.Component {
  render() {
    const { classes, source, type, group_id } = this.props;
    return (
      <div className={classes.container}>
        <div className={classes.fragment}>
          <Markdown source={source} />
          {type === 'fixed' && <LockIcon className={classes.typeIcon} />}
          {type === 'grouped' && (
            <div className={classes.grouped}>
              <LinkIcon className={classes.typeIcon} />
              {group_id}
            </div>
          )}
        </div>
      </div>
    );
  }
}

export default withStyles(styles)(Fragment);
