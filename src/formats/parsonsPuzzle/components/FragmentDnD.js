import React from 'react';
import { Draggable } from 'react-beautiful-dnd';

import Fragment from './Fragment';

class FragmentDnD extends React.Component {
  render() {
    const { id, index, ...props } = this.props;
    const isDragDisabled = props.isDragDisabled;
    return (
      <Draggable draggableId={id} index={index} isDragDisabled={isDragDisabled}>
        {(provided, snapshot) => (
          <div
            ref={provided.innerRef}
            {...provided.dragHandleProps}
            {...provided.draggableProps}
          >
            <Fragment {...props} />
            {provided.placeholder}
          </div>
        )}
      </Draggable>
    );
  }
}

export default FragmentDnD;
