export const INIT_FRAGMENTS = 'commoop.formats.ParsonsPuzzle.INIT_FRAGMENTS';

export const MOVE_FROM_SOURCE_TO_USER =
  'commoop.formats.ParsonsPuzzle.MOVE_FROM_SOURCE_TO_USER';

export const MOVE_FROM_USER_TO_SOURCE =
  'commoop.formats.ParsonsPuzzle.MOVE_FROM_USER_TO_SOURCE';

export const MOVE_WITHIN_USER =
  'commoop.formats.ParsonsPuzzle.MOVE_WITHIN_USER';
