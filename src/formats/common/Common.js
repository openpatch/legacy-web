import React from 'react';
import PropTypes from 'prop-types';

import Markdown from '../../common/components/Markdown';

export class Common extends React.Component {
  static propTypes = {
    content: PropTypes.shape({
      markdown: PropTypes.string
    })
  };

  static defaultProps = {};

  render() {
    const { content } = this.props;
    return (
      <div>
        <Markdown source={content.markdown} escapeHtml={false} />
      </div>
    );
  }
}

export default Common;
