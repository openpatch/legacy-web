import { fromJS } from 'immutable';

import pureSolutionSelector from './selectors';

const state = fromJS({});

describe('Common selectors', () => {
  it('should return pure solution', () => {
    expect(pureSolutionSelector(state)).toEqual(fromJS({}));
  });
});
