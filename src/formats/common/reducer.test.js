import { fromJS } from 'immutable';

import reducer from './reducer';
import * as constants from './constants';

describe('Common reducer', () => {
  it('returns the initial state', () => {
    expect(reducer(undefined, {})).toEqual(fromJS({}));
  });
});
