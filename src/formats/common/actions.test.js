import * as actions from './actions';
import * as constants from './constants';

describe('Instructions actions', () => {
  test('Default Action', () => {
    const expected = {
      type: constants.DEFAULT_ACTION
    };
    expect(actions.defaultAction()).toEqual(expected);
  });
});

