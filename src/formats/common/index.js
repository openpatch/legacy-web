/**
 * Nothing to do here!
 */
import messages from './messages';
import * as constants from './constants';
import reducer from './reducer';
import selectors from './selectors';
import Form from './Form';
import Common from './Common';
import banner from './images/banner.jpg';

export default Common;

export {
  Common as Component,
  Form,
  messages,
  constants,
  reducer,
  selectors,
  banner
};
