import formats from './index';

export function getFormatNameMessage(type) {
  if (formats.hasOwnProperty(type)) {
    const format = formats[type];
    const messages = format.messages;
    const nameMessage = messages.formatName;
    return nameMessage;
  }
  throw new Error(`formats ${type} is not supported`);
}
