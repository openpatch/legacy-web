/*
 *
 * HighlightText reducer
 *
 */

import { fromJS } from 'immutable';
import {
  DEFAULT_ACTION,
  SET_HIGHLIGHTS,
  DELETE_HIGHLIGHT,
  DELETE_HIGHLIGHTS
} from './constants';

const initialState = fromJS({
  highlights: 'type:TextRange'
});

function HighlightReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;
    case DELETE_HIGHLIGHT:
    case SET_HIGHLIGHTS:
      return state.set('highlights', action.highlights);
    case DELETE_HIGHLIGHTS:
      return initialState;
    default:
      return state;
  }
}

export default HighlightReducer;
