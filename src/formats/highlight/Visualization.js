/**
 *
 * Visualization
 *
 */

import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import { List } from 'immutable';
import Tooltip from '@material-ui/core/Tooltip';
import amber from '@material-ui/core/colors/amber';

const styles = theme => ({
  heatmap: {
    fontFamily: 'monospace'
  },
  heatmapRow: {
    marginBottom: theme.spacing.unit
  },
  heatmapEntry: {
    height: 18,
    width: 13,
    cursor: 'default',
    textAlign: 'center',
    display: 'inline-block',
    verticalAlign: 'middle',
    padding: 2
  }
});

const colorBuckets = [50, 100, 200, 300, 400, 500, 600, 700, 800, 900];

class Visualization extends React.Component {
  render() {
    const { classes, data } = this.props;
    const matrix = data.get('frequency_matrix', new List([]));

    let maxValue = 0;
    matrix.forEach(row =>
      row.forEach(entry => (maxValue = Math.max(maxValue, entry.get('value'))))
    );

    let index = 0;
    const heatmap = matrix.map(row => {
      const line = row.map(entry => (
        <Tooltip
          key={index++}
          title={`${entry.get('char', 'Break')}: ${entry.get('value')}`}
        >
          <div
            className={classes.heatmapEntry}
            style={{
              backgroundColor:
                amber[
                  colorBuckets[
                    Math.round(
                      (entry.get('value') / maxValue) *
                        (colorBuckets.length - 1)
                    )
                  ]
                ]
            }}
          >
            {entry.get('char') === '\n' ? '⤶' : entry.get('char')}
          </div>
        </Tooltip>
      ));
      return (
        <div className={classes.heatmapRow}>
          {line.push(<br key={'br' + index} />)}
        </div>
      );
    });

    return <div className={classes.heatmap}>{heatmap}</div>;
  }
}

export default withStyles(styles)(Visualization);
