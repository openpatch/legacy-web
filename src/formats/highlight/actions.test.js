import * as actions from './actions';

describe('MultipleChoice actions', () => {
  it('should create default action action', () => {
    expect(actions.defaultAction()).toMatchSnapshot();
  });

  it('should create set highlights action', () => {
    const highlights =
      'type:TextRange|0$15$1$highlight$hts|16$20$2$highlight$hts';
    expect(actions.setHighlights(highlights)).toMatchSnapshot();
  });

  it('should create delete highlights action', () => {
    const highlights =
      'type:TextRange|0$15$1$highlight$hts|16$20$2$highlight$hts';
    expect(actions.deleteHighlight(highlights)).toMatchSnapshot();
  });
});
