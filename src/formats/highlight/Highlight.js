import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import { injectIntl } from 'react-intl';
import _isEqual from 'lodash/isEqual';
import rangy from 'rangy';
import 'rangy/lib/rangy-textrange';
import 'rangy/lib/rangy-highlighter';
import 'rangy/lib/rangy-classapplier';

import { setHighlights, deleteHighlight, deleteHighlights } from './actions';

import Markdown from '../../common/components/Markdown';
import highlightMessages from './messages';

const styles = theme => ({
  deleteHighlights: {
    margin: 10,
    float: 'right'
  }
});

class Highlight extends React.Component {
  static defaultProps = {
    highlights: '',
    content: {
      text: ''
    }
  };

  shouldComponentUpdate(nextProps) {
    return !_isEqual(nextProps, this.props);
  }

  componentDidMount() {
    this.highlighter = rangy.createHighlighter(null, 'TextRange');
    this.highlighter.addClassApplier(
      rangy.createClassApplier('highlight', {
        ignoreWhiteSpace: true,
        elementTagName: 'mark',
        tagNames: ['a']
      })
    );
    window.addEventListener('touchend', this.onHighlightsChange);
    window.addEventListener('mouseup', this.onHighlightsChange);
    this.setHighlights();
  }

  componentWillUnmount() {
    window.addEventListener('touchend', this.onHighlightsChange);
    window.removeEventListener('mouseup', this.onHighlightsChange);
  }

  componentDidUpdate() {
    this.setHighlights();
  }

  componentWillUpdate() {
    this.cleanHighlights();
  }

  setHighlights = () => {
    if (this.props.highlights) {
      this.highlighter.deserialize(this.props.highlights);
      const highlights = this.hts.getElementsByClassName('highlight');
      setTimeout(() => {
        Array.from(highlights).forEach(highlight => {
          highlight.addEventListener('click', this.onHighlightDelete);
        });
      }, 100);
    }
  };

  onHighlightsChange = e => {
    e.preventDefault();
    this.highlighter.highlightSelection('highlight', {
      containerElementId: 'hts'
    });
    const highlights = this.highlighter.serialize('highlight');
    const selection = rangy.getSelection().saveCharacterRanges(this.hts)[0];
    if (
      highlights.localeCompare(this.props.highlights) !== 0 &&
      selection &&
      (selection.characterRange.start >= 0 && selection.characterRange.end >= 0)
    ) {
      this.props.dispatch(setHighlights(highlights));
    }
    rangy.getSelection().removeAllRanges();
  };

  onHighlightDelete = e => {
    const { dispatch } = this.props;
    const highlight = this.highlighter.getHighlightForElement(e.target);
    if (highlight) {
      this.highlighter.removeHighlights([highlight]);
      const highlights = this.highlighter.serialize('highlight');
      dispatch(deleteHighlight(highlights));
    }
  };

  onHighlightsDelete = e => {
    const { dispatch } = this.props;
    dispatch(deleteHighlights());
  };

  cleanHighlights = () => {
    const markings = this.hts.querySelectorAll('.highlight');
    markings.forEach(marking => {
      const parent = marking.parentNode;
      marking.removeEventListener('click', this.onHighlightDelete);
      while (marking.firstChild) {
        parent.insertBefore(marking.firstChild, marking);
      }
      parent.removeChild(marking);
    });
  };

  render() {
    const { content, intl, classes } = this.props;
    return (
      <div>
        <div id="hts" ref={hts => (this.hts = hts)}>
          <Markdown source={content.text} />
        </div>
        <Button
          variant="contained"
          className={classes.deleteHighlights}
          onClick={this.onHighlightsDelete}
        >
          {intl.formatMessage(highlightMessages.deleteHighlights)}
        </Button>
      </div>
    );
  }
}

export default withStyles(styles)(injectIntl(Highlight));
