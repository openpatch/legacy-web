/**
 *
 * Statistic
 *
 */

import React from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import { Map } from 'immutable';

const round = value => Math.round(value * 100) / 100;

const StatisticTable = ({ rows }) => (
  <Table>
    <TableHead>
      <TableRow>
        <TableCell>Metric</TableCell>
        <TableCell>Min</TableCell>
        <TableCell>Max</TableCell>
        <TableCell>Median</TableCell>
        <TableCell>SD</TableCell>
        <TableCell>Variance</TableCell>
        <TableCell>N</TableCell>
      </TableRow>
    </TableHead>
    <TableBody>
      {rows
        .map((value, key) => (
          <TableRow key={key}>
            <TableCell>{key}</TableCell>
            <TableCell>{round(value.get('min'))}</TableCell>
            <TableCell>{round(value.get('max'))}</TableCell>
            <TableCell>{round(value.get('median'))}</TableCell>
            <TableCell>{round(value.get('standard_deviation'))}</TableCell>
            <TableCell>{round(value.get('variance'))}</TableCell>
            <TableCell>{value.get('n')}</TableCell>
          </TableRow>
        ))
        .toList()}
    </TableBody>
  </Table>
);

class Statistic extends React.Component {
  render() {
    const { data } = this.props;
    const rows = new Map({
      alpha: data.get('alpha', new Map({})),
      rho: data.get('rho', new Map({})),
      kappa: data.get('kappa', new Map({}))
    });
    return (
      <div>
        <StatisticTable rows={rows} />
      </div>
    );
  }
}

export default Statistic;
