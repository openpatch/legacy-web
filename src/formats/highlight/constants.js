/**
 *
 * HighlightText constants
 *
 */

export const DEFAULT_ACTION = 'commoop.formats.Highlight.DEFAULT_ACTION';

export const SET_HIGHLIGHTS = 'commoop.formats.Highlight.SET_HIGHLIGHTS';

export const DELETE_HIGHLIGHT = 'commoop.formats.Highlight.DELETE_HIGHLIGHT';

export const DELETE_HIGHLIGHTS = 'commoop.formats.Highlights.DELETE_HIGHLIGHTS';