import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { Map } from 'immutable';
import Typography from '@material-ui/core/Typography';

import MarkdownEditor from '../../admin/components/MarkdownEditor';

const styles = theme => ({});

class Form extends React.PureComponent {
  static propTypes = {
    privateData: PropTypes.instanceOf(Map).isRequired,
    publicData: PropTypes.instanceOf(Map).isRequired,
    onPublicChange: PropTypes.func.isRequired,
    onPrivateChange: PropTypes.func.isRequired
  };

  onTextChange = value => {
    const { onPublicChange, publicData } = this.props;
    onPublicChange(publicData.set('text', value));
  };

  render() {
    const { publicData } = this.props;
    return (
      <div>
        <Typography>Text</Typography>
        <MarkdownEditor
          value={publicData.get('text')}
          onChange={this.onTextChange}
        />
      </div>
    );
  }
}

export default withStyles(styles)(Form);
