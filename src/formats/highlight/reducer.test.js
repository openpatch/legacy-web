import { fromJS } from 'immutable';

import reducer from './reducer';
import * as constants from './constants';

const initialStore = fromJS({
  highlights: 'type:TextRange'
});

const mockStore = fromJS({
  highlights: 'type:TextRange|0$15$1$highlight$hts|16$20$2$highlight$hts'
});

describe('HighlightText reducer', () => {
  it('should return the inital state', () => {
    expect(reducer(undefined, {})).toEqual(initialStore);
  });

  it('should handle DEFAULT_ACTION', () => {
    expect(
      reducer(mockStore, {
        type: constants.DEFAULT_ACTION
      })
    ).toEqual(mockStore);
  });

  it('should handle DELETE_HIGHLIGHT', () => {
    const highlights = 'type:TextRange';
    expect(
      reducer(mockStore, {
        type: constants.DELETE_HIGHLIGHT,
        highlights
      })
    ).toEqual(mockStore.set('highlights', highlights));
  });

  it('should handle SET_HIGHLIGHT', () => {
    const highlights = 'type:TextRange|0$15$1$highlight$hts';
    expect(
      reducer(mockStore, {
        type: constants.DELETE_HIGHLIGHT,
        highlights
      })
    ).toEqual(mockStore.set('highlights', highlights));
  });
});
