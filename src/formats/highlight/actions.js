/**
 *
 * MultipleChoice actions
 *
 */

import {
  DEFAULT_ACTION,
  SET_HIGHLIGHTS,
  DELETE_HIGHLIGHT,
  DELETE_HIGHLIGHTS
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION
  };
}

export function setHighlights(highlights) {
  return {
    type: SET_HIGHLIGHTS,
    highlights
  };
}

export function deleteHighlight(highlights) {
  return {
    type: DELETE_HIGHLIGHT,
    highlights
  };
}

export function deleteHighlights() {
  return {
    type: DELETE_HIGHLIGHTS
  };
}
