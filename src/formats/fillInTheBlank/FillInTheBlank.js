import React from 'react';
import PropTypes from 'prop-types';

import Markdown from '../../common/components/Markdown';
import CodeBlockWithInputs from './components/CodeBlockWithInputs';
import { setValueForId } from './actions';

export class FillInTheBlank extends React.Component {
  static propTypes = {
    inputs: PropTypes.object,
    content: PropTypes.shape({
      text: PropTypes.string
    })
  };

  static defaultProps = {
    inputs: {}
  };

  setRef = el => {
    this.inputsEl = el;
  };

  onInputChange = e => {
    const { dispatch } = this.props;
    const value = e.currentTarget.value;
    const id = e.currentTarget.id;
    dispatch(setValueForId(value, id));
  };

  componentDidUpdate() {
    const { inputs } = this.props;
    const htmlInputs = [...this.inputsEl.getElementsByTagName('input')];
    htmlInputs.forEach((input, i) => {
      if (input.id === '') {
        input.id = i;
      }
      input.value = inputs[input.id] || '';
    });
  }

  componentDidMount() {
    const { inputs } = this.props;
    const htmlInputs = [...this.inputsEl.getElementsByTagName('input')];
    htmlInputs.forEach((input, i) => {
      if (input.id === '') {
        input.id = i;
      }
      input.value = inputs[input.id] || '';
      input.addEventListener('change', this.onInputChange);
      input.addEventListener('keyup', this.onInputChange);
    });
  }

  componentWillUnmount() {
    const inputs = [...this.inputsEl.getElementsByTagName('input')];
    inputs.forEach((input, i) => {
      input.removeEventListener('change');
      input.removeEventListener('keyup');
    });
  }

  render() {
    const { content } = this.props;
    return (
      <div>
        <div ref={this.setRef}>
          <Markdown
            source={content.text}
            escapeHtml={false}
            renderers={{
              code: CodeBlockWithInputs
            }}
          />
        </div>
      </div>
    );
  }
}

export default FillInTheBlank;
