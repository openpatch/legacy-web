/**
 *
 * Statistic
 *
 */

import React from 'react';
import { Map } from 'immutable';
import _reduce from 'lodash/reduce';
import _map from 'lodash/map';
import Tooltip from '@material-ui/core/Tooltip';
import Divider from '@material-ui/core/Divider';

import TagCloud from 'react-tag-cloud';

const createTags = frequency => {
  const fontSize = 32;
  const max = _reduce(
    frequency,
    (max = 0, value) => (max < value ? value : max)
  );
  const tags = _map(frequency, (value, key) => (
    <div
      key={key}
      style={{
        fontSize: Math.max(Math.round((fontSize * value) / max), 6)
      }}
    >
      <Tooltip title={`${key}: ${value}`}>
        <div>{key}</div>
      </Tooltip>
    </div>
  ));

  return tags;
};

class Statistic extends React.Component {
  render() {
    const { data } = this.props;
    const frequencies = data.get('frequencies', new Map({}));
    return (
      <div>
        {frequencies
          .map((frequency, key) => (
            <React.Fragment>
              <Divider />
              <TagCloud
                key={key}
                style={{
                  width: 800,
                  height: 300,
                  padding: 5,
                  position: 'relative'
                }}
              >
                {createTags(frequency.toJS())}
              </TagCloud>
            </React.Fragment>
          ))
          .toList()}
      </div>
    );
  }
}

export default Statistic;
