/*
 *
 * CTest reducer
 *
 */

import { fromJS } from 'immutable';
import { DEFAULT_ACTION, SET_VALUE_FOR_ID } from './constants';

const initialState = fromJS({
  inputs: {}
});

function FillInTheBlankReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;
    case SET_VALUE_FOR_ID:
      return state.setIn(['inputs', action.id], action.value);
    default:
      return state;
  }
}

export default FillInTheBlankReducer;
