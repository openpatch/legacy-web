import * as actions from './actions';
import * as constants from './constants';

describe('CTest actions', () => {
  it('should create default action', () => {
    const expected = {
      type: constants.DEFAULT_ACTION
    };
    expect(actions.defaultAction()).toEqual(expected);
  });

  it('should create', () => {
    const expected = {
      type: constants.SET_VALUE_FOR_ID,
      value: 'hi',
      id: 'apple'
    };
    expect(actions.setValueForId('hi', 'apple')).toEqual(expected);
  });
});
