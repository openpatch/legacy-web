/**
 * For using json files for translations.
 * @see https://github.com/yahoo/react-intl
 */
import { defineMessages } from 'react-intl';

/**
 * Define all strings, which will be used in this format here. Then use
 * <FormattedMessages {...messages.example} /> to use the translatable message.
 *
 * Every message should have an id, which should be prefixed with
 * commoop.formats.CTest.{someUniqueIdentifier}
 */
export default defineMessages({
  formatName: {
    id: 'commoop.formats.FillInTheBlank.formatName',
    defaultMessage: 'Fill-in-the-blank'
  },
  text: {
    id: 'commoop.formats.FillInTheBlank.text',
    defaultMessage: 'Text with fill-ins'
  },
  hint: {
    id: 'commoop.formats.FillInTheBlank.hint',
    defaultMessage:
      'Insert <input /> to add a fill-in. You can optionally add an unique id to the fill-in with <input id="test" />. Fill-ins with the same id will be synchronized.'
  },
  fillOut: {
    id: 'commoop.formats.FillInTheBlank.fillOut',
    defaultMessage: 'Fill out the gaps.'
  }
});
