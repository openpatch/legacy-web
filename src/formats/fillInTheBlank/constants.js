/**
 *
 * FillInTheBlank constants
 *
 */

export const DEFAULT_ACTION = 'commoop.formats.FillInTheBlank.DEFAULT_ACTION';

export const SET_VALUE_FOR_ID =
  'commoop.formats.FillInTheBlank.SET_VALUE_FOR_ID';
