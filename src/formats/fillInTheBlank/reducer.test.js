import { fromJS } from 'immutable';

import reducer from './reducer';
import * as constants from './constants';

const initialState = fromJS({
  inputs: {}
});

const mockStore = fromJS({
  inputs: {
    test: 'hi',
    hallo: '23'
  }
});

describe('FillInTheBlank reducer', () => {
  it('returns the initial state', () => {
    expect(reducer(undefined, {})).toEqual(initialState);
  });

  it('should handle SET_VALUE_FOR_ID', () => {
    const expected = fromJS({
      inputs: {
        test: 'new hi',
        hallo: '23'
      }
    });

    expect(
      reducer(mockStore, {
        type: constants.SET_VALUE_FOR_ID,
        id: 'test',
        value: 'new hi'
      })
    ).toEqual(expected);
  });
});
