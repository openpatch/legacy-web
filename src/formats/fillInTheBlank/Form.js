import React from 'react';
import PropTypes from 'prop-types';
import { Map } from 'immutable';
import { withStyles } from '@material-ui/core/styles';
import { FormattedMessage } from 'react-intl';
import Typography from '@material-ui/core/Typography/Typography';

import messages from './messages';

import MarkdownEditor from '../../admin/components/MarkdownEditor';
import CodeBlockWithInputs from './components/CodeBlockWithInputs';

const styles = theme => ({});

export class Form extends React.Component {
  static propTypes = {
    privateData: PropTypes.instanceOf(Map).isRequired,
    publicData: PropTypes.instanceOf(Map).isRequired,
    onPublicChange: PropTypes.func.isRequired,
    onPrivateChange: PropTypes.func.isRequired
  };

  onMarkdownChange = value => {
    const { onPublicChange, publicData } = this.props;
    onPublicChange(publicData.set('text', value));
  };

  render() {
    const { publicData } = this.props;
    return (
      <div>
        <Typography type="headline">
          <FormattedMessage {...messages.text} />
        </Typography>
        <Typography type="body1">
          <FormattedMessage {...messages.hint} />
        </Typography>
        <MarkdownEditor
          value={publicData.get('text')}
          onChange={this.onMarkdownChange}
          renderers={{
            code: CodeBlockWithInputs
          }}
        />
      </div>
    );
  }
}

export default withStyles(styles)(Form);
