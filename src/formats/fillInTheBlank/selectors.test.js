import { fromJS } from 'immutable';

import pureSolutionSelector from './selectors';

const state = fromJS({
  inputs: {}
});

describe('CTest selectors', () => {
  it('should return pure solution', () => {
    expect(pureSolutionSelector(state)).toEqual(fromJS({ inputs: {} }));
  });
});
