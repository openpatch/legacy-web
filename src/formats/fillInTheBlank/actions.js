/**
 *
 * CTest actions
 *
 */

import { DEFAULT_ACTION, SET_VALUE_FOR_ID } from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION
  };
}

export function setValueForId(value, id) {
  return {
    type: SET_VALUE_FOR_ID,
    value,
    id
  };
}
