import { fromJS, Map } from 'immutable';

import { INIT, TOGGLE_BLOCK } from './constants';

const initialState = fromJS({
  active: {}
});

function TraceReducer(state = initialState, action) {
  switch (action.type) {
    case INIT:
      return state;
    case TOGGLE_BLOCK:
      const regex = new RegExp('^' + action.level + '.[0-9]+$', 'g');
      const sameLevels = state
        .get('active', new Map({}))
        .filter((value, key) => {
          if (key !== action.level + '.' + action.index)
            return key.match(regex);
          return false;
        })
        .keySeq();
      sameLevels.forEach(level => {
        state = state.deleteIn(['active', level]);
      });
      return state.updateIn(
        ['active', action.level + '.' + action.index],
        active => !active
      );
    default:
      return state;
  }
}

export default TraceReducer;
