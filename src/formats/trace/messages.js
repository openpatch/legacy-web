import { defineMessages } from 'react-intl';

export default defineMessages({
  formatName: {
    id: 'commoop.formats.Trace.formatName',
    defaultMessage: 'Trace'
  },
  answer: {
    id: 'commoop.formats.Trace.answer',
    defaultMessage: 'Answer'
  },
  answerPlaceholder: {
    id: 'commoop.formats.Trace.answerPlaceholder',
    defaultMessage: 'Insert you answer here.'
  },
  notes: {
    id: 'commoop.formats.Trace.notes',
    defaultMessage: 'Notes'
  },
  notesPlaceholder: {
    id: 'commoop.formats.Trace.notesPlaceholder',
    defaultMessage: 'Take notes here.'
  },
  blocks: {
    id: 'commoop.formats.Trace.blocks',
    defaultMessage: 'Blocks'
  },
  head: {
    id: 'commoop.formats.Trace.head',
    defaultMessage: 'Head'
  },
  content: {
    id: 'commoop.formats.Trace.content',
    defaultMessage: 'Content'
  },
  questions: {
    id: 'commoop.formats.Trace.questions',
    defaultMessage: 'Questions'
  },
  question: {
    id: 'commoop.formats.Trace.question',
    defaultMessage: 'Question'
  },
  answerFormat: {
    id: 'commoop.formats.Trace.answerFormat',
    defaultMessage: 'Answer Format'
  }
});
