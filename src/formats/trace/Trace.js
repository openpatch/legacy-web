import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import _isEqual from 'lodash/isEqual';

import { toggleBlock, init } from './actions';

import Markdown from '../../common/components/Markdown';
import CollapsibleBlock from './components/CollapsibleBlock';

const styles = theme => ({
  wrapper: {
    width: '100%',
    userSelect: 'none',
    cursor: 'default'
  },
  content: {
    paddingBottom: 16
  }
});

class Trace extends React.Component {
  static propTypes = {
    content: PropTypes.shape({
      questions: PropTypes.arrayOf(
        PropTypes.shape({
          question: PropTypes.string,
          type: PropTypes.string
        })
      ),
      blocks: PropTypes.arrayOf(
        PropTypes.shape({
          head: PropTypes.string,
          content: PropTypes.string,
          blocks: PropTypes.arrayOf(PropTypes.object)
        })
      )
    }),
    active: PropTypes.object
  };
  static defaultProps = {
    content: {
      questions: [],
      blocks: []
    },
    active: {}
  };

  constructor(props) {
    super(props);
    props.dispatch(init());
  }

  shouldComponentUpdate(nextProps) {
    return !_isEqual(nextProps, this.props);
  }

  onBlockToggle = (level, id) => {
    this.props.dispatch(toggleBlock(level, id));
  };

  renderBlocks = (blocks, level) => {
    const levelBlocks = blocks.map((block, index) => (
      <CollapsibleBlock
        onClick={() => this.onBlockToggle(level, index)}
        key={level + '.' + index}
        open={this.props.active[level + '.' + index]}
        head={<Markdown source={block.head} escapeHtml={false} />}
      >
        <div className={this.props.classes.content}>
          <Markdown source={block.content} escapeHtml={false} />
        </div>
        {block.blocks && block.blocks.length > 0
          ? this.renderBlocks(block.blocks, level + 1)
          : null}
      </CollapsibleBlock>
    ));

    return levelBlocks;
  };
  render() {
    const { classes, content } = this.props;
    return (
      <div className={classes.wrapper}>
        {'blocks' in content && this.renderBlocks(content.blocks, 0)}
      </div>
    );
  }
}

export default withStyles(styles)(Trace);
