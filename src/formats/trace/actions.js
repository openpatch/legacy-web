import { INIT, TOGGLE_BLOCK } from './constants';

export function init() {
  return {
    type: INIT
  };
}

export function toggleBlock(level, index) {
  return {
    type: TOGGLE_BLOCK,
    level,
    index
  };
}
