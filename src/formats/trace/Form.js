import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { Map, List, fromJS } from 'immutable';
import CollapsibleBlock from './components/CollapsibleBlock';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import AddIcon from '@material-ui/icons/Add';

import MarkdownEditor from '../../admin/components/MarkdownEditor';
import Typography from '@material-ui/core/Typography/Typography';

const styles = theme => ({
  head: {
    display: 'flex'
  },
  headEditor: {
    flexGrow: 1
  }
});

class Form extends React.Component {
  static propTypes = {
    privateData: PropTypes.instanceOf(Map).isRequired,
    publicData: PropTypes.instanceOf(Map).isRequired,
    onPublicChange: PropTypes.func.isRequired,
    onPrivateChange: PropTypes.func.isRequired
  };

  onHeadChange = (value, path) => {
    const { onPublicChange, publicData } = this.props;
    const blockPath = path.interpose('blocks');
    const blocks = publicData.get('blocks');
    onPublicChange(
      publicData.set(
        'blocks',
        blocks.updateIn(blockPath, block => block.set('head', value))
      )
    );
  };

  onContentChange = (value, path) => {
    const { onPublicChange, publicData } = this.props;

    const blockPath = path.interpose('blocks');
    const blocks = publicData.get('blocks');
    onPublicChange(
      publicData.set(
        'blocks',
        blocks.updateIn(blockPath, block => block.set('content', value))
      )
    );
  };

  deleteBlock = path => {
    const { onPublicChange, publicData } = this.props;

    const blockPath = path.interpose('blocks');
    const blocks = publicData.get('blocks');
    onPublicChange(publicData.set('blocks', blocks.deleteIn(blockPath)));
  };

  addBlock = path => {
    const { onPublicChange, publicData } = this.props;
    const blocks = publicData.get('blocks', new List([]));

    if (path.size > 0) {
      const blocksPath = path.interpose('blocks').push('blocks');
      const block = fromJS({
        head: '',
        content: '',
        blocks: []
      });
      onPublicChange(
        publicData.set(
          'blocks',
          blocks.updateIn(
            blocksPath,
            blocks => (blocks ? blocks.push(block) : new List([block]))
          )
        )
      );
    } else {
      onPublicChange(
        publicData.set(
          'blocks',
          blocks.push(
            fromJS({
              head: '',
              content: '',
              blocks: []
            })
          )
        )
      );
    }
  };
  stopEvent = e => {
    e.preventDefault();
    e.stopPropagation();
  };
  renderBlocks = (blocks, path) => {
    const { classes } = this.props;
    const levelBlocks = blocks.map((block, index) => {
      const newPath = path.push(index);
      return (
        <CollapsibleBlock
          key={newPath}
          head={
            <div onClick={this.stopEvent} className={classes.head}>
              <IconButton onClick={() => this.deleteBlock(newPath)}>
                <DeleteIcon />
              </IconButton>
              <div className={classes.headEditor}>
                <MarkdownEditor
                  height="80px"
                  onChange={value => this.onHeadChange(value, newPath)}
                  value={block.get('head')}
                />
              </div>
            </div>
          }
        >
          <MarkdownEditor
            onChange={value => this.onContentChange(value, newPath)}
            value={block.get('content')}
          />
          {block.get('blocks') && block.get('blocks').size > 0
            ? this.renderBlocks(block.get('blocks'), newPath)
            : null}
          <div>
            <IconButton onClick={() => this.addBlock(newPath)}>
              <AddIcon />
            </IconButton>
          </div>
        </CollapsibleBlock>
      );
    });

    return levelBlocks;
  };
  render() {
    const { publicData } = this.props;
    const blocks = publicData.get('blocks', new List([]));
    return (
      <div>
        <Typography>Blocks</Typography>
        {this.renderBlocks(blocks, new List([]))}
        <IconButton onClick={() => this.addBlock(new List([]))}>
          <AddIcon />
        </IconButton>
      </div>
    );
  }
}

export default withStyles(styles)(Form);
