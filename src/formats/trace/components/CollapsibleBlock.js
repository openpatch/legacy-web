import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Collapse from '@material-ui/core/Collapse';
import Paper from '@material-ui/core/Paper';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ExpandLessIcon from '@material-ui/icons/ExpandLess';

const styles = theme => ({
  head: {
    padding: theme.spacing.unit * 2,
    display: 'flex',
    alignItems: 'center',
    cursor: 'pointer'
  },
  headContent: {
    flex: '1'
  },
  wrapper: {
    border: '1px solid lightgrey',
    '& .hljs': {
      background: 'none'
    }
  },
  content: {
    padding: theme.spacing.unit * 2
  },
  state: {}
});

class CollapsibleBlock extends React.Component {
  state = {
    open: false
  };
  onClick = () => {
    this.setState({
      open: !this.state.open
    });
  };
  render() {
    const { classes, head, children } = this.props;
    let { onClick, open } = this.props;
    if (!onClick) {
      onClick = this.onClick;
    }
    if (!open) {
      open = this.state.open;
    }
    return (
      <Paper className={classes.wrapper}>
        <div
          className={classes.head}
          onClick={onClick}
          style={{
            background: open ? 'whitesmoke' : 'none',
            transition: 'all 0.5s'
          }}
        >
          <div className={classes.headContent}>{head}</div>
          <div className={classes.state}>
            {!open ? <ExpandMoreIcon /> : <ExpandLessIcon />}
          </div>
        </div>
        <Collapse in={open} unmountOnExit>
          <div className={classes.content}>{children}</div>
        </Collapse>
      </Paper>
    );
  }
}

export default withStyles(styles)(CollapsibleBlock);
