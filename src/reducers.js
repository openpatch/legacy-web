import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import { LOGOUT } from './common/constants/member';

import adminReducers from './admin/reducers';
import assessmentReducers from './assessment/reducers';
import commonReducers from './common/reducers';

const appReducer = combineReducers({
  router: routerReducer,
  admin: adminReducers,
  assessment: assessmentReducers,
  common: commonReducers
});

const rootReducer = (state, action) => {
  if (action.type === LOGOUT) {
    localStorage.clear();
    state = undefined;
  }

  return appReducer(state, action);
};

export default rootReducer;
