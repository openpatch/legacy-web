import { combineReducers } from 'redux-immutable';

import landing from './landing';
import assessment from './assessment';

const userReducers = combineReducers({
  landing,
  assessment
});

export default userReducers;
