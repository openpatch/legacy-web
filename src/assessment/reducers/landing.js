/*
 *
 * Landing reducer
 *
 */

import { fromJS } from 'immutable';
import {
  CHANGE_IDENTIFIER,
  SUBMIT_IDENTIFIER,
  SUBMIT_IDENTIFIER_COMMIT,
  SUBMIT_IDENTIFIER_ROLLBACK,
  CLEAR,
  CHANGE_PASSWORD
} from '../constants/landing';

const initialState = fromJS({
  identifier: '',
  password: ''
});

function LandingReducer(state = initialState, action) {
  switch (action.type) {
    case CHANGE_IDENTIFIER:
      return state
        .set('identifier', action.payload.identifier)
        .delete('submitting_identifier_failed');
    case CHANGE_PASSWORD:
      return state
        .set('password', action.payload.password)
        .delete('submitting_identifier_failed');
    case SUBMIT_IDENTIFIER:
      return state.set('submitting_identifier', true);
    case SUBMIT_IDENTIFIER_COMMIT:
      return state
        .delete('submitting_name')
        .set('session_hash', action.payload.session_hash)
        .set('survey', action.payload.survey)
        .set('submitting_identifier_failed', action.payload.status)
        .set('submitting_identifier_success', true);
    case SUBMIT_IDENTIFIER_ROLLBACK:
      return state
        .delete('submitting_identifier')
        .set('submitting_identifier_failed', true);
    case CLEAR:
      return initialState;
    default:
      return state;
  }
}

export default LandingReducer;
