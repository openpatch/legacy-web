/*
 *
 * Assessment reducer
 *
 */

import { fromJS, List } from 'immutable';
import {
  FETCH_ITEMS,
  FETCH_ITEMS_COMMIT,
  FETCH_ITEMS_ROLLBACK,
  SUBMIT_SOLUTIONS,
  SUBMIT_SOLUTIONS_COMMIT,
  SUBMIT_SOLUTIONS_ROLLBACK,
  CHANGE_ACTIVE_ITEM,
  DISPATCH_ITEM_ACTION,
  CHANGE_NOTE,
  END_SURVEY,
  END_SURVEY_DIALOG_CLOSE,
  END_SURVEY_DIALOG_OPEN,
  END_SURVEY_DIALOG_TEXT_CHANGE,
  START_SURVEY,
  START_SURVEY_ROLLBACK,
  START_SURVEY_COMMIT,
  SHOW_DIALOG_EMPTY_RESPONSE,
  CLOSE_DIALOG_EMPTY_RESPONSE
} from '../constants/assessment';
import { SUBMIT_IDENTIFIER_COMMIT } from '../constants/landing';

const initialState = fromJS({
  passed: false,
  items: [],
  notes: {},
  active: 0,
  start_date: null,
  dialog_empty_response: false,
  actions: {},
  states: {}
});

function AssessmentReducer(state = initialState, action) {
  switch (action.type) {
    case SUBMIT_IDENTIFIER_COMMIT: {
      return initialState
        .set('survey', fromJS(action.payload.survey))
        .set('session_hash', action.payload.session_hash)
        .set('phase', 'greeting');
    }
    case FETCH_ITEMS:
      return initialState
        .set('fetching', true)
        .set('survey', state.get('survey'))
        .set('session_hash', state.get('session_hash'))
        .set('phase', state.get('phase'));
    case FETCH_ITEMS_COMMIT:
      return state
        .delete('fetching')
        .set('start_date', Date.now())
        .set('items', fromJS(action.payload.items))
        .set('current_progress', action.payload.current_progress)
        .set('max_progress', action.payload.max_progress)
        .set(
          'phase',
          action.payload.items.length === 0 ? 'farewell' : state.get('phase')
        );
    case FETCH_ITEMS_ROLLBACK:
      return state.delete('fetching').set('fetching_failed', true);
    case SUBMIT_SOLUTIONS:
      return state
        .set('submitting', true)
        .delete('submitting_failed')
        .delete('checks');
    case SUBMIT_SOLUTIONS_COMMIT:
      return state
        .delete('submitting')
        .set('passed', action.payload.passed)
        .set('checks', fromJS(action.payload.checks));
    case SUBMIT_SOLUTIONS_ROLLBACK:
      return state.delete('submitting').set('submitting_failed', true);
    case CHANGE_ACTIVE_ITEM:
      return state.set('active', action.payload.id);
    case CHANGE_NOTE:
      return state.setIn(['notes', action.payload.id], action.payload.note);
    case DISPATCH_ITEM_ACTION:
      const nextState = action.payload.reducer(
        state.getIn(['states', action.payload.id]),
        action.payload.action
      );
      return state
        .updateIn(
          ['actions', action.payload.id],
          actions =>
            actions
              ? actions.push({
                  action: action.payload.action,
                  time: Date.now() - state.get('start_date')
                })
              : new List([
                  {
                    action: action.payload.action,
                    time: Date.now() - state.get('start_date')
                  }
                ])
        )
        .setIn(['states', action.payload.id], nextState);
    case START_SURVEY:
      return state.set('starting', true).delete('starting_failed');
    case START_SURVEY_COMMIT:
      return state
        .set('phase', 'items')
        .set('session_hash', action.payload.session_hash)
        .delete('starting');
    case START_SURVEY_ROLLBACK:
      return state.delete('starting').set('starting_failed', true);
    case END_SURVEY:
      return state.set('phase', 'farewell');
    case END_SURVEY_DIALOG_CLOSE:
      return state.set('end_survey_dialog', false);
    case END_SURVEY_DIALOG_OPEN:
      return state.set('end_survey_dialog', true);
    case END_SURVEY_DIALOG_TEXT_CHANGE:
      return state.set('end_survey_dialog_text', action.payload.text);
    case SHOW_DIALOG_EMPTY_RESPONSE:
      return state.set('dialog_empty_response', true);
    case CLOSE_DIALOG_EMPTY_RESPONSE:
      return state.set('dialog_empty_response', false);
    default:
      return state;
  }
}

export default AssessmentReducer;
