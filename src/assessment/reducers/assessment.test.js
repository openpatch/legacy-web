import { fromJS, List } from 'immutable';

import * as constants from '../constants/assessment';
import reducer from './assessment';

// mock date
Date.now = jest.fn(() => 1487076708000);

const initialState = fromJS({
  passed: false,
  items: [],
  notes: {},
  active: 0,
  start_date: null,
  dialog_empty_response: false,
  actions: {},
  states: {}
});

const mockState = fromJS({
  passed: true,
  items: [
    {
      name: 'item_1'
    },
    {
      name: 'item_2'
    },
    {
      name: 'item_3'
    }
  ],
  notes: {},
  active: 1,
  start_date: Date.now(),
  dialog_empty_response: false,
  actions: {
    0: []
  },
  states: {}
});

describe('assessment reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual(initialState);
  });

  it('should handle FETCH_ITEMS', () => {
    expect(
      reducer(mockState, {
        type: constants.FETCH_ITEMS
      })
    ).toEqual(
      initialState
        .set('fetching', true)
        .set('session_hash', undefined)
        .set('survey', undefined)
        .set('phase', undefined)
    );
  });

  it('should handle FETCH_ITEMS_COMMIT', () => {
    const items = [
      {
        name: 'new_item_1'
      },
      {
        name: 'new_item_2'
      }
    ];
    expect(
      reducer(mockState, {
        type: constants.FETCH_ITEMS_COMMIT,
        payload: {
          items
        }
      })
    ).toEqual(
      mockState
        .delete('fetching')
        .set('max_progress', undefined)
        .set('items', fromJS(items))
        .set('current_progress', undefined)
        .set('phase', undefined)
    );

    expect(
      reducer(mockState, {
        type: constants.FETCH_ITEMS_COMMIT,
        payload: {
          items: []
        }
      })
    ).toEqual(
      mockState
        .delete('fetching')
        .set('max_progress', undefined)
        .set('items', fromJS([]))
        .set('current_progress', undefined)
        .set('phase', 'farewell')
    );
  });

  it('should handle FETCH_ITEMS_ROLLBACK', () => {
    expect(
      reducer(mockState, {
        type: constants.FETCH_ITEMS_ROLLBACK
      })
    ).toEqual(mockState.delete('fetching').set('fetching_failed', true));
  });

  it('should handle SUBMIT_SOLUTIONS', () => {
    expect(
      reducer(mockState, {
        type: constants.SUBMIT_SOLUTIONS
      })
    ).toEqual(mockState.set('submitting', true).delete('submitting_failed'));
  });

  it('should handle SUBMIT_SOLUTIONS_COMMIT', () => {
    expect(
      reducer(mockState, {
        type: constants.SUBMIT_SOLUTIONS_COMMIT,
        payload: {
          passed: true,
          checks: [true, false]
        }
      })
    ).toEqual(
      mockState
        .delete('submitting')
        .set('passed', true)
        .set('checks', fromJS([true, false]))
    );
  });

  it('should handle SUBMIT_SOLUTIONS_ROLLBACK', () => {
    expect(
      reducer(mockState, {
        type: constants.SUBMIT_SOLUTIONS_ROLLBACK
      })
    ).toEqual(mockState.delete('submitting').set('submitting_failed', true));
  });

  it('should handle CHANGE_ACTIVE_ITEM', () => {
    expect(
      reducer(mockState, {
        type: constants.CHANGE_ACTIVE_ITEM,
        payload: {
          id: 1
        }
      })
    ).toEqual(mockState.set('active', 1));
  });

  it('should handle DISPATCH_ITEM_ACTION', () => {
    const action = {
      type: 'change_to_true'
    };
    expect(
      reducer(mockState, {
        type: constants.DISPATCH_ITEM_ACTION,
        payload: {
          id: 0,
          action,
          reducer: (state = fromJS({}), action) => ({
            checked: true
          })
        }
      }).toJS()
    ).toEqual(
      mockState
        .updateIn(
          ['actions', 0],
          actions =>
            new List([
              {
                action,
                time: 0
              }
            ])
        )
        .setIn(['states', '0'], {
          checked: true
        })
        .toJS()
    );
  });
});
