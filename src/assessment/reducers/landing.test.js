import { fromJS } from 'immutable';

import reducer from './landing';
import * as constants from '../constants/landing';

const initialState = fromJS({
  identifier: '',
  password: ''
});

const mockStore = fromJS({
  identifier: 'hallo',
  password: 'not-for-test'
});

describe('landing reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual(initialState);
  });

  it('should handle CHANGE_IDENTIFIER', () => {
    expect(
      reducer(mockStore, {
        type: constants.CHANGE_IDENTIFIER,
        payload: {
          identifier: 'test'
        }
      })
    ).toEqual(
      mockStore.set('identifier', 'test').delete('submitting_access_failed')
    );
  });

  it('should handle CHANGE_PASSWORD', () => {
    expect(
      reducer(mockStore, {
        type: constants.CHANGE_PASSWORD,
        payload: {
          password: 'tom'
        }
      })
    ).toEqual(
      mockStore.set('password', 'tom').delete('submitting_password_failed')
    );
  });

  it('should handle SUBMIT_IDENTIFIER', () => {
    expect(
      reducer(mockStore, {
        type: constants.SUBMIT_IDENTIFIER
      })
    ).toEqual(mockStore.set('submitting_identifier', true));
  });

  it('should handle SUBMIT_IDENTIFIER_COMMIT', () => {
    expect(
      reducer(mockStore, {
        type: constants.SUBMIT_IDENTIFIER_COMMIT,
        payload: {
          session_hash: 'A'
        }
      })
    ).toEqual(
      mockStore
        .delete('submitting_identifier')
        .set('session_hash', 'A')
        .set('survey', undefined)
        .set('submitting_identifier_failed', undefined)
        .set('submitting_identifier_success', true)
    );
  });

  it('should handle SUBMIT_IDENTIFIER_ROLLBACK', () => {
    expect(
      reducer(mockStore, {
        type: constants.SUBMIT_IDENTIFIER_ROLLBACK
      })
    ).toEqual(
      mockStore
        .delete('submitting_identifier')
        .set('submitting_identifier_failed', true)
    );
  });

  it('should handle CLEAR', () => {
    expect(
      reducer(mockStore, {
        type: constants.CLEAR
      })
    ).toEqual(initialState);
  });
});
