/**
 *
 * Assessment messages
 *
 * This contains all the text for the assessment domain.
 *
 */

import { defineMessages } from 'react-intl';

export default defineMessages({
  defaultGreeting: {
    id: 'commoop.user.assessment.defaultGreeting',
    defaultMessage: 'Welcome'
  },
  defaultFarewell: {
    id: 'commoop.user.assessment.defaultFarewell',
    defaultMessage: 'Farewell'
  },
  start: {
    id: 'commoop.user.assessment.start',
    defaultMessage: 'Start'
  },
  endSurvey: {
    id: 'commoop.user.assessment.endSurvey',
    defaultMessage: 'End Survey'
  },
  endSurveyDialogTitle: {
    id: 'commoop.user.assessment.endSurveyDialogTitle',
    defaultMessage: 'End Survey'
  },
  endSurveyDialogText: {
    id: 'commoop.user.assessment.endSurveyDialogText',
    defaultMessage: 'Why do you want to end this survey?'
  },
  endSurveyDialogReason: {
    id: 'commoop.user.assessment.endSurveyDialogReason',
    defaultMessage: 'Reason'
  },
  endSurveyDialogCancel: {
    id: 'commoop.user.assessment.endSurveyDialogCancel',
    defaultMessage: 'Cancel'
  },
  endSurveyDialogEnd: {
    id: 'commoop.user.assessment.endSurveyDialogEnd',
    defaultMessage: 'End Survey'
  },
  jumpableNext: {
    id: 'commoop.user.assessment.jumpableNext',
    defaultMessage: 'Next'
  },
  jumpablePrevious: {
    id: 'commoop.user.assessment.jumpablePrevious',
    defaultMessage: 'Previous'
  },
  nextItem: {
    id: 'commoop.user.assessment.nextItem',
    defaultMessage: 'Next'
  },
  incorrectItem: {
    id: 'commoop.user.assessment.incorrectItem',
    defaultMessage: 'This item is incorrect!'
  },
  emptyResponsesTitle: {
    id: 'commoop.user.assessment.emptyResponsesTitle',
    defaultMessage: 'Empty Reponses!'
  },
  emptyResponsesContent: {
    id: 'commoop.user.assessment.emptyResponsesContent',
    defaultMessage: 'You have empty responses. Do you want to continue?'
  }
});
