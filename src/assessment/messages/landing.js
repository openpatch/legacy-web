/**
 *
 * Landing messages
 *
 * This contains all the text for the Landing component.
 *
 */

import { defineMessages } from 'react-intl';

export default defineMessages({
  access: {
    id: 'commoop.user.landing.access',
    defaultMessage: 'with an access token'
  },
  name: {
    id: 'commoop.user.landing.name',
    defaultMessage: 'Name'
  },
  password: {
    id: 'commoop.user.landing.password',
    defaultMessage: 'Password (optional)'
  },
  start: {
    id: 'commoop.user.landing.start',
    defaultMessage: 'Start'
  },
  privateSurvey: {
    id: 'commoop.user.landing.privateSurvey',
    defaultMessage: 'Start a private survey'
  },
  publicSurvey: {
    id: 'commoop.user.landing.publicSurvey',
    defaultMessage: 'Start a public survey'
  },
  notificationSurveyClosed: {
    id: 'commoop.user.landing.notificationSurveyClosed',
    defaultMessage: 'The survey is closed!'
  },
  notificationNoSurveyWithName: {
    id: 'commoop.user.landing.notificationNoSurveyWithName',
    defaultMessage: 'There is no survey with this name!'
  },
  notificationIncorrectPassword: {
    id: 'commoop.user.landing.notificationIncorrectPassword',
    defaultMessage: 'The password is incorrect!'
  }
});
