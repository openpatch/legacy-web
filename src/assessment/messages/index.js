import * as assessment from './assessment';
import * as landing from './landing';

export default {
  assessment,
  landing
};
