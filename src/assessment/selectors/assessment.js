import { fromJS } from 'immutable';
import { createSelector } from 'reselect';

/**
 * Direct selector to the Assessment state domain
 */
const AssessmentSelector = state => state.assessment.get('assessment');

/**
 * Other specific selectors
 */

export const phaseSelector = () =>
  createSelector(AssessmentSelector, substate => substate.get('phase'));

export const itemsSelector = () =>
  createSelector(AssessmentSelector, substate => substate.get('items'));

export const activeItemSelector = () =>
  createSelector(AssessmentSelector, substate => substate.get('active'));

export const statesSelector = () =>
  createSelector(AssessmentSelector, substate => substate.get('states'));

export const actionsSelector = () =>
  createSelector(AssessmentSelector, substate => substate.get('actions'));

export const finishedSelector = () =>
  createSelector(AssessmentSelector, substate => substate.get('finished'));

export const passedSelector = () =>
  createSelector(AssessmentSelector, substate => substate.get('passed'));

export const notesSelector = () =>
  createSelector(AssessmentSelector, substate => substate.get('notes'));

export const checksSelector = () =>
  createSelector(AssessmentSelector, substate => substate.get('checks'));

export const endSurveyDialogSelector = () =>
  createSelector(AssessmentSelector, substate =>
    substate.get('end_survey_dialog')
  );

export const endSurveyDialogTextSelector = () =>
  createSelector(AssessmentSelector, substate =>
    substate.get('end_survey_dialog_text')
  );

export const progressSelector = () =>
  createSelector(
    AssessmentSelector,
    substate =>
      substate.get('current_progress') / substate.get('max_progress') * 100
  );

export const surveySelector = () =>
  createSelector(AssessmentSelector, substate => substate.get('survey'));

export const farewellSelector = () =>
  createSelector(AssessmentSelector, substate =>
    substate.getIn(['survey', 'farewell'])
  );

export const greetingSelector = () =>
  createSelector(AssessmentSelector, substate =>
    substate.getIn(['survey', 'greeting'])
  );

export const sessionHashSelector = () =>
  createSelector(AssessmentSelector, substate => substate.get('session_hash'));

export const statusSelector = () =>
  createSelector(AssessmentSelector, substate =>
    fromJS({
      submitting: substate.get('submitting'),
      submitting_failed: substate.get('submitting_failed'),
      fetching: substate.get('fetching'),
      fetching_failed: substate.get('fetching_failed')
    })
  );

export const dialogEmptyResponseSelector = () =>
  createSelector(AssessmentSelector, substate =>
    substate.get('dialog_empty_response', false)
  );

/**
 * Default selector used by Assessment
 */

export default () => createSelector(AssessmentSelector, substate => substate);
