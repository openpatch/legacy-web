import { fromJS } from 'immutable';
import { createSelector } from 'reselect';

/**
 * Direct selector to the Landing state domain
 */
export const LandingSelector = state => state.assessment.get('landing');

/**
 * Other specific selectors
 */

export const makeIdentifierSelector = () =>
  createSelector(LandingSelector, substate => substate.get('identifier'));

export const makePasswordSelector = () =>
  createSelector(LandingSelector, substate => substate.get('password'));

export const makeStatusSelector = () =>
  createSelector(LandingSelector, substate =>
    fromJS({
      submitting: substate.get('submitting_identifier'),
      submittingIdentifierSuccess: substate.get(
        'submitting_identifier_success'
      ),
      submittingIdentifierFailed: substate.get('submitting_identifier_failed')
    })
  );

export const makeSessionHashSelector = () =>
  createSelector(LandingSelector, substate => substate.get('session_hash'));

export const makeSurveySelector = () =>
  createSelector(LandingSelector, substate => substate.get('survey'));

/**
 * Default selector used by Landing
 */

export default () =>
  createSelector(LandingSelector, substate => substate.toJS());
