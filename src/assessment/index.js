import React from 'react';
import { Switch, Route } from 'react-router-dom';

import Landing from './containers/Landing';
import Assessment from './containers/Assessment';

// const Landing = Loadable({
//   loader: () => import('./containers/Landing'),
//   loading: Loading
// });
//
// const Assessment = Loadable({
//   loader: () => import('./containers/Assessment'),
//   loading: Loading
// });

class User extends React.Component {
  render() {
    const { match } = this.props;
    return (
      <Switch>
        <Route exact path={`${match.url}`} component={Landing} />
        <Route exact path={`${match.url}/test`} component={Assessment} />
      </Switch>
    );
  }
}

export default User;
