/**
 *
 * Assessment constants
 *
 */

export const INIT = 'commoop.user.assessment.INIT';

export const FETCH_ITEMS = 'commoop.user.assessment.FETCH_ITEMS';

export const FETCH_ITEMS_COMMIT = 'commoop.user.assessment.FETCH_ITEMS_COMMIT';

export const FETCH_ITEMS_ROLLBACK =
  'commoop.user.assessment.FETCH_ITEMS_ROLLBACK';

export const SUBMIT_SOLUTIONS = 'commoop.user.assessment.SUBMIT_SOLUTIONS';

export const SUBMIT_SOLUTIONS_COMMIT =
  'commoop.user.assessment.SUBMIT_SOLUTIONS_COMMIT';

export const SUBMIT_SOLUTIONS_ROLLBACK =
  'commoop.user.assessment.SUBMIT_SOLUTIONS_ROLLBACK';

export const CHANGE_ACTIVE_ITEM = 'commoop.user.assessment.CHANGE_ACTIVE_ITEM';

export const DISPATCH_ITEM_ACTION =
  'commoop.user.assessment.DISPATCH_ITEM_ACTION';

export const CHANGE_NOTE = 'commoop.user.assessment.CHANGE_NOTE';

export const END_SURVEY = 'commoop.user.assessment.END_SURVEY';

export const END_SURVEY_COMMIT = 'commoop.user.assessment.END_SURVEY_COMMIT';

export const END_SURVEY_ROLLBACK =
  'commoop.user.assessment.END_SURVEY_ROLLBACK';

export const END_SURVEY_DIALOG_OPEN =
  'commoop.user.assessment.END_SURVEY_DIALOG_OPEN';

export const END_SURVEY_DIALOG_CLOSE =
  'commoop.user.assessment.END_SURVEY_DIALOG_CLOSE';

export const END_SURVEY_DIALOG_TEXT_CHANGE =
  'commoop.user.assessment.END_SURVEY_DIALOG_TEXT_CHANGE';

export const CHANGE_PHASE = 'commoop.user.assessment.CHANGE_PHASE';

export const START_SURVEY = 'commoop.user.assessment.START_SURVEY';

export const START_SURVEY_COMMIT =
  'commoop.user.assessment.START_SURVEY_COMMIT';

export const START_SURVEY_ROLLBACK =
  'commoop.user.assessment.START_SURVEY_ROLLBACK';

export const SHOW_DIALOG_EMPTY_RESPONSE =
  'commoop.user.assessment.SHOW_DIALOG_EMPTY_RESPONSE';

export const CLOSE_DIALOG_EMPTY_RESPONSE =
  'commoop.user.assessment.CLOSE_DIALOG_EMPTY_RESPONSE';
