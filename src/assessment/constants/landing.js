/**
 *
 * Landing constants
 *
 */

export const CLEAR = 'commoop.user.landing.CLEAR';

export const CHANGE_IDENTIFIER = 'commoop.user.landing.CHANGE_NAME';

export const SUBMIT_IDENTIFIER = 'commoop.user.landing.SUBMIT_NAME';

export const SUBMIT_IDENTIFIER_COMMIT =
  'commoop.user.landing.SUBMIT_NAME_COMMIT';

export const SUBMIT_IDENTIFIER_ROLLBACK =
  'commoop.user.landing.SUBMIT_NAME_ROLLBACK';

export const CHANGE_PASSWORD = 'commoop.user.landing.CHANGE_PASSWORD';
