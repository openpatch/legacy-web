/**
 *
 * MenuPoint
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { ListItem, ListItemText } from '@material-ui/core/List';
import Collapse from '@material-ui/core/transitions/Collapse';

const styles = theme => ({
  menuPoint: {
    border: '1px solid blue'
  },
  menuPointContent: {
    padding: theme.spacing.unit
  }
});

export class CollapsibleMenuPoint extends React.Component {
  static propTypes = {
    classes: PropTypes.object.isRequired,
    onClick: PropTypes.func,
    label: PropTypes.oneOfType([PropTypes.string, PropTypes.element])
      .isRequired,
    open: PropTypes.bool
  };

  static defaultProps = { onClick: () => {} };

  state = {};

  render() {
    const { classes, onClick, label, children, open } = this.props;
    return (
      <div className={classes.menuPoint}>
        <ListItem button onClick={onClick}>
          <ListItemText primary={label} />
        </ListItem>
        <Collapse in={open} unmountOnExit>
          <div className={classes.menuPointContent}>{children}</div>
        </Collapse>
      </div>
    );
  }
}

export default withStyles(styles)(CollapsibleMenuPoint);
