/**
 *
 * Test actions
 *
 */

import {
  FETCH_ITEMS,
  FETCH_ITEMS_COMMIT,
  FETCH_ITEMS_ROLLBACK,
  SUBMIT_SOLUTIONS,
  SUBMIT_SOLUTIONS_COMMIT,
  SUBMIT_SOLUTIONS_ROLLBACK,
  CHANGE_ACTIVE_ITEM,
  DISPATCH_ITEM_ACTION,
  INIT,
  CHANGE_NOTE,
  END_SURVEY,
  END_SURVEY_COMMIT,
  END_SURVEY_ROLLBACK,
  END_SURVEY_DIALOG_CLOSE,
  END_SURVEY_DIALOG_OPEN,
  END_SURVEY_DIALOG_TEXT_CHANGE,
  CHANGE_PHASE,
  START_SURVEY,
  START_SURVEY_COMMIT,
  START_SURVEY_ROLLBACK,
  SHOW_DIALOG_EMPTY_RESPONSE,
  CLOSE_DIALOG_EMPTY_RESPONSE
} from '../constants/assessment';

export function init() {
  return {
    type: INIT
  };
}

export function changePhase(phase) {
  return {
    type: CHANGE_PHASE,
    payload: {
      phase
    }
  };
}

export function startSurvey(session) {
  return {
    type: START_SURVEY,
    meta: {
      offline: {
        effect: {
          url: 'v1/sessions/start',
          method: 'GET',
          params: {
            session_hash: session
          }
        },
        commit: {
          type: START_SURVEY_COMMIT
        },
        rollback: {
          type: START_SURVEY_ROLLBACK
        }
      }
    }
  };
}

export function endSurvey(session, reason) {
  return {
    type: END_SURVEY,
    meta: {
      offline: {
        effect: {
          url: 'v1/sessions/abort',
          method: 'POST',
          params: {
            session_hash: session
          },
          data: {
            reason
          }
        },
        commit: {
          type: END_SURVEY_COMMIT
        },
        rollback: {
          type: END_SURVEY_ROLLBACK
        }
      }
    }
  };
}

export function endSurveyDialogClose() {
  return {
    type: END_SURVEY_DIALOG_CLOSE
  };
}

export function endSurveyDialogOpen() {
  return {
    type: END_SURVEY_DIALOG_OPEN
  };
}

export function endSurveyDialogTextChange(text) {
  return {
    type: END_SURVEY_DIALOG_TEXT_CHANGE,
    payload: {
      text
    }
  };
}

export function changeActiveItem(id) {
  return {
    type: CHANGE_ACTIVE_ITEM,
    payload: {
      id
    }
  };
}

export function dispatchItemAction(action, reducer, id) {
  return {
    type: DISPATCH_ITEM_ACTION,
    payload: {
      reducer,
      action,
      id
    }
  };
}

export function changeNote(note, id) {
  return {
    type: CHANGE_NOTE,
    payload: {
      note,
      id
    }
  };
}

export function fetchItems(session) {
  return {
    type: FETCH_ITEMS,
    meta: {
      offline: {
        effect: {
          url: `v1/sessions`,
          method: 'GET',
          params: {
            session_hash: session
          }
        },
        commit: {
          type: FETCH_ITEMS_COMMIT
        },
        rollback: {
          type: FETCH_ITEMS_ROLLBACK
        }
      }
    }
  };
}

export function submitSolutions(session, solutions, actions, notes) {
  return {
    type: SUBMIT_SOLUTIONS,
    meta: {
      offline: {
        effect: {
          url: `v1/sessions`,
          method: 'POST',
          data: {
            session_hash: session,
            item_solutions: solutions,
            item_actions: actions,
            item_notes: notes
          }
        },
        commit: {
          type: SUBMIT_SOLUTIONS_COMMIT
        },
        rollback: {
          type: SUBMIT_SOLUTIONS_ROLLBACK
        }
      }
    }
  };
}

export function showDialogEmptyResponse() {
  return {
    type: SHOW_DIALOG_EMPTY_RESPONSE
  };
}

export function closeDialogEmptyResponse() {
  return {
    type: CLOSE_DIALOG_EMPTY_RESPONSE
  };
}
