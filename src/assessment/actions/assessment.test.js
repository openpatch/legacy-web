import * as actions from './assessment';

describe('assessment actions', () => {
  it('should create init action', () => {
    expect(actions.init()).toMatchSnapshot();
  });

  it('should create change active item action', () => {
    expect(actions.changeActiveItem(1)).toMatchSnapshot();
  });

  it('should create dispatch item action action', () => {
    expect(
      actions.dispatchItemAction(
        {
          type: 'checked',
          id: 1
        },
        (state = {}, action) => (state.checked = action.id)
      )
    ).toMatchSnapshot();
  });

  it('should create fetch items action', () => {
    expect(actions.fetchItems('AAAA')).toMatchSnapshot();
  });

  it('should create submit solutions action', () => {
    const solutions = [
      {
        answer: 'hallo'
      },
      {
        choices: [
          {
            id: 1,
            checked: true
          }
        ]
      }
    ];
    const userActions = [
      [
        {
          type: 'change input',
          value: 'hallo'
        }
      ],
      [
        {
          type: 'checked'
        }
      ]
    ];
    expect(
      actions.submitSolutions('AAAA', solutions, userActions)
    ).toMatchSnapshot();
  });
});
