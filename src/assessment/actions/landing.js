/**
 *
 * Landing actions
 *
 */

import {
  CHANGE_IDENTIFIER,
  SUBMIT_IDENTIFIER,
  SUBMIT_IDENTIFIER_COMMIT,
  SUBMIT_IDENTIFIER_ROLLBACK,
  CLEAR,
  CHANGE_PASSWORD
} from '../constants/landing';

export function clear() {
  return {
    type: CLEAR
  };
}

export function changeIdentifier(identifier) {
  return {
    type: CHANGE_IDENTIFIER,
    payload: {
      identifier
    }
  };
}

export function changePassword(password) {
  return {
    type: CHANGE_PASSWORD,
    payload: {
      password
    }
  };
}

export function submitIdentifier(identifier, password) {
  return {
    type: SUBMIT_IDENTIFIER,
    meta: {
      offline: {
        effect: {
          url: `v1/sessions/prepare`,
          method: 'POST',
          data: {
            survey_identifier: identifier,
            password
          }
        },
        commit: {
          type: SUBMIT_IDENTIFIER_COMMIT
        },
        rollback: {
          type: SUBMIT_IDENTIFIER_ROLLBACK
        }
      }
    }
  };
}
