import * as actions from './landing';

describe('landing actions', () => {
  it('should create clear action', () => {
    expect(actions.clear()).toMatchSnapshot();
  });

  it('should create change identifier action', () => {
    expect(actions.changeIdentifier('access')).toMatchSnapshot();
  });

  it('should create submit identifier action', () => {
    expect(actions.submitIdentifier('access')).toMatchSnapshot();
  });

  it('should create change password action', () => {
    expect(actions.changePassword('password')).toMatchSnapshot();
  });
});
