import Assessment from './Assessment';
import Greeting from './Greeting';
import Farewell from './Farewell';

export { Greeting, Farewell };

export default Assessment;
