import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { withStyles } from '@material-ui/core/styles';
import { injectIntl } from 'react-intl';

import assessmentMessages from '../../messages/assessment';

import Markdown from '../../../common/components/Markdown';
import { farewellSelector } from '../../selectors/assessment';

const styles = theme => ({
  farewell: {
    gridArea: 'main',
    padding: theme.spacing.unit * 2
  }
});

class Farewell extends React.Component {
  static propTypes = {
    classes: PropTypes.object.isRequired,
    intl: PropTypes.object.isRequired,
    farewell: PropTypes.string.isRequired
  };

  render() {
    const { classes, farewell, intl } = this.props;
    return (
      <div className={classes.farewell}>
        <Markdown
          source={
            farewell || intl.formatMessage(assessmentMessages.defaultFarewell)
          }
        />
      </div>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  farewell: farewellSelector()
});

export default connect(mapStateToProps)(
  withStyles(styles)(injectIntl(Farewell))
);
