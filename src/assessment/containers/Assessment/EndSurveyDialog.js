import React from 'react';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { injectIntl } from 'react-intl';

import assessmentMessages from '../../messages/assessment';

class EndSurveyDialog extends React.Component {
  static propTypes = {
    onClose: PropTypes.func.isRequired,
    onEnd: PropTypes.func.isRequired,
    intl: PropTypes.object.isRequired,
    onTextChange: PropTypes.func.isRequired,
    open: PropTypes.bool,
    text: PropTypes.string
  };

  static defaultProps = {
    open: false,
    text: ''
  };

  onTextChange = e => {
    const { onTextChange } = this.props;
    onTextChange(e.target.value);
  };

  render() {
    const { onClose, onEnd, open, text, intl } = this.props;
    return (
      <Dialog onClose={onClose} open={open} aria-labelledby="end-survey-title">
        <DialogTitle id="end-survey-title">
          {intl.formatMessage(assessmentMessages.endSurveyDialogTitle)}
        </DialogTitle>
        <DialogContent>
          <DialogContentText>
            {intl.formatMessage(assessmentMessages.endSurveyDialogText)}
          </DialogContentText>
          <TextField
            margin="dense"
            label={intl.formatMessage(assessmentMessages.endSurveyDialogReason)}
            value={text}
            fullWidth
            multiline
            onChange={this.onTextChange}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={onClose} color="primary">
            {intl.formatMessage(assessmentMessages.endSurveyDialogCancel)}
          </Button>
          <Button onClick={onEnd} color="primary">
            {intl.formatMessage(assessmentMessages.endSurveyDialogEnd)}
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

export default injectIntl(EndSurveyDialog);
