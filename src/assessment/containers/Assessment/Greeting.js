import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { withStyles } from '@material-ui/core/styles';
import { injectIntl } from 'react-intl';

import assessmentMessages from '../../messages/assessment';

import Markdown from '../../../common/components/Markdown';
import {
  greetingSelector,
  sessionHashSelector
} from '../../selectors/assessment';
import Button from '@material-ui/core/Button/Button';
import { startSurvey } from '../../actions/assessment';

const styles = theme => ({
  greeting: {
    gridArea: 'main',
    padding: theme.spacing.unit * 2
  },
  start: {
    padding: `${theme.spacing.unit * 2}px`,
    gridArea: 'resume'
  }
});

class Greeting extends React.Component {
  static propTypes = {
    classes: PropTypes.object.isRequired,
    greeting: PropTypes.string
  };

  render() {
    const { classes, greeting, sessionHash, onStart, intl } = this.props;
    return (
      <React.Fragment>
        <div className={classes.greeting}>
          <Markdown
            source={
              greeting || intl.formatMessage(assessmentMessages.defaultGreeting)
            }
          />
        </div>
        <div className={classes.start}>
          <Button
            color="primary"
            variant="contained"
            onClick={() => onStart(sessionHash)}
          >
            {intl.formatMessage(assessmentMessages.start)}
          </Button>
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  greeting: greetingSelector(),
  sessionHash: sessionHashSelector()
});

const mapDispatchToProps = dispatch => ({
  dispatch,
  onStart: sessionHash => dispatch(startSurvey(sessionHash))
});

export default connect(mapStateToProps, mapDispatchToProps)(
  withStyles(styles)(injectIntl(Greeting))
);
