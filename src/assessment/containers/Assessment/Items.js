/*
 *
 * Test
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';
import MobileStepper from '@material-ui/core/MobileStepper';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import { createStructuredSelector } from 'reselect';
import CircularProgress from '@material-ui/core/CircularProgress/CircularProgress';
import LinearProgress from '@material-ui/core/LinearProgress';
import { Map, List } from 'immutable';
import { injectIntl } from 'react-intl';

import assessmentMessages from '../../messages/assessment';
import formats from '../../../formats';
import {
  changeActiveItem,
  dispatchItemAction,
  fetchItems,
  submitSolutions,
  changeNote,
  endSurvey,
  endSurveyDialogOpen,
  endSurveyDialogClose,
  endSurveyDialogTextChange,
  showDialogEmptyResponse,
  closeDialogEmptyResponse
} from '../../actions/assessment';
import {
  statesSelector,
  itemsSelector,
  actionsSelector,
  passedSelector,
  activeItemSelector,
  notesSelector,
  checksSelector,
  statusSelector,
  endSurveyDialogSelector,
  endSurveyDialogTextSelector,
  progressSelector,
  sessionHashSelector,
  surveySelector,
  dialogEmptyResponseSelector
} from '../../selectors/assessment';
import ItemRenderer, {
  itemReducer
} from '../../../common/components/ItemRenderer';
import Loading from '../../../common/components/Loading';
import { addNotification } from '../../../common/actions/notifications';
import ConfirmDialog from '../../../common/components/ConfirmDialog';
import EndSurveyDialog from './EndSurveyDialog';

const styles = theme => ({
  main: {
    display: 'flex',
    flexDirection: 'column',
    padding: `${theme.spacing.unit * 2}px`,
    alignItems: 'center',
    '@supports (display: grid)': {
      alignSelf: 'stretch',
      gridArea: 'main'
    }
  },
  end: {
    padding: `${theme.spacing.unit * 2}px`,
    '@supports (display: grid)': {
      gridArea: 'end'
    }
  },
  stepper: {
    '@supports (display: grid)': {
      gridArea: 'stepper',
      width: '100%'
    }
  },
  submit: {
    padding: `${theme.spacing.unit * 2}px`,
    '@supports (display: grid)': {
      gridArea: 'submit'
    }
  },
  progressStart: {
    padding: `${theme.spacing.unit * 2}px`,
    '@supports (display: grid)': {
      gridArea: 'progressStart'
    }
  },
  progressEnd: {
    padding: `${theme.spacing.unit * 2}px`,
    '@supports (display: grid)': {
      gridArea: 'progressEnd'
    }
  },
  progress: {
    width: '100%',
    '@supports (display: grid)': {
      gridArea: 'progress'
    }
  },
  buttonProgress: {
    position: 'absolute',
    top: '50%',
    left: '50%',
    marginTop: -12,
    marginLeft: -12
  },
  resume: {
    padding: `${theme.spacing.unit * 2}px`,
    '@supports (display: grid)': {
      gridArea: 'resume'
    }
  }
});

export class Items extends React.Component {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    classes: PropTypes.object.isRequired,
    intl: PropTypes.object.isRequired,
    items: PropTypes.instanceOf(List).isRequired,
    active: PropTypes.number,
    states: PropTypes.instanceOf(Map).isRequired,
    actions: PropTypes.instanceOf(Map).isRequired,
    notes: PropTypes.instanceOf(Map).isRequired,
    passed: PropTypes.bool,
    checks: PropTypes.instanceOf(List),
    status: PropTypes.instanceOf(Map),
    endSurveyDialogOpen: PropTypes.bool,
    endSurveyDialogText: PropTypes.string,
    progress: PropTypes.number,
    sessionHash: PropTypes.string.isRequired,
    survey: PropTypes.instanceOf(Map).isRequired
  };

  componentDidMount() {
    const { sessionHash, dispatch } = this.props;
    dispatch(fetchItems(sessionHash));
  }

  componentWillReceiveProps(nextProps) {
    const { sessionHash, dispatch, intl } = this.props;
    if (nextProps.passed) {
      dispatch(fetchItems(sessionHash));
    } else if (
      nextProps.checks &&
      this.props.checks !== nextProps.checks &&
      !nextProps.status.get('submitting')
    ) {
      dispatch(
        addNotification({
          message: intl.formatMessage(assessmentMessages.incorrectItem)
        })
      );
      const entry = nextProps.checks.findEntry(value => !value);
      this.onJumpTo(entry[0]);
    }
  }

  dispatch = (action, id) => {
    const { items, dispatch } = this.props;
    const formatType = items.getIn([id, 'format', 'type']);
    const format = formats[formatType];
    const reducer = (state, action) =>
      itemReducer(state, action, format.reducer);
    dispatch(dispatchItemAction(action, reducer, id));
  };

  onCheckSubmit = () => {
    const { dispatch, states, items } = this.props;
    let itemNeedsActions = -1;
    // check if all responses are filled out
    for (let entry of items.entries()) {
      const id = entry[0];
      const item = entry[1];
      const numItemResponses = item.get('responses', new List([])).size;
      const numUserResponses = states.getIn([id, 'responses'], new List([]))
        .size;
      if (numItemResponses > numUserResponses) {
        // showDialog
        itemNeedsActions = id;
      }
    }
    if (itemNeedsActions > -1) {
      dispatch(showDialogEmptyResponse());
    } else {
      this.onSubmit();
    }
  };

  onSubmit = () => {
    const { dispatch, sessionHash, states, actions, items } = this.props;
    let { notes } = this.props;
    notes = states.map((state, key) => (notes.get(key) ? notes.get(key) : ''));

    const statesArray = items
      .map((item, i) => {
        const { selectors } = formats[item.getIn(['format', 'type'])];
        const state = states.get(i, new Map({}));
        return selectors(state.delete('note'));
      })
      .toList()
      .toJS();
    const actionsArray = actions.toList().toJS();
    const notesArray = notes.toList().toJS();

    dispatch(
      submitSolutions(sessionHash, statesArray, actionsArray, notesArray)
    );
    dispatch(closeDialogEmptyResponse());
  };

  onNoteChange = (e, i) => {
    const value = e.currentTargetValue;
    this.props.dispatch(changeNote(value, i));
  };

  onNext = () => {
    const { active, dispatch } = this.props;
    dispatch(changeActiveItem(active + 1));
  };

  onPrevious = () => {
    const { active, dispatch } = this.props;
    dispatch(changeActiveItem(active - 1));
  };

  onJumpTo = id => {
    const { dispatch } = this.props;
    dispatch(changeActiveItem(id));
  };

  onEndSurvey = () => {
    const { dispatch, sessionHash, endSurveyDialogText } = this.props;
    this.onSubmit();
    dispatch(endSurvey(sessionHash, endSurveyDialogText));
  };

  render() {
    const {
      classes,
      intl,
      sessionHash,
      items,
      active,
      states,
      notes,
      progress,
      survey,
      status
    } = this.props;
    const steps = items.size;
    return (
      <React.Fragment>
        {items.size > 1 && (
          <MobileStepper
            type="dots"
            position="static"
            steps={steps}
            activeStep={active}
            className={classes.stepper}
            nextButton={
              <Button
                size="small"
                onClick={this.onNext}
                disabled={active === steps - 1}
              >
                {intl.formatMessage(assessmentMessages.jumpableNext)}
                <KeyboardArrowRight />
              </Button>
            }
            backButton={
              <Button
                size="small"
                onClick={this.onPrevious}
                disabled={active === 0}
              >
                <KeyboardArrowLeft />
                {intl.formatMessage(assessmentMessages.jumpablePrevious)}
              </Button>
            }
          />
        )}
        <main className={classes.main}>
          {status.get('fetching') && <Loading />}
          {items.map((item, i) => {
            const state = states.get(i, new Map({}));
            const dispatch = action => this.dispatch(action, i);
            const hidden = i !== active;
            const note = notes.get(i, '');
            return (
              <ItemRenderer
                key={i}
                item={item}
                value={note}
                onNoteChange={e => this.onNoteChange(e, i)}
                hidden={hidden}
                dispatch={dispatch}
                state={state}
              />
            );
          })}
        </main>
        {survey.get('show_progress') && (
          <React.Fragment>
            <div className={classes.progressStart}>{Math.round(progress)}%</div>
            <div className={classes.progress}>
              <LinearProgress variant="determinate" value={progress} />
            </div>
            <div className={classes.progressEnd}>100%</div>
          </React.Fragment>
        )}
        <div className={classes.submit}>
          <Button
            onClick={this.onCheckSubmit}
            disabled={status.get('submitting')}
            color="primary"
            variant="contained"
          >
            {intl.formatMessage(assessmentMessages.nextItem)}
          </Button>
          {status.get('submitting') && (
            <CircularProgress size={24} className={classes.buttonProgress} />
          )}
        </div>
        {survey.get('show_session_id') && (
          <div className={classes.resume}>{sessionHash}</div>
        )}
        <div className={classes.end}>
          <Button onClick={this.props.onEndSurveyDialogOpen}>
            {intl.formatMessage(assessmentMessages.endSurvey)}
          </Button>
        </div>
        <EndSurveyDialog
          open={this.props.endSurveyDialogOpen}
          text={this.props.endSurveyDialogText}
          onClose={this.props.onEndSurveyDialogClose}
          onEnd={this.onEndSurvey}
          onTextChange={this.props.onEndSurveyDialogTextChange}
        />
        <ConfirmDialog
          title={intl.formatMessage(assessmentMessages.emptyResponsesTitle)}
          content={intl.formatMessage(assessmentMessages.emptyResponsesContent)}
          onOk={this.onSubmit}
          onCancel={this.props.onCloseEmptyResponseDialog}
          open={this.props.dialogEmptyResponseOpen}
        />
      </React.Fragment>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  items: itemsSelector(),
  active: activeItemSelector(),
  states: statesSelector(),
  actions: actionsSelector(),
  notes: notesSelector(),
  passed: passedSelector(),
  checks: checksSelector(),
  status: statusSelector(),
  dialogEmptyResponseOpen: dialogEmptyResponseSelector(),
  endSurveyDialogOpen: endSurveyDialogSelector(),
  endSurveyDialogText: endSurveyDialogTextSelector(),
  progress: progressSelector(),
  sessionHash: sessionHashSelector(),
  survey: surveySelector()
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    onEndSurveyDialogOpen: () => dispatch(endSurveyDialogOpen()),
    onEndSurveyDialogClose: () => dispatch(endSurveyDialogClose()),
    onEndSurveyDialogTextChange: text =>
      dispatch(endSurveyDialogTextChange(text)),
    onCloseEmptyResponseDialog: () => dispatch(closeDialogEmptyResponse())
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(styles)(injectIntl(Items)));
