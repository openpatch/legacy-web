import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { createStructuredSelector } from 'reselect';
import { withStyles } from '@material-ui/core/styles';
import Greeting from './Greeting';
import Items from './Items';
import Farewell from './Farewell';
import {
  sessionHashSelector,
  surveySelector,
  phaseSelector
} from '../../selectors/assessment';
import Footer from '../../../common/containers/Footer';

const styles = theme => ({
  grid: {
    fontFamily: 'Roboto',
    maxWidth: 1280,
    overflow: 'auto',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    position: 'absolute',
    bottom: 80,
    left: 0,
    right: 0,
    top: 0,
    '@supports (display: grid)': {
      display: 'grid',
      margin: '0 auto',
      justifyItems: 'center',
      alignItems: 'center',
      alignContent: 'space-between',
      gridTemplateColumns: '150px auto 150px',
      gridTemplateRows: 'auto 1fr auto',
      gridGap: `${theme.spacing.unit * 2}px`,
      gridTemplateAreas: `"stepper stepper stepper" "main main main" "progressStart progress progressEnd" "end resume submit"`
    }
  }
});

class Assessment extends React.Component {
  static propTypes = {
    classes: PropTypes.object.isRequired,
    phase: PropTypes.oneOf(['greeting', 'items', 'farewell']).isRequired,
    sessionHash: PropTypes.string.isRequired,
    survey: PropTypes.object.isRequired
  };

  componentDidMount() {
    window.onbeforeunload = function() {
      return false;
    };
  }

  componentWillUnmount() {
    window.onbeforeunload = null;
  }

  render() {
    const { classes, phase, sessionHash, survey } = this.props;
    if (!sessionHash && !survey) {
      return <Redirect to="/" />;
    }
    let section;
    switch (phase) {
      case 'greeting':
        section = <Greeting />;
        break;
      case 'items':
        section = <Items />;
        break;
      case 'farewell':
        window.onbeforeunload = null;
        section = <Farewell />;
        break;
      default:
        section = <Redirect to="/" />;
    }
    return (
      <div>
        <div className={classes.grid}>{section}</div>
        <Footer />
      </div>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  sessionHash: sessionHashSelector(),
  survey: surveySelector(),
  phase: phaseSelector()
});

export default connect(mapStateToProps)(withStyles(styles)(Assessment));
