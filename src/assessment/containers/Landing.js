/*
 *
 * Landing
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { injectIntl } from 'react-intl';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import AppBar from '@material-ui/core/AppBar';
import { createStructuredSelector } from 'reselect';
import Paper from '@material-ui/core/Paper';

import udeBW from './logo_ude_en_bw.png';
import cseBW from './logo_cse_en_bw.png';

import {
  clear,
  changeIdentifier,
  submitIdentifier,
  changePassword
} from '../actions/landing';
import {
  makeIdentifierSelector,
  makeStatusSelector,
  makeSessionHashSelector,
  makeSurveySelector,
  makePasswordSelector
} from '../selectors/landing';
import landingMessages from '../messages/landing';
import Typography from '@material-ui/core/Typography/Typography';
import PureTextField from '../../common/components/PureTextField';
import { addNotification } from '../../common/actions/notifications';
import { changeLocale } from '../../common/actions/language';
import Footer from '../../common/containers/Footer';

import gbFlag from '../images/gb.svg';
import deFlag from '../images/de.svg';

const styles = theme => ({
  header: {
    display: 'flex',
    padding: '16px 32px',
    background: '#004c93',
    color: 'white',
    height: 52,
    fontFamily: 'Roboto'
  },
  body: {
    position: 'absolute',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    bottom: 80,
    left: 0,
    right: 0,
    overflow: 'auto',
    top: 52
  },
  organizations: {
    left: 0,
    right: 0,
    display: 'flex',
    justifyContent: 'center'
  },
  organization: {
    margin: theme.spacing.unit * 2
  },
  flags: {
    display: 'flex',
    justifyContent: 'center'
  },
  flag: {
    margin: theme.spacing.unit * 2,
    overflow: 'hidden',
    border: 'none',
    background: 'none',
    padding: theme.spacing.unit * 2
  },
  surveys: {
    display: 'flex',
    justifyContent: 'center'
  },
  survey: {
    margin: theme.spacing.unit * 2,
    width: 500,
    overflow: 'auto',
    padding: theme.spacing.unit * 2
  }
});

export class Landing extends React.Component {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    classes: PropTypes.object.isRequired,
    intl: PropTypes.object.isRequired,
    identifier: PropTypes.string.isRequired,
    sessionHash: PropTypes.string,
    status: PropTypes.object.isRequired
  };

  static defaultProps = {
    sessionHash: null
  };

  constructor(props) {
    super(props);

    if (props.match.params && props.match.params.suffix) {
      props.dispatch(changeIdentifier(props.match.params.suffix));
    }
  }

  componentWillReceiveProps(nextProps) {
    const { dispatch, status, intl } = this.props;
    if (
      nextProps.status.get('submittingIdentifierFailed') === 'expired' &&
      nextProps.status.get('submittingIdentifierFailed') !==
        status.get('submittingIdentifierFailed')
    ) {
      dispatch(
        addNotification({
          message: intl.formatMessage(landingMessages.notificationSurveyClosed)
        })
      );
    }
    if (
      nextProps.status.get('submittingIdentifierFailed') === 'na' &&
      nextProps.status.get('submittingIdentifierFailed') !==
        status.get('submittingIdentifierFailed')
    ) {
      dispatch(
        addNotification({
          message: intl.formatMessage(
            landingMessages.notificationNoSurveyWithName
          )
        })
      );
    }
    if (
      nextProps.status.get('submittingIdentifierFailed') === 'forbidden' &&
      nextProps.status.get('submittingIdentifierFailed') !==
        status.get('submittingIdentifierFailed')
    ) {
      dispatch(
        addNotification({
          message: intl.formatMessage(
            landingMessages.notificationIncorrectPassword
          )
        })
      );
    }
  }

  onIdentifierSubmit = e => {
    e.preventDefault();
    const { dispatch, identifier, password } = this.props;
    dispatch(submitIdentifier(identifier, password));
  };

  onIdentifierChange = e => {
    const value = e.currentTarget.value;
    this.props.dispatch(changeIdentifier(value));
  };

  onPasswordChange = e => {
    const value = e.currentTarget.value;
    this.props.dispatch(changePassword(value));
  };

  componentWillUnmount() {
    this.props.dispatch(clear());
  }

  render() {
    const {
      classes,
      intl,
      identifier,
      password,
      status,
      publicSurveys,
      sessionHash,
      survey,
      changeLocale,
      match
    } = this.props;
    if (sessionHash && survey) {
      return <Redirect to={`${match.url}/test`} />;
    }
    return (
      <div>
        <AppBar position="static" className={classes.header}>
          COMMOOP
        </AppBar>
        <div className={classes.body}>
          <div className={classes.flags}>
            <button className={classes.flag} onClick={() => changeLocale('en')}>
              <img src={gbFlag} alt="english" width={200} />
            </button>
            <button className={classes.flag} onClick={() => changeLocale('de')}>
              <img src={deFlag} alt="german" width={200} />
            </button>
          </div>
          <div className={classes.surveys}>
            <Paper className={classes.survey}>
              <Typography type="title">
                {intl.formatMessage(landingMessages.privateSurvey)}
              </Typography>
              <PureTextField
                margin="normal"
                label={intl.formatMessage(landingMessages.name)}
                error={status.get('submittingIdentifierFailed') === 'na'}
                fullWidth
                onChange={this.onIdentifierChange}
                value={identifier}
              />
              <PureTextField
                margin="normal"
                label={intl.formatMessage(landingMessages.password)}
                fullWidth
                type="password"
                error={status.get('submittingIdentifierFailed') === 'forbidden'}
                onChange={this.onPasswordChange}
                value={password}
              />
              <Button
                variant="contained"
                color="primary"
                onClick={this.onIdentifierSubmit}
              >
                {intl.formatMessage(landingMessages.start)}
              </Button>
            </Paper>
            {publicSurveys && (
              <Paper className={classes.survey}>
                <Typography type="title">
                  {intl.formatMessage(landingMessages.publicSurvey)}
                </Typography>
              </Paper>
            )}
          </div>
          <section className={classes.organizations}>
            <div className={classes.organization}>
              <a
                href="https://www.uni-due.de"
                target="_blank"
                rel="noopener noreferrer"
              >
                <img
                  src={udeBW}
                  height={50}
                  alt="University of Duisburg-Essen"
                />
              </a>
            </div>
            <div className={classes.organization}>
              <div style={{ width: 32, display: 'inline-block' }} />
              <a
                href="https://www.ddi.wiwi.uni-due.de"
                target="_blank"
                rel="noopener noreferrer"
              >
                <img src={cseBW} height={50} alt="Computer Science Education" />
              </a>
            </div>
          </section>
        </div>
        <Footer />
      </div>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  identifier: makeIdentifierSelector(),
  status: makeStatusSelector(),
  sessionHash: makeSessionHashSelector(),
  survey: makeSurveySelector(),
  password: makePasswordSelector()
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    changeLocale: locale => dispatch(changeLocale(locale))
  };
}

export const LandingStyled = withStyles(styles)(Landing);

export default connect(mapStateToProps, mapDispatchToProps)(
  injectIntl(LandingStyled)
);
