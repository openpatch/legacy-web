// polyfills
import 'babel-polyfill';
import React from 'react';
import ReactDOM from 'react-dom';
import Raven from 'raven-js';

// import registerServiceWorker from './registerServiceWorker';
import store from './store';
import theme from './theme';
import App from './common/containers/App';

Raven.config('https://0d4d954f39dd49d5a20cab89bffdd14d@sentry.io/1773239', {
  environment: process.env.NODE_ENV,
  release: process.env.REACT_APP_VERSION
}).install();

// Polyfills
if (window.NodeList && !NodeList.prototype.forEach) {
  NodeList.prototype.forEach = function(callback, thisArg) {
    thisArg = thisArg || window;
    for (var i = 0; i < this.length; i++) {
      callback.call(thisArg, this[i], i, this);
    }
  };
}

ReactDOM.render(
  <App store={store} theme={theme} />,
  document.getElementById('root')
);
// registerServiceWorker();
