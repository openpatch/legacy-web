/*
 *
 * Format
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { Map, List } from 'immutable';
import { createStructuredSelector } from 'reselect';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import { FormattedMessage } from 'react-intl';

import AppBarLayout from '../../common/components/AppBarLayout';
import AppBar from '../../common/containers/AppBar';
import AddButton from '../../common/components/AddButton';
import ItemsTable from './ItemsTable';

import { setActive } from '../actions/formats';
import { fetchItemsForFormat, resetEditData } from '../actions/items';

import { makeActiveFormatSelector } from '../selectors/formats';
import { makeItemsActiveFormatSelector } from '../selectors/items';

import { getFormatNameMessage } from '../../formats/utils';
import formats from '../../formats';

const styles = theme => ({
  grid: {
    '@supports (display: grid)': {
      display: 'grid',
      gridTemplateColumns: '1fr 2fr',
      gridTemplateRows: '200px auto',
      gridGap: `${theme.spacing.unit * 2}px`,
      gridTemplateAreas: `"banner banner" "main main"`,

      [theme.breakpoints.down('md')]: {
        display: 'block',
        '& > div': {
          marginTop: theme.spacing.unit * 2,
          marginBottom: theme.spacing.unit * 2
        }
      }
    }
  },
  main: {
    '@supports (display: grid)': {
      gridArea: 'main'
    }
  },
  banner: {
    '@supports (display: grid)': {
      gridArea: 'banner'
    },
    '& > img': {
      height: '100%'
    },
    height: '200px',
    overflow: 'hidden'
  }
});

export class Format extends React.PureComponent {
  static propTypes = {
    classes: PropTypes.object.isRequired,
    data: PropTypes.instanceOf(Map),
    items: PropTypes.instanceOf(List),
    match: PropTypes.object.isRequired
  };

  static defaultProps = {
    items: new List([]),
    data: new Map({})
  };

  componentDidMount() {
    const { match, dispatch } = this.props;
    const format = match.params.format;
    dispatch(setActive(format));
    dispatch(fetchItemsForFormat(format));
  }

  onAddClick = () => {
    const { match } = this.props;
    this.props.dispatch(resetEditData());
    this.props.dispatch(push(`${match.url}/create`));
  };

  render() {
    const { data, match, classes, items } = this.props;
    const format = match.params.format;
    const formatBanner = formats[format].banner;
    return (
      <AppBarLayout
        className={classes.grid}
        appBar={
          data.get('type') !== undefined ? (
            <AppBar
              title={<FormattedMessage {...getFormatNameMessage(format)} />}
              bg={`#${data.get('color_hex')}`}
            >
              <AddButton onClick={this.onAddClick} />
            </AppBar>
          ) : (
            undefined
          )
        }
      >
        <Paper
          className={classes.banner}
          style={{
            backgroundImage: `url(${formatBanner})`,
            backgroundPosition: '50% 50%',
            backgroundSize: 'cover'
          }}
        />
        <main className={classes.main}>
          <ItemsTable items={items} />
        </main>
      </AppBarLayout>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  data: makeActiveFormatSelector(),
  items: makeItemsActiveFormatSelector()
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(styles)(Format));
