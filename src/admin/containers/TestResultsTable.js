import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import { createStructuredSelector } from 'reselect';
import { injectIntl } from 'react-intl';
import compareDateAsc from 'date-fns/compare_asc';
import { List } from 'immutable';
import {
  PagingState,
  IntegratedPaging,
  SortingState,
  IntegratedSorting,
  FilteringState,
  IntegratedFiltering,
  SelectionState,
  IntegratedSelection
} from '@devexpress/dx-react-grid';
import {
  Grid,
  Table,
  TableHeaderRow,
  TableSelection,
  PagingPanel,
  TableFilterRow
} from '@devexpress/dx-react-grid-material-ui';
import { Typography } from '@material-ui/core';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import CloudDownloadIcon from '@material-ui/icons/CloudDownload';

import ConfirmDialog from '../../common/components/ConfirmDialog';
import DateCellComponent from '../components/DateCellComponent';
import LinkCellComponent from '../components/LinkCellComponent';
import { resultsSelectedSelector } from '../selectors/tests';
import {
  setSelectedResults,
  deleteSelectedResults,
  exportResultsForTestId
} from '../actions/tests';
import testMessages from '../messages/tests';

const Row = ({ row, ...props }) => (
  <Table.Row
    {...props}
    style={{
      backgroundColor: row.abort ? '#FFEBEE' : !row.end ? '#FFFDE7' : '#F1F8E9'
    }}
  />
);

const Cell = props => {
  if (props.column.type === 'date') {
    return <DateCellComponent {...props} />;
  } else if (props.column.type === 'link') {
    return <LinkCellComponent {...props} />;
  }
  return <Table.Cell {...props} />;
};

const styles = theme => ({
  container: {
    display: 'flex',
    flexDirection: 'column'
  },
  toolbar: {
    textAlign: 'right'
  }
});

class TestResultsTable extends React.Component {
  static propType = {
    classes: PropTypes.object.isRequired,
    intl: PropTypes.object.isRequired,
    testId: PropTypes.number.isRequired,
    results: PropTypes.array,
    columns: PropTypes.array,
    pageSizes: PropTypes.array,
    columnExtensions: PropTypes.object,
    integratedSortingExtensions: PropTypes.array,
    resultsSelected: PropTypes.instanceOf(List)
  };

  static defaultProps = {
    results: new List([]),
    columns: [
      {
        name: 'id',
        title: 'ID'
      },
      {
        name: 'survey_id',
        title: 'Survey'
      },
      {
        name: 'session_hash',
        title: 'Session',
        type: 'link',
        getLink: row => `/admin/tests/${row.test_id}/results/${row.id}`
      },
      {
        name: 'start',
        title: 'Start',
        type: 'date'
      },
      {
        name: 'end',
        title: 'End',
        type: 'date'
      },
      {
        name: 'num_items_correct',
        title: '#Correct'
      },
      {
        name: 'abort_reason',
        title: 'Abort'
      }
    ],
    pageSizes: [5, 10, 25, 50, 100],
    columnExtensions: [],
    integratedSortingExtensions: [
      {
        columnName: 'start',
        compare: compareDateAsc
      },
      {
        columnName: 'end',
        compare: compareDateAsc
      }
    ]
  };

  state = {
    deleteResultsDialog: false
  };

  componentWillUnmount() {
    this.props.onSelectionChange([]);
  }

  onDeleteSelected = () => {
    const { resultsSelected, onDeleteSelected } = this.props;
    onDeleteSelected(resultsSelected.toJS());
    this.onCloseDeleteResultsDialog();
  };

  onOpenDeleteResultsDialog = () => {
    this.setState({
      deleteResultsDialog: true
    });
  };

  onCloseDeleteResultsDialog = () => {
    this.setState({
      deleteResultsDialog: false
    });
  };

  onExportResults = () => {
    const { onExportResults, testId, resultsSelected } = this.props;
    onExportResults(testId, resultsSelected.toJS());
  };

  render() {
    const {
      classes,
      columns,
      results,
      columnExtensions,
      intl,
      pageSizes,
      resultsSelected,
      integratedSortingExtensions,
      onSelectionChange
    } = this.props;
    const enhancedColumnExtensions = [
      { columnName: 'favorite', width: 80, align: 'left' },
      { columnName: 'menu', width: 80, align: 'right' },
      ...columnExtensions
    ];
    const enhancedIntegratedSortingExtensions = [
      ...integratedSortingExtensions
    ];
    return (
      <div className={classes.container}>
        <ConfirmDialog
          title={intl.formatMessage(testMessages.deleteSelectedResultsTitle)}
          content={intl.formatMessage(
            testMessages.deleteSelectedResultsContent,
            { resultsCount: resultsSelected.size }
          )}
          onOk={this.onDeleteSelected}
          onCancel={this.onCloseDeleteResultsDialog}
          open={this.state.deleteResultsDialog}
        />
        <Grid getRowId={row => row.id} columns={columns} rows={results}>
          <FilteringState />
          <IntegratedFiltering />
          <SortingState />
          <IntegratedSorting
            columnExtensions={enhancedIntegratedSortingExtensions}
          />
          <SelectionState
            selection={resultsSelected.toJS()}
            onSelectionChange={onSelectionChange}
          />
          <IntegratedSelection />
          <PagingState defaultCurrentPage={0} defaultPageSize={10} />
          <IntegratedPaging />
          <Table
            rowComponent={Row}
            cellComponent={Cell}
            columnExtensions={enhancedColumnExtensions}
          />
          <TableFilterRow />
          <TableSelection showSelectAll />
          <TableHeaderRow showSortingControls />
          <PagingPanel pageSizes={pageSizes} />
        </Grid>
        <div className={classes.toolbar}>
          <Typography>
            {resultsSelected.size} results selected
            <IconButton onClick={this.onOpenDeleteResultsDialog}>
              <DeleteIcon />
            </IconButton>
            <IconButton onClick={this.onExportResults}>
              <CloudDownloadIcon />
            </IconButton>
          </Typography>
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  dispatch,
  onSelectionChange: selection => dispatch(setSelectedResults(selection)),
  onDeleteSelected: ids => dispatch(deleteSelectedResults(ids)),
  onExportResults: (id, filter) => dispatch(exportResultsForTestId(id, filter))
});

const mapStateToProps = createStructuredSelector({
  resultsSelected: resultsSelectedSelector
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(styles)(injectIntl(TestResultsTable)));
