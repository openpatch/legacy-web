/*
 *
 * Test
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { List, Map } from 'immutable';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { createStructuredSelector } from 'reselect';
import { withStyles } from '@material-ui/core/styles';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import CloudDownloadIcon from '@material-ui/icons/CloudDownload';
import Tooltip from '@material-ui/core/Tooltip';

import {
  makeActiveTestSelector,
  makeActiveTestFilteredResultsSelector,
  makeActiveTestSurveysSelector,
  makeActiveTestPermittedMembersSelector,
  testMembersPermittedStatusSelector
} from '../selectors/tests';
import {
  fetchTest,
  fetchResultsForId,
  setActive,
  removeMemberFromPermittedForId,
  addMemberToPermittedForId,
  setResultsFilterBySurveyIds,
  exportTest
} from '../actions/tests';
import { fetchMembersPermittedForTestId } from '../actions/members';
import { fetchSurveysForTestId } from '../actions/surveys';

import AppBarLayout from '../../common/components/AppBarLayout';
import AppBar from '../../common/containers/AppBar';
import MembersPermittedPopup from '../components/MembersPermittedPopup';
import ShareButton from '../../common/components/ShareButton';
import ItemGroupList from './ItemGroupList';
import SelectionGridTable from '../components/SelectionGridTable';
import TestResultsTable from './TestResultsTable';
import StatisticSummaryTable, {
  generateRowData
} from '../../common/components/StatisticSummaryTable';
import RefreshButton from '../../common/components/RefreshButton';

const styles = theme => ({});

export class Test extends React.Component {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    match: PropTypes.object.isRequired,
    test: PropTypes.instanceOf(Map),
    results: PropTypes.instanceOf(List)
  };

  static defaultProps = {
    results: [],
    surveyColumns: [
      {
        name: 'id',
        title: 'id'
      },
      {
        name: 'member_id',
        title: 'Author',
        getCellValue: row => (row.member ? row.member.username : undefined)
      },
      {
        name: 'name',
        title: 'Name',
        getCellValue: row => (
          <Button size="small" component={Link} to={`/admin/surveys/${row.id}`}>
            {row.name}
          </Button>
        )
      },
      {
        name: 'city',
        title: 'City'
      },
      {
        name: 'term',
        title: 'Term'
      },
      {
        name: 'starts_on',
        title: 'Start',
        type: 'date'
      },
      {
        name: 'ends_on',
        title: 'End',
        type: 'date'
      },
      {
        name: 'created_on',
        title: 'Created On',
        type: 'date'
      },
      {
        name: 'count_results',
        title: '#Results',
        getCellValue: row => row.stats.num_results || 0
      }
    ]
  };

  state = {
    MembersPermittedPopupOpen: false
  };

  componentDidMount() {
    const id = this.props.match.params.testId;
    this.props.dispatch(setActive(id));
    this.props.dispatch(fetchTest(id));
    this.props.dispatch(fetchResultsForId(id));
    this.props.dispatch(fetchSurveysForTestId(id));
    this.props.dispatch(fetchMembersPermittedForTestId(id));
  }

  onMembersPermittedPopupOpen = () => {
    this.setState({
      membersPermittedPopupOpen: true
    });
  };

  onMembersPermittedPopupClose = () => {
    this.setState({
      membersPermittedPopupOpen: false
    });
  };

  onAddMemberToPermitted = username => {
    const { dispatch, test } = this.props;
    dispatch(addMemberToPermittedForId(test.get('id'), username));
    dispatch(fetchMembersPermittedForTestId(test.get('id')));
  };

  onRemoveMemberFromPermitted = username => {
    const { dispatch, test } = this.props;
    dispatch(removeMemberFromPermittedForId(test.get('id'), username));
  };

  onSurveyFilterChange = value => {
    this.setState({
      filteredSurveys: value
    });
  };

  onRefreshStatistic = () => {
    const id = this.props.match.params.testId;
    const value = this.state.filteredSurveys;
    this.props.dispatch(fetchTest(id, value));
    this.props.dispatch(setResultsFilterBySurveyIds(value));
    this.setState({
      currentSurveys: value
    });
  };

  onExport = () => {
    const { onExport, test } = this.props;
    onExport(test.get('id'));
  };

  render() {
    const {
      test,
      permittedMembers,
      permittedMembersStatus,
      surveys,
      surveyColumns,
      results
    } = this.props;
    return (
      <AppBarLayout
        appBar={
          <AppBar title={`Test Nr.${test.get('id')} `}>
            <ShareButton onClick={this.onMembersPermittedPopupOpen} />
            <Tooltip title="Download Meta Data">
              <IconButton color="inherit" onClick={this.onExport}>
                <CloudDownloadIcon />
              </IconButton>
            </Tooltip>
            <Tooltip title="Refresh Statistic">
              <RefreshButton
                color="inherit"
                onClick={this.onRefreshStatistic}
              />
            </Tooltip>
          </AppBar>
        }
      >
        <MembersPermittedPopup
          members={permittedMembers}
          open={this.state.membersPermittedPopupOpen}
          onClose={this.onMembersPermittedPopupClose}
          errorAdding={permittedMembersStatus.get('putting_failed')}
          onMemberDelete={this.onRemoveMemberFromPermitted}
          onMemberAdd={this.onAddMemberToPermitted}
        />
        <ExpansionPanel expanded={true}>
          <ExpansionPanelSummary>
            <Typography>Information</Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails style={{ display: 'block' }}>
            <StatisticSummaryTable
              rows={
                new Map({
                  scores: generateRowData(test.get('stats'), 'score'),
                  'times in s': generateRowData(test.get('stats'), 'time')
                })
              }
            />
            <br />
            <Typography>
              The statistic includes only test results which were fully
              completed.
            </Typography>
          </ExpansionPanelDetails>
        </ExpansionPanel>
        <ExpansionPanel defaultExpanded>
          <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
            <Typography>Items</Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails style={{ display: 'block' }}>
            <ItemGroupList surveyIds={this.state.currentSurveys} />
          </ExpansionPanelDetails>
        </ExpansionPanel>
        <ExpansionPanel defaultExpanded>
          <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
            <Typography>Surveys</Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails style={{ overflowX: 'auto' }}>
            <SelectionGridTable
              rows={surveys.toJS()}
              columns={surveyColumns}
              onSelectionChange={this.onSurveyFilterChange}
            />
          </ExpansionPanelDetails>
        </ExpansionPanel>
        <ExpansionPanel expanded={true}>
          <ExpansionPanelSummary>
            <Typography>Results</Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails
            style={{
              overflowX: 'auto'
            }}
          >
            <TestResultsTable
              testId={test.get('id')}
              results={results.toJS()}
            />
          </ExpansionPanelDetails>
        </ExpansionPanel>
      </AppBarLayout>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  test: makeActiveTestSelector(),
  surveys: makeActiveTestSurveysSelector(),
  results: makeActiveTestFilteredResultsSelector(),
  permittedMembers: makeActiveTestPermittedMembersSelector(),
  permittedMembersStatus: testMembersPermittedStatusSelector
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    onExport: id => dispatch(exportTest(id))
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(styles)(Test));
