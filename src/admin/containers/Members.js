/*
 *
 * Members
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { List as IList, Map } from 'immutable';
import { Link } from 'react-router-dom';
import { createStructuredSelector } from 'reselect';
import { withStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import IconButton from '@material-ui/core/IconButton';
import EditIcon from '@material-ui/icons/Edit';

import {
  fetchMembers,
  setFilter,
  setActive,
  openMemberCreateDialog,
  setEditData
} from '../actions/members';
import {
  filterSelector,
  makeFilteredDataSelector,
  makeActiveMemberSelector
} from '../selectors/members';

import AppBarLayout from '../../common/components/AppBarLayout';
import AppBar from '../../common/containers/AppBar';
import Search from '../../common/components/Search';
import AddButton from '../../common/components/AddButton';
import Avatar from '../../common/components/Avatar';
import MemberCreate from './MemberCreate';
import MemberInfo from '../components/MemberInfo';

const styles = theme => ({
  list: {
    position: 'absolute',
    width: 300,
    top: 64,
    bottom: 0,
    overflowY: 'auto',
    borderRight: '1px solid rgb(185,185,185)'
  },
  content: {
    marginLeft: 300,
    padding: '0 12px'
  }
});

export class Members extends React.PureComponent {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    data: PropTypes.instanceOf(IList).isRequired,
    filter: PropTypes.string,
    match: PropTypes.object.isRequired,
    activeMember: PropTypes.instanceOf(Map)
  };

  static defaultProps = {
    filter: undefined,
    activeMember: undefined
  };

  componentDidMount() {
    this.props.dispatch(fetchMembers());
  }

  onSearchChange = e => {
    const filter = e.currentTarget.value;
    this.props.dispatch(setFilter(filter));
  };

  onChangeActive = id => {
    this.props.dispatch(setActive(id));
  };

  onNew = () => {
    this.props.dispatch(openMemberCreateDialog());
  };

  onEdit = id => {
    const { dispatch, data } = this.props;
    const member = data.find(d => d.get('id') === id);
    dispatch(setEditData(member));
    dispatch(openMemberCreateDialog());
  };
  render() {
    const { classes, data, filter, match, activeMember } = this.props;
    const members = data;
    return (
      <AppBarLayout
        appBar={
          <AppBar title="Members">
            <Search
              value={filter}
              placeholder="Search..."
              onChange={this.onSearchChange}
            />
            <AddButton onClick={this.onNew} />
          </AppBar>
        }
      >
        <MemberCreate />
        <List className={classes.list}>
          {members.map(member => (
            <ListItem
              button
              onClick={() => this.onChangeActive(member.get('id'))}
              key={member.get('id')}
            >
              <Avatar name={member.get('username')} role={member.get('role')} />
              <ListItemText
                primary={member.get('username')}
                secondary={member.get('role')}
              />
              <ListItemSecondaryAction>
                <IconButton
                  aria-label="Edit"
                  onClick={() => this.onEdit(member.get('id'))}
                >
                  <EditIcon />
                </IconButton>
              </ListItemSecondaryAction>
            </ListItem>
          ))}
        </List>
        <div className={classes.content}>
          <MemberInfo member={activeMember} />
        </div>
      </AppBarLayout>
    );
  }
}

const mapStateToProps = state =>
  createStructuredSelector({
    data: makeFilteredDataSelector(),
    filter: filterSelector,
    activeMember: makeActiveMemberSelector()
  });

function mapDispatchToProps(dispatch) {
  return {
    dispatch
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(styles)(Members));
