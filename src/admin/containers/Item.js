/*
 *
 * Item
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { List, Map } from 'immutable';
import { createStructuredSelector } from 'reselect';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';

import {
  makeActiveItemSelector,
  makeActiveItemResultsSelector,
  makeActiveItemPermittedMembersSelector,
  itemMembersPermittedStatusSelector,
  itemPreviewPopupOpenSelector,
  itemStatisticPopupOpenSelector,
  itemVisualizationPopupOpenSelector,
  makeActiveItemStatisticSelector
} from '../selectors/items';
import {
  setActive,
  fetchResultsForId,
  fetchItem,
  addMemberToPermittedForId,
  removeMemberFromPermittedForId,
  openItemPreviewPopup,
  closeItemPreviewPopup,
  openItemVisualizationPopup,
  closeItemVisualizationPopup,
  openItemStatisticPopup,
  closeItemStatisticPopup,
  fetchStatisticForId
} from '../actions/items';
import { fetchMembersPermittedForItemId } from '../actions/members';

import formats from '../../formats';
import AppBarLayout from '../../common/components/AppBarLayout';
import AppBar from '../../common/containers/AppBar';
import ItemResultsTable from '../components/ItemResultsTable';
import ShareButton from '../../common/components/ShareButton';
import MembersPermittedPopup from '../components/MembersPermittedPopup';
import ItemPreviewPopup from '../components/ItemPreviewPopup';
import MemberCard from '../components/MemberCard';
import PreviewButton from '../../common/components/PreviewButton';
import StatisticButton from '../../common/components/StatisticButton';
import VisualizationButton from '../../common/components/VisualizationButton';
import ItemVisualizationPopup from '../components/ItemVisualizationPopup';
import ItemStatisticPopup from '../components/ItemStatisticPopup';
import ItemBasicStats from '../components/ItemBasicStats';

const styles = theme => ({
  grid: {
    '@supports (display: grid)': {
      display: 'grid',
      gridTemplateColumns: '2fr 1fr',
      gridTemplateRows: 'auto',
      gridGap: `${theme.spacing.unit * 2}px`,
      gridTemplateAreas: `"meta member" "main main"`,

      [theme.breakpoints.down('md')]: {
        display: 'block',
        '& > div': {
          marginTop: theme.spacing.unit * 2,
          marginBottom: theme.spacing.unit * 2
        }
      }
    }
  },
  main: {
    '@supports (display: grid)': {
      gridArea: 'main'
    }
  },
  member: {
    '@supports (display: grid)': {
      gridArea: 'member'
    }
  },
  meta: {
    '@supports (display: grid)': {
      gridArea: 'meta'
    },
    padding: theme.spacing.unit
  }
});

export class Item extends React.Component {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    match: PropTypes.object.isRequired,
    item: PropTypes.object.isRequired,
    results: PropTypes.instanceOf(List),
    statistic: PropTypes.instanceOf(Map)
  };

  static defaultProps = {
    results: []
  };

  state = {
    membersPermittedPopupOpen: false
  };

  componentDidMount() {
    const id = Number(this.props.match.params.itemId);
    this.props.dispatch(setActive(id));
    this.props.dispatch(fetchItem(id));
    this.props.dispatch(fetchResultsForId(id));
    this.props.dispatch(fetchMembersPermittedForItemId(id));
    this.props.dispatch(fetchStatisticForId(id));
  }

  onMembersPermittedPopupOpen = () => {
    this.setState({
      membersPermittedPopupOpen: true
    });
  };

  onMembersPermittedPopupClose = () => {
    this.setState({
      membersPermittedPopupOpen: false
    });
  };

  onAddMemberToPermitted = username => {
    const { dispatch, item } = this.props;
    dispatch(addMemberToPermittedForId(item.get('id'), username));
    dispatch(fetchMembersPermittedForItemId(item.get('id')));
  };

  onRemoveMemberFromPermitted = username => {
    const { dispatch, item } = this.props;
    dispatch(removeMemberFromPermittedForId(item.get('id'), username));
  };

  onPreviewItemOpen = () => {
    const { dispatch } = this.props;
    dispatch(openItemPreviewPopup());
  };

  onPreviewItemClose = () => {
    const { dispatch } = this.props;
    dispatch(closeItemPreviewPopup());
  };

  onStatisticItemOpen = () => {
    const { dispatch } = this.props;
    dispatch(openItemStatisticPopup());
  };

  onStatisticItemClose = () => {
    const { dispatch } = this.props;
    dispatch(closeItemStatisticPopup());
  };

  onVisualizationItemOpen = () => {
    const { dispatch } = this.props;
    dispatch(openItemVisualizationPopup());
  };

  onVisualizationItemClose = () => {
    const { dispatch } = this.props;
    dispatch(closeItemVisualizationPopup());
  };

  render() {
    const {
      classes,
      item,
      results,
      permittedMembers,
      statistic,
      previewItemPopupOpen,
      statisticItemPopupOpen,
      visualizationItemPopupOpen,
      permittedMembersStatus
    } = this.props;
    const colorHex = item.getIn(['format', 'color_hex']);
    return (
      <AppBarLayout
        className={classes.grid}
        appBar={
          <AppBar
            title={item.get('name')}
            bg={item.get('format') ? `#${colorHex}` : undefined}
          >
            <StatisticButton onClick={this.onStatisticItemOpen} />
            {'Visualization' in
            formats[item.getIn(['format', 'type'], 'common')] ? (
              <VisualizationButton onClick={this.onVisualizationItemOpen} />
            ) : null}
            <PreviewButton onClick={this.onPreviewItemOpen} />
            <ShareButton onClick={this.onMembersPermittedPopupOpen} />
          </AppBar>
        }
      >
        <ItemPreviewPopup
          item={item}
          open={previewItemPopupOpen}
          onClose={this.onPreviewItemClose}
        />
        {item.get('format') && (
          <React.Fragment>
            <ItemStatisticPopup
              format={item.get('format').toJS()}
              open={statisticItemPopupOpen}
              data={statistic}
              onClose={this.onStatisticItemClose}
            />
            {'Visualization' in
            formats[item.getIn(['format', 'type'], 'common')] ? (
              <ItemVisualizationPopup
                format={item.get('format').toJS()}
                data={statistic.get('format')}
                open={visualizationItemPopupOpen}
                onClose={this.onVisualizationItemClose}
              />
            ) : null}
          </React.Fragment>
        )}
        <MembersPermittedPopup
          members={permittedMembers}
          open={this.state.membersPermittedPopupOpen}
          onClose={this.onMembersPermittedPopupClose}
          errorAdding={permittedMembersStatus.get('putting_failed')}
          onMemberDelete={this.onRemoveMemberFromPermitted}
          onMemberAdd={this.onAddMemberToPermitted}
        />
        <Paper className={classes.meta}>
          <ItemBasicStats stats={item.get('stats')} />
        </Paper>
        <div className={classes.member}>
          <MemberCard member={item.get('member')} />
        </div>
        <main className={classes.main}>
          <ItemResultsTable results={results.toJS()} />
        </main>
      </AppBarLayout>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  item: makeActiveItemSelector(),
  previewItemPopupOpen: itemPreviewPopupOpenSelector,
  statisticItemPopupOpen: itemStatisticPopupOpenSelector,
  visualizationItemPopupOpen: itemVisualizationPopupOpenSelector,
  results: makeActiveItemResultsSelector(),
  statistic: makeActiveItemStatisticSelector(),
  permittedMembers: makeActiveItemPermittedMembersSelector(),
  permittedMembersStatus: itemMembersPermittedStatusSelector
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(styles)(Item));
