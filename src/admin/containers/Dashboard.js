/*
 *
 * Dashboard
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { List, Map } from 'immutable';
import { push } from 'react-router-redux';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';

import {
  makeFavoriteItemsDataSelector,
  makeFavoriteSurveysDataSelector,
  makeFavoriteTestsDataSelector
} from '../../common/selectors/member';

import { fetchFavoriteTests } from '../actions/tests';
import { fetchFavoriteSurveys } from '../actions/surveys';
import { fetchFavoriteItems } from '../actions/items';
import { toggleFavorite } from '../../common/actions/member';

import AppBarLayout from '../../common/components/AppBarLayout';
import AppBar from '../../common/containers/AppBar';
import FavoriteItemCard from '../components/FavoriteItemCard';
import FavoriteTestCard from '../components/FavoriteTestCard';
import { addNotification } from '../../common/actions/notifications';

const styles = theme => ({
  container: {
    display: 'grid',
    margin: theme.spacing.unit,
    gridTemplateColumns: '1fr 1fr',
    gridGap: theme.spacing.unit * 2 + 'px'
  }
});

export class Dashboard extends React.Component {
  static propTypes = {
    classes: PropTypes.object.isRequired,
    dispatch: PropTypes.func.isRequired,
    favorizedItems: PropTypes.instanceOf(List),
    favorizedTests: PropTypes.instanceOf(List),
    favorizedSurveys: PropTypes.instanceOf(List)
  };

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch(fetchFavoriteItems());
    dispatch(fetchFavoriteSurveys());
    dispatch(fetchFavoriteTests());
  }

  onFavoriteToggle = (type, id) => {
    const { dispatch } = this.props;
    dispatch(toggleFavorite(type, id));
    dispatch(
      addNotification({
        message: `Removed ${id} from favorite ${type}!`,
        autoHideDuration: 1000,
        actions: [
          {
            label: 'undo',
            callback: () => dispatch(toggleFavorite(type, id))
          }
        ]
      })
    );
  };

  onClick = (type, id) => {
    const { dispatch } = this.props;
    dispatch(push(`/admin/${type}/${id}`));
  };

  render() {
    const {
      classes,
      favorizedSurveys,
      favorizedTests,
      favorizedItems
    } = this.props;
    return (
      <AppBarLayout appBar={<AppBar title="Dashboard" />}>
        <div>
          <Typography variant="display1">Favorite Items</Typography>
          <div className={classes.container}>
            {favorizedItems.map(value => (
              <FavoriteItemCard
                onToggleFavorite={() =>
                  this.onFavoriteToggle('items', value.get('id'))
                }
                onClick={() => this.onClick('items', value.get('id'))}
                key={value.get('id')}
                name={value.get('name')}
                stats={value.get('stats', new Map({}))}
              />
            ))}
          </div>
          <Typography variant="display1">Favorite Tests</Typography>
          <div className={classes.container}>
            {favorizedTests.map(value => (
              <FavoriteTestCard
                onToggleFavorite={() =>
                  this.onFavoriteToggle('tests', value.get('id'))
                }
                onClick={() => this.onClick('tests', value.get('id'))}
                key={value.get('id')}
                name={value.get('name')}
                stats={value.get('stats', new Map({}))}
              />
            ))}
          </div>
          <Typography variant="display1">Favorite Surveys</Typography>
          <div className={classes.container}>
            {favorizedSurveys.map(value => (
              <FavoriteTestCard
                onToggleFavorite={() =>
                  this.onFavoriteToggle('surveys', value.get('id'))
                }
                onClick={() => this.onClick('surveys', value.get('id'))}
                key={value.get('id')}
                name={value.get('name')}
                stats={value.get('stats', new Map({}))}
              />
            ))}
          </div>
        </div>
      </AppBarLayout>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  favorizedItems: makeFavoriteItemsDataSelector(),
  favorizedSurveys: makeFavoriteSurveysDataSelector(),
  favorizedTests: makeFavoriteTestsDataSelector()
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(styles)(Dashboard));
