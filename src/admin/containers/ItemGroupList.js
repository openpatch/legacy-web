/*
 *
 * ItemGroupList
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import { createStructuredSelector } from 'reselect';
import _isEqual from 'lodash/isEqual';

import {
  makeActiveItemSelector,
  itemPreviewPopupOpenSelector
} from '../selectors/items';
import {
  fetchItemsForTestId,
  setActive as setActiveItem,
  openItemPreviewPopup,
  closeItemPreviewPopup
} from '../actions/items';
import {
  makeActiveTestItemGroupsSelector,
  activeSelector
} from '../selectors/tests';
import ItemGroup from '../components/ItemGroup';
import ItemPreviewPopup from './ItemInfoPopup';

const styles = theme => ({});

export class ItemGroupList extends React.Component {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    surveyIds: PropTypes.array,
    classes: PropTypes.object.isRequired
  };

  static defaultProps = {
    surveyIds: []
  };

  componentDidMount() {
    const { dispatch, activeTestId, surveyIds } = this.props;
    if (activeTestId !== undefined) {
      dispatch(fetchItemsForTestId(activeTestId, surveyIds));
    }
  }

  componentWillUpdate(nextProps) {
    const { dispatch, activeTestId, surveyIds } = this.props;
    if (
      activeTestId !== nextProps.activeTestId ||
      !_isEqual(surveyIds, nextProps.surveyIds)
    ) {
      dispatch(
        fetchItemsForTestId(nextProps.activeTestId, nextProps.surveyIds)
      );
    }
  }

  onPreviewItem = itemId => {
    const { dispatch } = this.props;
    dispatch(setActiveItem(itemId));
    dispatch(openItemPreviewPopup());
  };

  onPreviewItemClose = () => {
    const { dispatch } = this.props;
    dispatch(closeItemPreviewPopup());
  };

  render() {
    const {
      itemGroups,
      previewItem,
      previewItemPopupOpen,
      surveyIds
    } = this.props;
    return (
      <div>
        {itemGroups.map(itemGroup => (
          <ItemGroup
            key={itemGroup.get('id')}
            items={itemGroup.get('items')}
            randomized={itemGroup.get('is_randomized')}
            jumpable={itemGroup.get('is_jumpable')}
            needsToBeCorrect={itemGroup.get('needs_to_be_correct')}
            onPreviewItem={this.onPreviewItem}
          />
        ))}
        {previewItem.get('id') != undefined && (
          <ItemPreviewPopup
            item={previewItem}
            surveyIds={surveyIds}
            id={previewItem.get('id')}
            open={previewItemPopupOpen}
            onClose={this.onPreviewItemClose}
          />
        )}
      </div>
    );
  }
}

const mapStateToProps = (state, props) =>
  createStructuredSelector({
    activeTestId: activeSelector,
    previewItem: makeActiveItemSelector(),
    previewItemPopupOpen: itemPreviewPopupOpenSelector,
    itemGroups: makeActiveTestItemGroupsSelector()
  });

function mapDispatchToProps(dispatch) {
  return {
    dispatch
  };
}

export const StyledItemGroupList = withStyles(styles)(ItemGroupList);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(StyledItemGroupList);
