/*
 *
 * TestCreate
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import AddIcon from '@material-ui/icons/Add';
import TextField from '@material-ui/core/TextField';
import Paper from '@material-ui/core/Paper';
import SendIcon from '@material-ui/icons/Send';
import SaveIcon from '@material-ui/icons/Save';
import { DragDropContext } from 'react-beautiful-dnd';
import { Map, fromJS, List } from 'immutable';

import { setActive as setActiveFormat } from '../actions/formats';
import {
  dataSelector as formatDataSelector,
  makeActiveFormatSelector
} from '../selectors/formats';
import {
  fetchItemsForFormat,
  setActive as setActiveItem,
  openItemPreviewPopup,
  closeItemPreviewPopup,
  fetchItemsForTestId,
  setFilter as setFilterItems
} from '../actions/items';
import {
  makeActiveItemSelector,
  itemPreviewPopupOpenSelector,
  makeItemsActiveFormatFilterSelector,
  filterSelector
} from '../selectors/items';
import {
  setEditData,
  putTest,
  postTest,
  fetchEditData
} from '../actions/tests';
import {
  makeEditStatusSelector,
  makeEditItemGroupSelector,
  editSelector
} from '../selectors/tests';

import AppBarLayout from '../../common/components/AppBarLayout';
import AppBar from '../../common/containers/AppBar';
import ItemCompactCardListDrop from '../components/ItemCompactCardListDrop';
import FormatsList from '../components/FormatsList';
import ItemGroupsList from '../components/ItemGroupsDnDList';
import ItemPreviewPopup from './ItemInfoPopup';
import PureTextField from '../../common/components/PureTextField';

const styles = theme => ({
  root: {
    overflow: 'auto',
    padding: theme.spacing.unit
  },
  sticky: {
    marginBottom: theme.spacing.unit * 2
  },
  form: {
    padding: theme.spacing.unit
  },
  addItemGroup: {
    margin: theme.spacing.unit
  }
});

export class TestCreate extends React.Component {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    formats: PropTypes.instanceOf(List).isRequired,
    activeFormat: PropTypes.instanceOf(Map),
    items: PropTypes.instanceOf(List),
    data: PropTypes.instanceOf(Map).isRequired,
    previewItem: PropTypes.instanceOf(Map)
  };

  static defaultProps = {
    items: new List([]),
    activeFormat: new Map({}),
    previewItem: new Map({})
  };

  componentDidMount() {
    const { dispatch, match } = this.props;
    if (match.params.testId) {
      dispatch(fetchItemsForTestId(match.params.testId));
      dispatch(fetchEditData(match.params.testId));
    }
  }

  onFormatChange = type => {
    const { dispatch } = this.props;
    dispatch(setActiveFormat(type));
    dispatch(fetchItemsForFormat(type));
  };

  onAddItemGroup = () => {
    const { dispatch, data } = this.props;
    dispatch(
      setEditData(
        data.update(
          'item_groups',
          itemGroups =>
            itemGroups
              ? itemGroups.push(
                  fromJS({
                    is_randomized: false,
                    is_jumpable: false,
                    needs_to_be_correct: false,
                    items: []
                  })
                )
              : fromJS([
                  {
                    is_randomized: false,
                    is_jumpable: false,
                    needs_to_be_correct: false,
                    items: []
                  }
                ])
        )
      )
    );
  };

  onItemDragEnd = result => {
    const { dispatch, data, items } = this.props;
    const sourceId = result.source.index;
    const destId = result.destination.index;
    let itemGroupId;
    let nextData = data;

    if (result.destination.droppableId === 'items') {
      if (result.source.droppableId === 'items') {
        return;
      }
      itemGroupId = result.source.droppableId;
      nextData = nextData.deleteIn([
        'item_groups',
        itemGroupId,
        'items',
        sourceId
      ]);
      dispatch(setEditData(nextData));
    } else {
      let source;
      if (result.source.droppableId === 'items') {
        source = items.getIn([sourceId, 'id']);
      } else {
        itemGroupId = result.source.droppableId;
        source = data.getIn(['item_groups', itemGroupId, 'items', sourceId]);
        nextData = nextData.deleteIn([
          'item_groups',
          itemGroupId,
          'items',
          sourceId
        ]);
      }
      itemGroupId = result.destination.droppableId;
      nextData = nextData.updateIn(
        ['item_groups', itemGroupId, 'items'],
        items => items.insert(destId, source)
      );
      dispatch(setEditData(nextData));
    }
  };
  onItemGroupDragEnd = result => {
    const { dispatch, data } = this.props;
    const destId = result.destination.index;
    const sourceId = result.source.index;
    const source = data.getIn(['item_groups', sourceId]);
    const nextData = data
      .deleteIn(['item_groups', sourceId])
      .update('item_groups', itemGroups => itemGroups.insert(destId, source));
    dispatch(setEditData(nextData));
  };

  onDragEnd = result => {
    console.log(result);
    if (result.destination === null) return;
    switch (result.type) {
      case 'ITEM':
        this.onItemDragEnd(result);
        break;
      case 'ITEM_GROUP':
        this.onItemGroupDragEnd(result);
        break;
      default:
        return;
    }
  };

  onItemGroupDelete = id => {
    const { dispatch, data } = this.props;
    dispatch(setEditData(data.deleteIn(['item_groups', id])));
  };

  onItemGroupRandomized = id => {
    const { dispatch, data } = this.props;
    dispatch(
      setEditData(
        data.updateIn(['item_groups', id], itemGroup =>
          itemGroup.set('is_randomized', !itemGroup.get('is_randomized'))
        )
      )
    );
  };

  onItemGroupJumpable = id => {
    const { dispatch, data } = this.props;
    dispatch(
      setEditData(
        data.updateIn(['item_groups', id], itemGroup =>
          itemGroup.set('is_jumpable', !itemGroup.get('is_jumpable'))
        )
      )
    );
  };

  onItemGroupNeedsToBeCorrect = id => {
    const { dispatch, data } = this.props;
    dispatch(
      setEditData(
        data.updateIn(['item_groups', id], itemGroup =>
          itemGroup.set(
            'needs_to_be_correct',
            !itemGroup.get('needs_to_be_correct')
          )
        )
      )
    );
  };

  onItemDeleteFromItemGroup = (itemGroupId, itemId) => {
    const { dispatch, data } = this.props;
    dispatch(
      setEditData(data.deleteIn(['item_groups', itemGroupId, 'items', itemId]))
    );
  };

  onPreviewItem = itemId => {
    const { dispatch } = this.props;
    dispatch(setActiveItem(itemId));
    dispatch(openItemPreviewPopup());
  };

  onPreviewItemClose = () => {
    const { dispatch } = this.props;
    dispatch(closeItemPreviewPopup());
  };

  onNameChange = event => {
    const { dispatch, data } = this.props;
    dispatch(setEditData(data.set('name', event.target.value)));
  };

  onSave = () => {
    const { data, dispatch } = this.props;
    if (data.get('id')) {
      dispatch(putTest({ ...data.toJS() }));
    } else {
      dispatch(postTest({ ...data.toJS() }));
    }
  };

  onItemsFilter = e => {
    const filter = e.currentTarget.value;
    this.props.dispatch(setFilterItems(filter));
  };

  render() {
    const {
      classes,
      formats,
      activeFormat,
      items,
      itemsFilter,
      itemGroups,
      previewItem,
      previewItemPopupOpen,
      data,
      status,
      match
    } = this.props;
    if (match.params.itemId && status.get('fetching'))
      return <div>loading</div>;
    return (
      <AppBarLayout
        appBar={
          <AppBar
            title={
              data.get('id') ? `Edit Test ${data.get('id')}` : 'Create Test'
            }
            fab={
              data.get('id') ? (
                <Button variant="fab" color="secondary" onClick={this.onSave}>
                  <SendIcon />
                </Button>
              ) : (
                <Button variant="fab" color="secondary" onClick={this.onSave}>
                  <SaveIcon />
                </Button>
              )
            }
          />
        }
      >
        <div className={classes.root}>
          <DragDropContext onDragEnd={this.onDragEnd}>
            <Paper className={classes.sticky}>
              <FormatsList
                active={activeFormat.toJS()}
                onChange={this.onFormatChange}
                formats={formats.toJS()}
              />
              <PureTextField
                label="Filter"
                value={itemsFilter}
                margin="normal"
                onChange={this.onItemsFilter}
              />
              <ItemCompactCardListDrop
                id="items"
                items={items}
                prefix={0}
                onPreviewItem={this.onPreviewItem}
              />
            </Paper>
            <Paper className={classes.form}>
              <TextField
                onChange={this.onNameChange}
                label="Name"
                margin="normal"
                value={data.get('name')}
              />
              <ItemGroupsList
                id="itemGroups"
                itemGroups={itemGroups}
                onDelete={this.onItemGroupDelete}
                onJumpable={this.onItemGroupJumpable}
                onRandomized={this.onItemGroupRandomized}
                onNeedsToBeCorrect={this.onItemGroupNeedsToBeCorrect}
                onDeleteItem={this.onItemDeleteFromItemGroup}
                onPreviewItem={this.onPreviewItem}
              />
              <Button
                variant="contained"
                onClick={this.onAddItemGroup}
                className={classes.addItemGroup}
              >
                Add Item Group <AddIcon />
              </Button>
            </Paper>
          </DragDropContext>
        </div>
        <ItemPreviewPopup
          item={previewItem}
          id={previewItem.get('id')}
          open={previewItemPopupOpen}
          onClose={this.onPreviewItemClose}
        />
      </AppBarLayout>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  formats: formatDataSelector,
  activeFormat: makeActiveFormatSelector(),
  items: makeItemsActiveFormatFilterSelector(),
  itemsFilter: filterSelector,
  previewItem: makeActiveItemSelector(),
  previewItemPopupOpen: itemPreviewPopupOpenSelector,
  data: editSelector,
  itemGroups: makeEditItemGroupSelector(),
  status: makeEditStatusSelector()
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(styles)(TestCreate));
