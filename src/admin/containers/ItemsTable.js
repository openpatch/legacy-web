/*
 *
 * ItemsTable
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { createStructuredSelector } from 'reselect';
import compareDateAsc from 'date-fns/compare_asc';

import { toggleFavorite } from '../../common/actions/member';
import { favoriteItemsSelector } from '../../common/selectors/member';
import { deleteItem, setEditData, setActive } from '../actions/items';
import { makeActiveItemSelector } from '../selectors/items';

import FavoriteGridTable from '../components/FavoriteGridTable';
import { List } from 'immutable';

export class ItemsTable extends React.Component {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    items: PropTypes.instanceOf(List),
    columns: PropTypes.array
  };

  static defaultProps = {
    items: new List([]),
    columns: [
      {
        name: 'name',
        title: 'Name',
        type: 'link',
        getLink: row => `/admin/items/${row.id}`
      },
      {
        name: 'member',
        title: 'Author',
        getCellValue: row => (row.member ? row.member.username : undefined)
      },
      {
        name: 'created_on',
        title: 'Created On',
        type: 'date'
      },
      {
        name: 'count_results',
        title: '#Results',
        getCellValue: row =>
          row.stats && row.stats.num_results ? row.stats.num_results : 0
      },
      {
        name: 'count_tests',
        title: '#Tests',
        getCellValue: row =>
          row.stats && row.stats.num_tests ? row.stats.num_tests : 0
      },
      {
        name: 'time_mean',
        title: '⏱ (in s)',
        getCellValue: row =>
          row.stats && row.stats.time_mean ? Math.round(row.stats.time_mean) : 0
      }
    ],
    integratedSortingExtensions: [
      {
        columnName: 'starts_on',
        compare: compareDateAsc
      },
      {
        columnName: 'ends_on',
        compare: compareDateAsc
      },
      {
        columnName: 'created_on',
        compare: compareDateAsc
      }
    ],
    columnExtensions: [
      { columnName: 'member', width: 200 },
      { columnName: 'created_on', width: 200 },
      { columnName: 'item_amount', width: 100 },
      { columnName: 'count_results', width: 100 },
      { columnName: 'count_tests', width: 100 },
      { columnName: 'avg_duration', width: 100 }
    ]
  };

  onToggleFavorite = id => {
    this.props.dispatch(toggleFavorite('items', id));
  };

  onDeleteItem = () => {
    this.props.dispatch(deleteItem(this.props.activeItem.get('id')));
  };

  onCopyItem = () => {
    const { dispatch, activeItem } = this.props;
    dispatch(
      setEditData({
        content: activeItem.get('content'),
        solution: activeItem.get('solution'),
        name: activeItem.get('name'),
        assignment: activeItem.get('assignment'),
        responses: activeItem.get('responses'),
        allow_note: activeItem.get('allow_note')
      })
    );
    dispatch(
      push(`/admin/formats/${activeItem.getIn(['format', 'type'])}/create`)
    );
  };

  onEditItem = formatType => {
    const { dispatch, activeItem } = this.props;
    dispatch(
      push(
        `/admin/formats/${activeItem.getIn([
          'format',
          'type'
        ])}/${activeItem.get('id')}/edit`
      )
    );
  };

  onViewItem = () => {
    const { dispatch, activeItem } = this.props;
    dispatch(push(`/admin/items/${activeItem.get('id')}`));
  };

  onOpenItemMenu = id => {
    this.props.dispatch(setActive(id));
  };

  onCloseItemMenu = () => {
    this.props.dispatch(setActive(-1));
  };

  render() {
    const {
      favorites,
      columns,
      columnExtensions,
      integratedSortingExtensions,
      items,
      activeItem
    } = this.props;
    const isDeletable = activeItem.get('is_deletable');
    const isEditable = activeItem.get('is_editable');
    return (
      <FavoriteGridTable
        rows={items.toJS()}
        columns={columns}
        columnExtensions={columnExtensions}
        favorites={favorites.toJS()}
        onToggleFavorite={this.onToggleFavorite}
        onViewEntry={this.onViewItem}
        integratedSortingExtensions={integratedSortingExtensions}
        onDeleteEntry={isDeletable ? this.onDeleteItem : undefined}
        onEditEntry={isEditable ? this.onEditItem : undefined}
        onCopyEntry={this.onCopyItem}
        onOpenMenu={this.onOpenItemMenu}
        onCloseMenu={this.onCloseItemMenu}
      />
    );
  }
}

const mapStateToProps = createStructuredSelector({
  favorites: favoriteItemsSelector,
  activeItem: makeActiveItemSelector()
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ItemsTable);
