/*
 *
 * SurveyCreate
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Map, List } from 'immutable';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import FormHelperText from '@material-ui/core/FormHelperText';
import SendIcon from '@material-ui/icons/Send';
import SaveIcon from '@material-ui/icons/Save';
import CloseIcon from '@material-ui/icons/Close';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Typography from '@material-ui/core/Typography/Typography';
import format from 'date-fns/format';

import { makeDataSelector as makeTestDataSelector } from '../selectors/tests';
import {
  postSurvey,
  closeSurveyCreateDialog,
  setEditData,
  putSurvey,
  resetEditData,
  fetchSurvey
} from '../actions/surveys';
import { fetchTests } from '../actions/tests';
import {
  makeCreateDialogOpenSelector,
  makeEditStatusSelector,
  editDataSelector
} from '../selectors/surveys';

import MarkdownEditor from '../components/MarkdownEditor';
import DatePickers from '../components/DatePickers';
import IntegrationAutoSuggest from '../components/IntegrationAutoSuggest';
import LabeledSwitch from '../../common/components/LabeledSwitch';
import PureTextField from '../../common/components/PureTextField';

const styles = theme => ({
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: '45%'
  },
  actionButtonIcon: {
    marginLeft: theme.spacing.unit
  },
  previewName: {
    display: 'inline',
    verticalAlign: 'text-top'
  }
});

export class SurveyCreate extends React.Component {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    data: PropTypes.instanceOf(Map).isRequired,
    classes: PropTypes.object.isRequired,
    open: PropTypes.bool,
    status: PropTypes.instanceOf(Map).isRequired,
    tests: PropTypes.instanceOf(List).isRequired
  };

  static defaultProps = {
    open: false
  };

  componentDidMount() {
    this.props.dispatch(fetchTests());
  }

  componentDidUpdate(prevProps) {
    if (!this.props.open && prevProps.open) {
      // wait for animation to be over. Default exit duration of a exit dialog
      // is 195.
      // See: https://github.com/mui-org/material-ui/blob/ddde3c93235f170d2781749a8907005fc12372eb/src/styles/transitions.js#L33
      setTimeout(() => this.props.dispatch(resetEditData()), 300);
    }
  }

  onNameChange = e => {
    this.props.dispatch(
      setEditData(this.props.data.set('name', e.currentTarget.value))
    );
  };

  onCityChange = e => {
    this.props.dispatch(
      setEditData(this.props.data.set('city', e.currentTarget.value))
    );
  };

  onEndDateChange = e => {
    this.props.dispatch(
      setEditData(this.props.data.set('ends_on', e.currentTarget.value))
    );
  };

  onStartDateChange = e => {
    this.props.dispatch(
      setEditData(this.props.data.set('starts_on', e.currentTarget.value))
    );
  };

  onTermChange = e => {
    this.props.dispatch(
      setEditData(this.props.data.set('term', e.currentTarget.value))
    );
  };

  onTestChange = (e, { suggestion }) => {
    this.props.dispatch(
      setEditData(this.props.data.set('test_id', suggestion.id))
    );
  };

  onPasswordChange = e => {
    this.props.dispatch(
      setEditData(this.props.data.set('password', e.currentTarget.value))
    );
  };

  onPasswordRepeatChange = e => {
    this.props.dispatch(
      setEditData(this.props.data.set('password_repeat', e.currentTarget.value))
    );
  };

  onGreetingChange = value => {
    this.props.dispatch(setEditData(this.props.data.set('greeting', value)));
  };

  onFarewellChange = value => {
    this.props.dispatch(setEditData(this.props.data.set('farewell', value)));
  };

  onToggleShowSessionId = () => {
    this.props.dispatch(
      setEditData(
        this.props.data.set(
          'show_session_id',
          !this.props.data.get('show_session_id')
        )
      )
    );
  };

  onToggleShowProgress = () => {
    this.props.dispatch(
      setEditData(
        this.props.data.set(
          'show_progress',
          !this.props.data.get('show_progress')
        )
      )
    );
  };

  onToggleShowResult = () => {
    this.props.dispatch(
      setEditData(
        this.props.data.set('show_result', !this.props.data.get('show_result'))
      )
    );
  };

  onClose = () => {
    const { dispatch, data } = this.props;
    dispatch(closeSurveyCreateDialog());
    if (data.get('id')) {
      dispatch(fetchSurvey(data.get('id')));
    }
  };

  onTogglePasswordProtection = () => {
    this.props.dispatch(
      setEditData(
        this.props.data.set(
          'password_protection',
          !this.props.data.get('password_protection')
        )
      )
    );
  };

  onToggleGenerateName = () => {
    this.props.dispatch(
      setEditData(
        this.props.data.set(
          'generate_name',
          !this.props.data.get('generate_name')
        )
      )
    );
  };

  onSave = () => {
    const { dispatch, data } = this.props;
    if (data.get('name') !== '' || data.get('generate_name')) {
      let cleanedData = data;
      if (cleanedData.get('password') === '') {
        cleanedData = cleanedData.delete('password');
      }
      if (!cleanedData.get('password_protection')) {
        cleanedData = cleanedData.delete('password');
      }
      if (cleanedData.get('starts_on', '') !== '') {
        cleanedData = cleanedData.set(
          'starts_on',
          format(data.get('starts_on'), 'YYYY-MM-DD')
        );
      } else {
        cleanedData = cleanedData.delete('starts_on');
      }
      if (cleanedData.get('ends_on', '') !== '') {
        cleanedData = cleanedData.set(
          'ends_on',
          format(data.get('ends_on'), 'YYYY-MM-DD')
        );
      } else {
        cleanedData = cleanedData.delete('ends_on');
      }
      if (cleanedData.get('id')) {
        dispatch(putSurvey({ ...cleanedData.toJS() }));
      } else {
        dispatch(postSurvey({ ...cleanedData.toJS() }));
      }
    }
  };

  render() {
    const { status, data, tests, open, classes } = this.props;
    if (data.get('id') && status.fetching) return <div>loading</div>;
    return (
      <Dialog
        open={open}
        maxWidth="md"
        onClose={this.onClose}
        aria-labelledby="survey-create-title"
      >
        <DialogTitle id="survey-create-title">
          {data.get('id') ? `Edit Survey ${data.get('id')}` : 'Create Survey'}
        </DialogTitle>
        <DialogContent>
          <LabeledSwitch
            onChange={this.onToggleGenerateName}
            label="Generate Name"
            checked={data.get('generate_name')}
          />
          {!data.get('generate_name') ? (
            <PureTextField
              value={data.get('name', '')}
              required
              className={classes.textField}
              style={{ width: '100%' }}
              error={data.get('name') === ''}
              id="name"
              label="Name"
              margin="normal"
              onChange={this.onNameChange}
            />
          ) : (
            <FormHelperText className={classes.previewName}>
              {[data.get('city'), data.get('term')].join('_')}
            </FormHelperText>
          )}
          <br />
          <PureTextField
            value={data.get('city', '')}
            className={classes.textField}
            id="city"
            label="City"
            margin="normal"
            onChange={this.onCityChange}
          />
          <PureTextField
            value={data.get('term', '')}
            className={classes.textField}
            id="term"
            label="Term"
            margin="normal"
            onChange={this.onTermChange}
          />
          <DatePickers
            id="starts_on"
            label="starts on"
            className={classes.textField}
            value={
              data.get('starts_on')
                ? format(data.get('starts_on'), 'YYYY-MM-DD')
                : ''
            }
            margin="normal"
            onChange={this.onStartDateChange}
          />
          <DatePickers
            id="ends_on"
            label="ends on"
            className={classes.textField}
            value={
              data.get('ends_on')
                ? format(data.get('ends_on'), 'YYYY-MM-DD')
                : ''
            }
            margin="normal"
            onChange={this.onEndDateChange}
          />
          <LabeledSwitch
            onChange={this.onToggleShowProgress}
            checked={data.get('show_progress')}
            label="Show Progress"
          />
          <LabeledSwitch
            onChange={this.onToggleShowSessionId}
            checked={data.get('show_session_id')}
            label="Show Session Id"
          />
          <LabeledSwitch
            onChange={this.onToggleShowResult}
            checked={data.get('show_result')}
            label="Show Result"
          />
          <LabeledSwitch
            onChange={this.onTogglePasswordProtection}
            checked={data.get('password_protection')}
            label="Enable Password Protection"
          />
          <br />
          {data.get('password_protection') && (
            <React.Fragment>
              <PureTextField
                value={data.get('password', '')}
                className={classes.textField}
                error={data.get('password') !== data.get('password_repeat')}
                id="password"
                type="password"
                label="Password (optional)"
                margin="normal"
                onChange={this.onPasswordChange}
              />
              <PureTextField
                value={data.get('password_repeat', '')}
                className={classes.textField}
                id="password_check"
                type="password"
                label="Repeat Password"
                margin="normal"
                onChange={this.onPasswordRepeatChange}
              />
            </React.Fragment>
          )}
          <IntegrationAutoSuggest
            suggestions={tests
              .map(test => ({
                label: test.get('name'),
                id: test.get('id')
              }))
              .toJS()}
            className={classes.textField}
            alwaysRenderSuggestions={true}
            value={data.get('test_id')}
            onSuggestionSelected={this.onTestChange}
          />
          <Typography>Greeting Message</Typography>
          <MarkdownEditor
            value={data.get('greeting')}
            onChange={this.onGreetingChange}
          />
          <Typography>Farewell Message</Typography>
          <MarkdownEditor
            value={data.get('farewell')}
            onChange={this.onFarewellChange}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={this.onClose}>
            Abort
            <CloseIcon className={classes.actionButtonIcon} />
          </Button>
          <Button onClick={this.onSave}>
            {data.get('id') ? (
              <React.Fragment>
                Update
                <SendIcon className={classes.actionButtonIcon} />
              </React.Fragment>
            ) : (
              <React.Fragment>
                Save
                <SaveIcon className={classes.actionButtonIcon} />
              </React.Fragment>
            )}
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  tests: makeTestDataSelector(),
  data: editDataSelector,
  status: makeEditStatusSelector(),
  open: makeCreateDialogOpenSelector()
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(styles)(SurveyCreate));
