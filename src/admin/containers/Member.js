/*
 *
 * Member
 * */

import React from 'react';
import PropTypes from 'prop-types';
import { Map } from 'immutable';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import IconButton from '@material-ui/core/IconButton';
import EditIcon from '@material-ui/icons/Edit';

import MemberInfo from '../components/MemberInfo';
import MemberCreate from './MemberCreate';
import AppBarLayout from '../../common/components/AppBarLayout';
import AppBar from '../../common/containers/AppBar';

import {
  setActive,
  openMemberCreateDialog,
  fetchMember,
  setEditData
} from '../actions/members';
import { makeActiveMemberSelector } from '../selectors/members';
import { makeMemberSelector } from '../../common/selectors/member';

export class Member extends React.Component {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    member: PropTypes.instanceOf(Map),
    me: PropTypes.instanceOf(Map)
  };

  componentDidMount() {
    const { match, dispatch, me } = this.props;
    let id = me.get('id');
    if (match.params.member) {
      id = match.params.member;
    }
    dispatch(fetchMember(id));
    dispatch(setActive(id));
  }

  onEdit = () => {
    const { dispatch, member } = this.props;
    dispatch(setEditData(member));
    dispatch(openMemberCreateDialog());
  };

  render() {
    const { member, match } = this.props;
    return (
      <AppBarLayout
        appBar={
          <AppBar title={`Member ${member.get('username', '')}`}>
            <IconButton color="inherit" onClick={this.onEdit}>
              <EditIcon />
            </IconButton>
          </AppBar>
        }
      >
        <MemberCreate />
        <MemberInfo member={member} />
      </AppBarLayout>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  me: makeMemberSelector(),
  member: makeActiveMemberSelector()
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Member);
