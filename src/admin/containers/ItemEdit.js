/*
 *
 * ItemEdit
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Prompt } from 'react-router-dom';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { Map, fromJS } from 'immutable';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Button from '@material-ui/core/Button';
import SaveIcon from '@material-ui/icons/Save';
import SendIcon from '@material-ui/icons/Send';

import commonMessages from '../messages/common';

import {
  setEditData,
  fetchEditData,
  postItem,
  putItem,
  resetEditData,
  evaluateItem
} from '../actions/items';
import { makeEditStatusSelector, editSelector } from '../selectors/items';
import { setActive as setActiveFormat } from '../actions/formats';
import { makeActiveFormatSelector } from '../selectors/formats';

import AppBarLayout from '../../common/components/AppBarLayout';
import AppBar from '../../common/containers/AppBar';
import ItemForm from '../components/ItemForm';
import ItemPreview from '../components/ItemPreview';
import ItemSource from '../components/ItemSource';
import SaveButton from '../../common/components/SaveButton';

export class ItemEdit extends React.Component {
  state = {
    tab: 0
  };

  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    match: PropTypes.object.isRequired,
    status: PropTypes.instanceOf(Map).isRequired,
    format: PropTypes.instanceOf(Map).isRequired,
    data: PropTypes.instanceOf(Map).isRequired
  };
  componentDidMount() {
    const { dispatch, match } = this.props;
    dispatch(setActiveFormat(match.params.format));
    if (match.params.itemId) {
      dispatch(fetchEditData(match.params.itemId));
    }
    //this.props.router.setRouteLeaveHook(this.props.route, () => {
    //  if (data.get('unsaved')) {
    //    return 'Unsaved information';
    //  }
    //});
  }

  componentWillUnmount() {
    const { dispatch } = this.props;
    dispatch(resetEditData());
  }

  onTabChange = (e, tab) => {
    this.setState({
      tab
    });
  };
  onFormChange = (keys, value) => {
    // TODO warn that the solution should be reset. Show Popup to reset solution
    this.props.dispatch(setEditData(this.props.data.setIn(keys, value)));
  };
  onSave = () => {
    const { data, dispatch, format } = this.props;
    if (data.get('id')) {
      dispatch(
        putItem({
          ...data.toJS(),
          format: format.get('type')
        })
      );
    } else {
      dispatch(
        postItem({
          ...data.toJS(),
          format: format.get('type')
        })
      );
    }
  };
  onSolutionEvaluate = solution => {
    const { data, dispatch, format } = this.props;
    dispatch(
      evaluateItem(
        {
          ...data.toJS(),
          format: format.get('type')
        },
        solution
      )
    );
  };
  onSolutionSubmit = solution => {
    this.props.dispatch(
      setEditData(this.props.data.set('solution', fromJS(solution)))
    );
  };
  render() {
    const { match, format, data, status } = this.props;
    const { tab } = this.state;
    if (match.params.itemId && status.get('fetching'))
      return <div>loading</div>;

    return (
      <AppBarLayout
        appBar={
          <AppBar
            title={
              data.get('id')
                ? `${data.get('name')}`
                : `Create ${format.get('type')} Item`
            }
            bg={`#${format.get('color_hex')}`}
            tabs={
              <Tabs value={tab} onChange={this.onTabChange}>
                <Tab label={<FormattedMessage {...commonMessages.form} />} />
                <Tab label={<FormattedMessage {...commonMessages.source} />} />
                <Tab label={<FormattedMessage {...commonMessages.preview} />} />
              </Tabs>
            }
            fab={
              data.get('id') ? (
                <SaveButton
                  fab={true}
                  status={status.get('unsaved') ? null : 'saved'}
                  onClick={this.onSave}
                />
              ) : (
                <SaveButton fab={true} status="new" onClick={this.onSave} />
              )
            }
          />
        }
      >
        <div style={{ display: tab === 0 ? 'block' : 'none' }}>
          <Prompt
            when={status.get('unsaved')}
            message={`There a some unsaved changes. If you continue these will be discarded.`}
          />
          <ItemForm
            format={format.get('type')}
            onChange={this.onFormChange}
            name={data.get('name', '')}
            allowNote={data.get('allow_note', false)}
            assignment={data.get('assignment', '')}
            responses={data.get('responses', new Map({}))}
            content={data.get('content', new Map({}))}
          />
        </div>
        {tab === 1 && (
          <ItemSource
            source={{
              content: data.get('content').toJS(),
              solution: data.get('solution').toJS(),
              name: data.get('name'),
              assignment: data.get('assignment'),
              responses: data.get('responses', new Map({})).toJS()
            }}
          />
        )}
        {tab === 2 && (
          <ItemPreview
            format={match.params.format}
            item={data.setIn(['format', 'type'], match.params.format)}
            onStateSubmit={this.onSolutionSubmit}
            onStateEvaluate={this.onSolutionEvaluate}
            solution={data.get('solution', new Map({})).toJS()}
          />
        )}
      </AppBarLayout>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  data: editSelector,
  status: makeEditStatusSelector(),
  format: makeActiveFormatSelector()
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ItemEdit);
