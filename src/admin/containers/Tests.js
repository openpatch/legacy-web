/*
 *
 * Tests
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { List } from 'immutable';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { createStructuredSelector } from 'reselect';
import compareDateAsc from 'date-fns/compare_asc';

import { makeDataSelector, makeActiveTestSelector } from '../selectors/tests';
import {
  fetchTests,
  setActive,
  deleteTest,
  setEditData,
  resetEditData
} from '../actions/tests';
import {
  setEditDataTestId as setSurveyEditDataTestId,
  openSurveyCreateDialog
} from '../actions/surveys';
import { fetchItemsForTestId } from '../actions/items';
import { favoriteTestsSelector } from '../../common/selectors/member';
import { toggleFavorite } from '../../common/actions/member';

import AppBarLayout from '../../common/components/AppBarLayout';
import AppBar from '../../common/containers/AppBar';
import AddButton from '../../common/components/AddButton';
import FavoriteGridTable from '../components/FavoriteGridTable';
import SurveyCreate from './SurveyCreate';
import { Survey as SurveyIcon } from '../../common/components/Icons';

export class Tests extends React.Component {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    data: PropTypes.instanceOf(List).isRequired,
    columns: PropTypes.array,
    columnExtensions: PropTypes.array,
    pageSizes: PropTypes.array
  };

  static defaultProps = {
    columns: [
      {
        name: 'member',
        title: 'Author',
        getCellValue: row => (row.member ? row.member.username : undefined)
      },
      {
        name: 'name',
        title: 'Name',
        type: 'link',
        getLink: row => `/admin/tests/${row.id}`
      },
      {
        name: 'created_on',
        title: 'Created On',
        type: 'date'
      },
      {
        name: 'item_amount',
        title: '#Items'
      },
      {
        name: 'count_results',
        title: '#Results',
        getCellValue: row =>
          row.stats && row.stats.num_results ? row.stats.num_results : 0
      },
      {
        name: 'count_aborted',
        title: '#Aborted',
        getCellValue: row => row.stats.num_aborted || 0
      },
      {
        name: 'count_unfinished',
        title: '#Unfinished',
        getCellValue: row => row.stats.num_unfinished || 0
      },
      {
        name: 'count_surveys',
        title: '#Surveys',
        getCellValue: row =>
          row.stats && row.stats.num_surveys ? row.stats.num_surveys : 0
      }
    ],
    integratedSortingExtensions: [
      {
        columnName: 'starts_on',
        compare: compareDateAsc
      },
      {
        columnName: 'ends_on',
        compare: compareDateAsc
      },
      {
        columnName: 'created_on',
        compare: compareDateAsc
      }
    ],
    columnExtensions: [
      { columnName: 'member', width: 200 },
      { columnName: 'created_on', width: 200 },
      { columnName: 'item_amount', width: 100 },
      { columnName: 'count_results', width: 100 },
      { columnName: 'count_aborted', width: 100 },
      { columnName: 'count_unfinished', width: 100 },
      { columnName: 'count_surveys', width: 100 }
    ],
    additionalMenuItems: []
  };

  state = {
    testMenuOpen: false
  };

  componentDidMount() {
    this.props.dispatch(fetchTests());
  }

  onToggleFavorite = id => {
    this.props.dispatch(toggleFavorite('tests', id));
  };

  onView = () => {
    this.props.dispatch(
      push(`/admin/tests/${this.props.activeTest.get('id')}`)
    );
  };

  onEdit = () => {
    this.props.dispatch(
      push(`/admin/tests/${this.props.activeTest.get('id')}/edit`)
    );
  };

  onSurvey = () => {
    this.props.dispatch(
      setSurveyEditDataTestId(this.props.activeTest.get('id'))
    );
    this.props.dispatch(openSurveyCreateDialog());
  };

  onCopy = () => {
    this.props.dispatch(
      setEditData({
        item_groups: this.props.activeTest.get('item_groups'),
        name: this.props.activeTest.get('name')
      })
    );
    this.props.dispatch(fetchItemsForTestId(this.props.activeTest.get('id')));
    this.props.dispatch(push(`/admin/tests/create`));
  };

  onDelete = () => {
    this.props.dispatch(deleteTest(this.props.activeTest.get('id')));
  };

  onOpenTestMenu = id => {
    this.props.dispatch(setActive(id));
  };

  onCloseTestMenu = () => {
    this.props.dispatch(setActive(-1));
  };

  onTestCreate = () => {
    const { match, dispatch } = this.props;
    dispatch(resetEditData());
    dispatch(push(`${match.url}/create`));
  };

  render() {
    const {
      data,
      favorites,
      columns,
      columnExtensions,
      integratedSortingExtensions,
      activeTest
    } = this.props;
    const isDeletable = activeTest.get('is_deletable');
    const isEditable = activeTest.get('is_editable');

    return (
      <AppBarLayout
        appBar={
          <AppBar title="Tests">
            <AddButton onClick={this.onTestCreate} />
          </AppBar>
        }
      >
        <SurveyCreate />
        <FavoriteGridTable
          rows={data.toJS()}
          additionalMenuItems={[
            {
              key: 'survey',
              onClick: this.onSurvey,
              Icon: SurveyIcon,
              label: 'Create Survey'
            }
          ]}
          columns={columns}
          columnExtensions={columnExtensions}
          favorites={favorites.toJS()}
          onViewEntry={this.onView}
          onCopyEntry={this.onCopy}
          onEditEntry={isEditable ? this.onEdit : undefined}
          integratedSortingExtensions={integratedSortingExtensions}
          onDeleteEntry={isDeletable ? this.onDelete : undefined}
          onToggleFavorite={this.onToggleFavorite}
          onOpenMenu={this.onOpenTestMenu}
          onCloseMenu={this.onCloseTestMenu}
        />
      </AppBarLayout>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  favorites: favoriteTestsSelector,
  data: makeDataSelector(),
  activeTest: makeActiveTestSelector()
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Tests);
