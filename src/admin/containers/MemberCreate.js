/*
 *
 * MemberCreate
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Map } from 'immutable';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import SendIcon from '@material-ui/icons/Send';
import SaveIcon from '@material-ui/icons/Save';
import CloseIcon from '@material-ui/icons/Close';
import DeleteIcon from '@material-ui/icons/Delete';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';

import PureTextField from '../../common/components/PureTextField';
import LabeledSwitch from '../../common/components/LabeledSwitch';

import {
  makeCreateDialogOpenSelector,
  makeEditStatusSelector,
  editSelector
} from '../selectors/members';
import { roleSelector } from '../../common/selectors/member';
import {
  fetchEditData,
  setEditData,
  setActive,
  fetchMember,
  postMember,
  deleteMember,
  putMember,
  resetEditData,
  closeMemberCreateDialog
} from '../actions/members';

const styles = theme => ({
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: '45%'
  },
  actionButtonIcon: {
    marginLeft: theme.spacing.unit
  },
  previewName: {
    display: 'inline',
    verticalAlign: 'text-top'
  }
});

const validateEmail = email => {
  // following simplified RFC 2822 see https://stackoverflow.com/a/1373724/4205043
  const re = /^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
  return re.test(String(email).toLowerCase());
};

const validateRole = role => {
  const roles = ['admin', 'data_anaylst', 'author'];
  return roles.includes(role);
};

export class MemberCreate extends React.Component {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    classes: PropTypes.object.isRequired,
    data: PropTypes.instanceOf(Map).isRequired,
    open: PropTypes.bool,
    status: PropTypes.object.isRequired,
    role: PropTypes.string.isRequired
  };

  static defaultProps = {
    open: false,
    role: ''
  };

  componentDidUpdate(prevProps) {
    if (!this.props.open && prevProps.open) {
      // wait for animation to be over. Default exit duration of a exit dialog
      // is 195.
      // See: https://github.com/mui-org/material-ui/blob/ddde3c93235f170d2781749a8907005fc12372eb/src/styles/transitions.js#L33
      setTimeout(() => this.props.dispatch(resetEditData()), 300);
    }
  }

  onDataChange = (key, value) => {
    const { dispatch, data } = this.props;
    dispatch(setEditData(data.set(key, value)));
  };

  onUsernameChange = e => {
    const value = e.currentTarget.value;
    this.onDataChange('username', value);
  };

  onFirstNameChange = e => {
    const value = e.currentTarget.value;
    this.onDataChange('first_name', value);
  };

  onLastNameChange = e => {
    const value = e.currentTarget.value;
    this.onDataChange('last_name', value);
  };

  onEmailChange = e => {
    const value = e.currentTarget.value;
    this.onDataChange('email', value);
  };

  onPasswordChange = e => {
    const value = e.currentTarget.value;
    this.onDataChange('password', value);
  };

  onPasswordRepeatChange = e => {
    const value = e.currentTarget.value;
    this.onDataChange('password_repeat', value);
  };

  onTogglePasswordChange = e => {
    this.props.dispatch(
      setEditData(
        this.props.data.set(
          'password_change',
          !this.props.data.get('password_change')
        )
      )
    );
  };

  onRoleChange = e => {
    const value = e.currentTarget.value;
    this.onDataChange('role', value);
  };

  onClose = () => {
    const { dispatch, data } = this.props;
    dispatch(fetchMember(data.get('id')));
    dispatch(closeMemberCreateDialog());
  };

  onSave = () => {
    const { dispatch, data } = this.props;
    let cleanedData = data;
    cleanedData = cleanedData.delete('password_repeat');
    if (!cleanedData.get('password_change') && data.get('id')) {
      cleanedData = cleanedData.delete('password');
    }
    if (cleanedData.get('id')) {
      dispatch(putMember({ ...cleanedData.toJS() }));
    } else {
      dispatch(postMember({ ...cleanedData.toJS() }));
    }
  };

  onDelete = () => {
    const { dispatch, data } = this.props;
    dispatch(deleteMember(data.get('id')));
  };

  render() {
    const { status, data, open, classes } = this.props;
    if (data.get('id') && status.fetching) return <div>loading</div>;
    return (
      <Dialog
        open={open}
        maxWidth="md"
        onClose={this.onClose}
        aria-labelledby="member-create-title"
      >
        <DialogTitle id="member-create-title">
          {data.get('id') ? `Edit Member ${data.get('id')}` : 'Create Member'}
        </DialogTitle>
        <DialogContent>
          <PureTextField
            value={data.get('username', '') || ''}
            className={classes.textField}
            id="username"
            label="Username"
            error={!data.get('username')}
            disabled={data.get('id') !== undefined}
            margin="normal"
            onChange={this.onUsernameChange}
          />
          <PureTextField
            value={data.get('first_name', '') || ''}
            className={classes.textField}
            error={!data.get('first_name')}
            id="first_name"
            label="First Name"
            margin="normal"
            onChange={this.onFirstNameChange}
          />
          <PureTextField
            value={data.get('last_name', '') || ''}
            className={classes.textField}
            error={!data.get('last_name')}
            id="last_name"
            label="Last Name"
            margin="normal"
            onChange={this.onLastNameChange}
          />
          <PureTextField
            value={data.get('email', '') || ''}
            className={classes.textField}
            id="email"
            error={!validateEmail(data.get('email'))}
            label="E-Mail"
            margin="normal"
            onChange={this.onEmailChange}
          />
          <PureTextField
            value={data.get('role', '')}
            className={classes.textField}
            id="role"
            error={!validateRole(data.get('role'))}
            label="Role"
            margin="normal"
            onChange={this.onRoleChange}
          />
          {data.get('id') !== undefined && (
            <LabeledSwitch
              onChange={this.onTogglePasswordChange}
              checked={data.get('password_change')}
              label="Change Password"
            />
          )}
          <br />
          {(data.get('id') === undefined || data.get('password_change')) && (
            <React.Fragment>
              <PureTextField
                value={data.get('password', '')}
                className={classes.textField}
                error={data.get('password') !== data.get('password_repeat')}
                id="password"
                type="password"
                label="Password"
                margin="normal"
                onChange={this.onPasswordChange}
              />
              <PureTextField
                value={data.get('password_repeat', '')}
                className={classes.textField}
                id="password_check"
                type="password"
                label="Repeat Password"
                margin="normal"
                onChange={this.onPasswordRepeatChange}
              />
            </React.Fragment>
          )}
        </DialogContent>
        <DialogActions>
          {data.get('id') !== undefined && (
            <Button onClick={this.onDelete}>
              Delete
              <DeleteIcon className={classes.actionButtonIcon} />
            </Button>
          )}
          <Button onClick={this.onClose}>
            Abort
            <CloseIcon className={classes.actionButtonIcon} />
          </Button>
          <Button onClick={this.onSave}>
            {data.get('id') !== undefined ? (
              <React.Fragment>
                Update
                <SendIcon className={classes.actionButtonIcon} />
              </React.Fragment>
            ) : (
              <React.Fragment>
                Save
                <SaveIcon className={classes.actionButtonIcon} />
              </React.Fragment>
            )}
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  data: editSelector,
  status: makeEditStatusSelector(),
  open: makeCreateDialogOpenSelector(),
  role: roleSelector
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(styles)(MemberCreate));
