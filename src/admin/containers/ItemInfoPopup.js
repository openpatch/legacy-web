/**
 *
 * ItemInfoPopup
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Map } from 'immutable';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import withMobileDialog from '@material-ui/core/withMobileDialog';
import Button from '@material-ui/core/Button';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import _isEqual from 'lodash/isEqual';

import formats from '../../formats';
import ItemPreview from '../components/ItemPreview';
import ItemStatistic from '../components/ItemStatistic';
import ItemVisualization from '../components/ItemVisualization';
import { fetchStatisticForId, setActive, evaluateItem } from '../actions/items';
import {
  makeActiveItemSelector,
  makeActiveItemStatisticSelector
} from '../selectors/items';

class ItemInfoPopup extends React.Component {
  static propTypes = {
    fullScreen: PropTypes.bool.isRequired,
    onClose: PropTypes.func.isRequired,
    open: PropTypes.bool,
    id: PropTypes.number.isRequired,
    item: PropTypes.instanceOf(Map).isRequired,
    surveyIds: PropTypes.array,
    statistic: PropTypes.instanceOf(Map).isRequired
  };

  state = {
    tab: 0,
    open: false,
    surveyIds: []
  };

  onTabChange = (event, value) => {
    this.setState({
      tab: value
    });
  };

  componentDidMount() {
    const { id, dispatch } = this.props;
    dispatch(setActive(id));
    dispatch(fetchStatisticForId(id));
  }

  componentDidUpdate(prevProps) {
    const { id, dispatch, surveyIds } = this.props;
    if (id !== prevProps.id || !_isEqual(surveyIds, prevProps.surveyIds)) {
      this.setState({
        tab: 0
      });
      dispatch(setActive(id));
      dispatch(fetchStatisticForId(id, surveyIds));
    }
  }

  onSolutionEvaluate = solution => {
    const { item, dispatch } = this.props;
    dispatch(
      evaluateItem(
        {
          ...item.toJS(),
          format: item.getIn(['format', 'type'])
        },
        solution
      )
    );
  };

  render() {
    const { open, onClose, fullScreen, item, statistic } = this.props;
    return (
      <Dialog
        open={open === true}
        fullScreen={fullScreen}
        onClose={onClose}
        maxWidth={false}
        aria-labelledby="item-info-popup-title"
      >
        {item.get('name') !== undefined && (
          <React.Fragment>
            <DialogTitle id="item-info-popup-title">
              {item.get('name')}
            </DialogTitle>
            <DialogContent>
              <Tabs
                value={this.state.tab}
                onChange={this.onTabChange}
                indicatorColor="primary"
                textColor="primary"
                centered
              >
                <Tab label="Preview" />
                <Tab label="Solution" />
                <Tab label="Statistic" />
                {'Visualization' in
                formats[item.getIn(['format', 'type'], 'common')] ? (
                  <Tab label="Visualization" />
                ) : null}
              </Tabs>
              {this.state.tab === 0 && (
                <ItemPreview
                  item={item}
                  format={item.getIn(['format', 'type'])}
                  showLog={false}
                  onStateEvaluate={this.onSolutionEvaluate}
                  showHeaders={false}
                />
              )}
              {this.state.tab === 1 && (
                <ItemPreview
                  item={item}
                  solution={item.get('solution', {})}
                  format={item.getIn(['format', 'type'])}
                  showLog={false}
                  showHeaders={false}
                />
              )}
              {this.state.tab === 2 && (
                <ItemStatistic
                  format={item.get('format').toJS()}
                  data={statistic}
                />
              )}
              {this.state.tab === 3 && (
                <ItemVisualization
                  format={item.get('format').toJS()}
                  data={statistic.get('format')}
                />
              )}
            </DialogContent>
            <DialogActions>
              <Button onClick={onClose}>Close</Button>
            </DialogActions>
          </React.Fragment>
        )}
      </Dialog>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  item: makeActiveItemSelector(),
  statistic: makeActiveItemStatisticSelector()
});

export default connect(mapStateToProps)(withMobileDialog()(ItemInfoPopup));
