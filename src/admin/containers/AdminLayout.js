import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { List as IList } from 'immutable';
import { withStyles } from '@material-ui/core/styles';
import Collapse from '@material-ui/core/Collapse';
import Divider from '@material-ui/core/Divider';
import Tooltip from '@material-ui/core/Tooltip/Tooltip';
import HomeIcon from '@material-ui/icons/Home';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import SupervisorAccountIcon from '@material-ui/icons/SupervisorAccount';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import BugReportIcon from '@material-ui/icons/BugReport';
import ExitIcon from '@material-ui/icons/ExitToApp';
import LabelIcon from '@material-ui/icons/Label';
import { injectIntl } from 'react-intl';
import _isEqual from 'lodash/isEqual';

import { roleSelector, usernameSelector } from '../../common/selectors/member';
import { dataSelector as formatDataSelector } from '../selectors/formats';
import { fetchFormats } from '../actions/formats';

import {
  Survey as SurveyIcon,
  Test as TestIcon,
  Format as FormatIcon
} from '../../common/components/Icons';
import messages from '../messages';
import commonMessages from '../../common/messages/common';
import DrawerLayout from '../../common/containers/DrawerLayout';
import { getFormatNameMessage } from '../../formats/utils';
import Avatar from '../../common/components/Avatar';
import IconButton from '@material-ui/core/IconButton/IconButton';
import { logout } from '../../common/actions/member';
import PureList, {
  PureListItem,
  PureListItemIcon,
  PureListItemText
} from '../../common/components/PureList';
import { toggleImprintPrivacyOpen } from '../../common/actions/ui';

const styles = theme => ({
  nested: {
    paddingLeft: theme.spacing.unit * 4
  },
  drawerHeader: {
    padding: 16
  },
  member: {
    padding: 0
  }
});

class AdminLayout extends React.Component {
  state = { formatsOpen: true };

  static propTypes = {
    children: PropTypes.element.isRequired,
    classes: PropTypes.object.isRequired,
    dispatch: PropTypes.func.isRequired,
    url: PropTypes.string.isRequired,
    role: PropTypes.string.isRequired,
    formats: PropTypes.instanceOf(IList),
    username: PropTypes.string.isRequired
  };

  static defaultProps = {
    formats: new IList([])
  };

  shouldComponentUpdate(nextProps, nextState) {
    if (nextState.formatOpen !== this.state.formatsOpen) {
      return true;
    }
    return !_isEqual(nextProps, this.props);
  }

  componentDidMount() {
    this.props.dispatch(fetchFormats());
  }

  toggleFormatsOpen = () => {
    this.setState({ formatsOpen: !this.state.formatsOpen });
  };

  onLogout = () => {
    this.props.dispatch(logout());
  };

  onBugReport = () => {
    window.location.href = `mailto:mike.barkmin@uni-due.de?subject=[COMMOOP] Bug Report&body=Was wollte ich machen? ... Was habe ich erwartet? ... Was ist passiert? ...`;
  };

  render() {
    const {
      classes,
      dispatch,
      role,
      username,
      url,
      formats,
      children,
      intl,
      ...props
    } = this.props;
    const drawerHeader = (
      <div className={classes.drawerHeader}>
        <PureListItem className={classes.member}>
          <Avatar name={username} role={role} />
          <PureListItemText primary={username} secondary={role} />
          <Tooltip title="logout">
            <IconButton onClick={this.onLogout} aria-label="logout">
              <ExitIcon />
            </IconButton>
          </Tooltip>
        </PureListItem>
      </div>
    );
    const drawerContent = (
      <div>
        <PureList style={{ flex: 1 }}>
          <PureListItem button component={Link} to={`${url}`}>
            <PureListItemIcon>
              <HomeIcon />
            </PureListItemIcon>
            <PureListItemText
              primary={intl.formatMessage(messages.dashboard)}
            />
          </PureListItem>
          <PureListItem button onClick={this.toggleFormatsOpen}>
            <PureListItemIcon>
              <FormatIcon />
            </PureListItemIcon>
            <PureListItemText primary={intl.formatMessage(messages.formats)} />
            {this.state.formatsOpen ? <ExpandLess /> : <ExpandMore />}
          </PureListItem>
          <Collapse in={this.state.formatsOpen} unmountOnExit>
            {formats.map((format, i) => (
              <PureListItem
                button
                key={format.get('type')}
                className={classes.nested}
                component={Link}
                to={`${url}/formats/${format.get('type')}`}
              >
                <PureListItemIcon>
                  <LabelIcon
                    style={{
                      fill: `#${format.get('color_hex')}`
                    }}
                  />
                </PureListItemIcon>
                <PureListItemText
                  inset
                  primary={intl.formatMessage(
                    getFormatNameMessage(format.get('type'))
                  )}
                />
              </PureListItem>
            ))}
          </Collapse>
          <PureListItem button component={Link} to={`${url}/tests`}>
            <PureListItemIcon>
              <TestIcon />
            </PureListItemIcon>
            <PureListItemText primary={intl.formatMessage(messages.tests)} />
          </PureListItem>
          <PureListItem button component={Link} to={`${url}/surveys`}>
            <PureListItemIcon>
              <SurveyIcon />
            </PureListItemIcon>
            <PureListItemText primary={intl.formatMessage(messages.surveys)} />
          </PureListItem>
          {role === 'admin' ? (
            <PureListItem button component={Link} to={`${url}/members`}>
              <PureListItemIcon>
                <SupervisorAccountIcon />
              </PureListItemIcon>
              <PureListItemText
                primary={intl.formatMessage(messages.members)}
              />
            </PureListItem>
          ) : null}
        </PureList>
        <Divider />
        <PureList>
          <PureListItem button component={Link} to={`${url}/members/me`}>
            <PureListItemIcon>
              <AccountCircleIcon />
            </PureListItemIcon>
            <PureListItemText
              primary={intl.formatMessage(messages.myProfile)}
            />
          </PureListItem>
          <PureListItem button onClick={this.onBugReport}>
            <PureListItemIcon>
              <BugReportIcon />
            </PureListItemIcon>
            <PureListItemText
              primary={intl.formatMessage(messages.reportABug)}
            />
          </PureListItem>
          <PureListItem
            button
            onClick={() => dispatch(toggleImprintPrivacyOpen())}
          >
            <PureListItemText
              primary={intl.formatMessage(commonMessages.imprintPrivacy)}
            />
          </PureListItem>
        </PureList>
      </div>
    );
    return (
      <DrawerLayout
        drawerHeader={drawerHeader}
        drawerContent={drawerContent}
        {...props}
      >
        {children}
      </DrawerLayout>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  role: roleSelector,
  username: usernameSelector,
  formats: formatDataSelector
});

export default connect(mapStateToProps)(
  withStyles(styles)(injectIntl(AdminLayout))
);
