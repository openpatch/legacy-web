/*
 *
 * Survey
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { List, Map } from 'immutable';
import { connect } from 'react-redux';
import { injectIntl } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { withStyles } from '@material-ui/core/styles';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import UndoIcon from '@material-ui/icons/Undo';
import Tooltip from '@material-ui/core/Tooltip';

import {
  makeActiveSurveySelector,
  makeActiveSurveyResultsSelector,
  makeActiveSurveyPermittedMembersSelector,
  surveyMembersPermittedStatusSelector
} from '../selectors/surveys';
import {
  fetchSurvey,
  setActive,
  removeMemberFromPermittedForId,
  addMemberToPermittedForId
} from '../actions/surveys';
import { fetchMembersPermittedForSurveyId } from '../actions/members';
import {
  fetchResultsForSurveyId,
  setActive as setActiveTest,
  deleteResultsForSurveyId,
  fetchTestForSurveyId
} from '../actions/tests';
import surveysMessages from '../messages/surveys';

import AppBarLayout from '../../common/components/AppBarLayout';
import AppBar from '../../common/containers/AppBar';
import MembersPermittedPopup from '../components/MembersPermittedPopup';
import ShareButton from '../../common/components/ShareButton';
import ItemGroupList from './ItemGroupList';
import ConfirmDialog from '../../common/components/ConfirmDialog';
import TestResultsTable from './TestResultsTable';
import StatisticSummaryTable, {
  generateRowData
} from '../../common/components/StatisticSummaryTable';

const styles = theme => ({});

export class Survey extends React.Component {
  static propTypes = {
    intl: PropTypes.object.isRequired,
    dispatch: PropTypes.func.isRequired,
    match: PropTypes.object.isRequired,
    survey: PropTypes.instanceOf(Map).isRequired,
    results: PropTypes.instanceOf(List)
  };

  static defaultProps = {
    results: []
  };

  state = {
    MembersPermittedPopupOpen: false
  };

  componentDidMount() {
    const id = this.props.match.params.surveyId;
    this.props.dispatch(setActive(id));
    this.props.dispatch(fetchSurvey(id));
    this.props.dispatch(fetchTestForSurveyId(id));
    this.props.dispatch(fetchResultsForSurveyId(id));
    this.props.dispatch(fetchMembersPermittedForSurveyId(id));
  }

  componentWillUpdate(nextProps) {
    if (this.props.survey !== nextProps.survey) {
      this.props.dispatch(setActiveTest(nextProps.survey.get('test_id')));
    }
  }

  onMembersPermittedPopupOpen = () => {
    this.setState({
      membersPermittedPopupOpen: true
    });
  };

  onMembersPermittedPopupClose = () => {
    this.setState({
      membersPermittedPopupOpen: false
    });
  };

  onAddMemberToPermitted = username => {
    const { dispatch, survey } = this.props;
    dispatch(addMemberToPermittedForId(survey.get('id'), username));
    dispatch(fetchMembersPermittedForSurveyId(survey.get('id')));
  };

  onRemoveMemberFromPermitted = username => {
    const { dispatch, survey } = this.props;
    dispatch(removeMemberFromPermittedForId(survey.get('id'), username));
  };

  onDeleteResults = () => {
    const { dispatch, survey } = this.props;
    dispatch(deleteResultsForSurveyId(survey.get('id')));
    this.onCloseDeleteResultsDialog();
  };

  onOpenDeleteResultsDialog = () => {
    this.setState({
      deleteResultsDialogOpen: true
    });
  };

  onCloseDeleteResultsDialog = () => {
    this.setState({
      deleteResultsDialogOpen: false
    });
  };

  render() {
    const {
      survey,
      permittedMembers,
      permittedMembersStatus,
      results,
      intl
    } = this.props;
    return (
      <AppBarLayout
        appBar={
          <AppBar title={`Survey ${survey.get('name')} `}>
            <Tooltip title="Reset results">
              <IconButton
                color="inherit"
                onClick={this.onOpenDeleteResultsDialog}
              >
                <UndoIcon />
              </IconButton>
            </Tooltip>
            <ShareButton onClick={this.onMembersPermittedPopupOpen} />
          </AppBar>
        }
      >
        <MembersPermittedPopup
          members={permittedMembers}
          open={this.state.membersPermittedPopupOpen}
          onClose={this.onMembersPermittedPopupClose}
          errorAdding={permittedMembersStatus.get('putting_failed')}
          onMemberDelete={this.onRemoveMemberFromPermitted}
          onMemberAdd={this.onAddMemberToPermitted}
        />
        <ConfirmDialog
          title={intl.formatMessage(surveysMessages.resetSurveyResultsTitle)}
          content={intl.formatMessage(
            surveysMessages.resetSurveyResultsContent
          )}
          onOk={this.onDeleteResults}
          onCancel={this.onCloseDeleteResultsDialog}
          open={this.state.deleteResultsDialogOpen}
        />
        <ExpansionPanel expanded={true}>
          <ExpansionPanelSummary>
            <Typography>Information</Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails style={{ display: 'block' }}>
            <StatisticSummaryTable
              rows={
                new Map({
                  scores: generateRowData(survey.get('stats'), 'score'),
                  'times in s': generateRowData(survey.get('stats'), 'time')
                })
              }
            />
            <br />
            <Typography>
              The statistic includes only test results which were fully
              completed.
            </Typography>
          </ExpansionPanelDetails>
        </ExpansionPanel>
        <ExpansionPanel defaultExpanded>
          <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
            <Typography>Items</Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails style={{ display: 'block' }}>
            {survey.get('id') !== undefined && (
              <ItemGroupList surveyIds={[survey.get('id', null)]} />
            )}
          </ExpansionPanelDetails>
        </ExpansionPanel>
        <ExpansionPanel expanded={true}>
          <ExpansionPanelSummary>
            <Typography>Results</Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails
            style={{
              overflowX: 'auto',
              display: 'flex',
              flexDirection: 'column'
            }}
          >
            <TestResultsTable results={results.toJS()} />
          </ExpansionPanelDetails>
        </ExpansionPanel>
      </AppBarLayout>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  survey: makeActiveSurveySelector(),
  results: makeActiveSurveyResultsSelector(),
  permittedMembers: makeActiveSurveyPermittedMembersSelector(),
  permittedMembersStatus: surveyMembersPermittedStatusSelector
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(styles)(injectIntl(Survey)));
