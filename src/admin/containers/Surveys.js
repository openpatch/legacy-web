/*
 *
 * Surveys
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { Map, List } from 'immutable';
import { createStructuredSelector } from 'reselect';
import compareDateAsc from 'date-fns/compare_asc';

import { favoriteSurveysSelector } from '../../common/selectors/member';
import { toggleFavorite } from '../../common/actions/member';
import {
  fetchSurveys,
  setActive,
  setEditData,
  deleteSurvey,
  openSurveyCreateDialog
} from '../actions/surveys';
import {
  makeDataSelector,
  makeActiveSurveySelector
} from '../selectors/surveys';

import AppBarLayout from '../../common/components/AppBarLayout';
import AppBar from '../../common/containers/AppBar';
import AddButton from '../../common/components/AddButton';
import FavoriteGridTable from '../components/FavoriteGridTable';
import SurveyCreate from './SurveyCreate';

export class Surveys extends React.Component {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    data: PropTypes.instanceOf(List).isRequired,
    match: PropTypes.object.isRequired,
    activeSurvey: PropTypes.instanceOf(Map),
    favorites: PropTypes.instanceOf(List)
  };

  static defaultProps = {
    columns: [
      {
        name: 'member_id',
        title: 'Author',
        getCellValue: row => (row.member ? row.member.username : undefined)
      },
      {
        name: 'name',
        title: 'Name',
        type: 'link',
        getLink: row => `/admin/surveys/${row.id}`,
        getCellValue: row =>
          `${row.name} ${row.password_protection ? '🔒' : ''}`
      },
      {
        name: 'city',
        title: 'City'
      },
      {
        name: 'term',
        title: 'Term'
      },
      {
        name: 'test_id',
        title: 'Test'
      },
      {
        name: 'starts_on',
        title: 'Start',
        type: 'date'
      },
      {
        name: 'ends_on',
        title: 'End',
        type: 'date'
      },
      {
        name: 'created_on',
        title: 'Created On',
        type: 'date'
      },
      {
        name: 'count_results',
        title: '#Results',
        getCellValue: row => row.stats.num_results || 0
      },
      {
        name: 'count_aborted',
        title: '#Aborted',
        getCellValue: row => row.stats.num_aborted || 0
      },
      {
        name: 'count_unfinished',
        title: '#Unfinished',
        getCellValue: row => row.stats.num_unfinished || 0
      }
    ],
    integratedSortingExtensions: [
      {
        columnName: 'starts_on',
        compare: compareDateAsc
      },
      {
        columnName: 'ends_on',
        compare: compareDateAsc
      },
      {
        columnName: 'created_on',
        compare: compareDateAsc
      }
    ],
    columnExtensions: [
      { columnName: 'member', width: 200 },
      { columnName: 'created_on', width: 200 },
      { columnName: 'count_results', width: 100 },
      { columnName: 'count_aborted', width: 100 },
      { columnName: 'count_unfinished', width: 100 },
      { columnName: 'count_surveys', width: 100 }
    ]
  };

  componentDidMount() {
    this.props.dispatch(fetchSurveys());
  }

  onToggleFavorite = id => {
    this.props.dispatch(toggleFavorite('surveys', id));
  };

  onView = () => {
    const { dispatch, activeSurvey } = this.props;
    dispatch(push(`/admin/surveys/${activeSurvey.get('id')}`));
  };

  onCopy = () => {
    const { dispatch, activeSurvey } = this.props;
    dispatch(setEditData(activeSurvey.delete('member').delete('id')));
    dispatch(openSurveyCreateDialog());
  };

  onEdit = () => {
    const { activeSurvey, dispatch } = this.props;
    dispatch(setEditData(activeSurvey));
    dispatch(openSurveyCreateDialog());
  };

  onDelete = () => {
    this.props.dispatch(deleteSurvey(this.props.activeSurvey.get('id')));
  };

  onOpenSurveyMenu = id => {
    this.props.dispatch(setActive(id));
  };

  onCloseSurveyMenu = () => {
    this.props.dispatch(setActive(-1));
  };

  onNew = () => {
    this.props.dispatch(openSurveyCreateDialog());
  };

  render() {
    const {
      data,
      columns,
      columnExtensions,
      integratedSortingExtensions,
      favorites,
      activeSurvey
    } = this.props;
    return (
      <AppBarLayout
        appBar={
          <AppBar title="Surveys">
            <AddButton onClick={this.onNew} />
          </AppBar>
        }
      >
        <SurveyCreate />
        <FavoriteGridTable
          rows={data.toJS()}
          columns={columns}
          columnExtensions={columnExtensions}
          favorites={favorites.toJS()}
          onViewEntry={this.onView}
          onToggleFavorite={this.onToggleFavorite}
          onDeleteEntry={
            activeSurvey.get('is_deletable') ? this.onDelete : undefined
          }
          onEditEntry={
            activeSurvey.get('is_editable') ? this.onEdit : undefined
          }
          onCopyEntry={this.onCopy}
          onOpenMenu={this.onOpenSurveyMenu}
          integratedSortingExtensions={integratedSortingExtensions}
          onCloseMenu={this.onCloseSurveyMenu}
        />
      </AppBarLayout>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  data: makeDataSelector(),
  favorites: favoriteSurveysSelector,
  activeSurvey: makeActiveSurveySelector()
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Surveys);
