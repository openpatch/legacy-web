import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { fromJS, List, Map } from 'immutable';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import MuiAppBar from '@material-ui/core/AppBar';
import differenceInMilliseconds from 'date-fns/difference_in_milliseconds';

import AppBarLayout from '../../common/components/AppBarLayout';
import AppBar from '../../common/containers/AppBar';
import ItemResultPlayer from '../components/ItemResultPlayer';
import ItemResultComments from '../components/ItemResultComments';
import formats from '../../formats';
import {
  fetchItem,
  setActive,
  setActiveResult,
  fetchResult,
  putItemResultComment,
  deleteItemResultComment
} from '../actions/items';
import {
  makeActiveItemSelector,
  makeActiveItemResultSelector
} from '../selectors/items';
import ItemResultTranscript from '../components/ItemResultTranscript';
import ItemResultSolution from '../components/ItemResultSolution';
import { itemReducer } from '../../common/components/ItemRenderer';
import { idSelector } from '../../common/selectors/member';

const styles = theme => ({
  tabs: {
    marginTop: theme.spacing.unit * 2
  },
  tabContainer: {
    padding: theme.spacing.unit,
    borderRadius: 0
  },
  player: {
    maxWidth: 1280,
    margin: '0 auto'
  }
});

class ItemResult extends React.Component {
  static propTypes = {
    item: PropTypes.instanceOf(Map),
    result: PropTypes.instanceOf(Map),
    memberId: PropTypes.number.isRequired
  };

  static defaultProps = {
    item: new Map({}),
    result: new Map({})
  };

  state = {
    tab: 0,
    time: 0,
    playbackStatus: 'stop',
    currentActionIndex: 0,
    speed: 1,
    state: fromJS({}),
    endTime: 0,
    commentForm: fromJS({ duration: 0, category: '#f44336' }),
    replyCommentForm: fromJS({})
  };

  componentWillMount() {
    const { match, dispatch } = this.props;
    const { itemId, resultId } = match.params;
    dispatch(setActive(itemId));
    dispatch(fetchItem(itemId));
    dispatch(setActiveResult(resultId));
    dispatch(fetchResult(resultId));
  }

  componentWillUnmount() {
    cancelAnimationFrame(this.state.playbackIntervalId);
    window.removeEventListener('keypress', this.shortcuts);
  }

  componentDidMount() {
    window.addEventListener('keypress', this.shortcuts);
  }

  componentWillReceiveProps(nextProps) {
    const startTime = nextProps.result.get('start_time');
    let endTime = nextProps.result.get('end_time', 1);

    if (startTime && endTime) {
      endTime = differenceInMilliseconds(
        new Date(endTime),
        new Date(startTime)
      );
    }
    this.setState({
      endTime
    });
  }

  shortcuts = event => {
    if (event.target.localName === 'body') {
      const { commentForm, endTime, time, playbackStatus } = this.state;
      switch (event.key) {
        case '+':
          event.preventDefault();
          this.setState({
            commentForm: commentForm.update('duration', duration => {
              const newDuration = duration + 100;
              if (newDuration <= endTime - time) {
                return newDuration;
              }
              return endTime - time;
            })
          });
          break;
        case '-':
          event.preventDefault();
          this.setState({
            commentForm: commentForm.update('duration', duration => {
              const newDuration = duration - 100;
              if (newDuration >= 0) {
                return newDuration;
              }
              return 0;
            })
          });
          break;
        case ' ':
          event.preventDefault();
          if (playbackStatus === 'play') {
            this.onPause();
          } else {
            this.onPlay();
          }
          break;
        case 'ArrowRight':
          event.preventDefault();
          let newTime = time + 100;
          this.onTimeChange(newTime <= endTime ? newTime : endTime);
          break;
        case 'ArrowLeft':
          event.preventDefault();
          newTime = time - 100;
          this.onTimeChange(newTime >= 0 ? newTime : 0);
          break;
        default:
          break;
      }
    }
  };

  onTimeChange = time => {
    this.onPause();
    const { result, item } = this.props;
    const endTime = this.state.endTime;
    time = endTime >= time ? time : endTime;
    const reducer = formats[item.getIn(['format', 'type'])].reducer;

    let state = fromJS({});
    let currentActionIndex = 0;
    let currentAction = result.getIn(['actions', currentActionIndex]);
    let foundAction = false;
    while (
      currentAction &&
      time >= currentAction.get('time') &&
      time !== endTime
    ) {
      state = itemReducer(state, currentAction.get('action').toJS(), reducer);
      currentAction = result.getIn(['actions', currentActionIndex]);
      currentActionIndex++;
      foundAction = true;
    }
    this.setState({
      time,
      currentActionIndex: foundAction
        ? currentActionIndex - 1
        : currentActionIndex,
      state
    });
  };

  onTimeUpdate = timeStamp => {
    if (this.state.playbackStatus === 'play') {
      const { result, item } = this.props;
      let currentActionIndex = this.state.currentActionIndex;
      const currentAction = result.getIn(['actions', currentActionIndex]);

      // end
      if (this.state.time >= this.state.endTime) {
        this.onFinish();
        return;
      }
      let passedTime = timeStamp - this.state.lastTimeStamp;
      // increase at max 50ms to avoid jumps
      if (passedTime > 50) {
        passedTime = 50;
      }
      const playbackIntervalId = requestAnimationFrame(this.onTimeUpdate);
      if (currentAction && this.state.time >= currentAction.get('time')) {
        currentActionIndex += 1;
        // update state
        const reducer = formats[item.getIn(['format', 'type'])].reducer;
        const nextState = itemReducer(
          this.state.state,
          currentAction.get('action').toJS(),
          reducer
        );
        this.setState({
          time: this.state.time + passedTime * this.state.speed,
          lastTimeStamp: timeStamp,
          currentActionIndex,
          state: nextState,
          playbackIntervalId
        });
      } else {
        this.setState({
          time: this.state.time + passedTime * this.state.speed,
          lastTimeStamp: timeStamp,
          currentActionIndex,
          playbackIntervalId
        });
      }
    }
  };
  onPlay = () => {
    const playbackIntervalId = requestAnimationFrame(this.onTimeUpdate);
    this.setState({
      playbackStatus: 'play',
      playbackIntervalId,
      lastTimeStamp: performance.now()
    });
  };
  onPause = () => {
    cancelAnimationFrame(this.state.playbackIntervalId);
    this.setState({
      playbackStatus: 'pause'
    });
  };
  onFinish = () => {
    cancelAnimationFrame(this.state.playbackIntervalId);
    this.setState({
      playbackStatus: 'stop',
      playbackIntervalId: null,
      time: this.state.endTime
    });
  };
  onStop = () => {
    cancelAnimationFrame(this.state.playbackIntervalId);
    this.setState({
      playbackStatus: 'stop',
      time: 0,
      currentActionIndex: 0,
      state: fromJS({}),
      playbackIntervalId: null
    });
  };
  onSpeedChange = event => {
    this.setState({
      speed: event.target.value
    });
  };
  onTabChange = (e, tab) => {
    this.setState({ tab });
  };
  onCommentsChange = (key, value) => {
    const { commentForm, endTime } = this.state;
    if (key === 'time') {
      this.onTimeChange(value);
    } else if (key === 'duration') {
      if (value >= 0 && value <= endTime) {
        this.setState({
          commentForm: commentForm.set('duration', value)
        });
      }
    } else if (key === 'category') {
      this.setState({
        commentForm: commentForm.set('category', value)
      });
    } else if (key === 'comment') {
      this.setState({
        commentForm: commentForm.set('comment', value)
      });
    } else if (key === 'parent_id') {
      this.setState({
        commentForm: commentForm
          .delete('duration')
          .delete('category')
          .set('parent_id', value)
      });
    }
  };

  onReplyCommentChange = (key, value) => {
    const { replyCommentForm } = this.state;
    if (key === 'comment') {
      this.setState({
        replyCommentForm: replyCommentForm.set('comment', value)
      });
    } else if (key === 'parent_id') {
      if (value === this.state.replyCommentForm.get('parent_id')) {
        this.setState({
          replyCommentForm: replyCommentForm.delete('parent_id')
        });
      } else {
        this.setState({
          replyCommentForm: replyCommentForm.set('parent_id', value)
        });
      }
    }
  };
  onCommentSubmit = () => {
    const { result, onComment, memberId } = this.props;
    const { commentForm, time } = this.state;
    const comment = {
      start_time: time,
      member_id: memberId,
      end_time: time + commentForm.get('duration'),
      text: commentForm.get('comment'),
      category: commentForm.get('category')
    };
    this.setState({
      commentForm: fromJS({ duration: 0, category: '#f44336' })
    });
    onComment(result.get('id'), comment);
  };
  onCommentDelete = (commentId, parentId = null) => {
    const { onCommentDelete, result } = this.props;
    onCommentDelete(result.get('id'), commentId, parentId);
  };
  onReplyCommentSubmit = () => {
    const { result, onComment, memberId } = this.props;
    const { replyCommentForm } = this.state;
    const comment = {
      text: replyCommentForm.get('comment'),
      member_id: memberId,
      parent_id: replyCommentForm.get('parent_id')
    };
    this.setState({
      replyCommentForm: fromJS({})
    });
    onComment(result.get('id'), comment);
  };
  render() {
    const { classes, result, item, match, memberId } = this.props;
    const { resultId } = match.params;
    return (
      <AppBarLayout
        appBar={
          <AppBar
            title={`Result ${resultId}`}
            bg={item ? `#${item.getIn(['format', 'color_hex'])}` : null}
          />
        }
      >
        {item &&
        item.getIn(['format', 'type']) &&
        this.state.endTime > 0 &&
        result ? (
          <React.Fragment>
            <div className={classes.player}>
              <ItemResultPlayer
                format={formats[item.getIn(['format', 'type'])]}
                actions={result.get('actions', new List([]))}
                state={this.state.state}
                time={this.state.time}
                endTime={this.state.endTime}
                commentDuration={this.state.commentForm.get('duration')}
                commentCategory={this.state.commentForm.get('category')}
                comments={result.get('item_result_comments', new List([]))}
                item={item}
                playbackStatus={this.state.playbackStatus}
                onPlay={this.onPlay}
                speed={this.state.speed}
                onPause={this.onPause}
                onStop={this.onStop}
                onSpeedChange={this.onSpeedChange}
                onTimeChange={this.onTimeChange}
              />
            </div>
            <MuiAppBar
              position="static"
              color="default"
              className={classes.tabs}
            >
              <Tabs value={this.state.tab} onChange={this.onTabChange}>
                <Tab label="Comments" />
                <Tab label="Transcript" />
                {item.get('solution', new Map({})).size !== 0 && (
                  <Tab label="Solution" />
                )}
              </Tabs>
            </MuiAppBar>
            {this.state.tab === 0 && (
              <Paper className={classes.tabContainer}>
                <ItemResultComments
                  time={this.state.time}
                  currentAction={this.state.currentActionIndex - 1}
                  onClick={this.onTimeChange}
                  memberId={memberId}
                  onChange={this.onCommentsChange}
                  onSubmit={this.onCommentSubmit}
                  onReplyChange={this.onReplyCommentChange}
                  onReplySubmit={this.onReplyCommentSubmit}
                  onDelete={this.onCommentDelete}
                  replyForm={this.state.replyCommentForm}
                  comments={result.get('item_result_comments')}
                  form={this.state.commentForm}
                />
              </Paper>
            )}
            {this.state.tab === 1 && (
              <Paper className={classes.tabContainer}>
                <ItemResultTranscript
                  currentAction={this.state.currentActionIndex - 1}
                  onClick={this.onTimeChange}
                  actions={result.get('actions')}
                />
              </Paper>
            )}
            {this.state.tab === 2 && (
              <Paper className={classes.tabContainer}>
                <ItemResultSolution solution={item.get('solution')} />
              </Paper>
            )}
          </React.Fragment>
        ) : (
          <div>No actions therefore no replay</div>
        )}
      </AppBarLayout>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  item: makeActiveItemSelector(),
  result: makeActiveItemResultSelector(),
  memberId: idSelector
});

const mapDispatchToProps = dispatch => ({
  dispatch,
  onComment: (resultId, comment) =>
    dispatch(putItemResultComment(resultId, comment)),
  onCommentDelete: (resultId, commentId, parentId) =>
    dispatch(deleteItemResultComment(resultId, commentId, parentId))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(styles)(ItemResult));
