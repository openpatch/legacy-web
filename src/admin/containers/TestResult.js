/*
 *
 * TestResult
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Map, List } from 'immutable';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import { createStructuredSelector } from 'reselect';

import {
  makeActiveTestSelector,
  makeActiveTestResultSelector
} from '../selectors/tests';
import AppBarLayout from '../../common/components/AppBarLayout';
import AppBar from '../../common/containers/AppBar';
import {
  setActiveResult,
  setActive,
  fetchTest,
  fetchResult
} from '../actions/tests';
import ItemResultsTable from '../components/ItemResultsTable';
import { makeActiveTestResultItemsSelector } from '../selectors/items';
import { fetchResultsForTestResultId } from '../actions/items';

const styles = theme => ({});

export class TestResult extends React.Component {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    classes: PropTypes.object.isRequired,
    test: PropTypes.instanceOf(Map),
    result: PropTypes.instanceOf(Map)
  };

  static defaultProps = {
    test: new Map({}),
    result: new Map({}),
    itemResults: new List([])
  };

  componentDidMount() {
    const { dispatch, match } = this.props;
    const { testId, resultId } = match.params;
    dispatch(fetchResult(resultId));
    dispatch(fetchTest(testId));
    dispatch(fetchResultsForTestResultId(resultId));
    dispatch(setActive(testId));
    dispatch(setActiveResult(resultId));
  }

  render() {
    const { match, test, result, itemResults } = this.props;
    const { testId, resultId } = match.params;
    return (
      <AppBarLayout
        appBar={<AppBar title={`Result Nr.${resultId} of Test ${testId} `} />}
      >
        {JSON.stringify(test.toJS(), null, 2)}
        <br />
        {JSON.stringify(result.toJS(), null, 2)}
        {result.get('id')}
        <br />
        <ItemResultsTable results={itemResults.toJS()} />
      </AppBarLayout>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  test: makeActiveTestSelector(),
  result: makeActiveTestResultSelector(),
  itemResults: makeActiveTestResultItemsSelector()
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(styles)(TestResult));
