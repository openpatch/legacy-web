/**
 *
 * Members constants
 *
 */

export const DEFAULT_ACTION = 'commoop.admin.members.DEFAULT_ACTION';

export const FETCH_MEMBERS = 'commoop.admin.members.FETCH_MEMBERS';

export const FETCH_MEMBERS_COMMIT =
  'commoop.admin.members.FETCH_MEMBERS_COMMIT';

export const FETCH_MEMBERS_ROLLBACK =
  'commoop.admin.members.FETCH_MEMBERS_ROLLBACK';

export const FILTER = 'commoop.admin.members.FILTER';

export const SET_ACTIVE = 'commoop.admin.members.SET_ACTIVE';

export const SET_EDIT_DATA = 'commoop.admin.members.SET_EDIT_DATA';

export const RESET_EDIT_DATA = 'commoop.admin.members.RESET_EDIT_DATA';

export const FETCH_EDIT_DATA = 'commoop.admin.members.FETCH_EDIT_DATA';

export const FETCH_EDIT_DATA_COMMIT =
  'commoop.admin.members.FETCH_EDIT_DATA_COMMIT';

export const FETCH_EDIT_DATA_ROLLBACK =
  'commoop.admin.members.FETCH_EDIT_DATA_ROLLBACK';

export const FETCH_MEMBER = 'commoop.admin.members.FETCH_MEMBER';

export const FETCH_MEMBER_COMMIT = 'commoop.admin.members.FETCH_MEMBER_COMMIT';

export const FETCH_MEMBER_ROLLBACK =
  'commoop.admin.members.FETCH_MEMBER_ROLLBACK';

export const POST_MEMBER = 'commoop.admin.members.POST_MEMBER';

export const POST_MEMBER_COMMIT = 'commoop.admin.members.POST_MEMBER_COMMIT';

export const POST_MEMBER_ROLLBACK =
  'commoop.admin.members.POST_MEMBER_ROLLBACK';

export const PUT_MEMBER = 'commoop.admin.members.PUT_MEMBER';

export const PUT_MEMBER_COMMIT = 'commoop.admin.members.PUT_MEMBER_COMMIT';

export const PUT_MEMBER_ROLLBACK = 'commoop.admin.members.PUT_MEMBER_ROLLBACK';

export const DELETE_MEMBER = 'commoop.admin.members.DELETE_MEMBER';

export const DELETE_MEMBER_COMMIT =
  'commoop.admin.members.DELETE_MEMBER_COMMIT';

export const DELETE_MEMBER_ROLLBACK =
  'commoop.admin.members.DELETE_MEMBER_ROLLBACK';

export const OPEN_MEMBER_CREATE_DIALOG =
  'commoop.admin.members.OPEN_MEMBER_CREATE_DIALOG';

export const CLOSE_MEMBER_CREATE_DIALOG =
  'commoop.admin.members.CLOSE_MEMBER_CREATE_DIALOG';

export const ADD_FAVORITE = 'commoop.admin.members.ADD_FAVORITE';

export const ADD_FAVORITE_ROLLBACK =
  'commoop.admin.members.ADD_FAVORITE_ROLLBACK';

export const REMOVE_FAVORITE = 'commoop.admin.members.REMOVE_FAVORITE';

export const REMOVE_FAVORITE_ROLLBACK =
  'commoop.admin.members.REMOVE_FAVORITE_ROLLBACK';

export const FETCH_MEMBERS_PERMITTED_FOR_ITEM_ID =
  'commoop.admin.members.FETCH_MEMBERS_PERMITTED_FOR_ITEM_ID';

export const FETCH_MEMBERS_PERMITTED_FOR_ITEM_ID_COMMIT =
  'commoop.admin.members.FETCH_MEMBERS_PERMITTED_FOR_ITEM_ID_COMMIT';

export const FETCH_MEMBERS_PERMITTED_FOR_ITEM_ID_ROLLBACK =
  'commoop.admin.members.FETCH_MEMBERS_PERMITTED_FOR_ITEM_ID_ROLLBACK';

export const FETCH_MEMBERS_PERMITTED_FOR_TEST_ID =
  'commoop.admin.members.FETCH_MEMBERS_PERMITTED_FOR_TEST_ID';

export const FETCH_MEMBERS_PERMITTED_FOR_TEST_ID_COMMIT =
  'commoop.admin.members.FETCH_MEMBERS_PERMITTED_FOR_TEST_ID_COMMIT';

export const FETCH_MEMBERS_PERMITTED_FOR_TEST_ID_ROLLBACK =
  'commoop.admin.members.FETCH_MEMBERS_PERMITTED_FOR_TEST_ID_ROLLBACK';

export const FETCH_MEMBERS_PERMITTED_FOR_SURVEY_ID =
  'commoop.admin.members.FETCH_MEMBERS_PERMITTED_FOR_SURVEY_ID';

export const FETCH_MEMBERS_PERMITTED_FOR_SURVEY_ID_COMMIT =
  'commoop.admin.members.FETCH_MEMBERS_PERMITTED_FOR_SURVEY_ID_COMMIT';

export const FETCH_MEMBERS_PERMITTED_FOR_SURVEY_ID_ROLLBACK =
  'commoop.admin.members.FETCH_MEMBERS_PERMITTED_FOR_SURVEY_ID_ROLLBACK';
