/**
 *
 * Surveys constants
 *
 */

export const DEFAULT_ACTION = 'commoop.admin.surveys.DEFAULT_ACTION';

export const SET_ACTIVE = 'commoop.admin.surveys.SET_ACTIVE';

export const SET_FILTER = 'commoop.admin.surveys.SET_FILTER';

export const FETCH_SURVEYS = 'commoop.admin.surveys.FETCH_SURVEYS';

export const FETCH_SURVEYS_COMMIT =
  'commoop.admin.surveys.FETCH_SURVEYS_COMMIT';

export const FETCH_SURVEYS_ROLLBACK =
  'commoop.admin.surveys.FETCH_SURVEYS_ROLLBACK';

export const FETCH_SURVEYS_FOR_TEST_ID =
  'commoop.admin.surveys.FETCH_SURVEYS_FOR_TEST_ID';

export const FETCH_SURVEYS_FOR_TEST_ID_COMMIT =
  'commoop.admin.surveys.FETCH_SURVEYS_FOR_TEST_ID_COMMIT';

export const FETCH_SURVEYS_FOR_TEST_ID_ROLLBACK =
  'commoop.admin.surveys.FETCH_SURVEYS_FOR_TEST_ID_ROLLBACK';

export const FETCH_SURVEY = 'commoop.admin.surveys.FETCH_SURVEY';

export const FETCH_SURVEY_COMMIT = 'commoop.admin.surveys.FETCH_SURVEY_COMMIT';

export const FETCH_SURVEY_ROLLBACK =
  'commoop.admin.surveys.FETCH_SURVEY_ROLLBACK';

export const FETCH_FAVORITE_SURVEYS =
  'commoop.admin.surveys.FETCH_FAVORITE_SURVEYS';

export const FETCH_FAVORITE_SURVEYS_COMMIT =
  'commoop.admin.surveys.FETCH_FAVORITE_SURVEYS_COMMIT';

export const FETCH_FAVORITE_SURVEYS_ROLLBACK =
  'commoop.admin.surveys.FETCH_FAVORITE_SURVEYS_ROLLBACK';

export const POST_SURVEY = 'commoop.admin.surveys.POST_SURVEY';

export const POST_SURVEY_COMMIT = 'commoop.admin.surveys.POST_SURVEY_COMMIT';

export const POST_SURVEY_ROLLBACK =
  'commoop.admin.surveys.POST_SURVEY_ROLLBACK';

export const PUT_SURVEY = 'commoop.admin.surveys.PUT_SURVEY';

export const PUT_SURVEY_COMMIT = 'commoop.admin.surveys.PUT_SURVEY_COMMIT';

export const PUT_SURVEY_ROLLBACK = 'commoop.admin.surveys.PUT_SURVEY_ROLLBACK';

export const DELETE_SURVEY = 'commoop.admin.surveys.DELETE_SURVEY';

export const DELETE_SURVEY_COMMIT =
  'commoop.admin.surveys.DELETE_SURVEY_COMMIT';

export const DELETE_SURVEY_ROLLBACK =
  'commoop.admin.surveys.DELETE_SURVEY_ROLLBACK';

export const FETCH_EDIT_DATA = 'commoop.admin.surveys.FETCH_EDIT_DATA';

export const FETCH_EDIT_DATA_COMMIT =
  'commoop.admin.surveys.FETCH_EDIT_DATA_COMMIT';

export const FETCH_EDIT_DATA_ROLLBACK =
  'commoop.admin.surveys.FETCH_EDIT_DATA_ROLLBACK';

export const SET_EDIT_DATA = 'commoop.admin.surveys.SET_EDIT_DATA';

export const SET_EDIT_DATA_TEST_ID =
  'commoop.admin.surveys.SET_EDIT_DATA_TEST_ID';

export const RESET_EDIT_DATA = 'commoop.admin.surveys.RESET_EDIT_DATA';

export const OPEN_SURVEY_CREATE_DIALOG =
  'commoop.admin.surveys.OPEN_SURVEY_CREATE_DIALOG';

export const CLOSE_SURVEY_CREATE_DIALOG =
  'commoop.admin.surveys.CLOSE_SURVEY_CREATE_DIALOG';

export const ADD_MEMBER_TO_PERMITTED_FOR_ID =
  'commoop.admin.surveys.ADD_MEMBER_TO_PERMITTED_FOR_ID';

export const ADD_MEMBER_TO_PERMITTED_FOR_ID_COMMIT =
  'commoop.admin.surveys.ADD_MEMBER_TO_PERMITTED_FOR_ID_COMMIT';

export const ADD_MEMBER_TO_PERMITTED_FOR_ID_ROLLBACK =
  'commoop.admin.surveys.ADD_MEMBER_TO_PERMITTED_FOR_ID_ROLLBACK';

export const REMOVE_MEMBER_FROM_PERMITTED_FOR_ID =
  'commoop.admin.surveys.REMOVE_MEMBER_FROM_PERMITTED_FOR_ID';

export const REMOVE_MEMBER_FROM_PERMITTED_FOR_ID_COMMIT =
  'commoop.admin.surveys.REMOVE_MEMBER_FROM_PERMITTED_FOR_ID_COMMIT';

export const REMOVE_MEMBER_FROM_PERMITTED_FOR_ID_ROLLBACK =
  'commoop.admin.surveys.REMOVE_MEMBER_FROM_PERMITTED_FOR_ID_ROLLBACK';
