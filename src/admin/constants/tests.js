/**
 *
 * Tests constants
 *
 */

export const DEFAULT_ACTION = 'commoop.tests.DEFAULT_ACTION';

export const SET_FILTER = 'commoop.admin.tests.SET_FILTER';

export const FETCH_TESTS = 'commoop.admin.tests.FETCH_TESTS';

export const FETCH_TESTS_COMMIT = 'commoop.admin.tests.FETCH_TESTS_COMMIT';

export const FETCH_TESTS_ROLLBACK = 'commoop.admin.tests.FETCH_TESTS_ROLLBACK';

export const FETCH_TEST = 'commoop.admin.tests.FETCH_TEST';

export const FETCH_TEST_COMMIT = 'commoop.admin.tests.FETCH_TEST_COMMIT';

export const FETCH_TEST_ROLLBACK = 'commoop.admin.tests.FETCH_TEST_ROLLBACK';

export const EXPORT_TEST = 'commoop.admin.tests.EXPORT_TEST';

export const EXPORT_TEST_COMMIT = 'commoop.admin.tests.EXPORT_TEST_COMMIT';

export const EXPORT_TEST_ROLLBACK = 'commoop.admin.tests.EXPORT_TEST_ROLLBACK';

export const FETCH_TEST_FOR_SURVEY_ID =
  'commoop.admin.tests.FETCH_TEST_FOR_SURVEY_ID';

export const FETCH_TEST_FOR_SURVEY_ID_COMMIT =
  'commoop.admin.tests.FETCH_TEST_FOR_SURVEY_ID_COMMIT';

export const FETCH_TEST_FOR_SURVEY_ID_ROLLBACK =
  'commoop.admin.tests.FETCH_TEST_FOR_SURVEY_ID_ROLLBACK';

export const FETCH_FAVORITE_TESTS = 'commoop.admin.tests.FETCH_FAVORITE_TESTS';

export const FETCH_FAVORITE_TESTS_COMMIT =
  'commoop.admin.tests.FETCH_FAVORITE_TESTS_COMMIT';

export const FETCH_FAVORITE_TESTS_ROLLBACK =
  'commoop.admin.tests.FETCH_FAVORITE_TESTS_ROLLBACK';

export const SET_EDIT_DATA = 'commoop.admin.tests.SET_EDIT_DATA';

export const RESET_EDIT_DATA = 'commoop.admin.tests.RESET_EDIT_DATA';

export const SET_ACTIVE = 'commoop.admin.tests.SET_ACTIVE';

export const FETCH_EDIT_DATA = 'commoop.admin.tests.FETCH_EDIT_DATA';

export const FETCH_EDIT_DATA_COMMIT =
  'commoop.admin.tests.FETCH_EDIT_DATA_COMMIT';

export const FETCH_EDIT_DATA_ROLLBACK =
  'commoop.admin.tests.FETCH_EDIT_DATA_ROLLBACK';

export const POST_TEST = 'commoop.admin.tests.POST_TEST';

export const POST_TEST_COMMIT = 'commoop.admin.tests.POST_TEST_COMMIT';

export const POST_TEST_ROLLBACK = 'commoop.admin.tests.POST_TEST_ROLLBACK';

export const PUT_TEST = 'commoop.admin.tests.PUT_TEST';

export const PUT_TEST_COMMIT = 'commoop.admin.tests.PUT_TEST_COMMIT';

export const PUT_TEST_ROLLBACK = 'commoop.admin.tests.PUT_TEST_ROLLBACK';

export const FETCH_RESULTS_FOR_ID = 'commoop.admin.tests.FETCH_RESULTS_FOR_ID';

export const FETCH_RESULTS_FOR_ID_COMMIT =
  'commoop.admin.tests.FETCH_RESULTS_FOR_ID_COMMIT';

export const FETCH_RESULTS_FOR_ID_ROLLBACK =
  'commoop.admin.tests.FETCH_RESULTS_FOR_ID_ROLLBACK';

export const EXPORT_RESULTS_FOR_TEST_ID =
  'commoop.admin.tests.EXPORT_RESULTS_FOR_TEST_ID';

export const EXPORT_RESULTS_FOR_TEST_ID_COMMIT =
  'commoop.admin.tests.EXPORT_RESULTS_FOR_TEST_ID_COMMIT';

export const EXPORT_RESULTS_FOR_TEST_ID_ROLLBACK =
  'commoop.admin.tests.EXPORT_RESULTS_FOR_TEST_ID_ROLLBACK';

export const FETCH_RESULT = 'commoop.admin.tests.FETCH_RESULT';

export const FETCH_RESULT_COMMIT = 'commoop.admin.tests.FETCH_RESULT_COMMIT';

export const FETCH_RESULT_ROLLBACK =
  'commoop.admin.tests.FETCH_RESULT_ROLLBACK';

export const FETCH_RESULTS_FOR_SURVEY_ID =
  'commoop.admin.tests.FETCH_RESULTS_FOR_SURVEY_ID';

export const FETCH_RESULTS_FOR_SURVEY_ID_COMMIT =
  'commoop.admin.tests.FETCH_RESULTS_FOR_SURVEY_ID_COMMIT';

export const FETCH_RESULTS_FOR_SURVEY_ID_ROLLBACK =
  'commoop.admin.tests.FETCH_RESULTS_FOR_SURVEY_ID_ROLLBACK';

export const DELETE_RESULTS_FOR_SURVEY_ID =
  'commoop.admin.tests.DELETE_RESULTS_FOR_SURVEY_ID';

export const DELETE_RESULTS_FOR_SURVEY_ID_COMMIT =
  'commoop.admin.tests.DELETE_RESULTS_FOR_SURVEY_ID_COMMIT';

export const DELETE_RESULTS_FOR_SURVEY_ID_ROLLBACK =
  'commoop.admin.tests.DELETE_RESULTS_FOR_SURVEY_ID_ROLLBACK';

export const SET_SELECTED_RESULTS = 'commoop.admin.tests.SET_SELECTED_RESULTS';

export const DELETE_SELECTED_RESULTS =
  'commoop.admin.tests.DELETE_SELECTED_RESULTS';

export const DELETE_SELECTED_RESULTS_COMMIT =
  'commoop.admin.tests.DELETE_SELECTED_RESULTS_COMMIT';

export const DELETE_SELECTED_RESULTS_ROLLBACK =
  'commoop.admin.tests.DELETE_SELECTED_RESULTS_ROLLBACK';

export const DELETE_TEST = 'commoop.admin.tests.DELETE_TEST';

export const DELETE_TEST_COMMIT = 'commoop.admin.tests.DELETE_TEST_COMMIT';

export const DELETE_TEST_ROLLBACK = 'commoop.admin.tests.DELETE_TEST_ROLLBACK';

export const ADD_MEMBER_TO_PERMITTED_FOR_ID =
  'commoop.admin.tests.ADD_MEMBER_TO_PERMITTED_FOR_ID';

export const ADD_MEMBER_TO_PERMITTED_FOR_ID_COMMIT =
  'commoop.admin.tests.ADD_MEMBER_TO_PERMITTED_FOR_ID_COMMIT';

export const ADD_MEMBER_TO_PERMITTED_FOR_ID_ROLLBACK =
  'commoop.admin.tests.ADD_MEMBER_TO_PERMITTED_FOR_ID_ROLLBACK';

export const REMOVE_MEMBER_FROM_PERMITTED_FOR_ID =
  'commoop.admin.tests.REMOVE_MEMBER_FROM_PERMITTED_FOR_ID';

export const REMOVE_MEMBER_FROM_PERMITTED_FOR_ID_COMMIT =
  'commoop.admin.tests.REMOVE_MEMBER_FROM_PERMITTED_FOR_ID_COMMIT';

export const REMOVE_MEMBER_FROM_PERMITTED_FOR_ID_ROLLBACK =
  'commoop.admin.tests.REMOVE_MEMBER_FROM_PERMITTED_FOR_ID_ROLLBACK';

export const SET_RESULTS_FILTER_BY_SURVEY_IDS =
  'commoop.admin.tests.SET_RESULTS_FILTER_BY_SURVEY_IDS';

export const SET_ACTIVE_RESULT = 'commoop.admin.tests.SET_ACTIVE_RESULT';
