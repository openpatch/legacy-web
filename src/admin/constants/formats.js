/**
 *
 * Formats constants
 *
 */

export const DEFAULT_ACTION = 'commoop.admin.formats.DEFAULT_ACTION';

export const FETCH_FORMATS = 'commoop.admin.formats.FETCH_FORMATS';

export const FETCH_FORMATS_COMMIT =
  'commoop.admin.formats.FETCH_FORMATS_COMMIT';

export const FETCH_FORMATS_ROLLBACK =
  'commoop.admin.formats.FETCH_FORMATS_ROLLBACK';

export const SET_ACTIVE = 'commoop.admin.formats.SET_ACTIVE';
