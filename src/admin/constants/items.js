/**
 *
 * Items constants
 *
 */

export const DEFAULT_ACTION = 'commoop.admin.items.DEFAULT_ACTION';

export const FETCH_ITEMS = 'commoop.admin.items.FETCH_ITEMS';

export const FETCH_ITEMS_COMMIT = 'commoop.admin.items.FETCH_ITEMS_COMMIT';

export const FETCH_ITEMS_ROLLBACK = 'commoop.admin.items.FETCH_ITEMS_ROLLBACK';

export const FETCH_ITEMS_FOR_FORMAT =
  'commoop.admin.items.FETCH_ITEMS_FOR_FORMAT';

export const FETCH_ITEMS_FOR_FORMAT_COMMIT =
  'commoop.admin.items.FETCH_ITEMS_FOR_FORMAT_COMMIT';

export const FETCH_ITEMS_FOR_FORMAT_ROLLBACK =
  'commoop.admin.items.FETCH_ITEMS_FOR_FORMAT_ROLLBACK';

export const FETCH_ITEMS_FOR_TEST_ID =
  'commoop.admin.items.FETCH_ITEMS_FOR_TEST_ID';

export const FETCH_ITEMS_FOR_TEST_ID_COMMIT =
  'commoop.admin.items.FETCH_ITEMS_FOR_TEST_ID_COMMIT';

export const FETCH_ITEMS_FOR_TEST_ID_ROLLBACK =
  'commoop.admin.items.FETCH_ITEMS_FOR_TEST_ID_ROLLBACK';

export const FETCH_FAVORITE_ITEMS = 'commoop.admin.items.FETCH_FAVORITE_ITEMS';

export const FETCH_FAVORITE_ITEMS_COMMIT =
  'commoop.admin.items.FETCH_FAVORITE_ITEMS_COMMIT';

export const FETCH_FAVORITE_ITEMS_ROLLBACK =
  'commoop.admin.items.FETCH_FAVORITE_ITEMS_ROLLBACK';

export const FETCH_ITEM = 'commoop.admin.items.FETCH_ITEM';

export const FETCH_ITEM_COMMIT = 'commoop.admin.items.FETCH_ITEM_COMMIT';

export const FETCH_ITEM_ROLLBACK = 'commoop.admin.items.FETCH_ITEM_ROLLBACK';

export const POST_ITEM = 'commoop.admin.items.POST_ITEM';

export const POST_ITEM_COMMIT = 'commoop.admin.items.POST_ITEM_COMMIT';

export const POST_ITEM_ROLLBACK = 'commoop.admin.items.POST_ITEM_ROLLBACK';

export const PUT_ITEM = 'commoop.admin.items.PUT_ITEM';

export const PUT_ITEM_COMMIT = 'commoop.admin.items.PUT_ITEM_COMMIT';

export const PUT_ITEM_ROLLBACK = 'commoop.admin.items.PUT_ITEM_ROLLBACK';

export const DELETE_ITEM = 'commoop.admin.items.DELETE_ITEM';

export const DELETE_ITEM_COMMIT = 'commoop.admin.items.DELETE_ITEM_COMMIT';

export const DELETE_ITEM_ROLLBACK = 'commoop.admin.items.DELETE_ITEM_ROLLBACK';

export const EVALUATE_ITEM = 'commoop.admin.items.EVALUATE_ITEM';

export const EVALUATE_ITEM_COMMIT = 'commoop.admin.items.EVALUATE_ITEM_COMMIT';

export const EVALUATE_ITEM_ROLLBACK =
  'commoop.admin.items.EVALUATE_ITEM_ROLLBACK';

export const SET_ACTIVE = 'commoop.admin.items.SET_ACTIVE';

export const SET_ACTIVE_RESULT = 'commoop.admin.items.SET_ACTIVE_RESULT';

export const FETCH_RESULT = 'commoop.admin.items.FETCH_RESULT';

export const FETCH_RESULT_COMMIT = 'commoop.admin.items.FETCH_RESULT_COMMIT';

export const FETCH_RESULT_ROLLBACK =
  'commoop.admin.items.FETCH_RESULT_ROLLBACK';

export const FETCH_RESULTS_FOR_ID = 'commoop.admin.items.FETCH_RESULTS_FOR_ID';

export const FETCH_RESULTS_FOR_ID_COMMIT =
  'commoop.admin.items.FETCH_RESULTS_FOR_ID_COMMIT';

export const FETCH_RESULTS_FOR_ID_ROLLBACK =
  'commoop.admin.items.FETCH_RESULTS_FOR_ID_ROLLBACK';

export const FETCH_STATISTIC_FOR_ID =
  'commoop.admin.items.FETCH_STATISTIC_FOR_ID';

export const FETCH_STATISTIC_FOR_ID_COMMIT =
  'commoop.admin.items.FETCH_STATISTIC_FOR_ID_COMMIT';

export const FETCH_STATISTIC_FOR_ID_ROLLBACK =
  'commoop.admin.items.FETCH_STATISTIC_FOR_ID_ROLLBACK';

export const FETCH_RESULTS_FOR_TEST_RESULT_ID =
  'commoop.admin.items.FETCH_RESULTS_FOR_TEST_RESULT_ID';

export const FETCH_RESULTS_FOR_TEST_RESULT_ID_COMMIT =
  'commoop.admin.items.FETCH_RESULTS_FOR_TEST_RESULT_ID_COMMIT';

export const FETCH_RESULTS_FOR_TEST_RESULT_ID_ROLLBACK =
  'commoop.admin.items.FETCH_RESULTS_FOR_TEST_RESULT_ID_ROLLBACK';

export const SET_EDIT_DATA = 'commoop.admin.items.SET_EDIT_DATA';

export const RESET_EDIT_DATA = 'commoop.admin.items.RESET_EDIT_DATA';

export const FETCH_EDIT_DATA = 'commoop.admin.items.FETCH_EDIT_DATA';

export const FETCH_EDIT_DATA_COMMIT =
  'commoop.admin.items.FETCH_EDIT_DATA_COMMIT';

export const FETCH_EDIT_DATA_ROLLBACK =
  'commoop.admin.items.FETCH_EDIT_DATA_ROLLBACK';

export const OPEN_ITEM_PREVIEW_POPUP =
  'commoop.admin.items.OPEN_ITEM_PREVIEW_POPUP';

export const CLOSE_ITEM_PREVIEW_POPUP =
  'commoop.admin.items.CLOSE_ITEM_PREVIEW_POPUP';

export const OPEN_ITEM_VISUALIZATION_POPUP =
  'commoop.admin.items.OPEN_ITEM_VISUALIZATION_POPUP';

export const CLOSE_ITEM_VISUALIZATION_POPUP =
  'commoop.admin.items.CLOSE_ITEM_VISUALIZATION_POPUP';

export const OPEN_ITEM_STATISTIC_POPUP =
  'commoop.admin.items.OPEN_ITEM_STATISTIC_POPUP';

export const CLOSE_ITEM_STATISTIC_POPUP =
  'commoop.admin.items.CLOSE_ITEM_STATISTIC_POPUP';

export const SET_FILTER = 'commoop.admin.items.SET_FILTER';

export const ADD_MEMBER_TO_PERMITTED_FOR_ID =
  'commoop.admin.items.ADD_MEMBER_TO_PERMITTED_FOR_ID';

export const ADD_MEMBER_TO_PERMITTED_FOR_ID_COMMIT =
  'commoop.admin.items.ADD_MEMBER_TO_PERMITTED_FOR_ID_COMMIT';

export const ADD_MEMBER_TO_PERMITTED_FOR_ID_ROLLBACK =
  'commoop.admin.items.ADD_MEMBER_TO_PERMITTED_FOR_ID_ROLLBACK';

export const REMOVE_MEMBER_FROM_PERMITTED_FOR_ID =
  'commoop.admin.items.REMOVE_MEMBER_FROM_PERMITTED_FOR_ID';

export const REMOVE_MEMBER_FROM_PERMITTED_FOR_ID_COMMIT =
  'commoop.admin.items.REMOVE_MEMBER_FROM_PERMITTED_FOR_ID_COMMIT';

export const REMOVE_MEMBER_FROM_PERMITTED_FOR_ID_ROLLBACK =
  'commoop.admin.items.REMOVE_MEMBER_FROM_PERMITTED_FOR_ID_ROLLBACK';

export const PUT_ITEM_RESULT_COMMENT =
  'commoop.admin.items.PUT_ITEM_RESULT_COMMENT';

export const PUT_ITEM_RESULT_COMMENT_COMMIT =
  'commoop.admin.items.PUT_ITEM_RESULT_COMMENT_COMMIT';

export const PUT_ITEM_RESULT_COMMENT_ROLLBACK =
  'commoop.admin.items.PUT_ITEM_RESULT_COMMENT_ROLLBACK';

export const DELETE_ITEM_RESULT_COMMENT =
  'commoop.admin.items.DELETE_ITEM_RESULT_COMMENT';

export const DELETE_ITEM_RESULT_COMMENT_COMMIT =
  'commoop.admin.items.DELETE_ITEM_RESULT_COMMENT_COMMIT';

export const DELETE_ITEM_RESULT_COMMENT_ROLLBACK =
  'commoop.admin.items.DELETE_ITEM_RESULT_COMMENT_ROLLBACK';
