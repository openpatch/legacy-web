/**
 *
 * Items actions
 *
 */

import {
  DEFAULT_ACTION,
  FETCH_ITEMS,
  FETCH_ITEMS_COMMIT,
  FETCH_ITEMS_ROLLBACK,
  POST_ITEM,
  POST_ITEM_COMMIT,
  POST_ITEM_ROLLBACK,
  SET_ACTIVE,
  FETCH_RESULTS_FOR_ID,
  FETCH_RESULTS_FOR_ID_COMMIT,
  FETCH_RESULTS_FOR_ID_ROLLBACK,
  FETCH_STATISTIC_FOR_ID,
  FETCH_STATISTIC_FOR_ID_COMMIT,
  FETCH_STATISTIC_FOR_ID_ROLLBACK,
  FETCH_ITEM,
  FETCH_ITEM_COMMIT,
  FETCH_ITEM_ROLLBACK,
  FETCH_ITEMS_FOR_FORMAT,
  FETCH_ITEMS_FOR_FORMAT_COMMIT,
  FETCH_ITEMS_FOR_FORMAT_ROLLBACK,
  FETCH_FAVORITE_ITEMS,
  FETCH_FAVORITE_ITEMS_COMMIT,
  FETCH_FAVORITE_ITEMS_ROLLBACK,
  PUT_ITEM,
  PUT_ITEM_COMMIT,
  PUT_ITEM_ROLLBACK,
  PUT_ITEM_RESULT_COMMENT,
  PUT_ITEM_RESULT_COMMENT_COMMIT,
  PUT_ITEM_RESULT_COMMENT_ROLLBACK,
  DELETE_ITEM,
  DELETE_ITEM_COMMIT,
  DELETE_ITEM_ROLLBACK,
  SET_EDIT_DATA,
  FETCH_EDIT_DATA,
  FETCH_EDIT_DATA_COMMIT,
  FETCH_EDIT_DATA_ROLLBACK,
  OPEN_ITEM_PREVIEW_POPUP,
  CLOSE_ITEM_PREVIEW_POPUP,
  OPEN_ITEM_STATISTIC_POPUP,
  CLOSE_ITEM_STATISTIC_POPUP,
  OPEN_ITEM_VISUALIZATION_POPUP,
  CLOSE_ITEM_VISUALIZATION_POPUP,
  FETCH_ITEMS_FOR_TEST_ID,
  FETCH_ITEMS_FOR_TEST_ID_COMMIT,
  FETCH_ITEMS_FOR_TEST_ID_ROLLBACK,
  RESET_EDIT_DATA,
  SET_FILTER,
  ADD_MEMBER_TO_PERMITTED_FOR_ID,
  ADD_MEMBER_TO_PERMITTED_FOR_ID_COMMIT,
  ADD_MEMBER_TO_PERMITTED_FOR_ID_ROLLBACK,
  REMOVE_MEMBER_FROM_PERMITTED_FOR_ID,
  REMOVE_MEMBER_FROM_PERMITTED_FOR_ID_COMMIT,
  REMOVE_MEMBER_FROM_PERMITTED_FOR_ID_ROLLBACK,
  SET_ACTIVE_RESULT,
  FETCH_RESULT,
  FETCH_RESULT_COMMIT,
  FETCH_RESULT_ROLLBACK,
  FETCH_RESULTS_FOR_TEST_RESULT_ID,
  FETCH_RESULTS_FOR_TEST_RESULT_ID_COMMIT,
  FETCH_RESULTS_FOR_TEST_RESULT_ID_ROLLBACK,
  DELETE_ITEM_RESULT_COMMENT,
  DELETE_ITEM_RESULT_COMMENT_COMMIT,
  DELETE_ITEM_RESULT_COMMENT_ROLLBACK,
  EVALUATE_ITEM,
  EVALUATE_ITEM_COMMIT,
  EVALUATE_ITEM_ROLLBACK
} from '../constants/items';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION
  };
}

export function setFilter(filter) {
  return {
    type: SET_FILTER,
    payload: {
      filter
    }
  };
}

export function setActive(itemId) {
  return {
    type: SET_ACTIVE,
    payload: {
      active: itemId
    }
  };
}

export function setActiveResult(resultId) {
  return {
    type: SET_ACTIVE_RESULT,
    payload: {
      active: resultId
    }
  };
}

export function fetchEditData(id) {
  return {
    type: FETCH_EDIT_DATA,
    meta: {
      offline: {
        effect: {
          url: `v1/items/${id}`,
          method: 'GET'
        },
        commit: {
          type: FETCH_EDIT_DATA_COMMIT
        },
        rollback: {
          type: FETCH_EDIT_DATA_ROLLBACK
        }
      }
    }
  };
}

export function setEditData(data) {
  return {
    type: SET_EDIT_DATA,
    payload: {
      data
    }
  };
}

export function resetEditData(data) {
  return {
    type: RESET_EDIT_DATA
  };
}

export function postItem(item) {
  return {
    type: POST_ITEM,
    meta: {
      offline: {
        effect: {
          url: 'v1/items',
          method: 'POST',
          data: {
            ...item
          }
        },
        commit: {
          type: POST_ITEM_COMMIT
        },
        rollback: {
          type: POST_ITEM_ROLLBACK
        }
      }
    }
  };
}

export function putItem(item) {
  return {
    type: PUT_ITEM,
    meta: {
      offline: {
        effect: {
          url: `v1/items/${item.id}`,
          method: 'PUT',
          data: {
            ...item
          }
        },
        commit: {
          type: PUT_ITEM_COMMIT
        },
        rollback: {
          type: PUT_ITEM_ROLLBACK
        }
      }
    }
  };
}

export function evaluateItem(item, submittedSolution = {}) {
  return {
    type: EVALUATE_ITEM,
    meta: {
      offline: {
        effect: {
          url: `v1/items/evaluate`,
          method: 'POST',
          data: {
            ...item,
            submittedSolution
          }
        },
        commit: {
          type: EVALUATE_ITEM_COMMIT
        },
        rollback: {
          type: EVALUATE_ITEM_ROLLBACK
        }
      }
    }
  };
}

export function deleteItem(id) {
  return {
    type: DELETE_ITEM,
    payload: {
      id
    },
    meta: {
      offline: {
        effect: {
          url: `v1/items/${id}`,
          method: 'DELETE'
        },
        commit: {
          type: DELETE_ITEM_COMMIT,
          meta: {
            id
          }
        },
        rollback: {
          type: DELETE_ITEM_ROLLBACK
        }
      }
    }
  };
}

export function fetchItem(id) {
  return {
    type: FETCH_ITEM,
    meta: {
      offline: {
        effect: {
          url: `v1/items/${id}`,
          method: 'GET'
        },
        commit: {
          type: FETCH_ITEM_COMMIT,
          meta: {
            id
          }
        },
        rollback: {
          type: FETCH_ITEM_ROLLBACK,
          meta: {
            id
          }
        }
      }
    }
  };
}

export function fetchItems() {
  return {
    type: FETCH_ITEMS,
    meta: {
      offline: {
        effect: {
          url: 'v1/items',
          method: 'GET'
        },
        commit: {
          type: FETCH_ITEMS_COMMIT
        },
        rollback: {
          type: FETCH_ITEMS_ROLLBACK
        }
      }
    }
  };
}

export function fetchFavoriteItems() {
  return {
    type: FETCH_FAVORITE_ITEMS,
    meta: {
      offline: {
        effect: {
          url: 'v1/members/me/favorite-items',
          method: 'GET'
        },
        commit: {
          type: FETCH_FAVORITE_ITEMS_COMMIT
        },
        rollback: {
          type: FETCH_FAVORITE_ITEMS_ROLLBACK
        }
      }
    }
  };
}

export function fetchItemsForFormat(format) {
  return {
    type: FETCH_ITEMS_FOR_FORMAT,
    meta: {
      offline: {
        effect: {
          url: `v1/items?format=${format}`,
          method: 'GET'
        },
        commit: {
          type: FETCH_ITEMS_FOR_FORMAT_COMMIT
        },
        rollback: {
          type: FETCH_ITEMS_FOR_FORMAT_ROLLBACK
        }
      }
    }
  };
}

export function fetchItemsForTestId(testId, surveyIds = []) {
  let query = '';
  if (surveyIds.length > 0) {
    query += '?';
    surveyIds.forEach(s => (query += `survey_ids[]=${s}&`));
  }
  return {
    type: FETCH_ITEMS_FOR_TEST_ID,
    meta: {
      offline: {
        effect: {
          url: `v1/tests/${testId}/items${query}`,
          method: 'GET'
        },
        commit: {
          type: FETCH_ITEMS_FOR_TEST_ID_COMMIT
        },
        rollback: {
          type: FETCH_ITEMS_FOR_TEST_ID_ROLLBACK
        }
      }
    }
  };
}

export function fetchResult(id) {
  return {
    type: FETCH_RESULT,
    meta: {
      offline: {
        effect: {
          url: `v1/items/results/${id}`,
          method: 'GET'
        },
        commit: {
          type: FETCH_RESULT_COMMIT,
          meta: {
            id
          }
        },
        rollback: {
          type: FETCH_RESULT_ROLLBACK,
          meta: {
            id
          }
        }
      }
    }
  };
}

export function fetchResultsForId(id) {
  return {
    type: FETCH_RESULTS_FOR_ID,
    meta: {
      offline: {
        effect: {
          url: `v1/items/${id}/results`,
          method: 'GET'
        },
        commit: {
          type: FETCH_RESULTS_FOR_ID_COMMIT,
          meta: {
            id
          }
        },
        rollback: {
          type: FETCH_RESULTS_FOR_ID_ROLLBACK,
          meta: {
            id
          }
        }
      }
    }
  };
}

export function fetchStatisticForId(id, surveyIds = []) {
  let query = '';
  if (surveyIds.length > 0) {
    query += '?';
    surveyIds.forEach(s => (query += `survey_ids[]=${s}&`));
  }
  return {
    type: FETCH_STATISTIC_FOR_ID,
    meta: {
      offline: {
        effect: {
          url: `v1/items/${id}/statistic${query}`,
          method: 'GET'
        },
        commit: {
          type: FETCH_STATISTIC_FOR_ID_COMMIT,
          meta: {
            id
          }
        },
        rollback: {
          type: FETCH_STATISTIC_FOR_ID_ROLLBACK,
          meta: {
            id
          }
        }
      }
    }
  };
}

export function fetchResultsForTestResultId(id) {
  return {
    type: FETCH_RESULTS_FOR_TEST_RESULT_ID,
    meta: {
      offline: {
        effect: {
          url: `v1/tests/results/${id}/item_results`,
          method: 'GET'
        },
        commit: {
          type: FETCH_RESULTS_FOR_TEST_RESULT_ID_COMMIT,
          meta: {
            id
          }
        },
        rollback: {
          type: FETCH_RESULTS_FOR_TEST_RESULT_ID_ROLLBACK,
          meta: {
            id
          }
        }
      }
    }
  };
}

export function openItemPreviewPopup() {
  return {
    type: OPEN_ITEM_PREVIEW_POPUP
  };
}

export function closeItemPreviewPopup() {
  return {
    type: CLOSE_ITEM_PREVIEW_POPUP
  };
}

export function openItemStatisticPopup() {
  return {
    type: OPEN_ITEM_STATISTIC_POPUP
  };
}

export function closeItemStatisticPopup() {
  return {
    type: CLOSE_ITEM_STATISTIC_POPUP
  };
}

export function openItemVisualizationPopup() {
  return {
    type: OPEN_ITEM_VISUALIZATION_POPUP
  };
}

export function closeItemVisualizationPopup() {
  return {
    type: CLOSE_ITEM_VISUALIZATION_POPUP
  };
}

export function addMemberToPermittedForId(id, memberIdentifier) {
  return {
    type: ADD_MEMBER_TO_PERMITTED_FOR_ID,
    meta: {
      offline: {
        effect: {
          url: `v1/items/${id}/permissions/${memberIdentifier}`,
          method: 'PUT'
        },
        commit: {
          type: ADD_MEMBER_TO_PERMITTED_FOR_ID_COMMIT,
          meta: {
            id,
            memberIdentifier
          }
        },
        rollback: {
          type: ADD_MEMBER_TO_PERMITTED_FOR_ID_ROLLBACK
        }
      }
    }
  };
}

export function removeMemberFromPermittedForId(id, memberIdentifier) {
  return {
    type: REMOVE_MEMBER_FROM_PERMITTED_FOR_ID,
    meta: {
      offline: {
        effect: {
          url: `v1/items/${id}/permissions/${memberIdentifier}`,
          method: 'DELETE'
        },
        commit: {
          type: REMOVE_MEMBER_FROM_PERMITTED_FOR_ID_COMMIT,
          meta: {
            id
          }
        },
        rollback: {
          type: REMOVE_MEMBER_FROM_PERMITTED_FOR_ID_ROLLBACK
        }
      }
    }
  };
}

export function putItemResultComment(resultId, comment) {
  return {
    type: PUT_ITEM_RESULT_COMMENT,
    meta: {
      offline: {
        effect: {
          url: `v1/items/results/${resultId}/comments`,
          method: 'PUT',
          data: {
            ...comment
          }
        },
        commit: {
          type: PUT_ITEM_RESULT_COMMENT_COMMIT,
          meta: {
            comment,
            resultId
          }
        },
        rollback: {
          type: PUT_ITEM_RESULT_COMMENT_ROLLBACK
        }
      }
    }
  };
}

export function deleteItemResultComment(resultId, commentId, parentId = null) {
  return {
    type: DELETE_ITEM_RESULT_COMMENT,
    meta: {
      offline: {
        effect: {
          url: `v1/items/results/${resultId}/comments/${commentId}`,
          method: 'DELETE'
        },
        commit: {
          type: DELETE_ITEM_RESULT_COMMENT_COMMIT,
          meta: {
            resultId,
            commentId,
            parentId
          }
        },
        rollback: {
          type: DELETE_ITEM_RESULT_COMMENT_ROLLBACK
        }
      }
    }
  };
}
