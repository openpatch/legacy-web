/**
 *
 * Tests actions
 *
 */

import {
  DEFAULT_ACTION,
  FETCH_TESTS,
  FETCH_TEST,
  FETCH_TEST_COMMIT,
  FETCH_TEST_ROLLBACK,
  FETCH_TESTS_COMMIT,
  FETCH_TESTS_ROLLBACK,
  SET_FILTER,
  SET_EDIT_DATA,
  POST_TEST,
  POST_TEST_COMMIT,
  POST_TEST_ROLLBACK,
  PUT_TEST,
  PUT_TEST_COMMIT,
  PUT_TEST_ROLLBACK,
  DELETE_TEST,
  DELETE_TEST_COMMIT,
  DELETE_TEST_ROLLBACK,
  FETCH_EDIT_DATA,
  FETCH_EDIT_DATA_COMMIT,
  FETCH_EDIT_DATA_ROLLBACK,
  FETCH_RESULTS_FOR_ID,
  FETCH_RESULTS_FOR_ID_COMMIT,
  FETCH_RESULTS_FOR_ID_ROLLBACK,
  FETCH_FAVORITE_TESTS,
  FETCH_FAVORITE_TESTS_COMMIT,
  FETCH_FAVORITE_TESTS_ROLLBACK,
  SET_ACTIVE,
  RESET_EDIT_DATA,
  ADD_MEMBER_TO_PERMITTED_FOR_ID,
  ADD_MEMBER_TO_PERMITTED_FOR_ID_COMMIT,
  ADD_MEMBER_TO_PERMITTED_FOR_ID_ROLLBACK,
  REMOVE_MEMBER_FROM_PERMITTED_FOR_ID,
  REMOVE_MEMBER_FROM_PERMITTED_FOR_ID_COMMIT,
  REMOVE_MEMBER_FROM_PERMITTED_FOR_ID_ROLLBACK,
  FETCH_RESULTS_FOR_SURVEY_ID,
  FETCH_RESULTS_FOR_SURVEY_ID_COMMIT,
  FETCH_RESULTS_FOR_SURVEY_ID_ROLLBACK,
  DELETE_RESULTS_FOR_SURVEY_ID,
  DELETE_RESULTS_FOR_SURVEY_ID_COMMIT,
  DELETE_RESULTS_FOR_SURVEY_ID_ROLLBACK,
  SET_RESULTS_FILTER_BY_SURVEY_IDS,
  FETCH_TEST_FOR_SURVEY_ID,
  FETCH_TEST_FOR_SURVEY_ID_COMMIT,
  FETCH_TEST_FOR_SURVEY_ID_ROLLBACK,
  SET_SELECTED_RESULTS,
  DELETE_SELECTED_RESULTS,
  DELETE_SELECTED_RESULTS_COMMIT,
  DELETE_SELECTED_RESULTS_ROLLBACK,
  SET_ACTIVE_RESULT,
  FETCH_RESULT,
  FETCH_RESULT_COMMIT,
  FETCH_RESULT_ROLLBACK,
  EXPORT_RESULTS_FOR_TEST_ID,
  EXPORT_RESULTS_FOR_TEST_ID_COMMIT,
  EXPORT_RESULTS_FOR_TEST_ID_ROLLBACK,
  EXPORT_TEST,
  EXPORT_TEST_COMMIT,
  EXPORT_TEST_ROLLBACK
} from '../constants/tests';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION
  };
}

export function setFilter(filter) {
  return {
    type: SET_FILTER,
    payload: {
      filter
    }
  };
}

export function fetchTests() {
  return {
    type: FETCH_TESTS,
    meta: {
      offline: {
        effect: {
          url: 'v1/tests',
          method: 'GET'
        },
        commit: {
          type: FETCH_TESTS_COMMIT
        },
        rollback: {
          type: FETCH_TESTS_ROLLBACK
        }
      }
    }
  };
}

export function fetchFavoriteTests() {
  return {
    type: FETCH_FAVORITE_TESTS,
    meta: {
      offline: {
        effect: {
          url: 'v1/members/me/favorite-tests',
          method: 'GET'
        },
        commit: {
          type: FETCH_FAVORITE_TESTS_COMMIT
        },
        rollback: {
          type: FETCH_FAVORITE_TESTS_ROLLBACK
        }
      }
    }
  };
}

export function setActive(testId) {
  return {
    type: SET_ACTIVE,
    payload: {
      active: testId
    }
  };
}

export function fetchTest(id, surveyIds = []) {
  let query = '';
  if (surveyIds.length > 0) {
    query += '?';
    surveyIds.forEach(s => (query += `survey_ids[]=${s}&`));
  }
  return {
    type: FETCH_TEST,
    meta: {
      offline: {
        effect: {
          url: `v1/tests/${id}${query}`,
          method: 'GET'
        },
        commit: {
          type: FETCH_TEST_COMMIT
        },
        rollback: {
          type: FETCH_TEST_ROLLBACK
        }
      }
    }
  };
}

export function fetchTestForSurveyId(id) {
  return {
    type: FETCH_TEST_FOR_SURVEY_ID,
    meta: {
      offline: {
        effect: {
          url: `v1/surveys/${id}/test`,
          method: 'GET'
        },
        commit: {
          type: FETCH_TEST_FOR_SURVEY_ID_COMMIT
        },
        rollback: {
          type: FETCH_TEST_FOR_SURVEY_ID_ROLLBACK
        }
      }
    }
  };
}

export function fetchResult(resultId) {
  return {
    type: FETCH_RESULT,
    meta: {
      offline: {
        effect: {
          url: `v1/tests/results/${resultId}`,
          method: 'GET'
        },
        commit: {
          type: FETCH_RESULT_COMMIT,
          meta: {
            resultId
          }
        },
        rollback: {
          type: FETCH_RESULT_ROLLBACK,
          meta: {
            resultId
          }
        }
      }
    }
  };
}

export function fetchResultsForId(testId) {
  return {
    type: FETCH_RESULTS_FOR_ID,
    meta: {
      offline: {
        effect: {
          url: `v1/tests/${testId}/results`,
          method: 'GET'
        },
        commit: {
          type: FETCH_RESULTS_FOR_ID_COMMIT,
          meta: {
            testId
          }
        },
        rollback: {
          type: FETCH_RESULTS_FOR_ID_ROLLBACK,
          meta: {
            testId
          }
        }
      }
    }
  };
}

export function fetchResultsForSurveyId(surveyId) {
  return {
    type: FETCH_RESULTS_FOR_SURVEY_ID,
    meta: {
      offline: {
        effect: {
          url: `v1/surveys/${surveyId}/results`,
          method: 'GET'
        },
        commit: {
          type: FETCH_RESULTS_FOR_SURVEY_ID_COMMIT,
          meta: {
            surveyId
          }
        },
        rollback: {
          type: FETCH_RESULTS_FOR_SURVEY_ID_ROLLBACK,
          meta: {
            surveyId
          }
        }
      }
    }
  };
}

export function deleteResultsForSurveyId(surveyId) {
  return {
    type: DELETE_RESULTS_FOR_SURVEY_ID,
    meta: {
      offline: {
        effect: {
          url: `v1/surveys/${surveyId}/results`,
          method: 'DELETE'
        },
        commit: {
          type: DELETE_RESULTS_FOR_SURVEY_ID_COMMIT,
          meta: {
            surveyId
          }
        },
        rollback: {
          type: DELETE_RESULTS_FOR_SURVEY_ID_ROLLBACK,
          meta: {
            surveyId
          }
        }
      }
    }
  };
}

export function fetchEditData(id) {
  return {
    type: FETCH_EDIT_DATA,
    meta: {
      offline: {
        effect: {
          url: `v1/tests/${id}`,
          method: 'GET'
        },
        commit: {
          type: FETCH_EDIT_DATA_COMMIT
        },
        rollback: {
          type: FETCH_EDIT_DATA_ROLLBACK
        }
      }
    }
  };
}

export function setEditData(data) {
  return {
    type: SET_EDIT_DATA,
    payload: {
      data
    }
  };
}

export function resetEditData() {
  return {
    type: RESET_EDIT_DATA
  };
}

export function postTest(test) {
  return {
    type: POST_TEST,
    meta: {
      offline: {
        effect: {
          url: 'v1/tests',
          method: 'POST',
          data: {
            ...test
          }
        },
        commit: {
          type: POST_TEST_COMMIT
        },
        rollback: {
          type: POST_TEST_ROLLBACK
        }
      }
    }
  };
}

export function putTest(test) {
  return {
    type: PUT_TEST,
    meta: {
      offline: {
        effect: {
          url: `v1/tests/${test.id}`,
          method: 'PUT',
          data: {
            ...test
          }
        },
        commit: {
          type: PUT_TEST_COMMIT
        },
        rollback: {
          type: PUT_TEST_ROLLBACK
        }
      }
    }
  };
}

export function deleteTest(id) {
  return {
    type: DELETE_TEST,
    payload: {
      id
    },
    meta: {
      offline: {
        effect: {
          url: `v1/tests/${id}`,
          method: 'DELETE'
        },
        commit: {
          type: DELETE_TEST_COMMIT,
          meta: {
            id
          }
        },
        rollback: {
          type: DELETE_TEST_ROLLBACK
        }
      }
    }
  };
}

export function addMemberToPermittedForId(id, memberIdentifier) {
  return {
    type: ADD_MEMBER_TO_PERMITTED_FOR_ID,
    meta: {
      offline: {
        effect: {
          url: `v1/tests/${id}/permissions/${memberIdentifier}`,
          method: 'PUT'
        },
        commit: {
          type: ADD_MEMBER_TO_PERMITTED_FOR_ID_COMMIT,
          meta: {
            id
          }
        },
        rollback: {
          type: ADD_MEMBER_TO_PERMITTED_FOR_ID_ROLLBACK
        }
      }
    }
  };
}

export function removeMemberFromPermittedForId(id, memberIdentifier) {
  return {
    type: REMOVE_MEMBER_FROM_PERMITTED_FOR_ID,
    meta: {
      offline: {
        effect: {
          url: `v1/tests/${id}/permissions/${memberIdentifier}`,
          method: 'DELETE'
        },
        commit: {
          type: REMOVE_MEMBER_FROM_PERMITTED_FOR_ID_COMMIT,
          meta: {
            id
          }
        },
        rollback: {
          type: REMOVE_MEMBER_FROM_PERMITTED_FOR_ID_ROLLBACK
        }
      }
    }
  };
}

export function setResultsFilterBySurveyIds(surveyIds) {
  return {
    type: SET_RESULTS_FILTER_BY_SURVEY_IDS,
    payload: {
      surveyIds
    }
  };
}

export function setSelectedResults(ids) {
  return {
    type: SET_SELECTED_RESULTS,
    payload: {
      ids
    }
  };
}

export function deleteSelectedResults(ids) {
  return {
    type: DELETE_SELECTED_RESULTS,
    meta: {
      offline: {
        effect: {
          url: `v1/tests/results`,
          method: 'DELETE',
          data: {
            ids
          }
        },
        commit: {
          type: DELETE_SELECTED_RESULTS_COMMIT,
          meta: {
            ids
          }
        },
        rollback: {
          type: DELETE_SELECTED_RESULTS_ROLLBACK
        }
      }
    }
  };
}

export function setActiveResult(id) {
  return {
    type: SET_ACTIVE_RESULT,
    payload: {
      id
    }
  };
}

export function exportResultsForTestId(id, filter, format = 'json') {
  let url = `v1/tests/${id}/results/export?format=${format}`;
  if (filter.length > 0) {
    url += `&filter=[${String(filter)}]`;
  }
  return {
    type: EXPORT_RESULTS_FOR_TEST_ID,
    meta: {
      offline: {
        effect: {
          url,
          method: 'GET',
          responseType: 'stream'
        },
        commit: {
          type: EXPORT_RESULTS_FOR_TEST_ID_COMMIT,
          meta: {
            id,
            format
          }
        },
        rollback: {
          type: EXPORT_RESULTS_FOR_TEST_ID_ROLLBACK
        }
      }
    }
  };
}

export function exportTest(id, format = 'json') {
  return {
    type: EXPORT_TEST,
    meta: {
      offline: {
        effect: {
          url: `v1/tests/${id}/export`,
          method: 'GET',
          responseType: 'stream'
        },
        commit: {
          type: EXPORT_TEST_COMMIT,
          meta: {
            id,
            format
          }
        },
        rollback: {
          type: EXPORT_TEST_ROLLBACK
        }
      }
    }
  };
}
