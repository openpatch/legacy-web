/**
 *
 * Surveys actions
 *
 */

import {
  DEFAULT_ACTION,
  FETCH_SURVEYS,
  FETCH_SURVEYS_COMMIT,
  FETCH_SURVEYS_ROLLBACK,
  SET_FILTER,
  POST_SURVEY,
  POST_SURVEY_COMMIT,
  POST_SURVEY_ROLLBACK,
  SET_ACTIVE,
  FETCH_EDIT_DATA,
  FETCH_EDIT_DATA_COMMIT,
  FETCH_EDIT_DATA_ROLLBACK,
  FETCH_SURVEY,
  FETCH_SURVEY_COMMIT,
  FETCH_SURVEY_ROLLBACK,
  FETCH_FAVORITE_SURVEYS,
  FETCH_FAVORITE_SURVEYS_COMMIT,
  FETCH_FAVORITE_SURVEYS_ROLLBACK,
  SET_EDIT_DATA,
  PUT_SURVEY,
  PUT_SURVEY_COMMIT,
  PUT_SURVEY_ROLLBACK,
  DELETE_SURVEY,
  DELETE_SURVEY_COMMIT,
  DELETE_SURVEY_ROLLBACK,
  RESET_EDIT_DATA,
  SET_EDIT_DATA_TEST_ID,
  OPEN_SURVEY_CREATE_DIALOG,
  CLOSE_SURVEY_CREATE_DIALOG,
  ADD_MEMBER_TO_PERMITTED_FOR_ID,
  ADD_MEMBER_TO_PERMITTED_FOR_ID_COMMIT,
  ADD_MEMBER_TO_PERMITTED_FOR_ID_ROLLBACK,
  REMOVE_MEMBER_FROM_PERMITTED_FOR_ID,
  REMOVE_MEMBER_FROM_PERMITTED_FOR_ID_COMMIT,
  REMOVE_MEMBER_FROM_PERMITTED_FOR_ID_ROLLBACK,
  FETCH_SURVEYS_FOR_TEST_ID,
  FETCH_SURVEYS_FOR_TEST_ID_ROLLBACK,
  FETCH_SURVEYS_FOR_TEST_ID_COMMIT
} from '../constants/surveys';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION
  };
}

export function setActive(surveyId) {
  return {
    type: SET_ACTIVE,
    payload: {
      active: surveyId
    }
  };
}

export function setFilter(filter) {
  return {
    type: SET_FILTER,
    payload: {
      filter
    }
  };
}

export function fetchSurveys() {
  return {
    type: FETCH_SURVEYS,
    meta: {
      offline: {
        effect: {
          url: 'v1/surveys',
          method: 'GET'
        },
        commit: {
          type: FETCH_SURVEYS_COMMIT
        },
        rollback: {
          type: FETCH_SURVEYS_ROLLBACK
        }
      }
    }
  };
}

export function fetchFavoriteSurveys() {
  return {
    type: FETCH_FAVORITE_SURVEYS,
    meta: {
      offline: {
        effect: {
          url: 'v1/members/me/favorite-surveys',
          method: 'GET'
        },
        commit: {
          type: FETCH_FAVORITE_SURVEYS_COMMIT
        },
        rollback: {
          type: FETCH_FAVORITE_SURVEYS_ROLLBACK
        }
      }
    }
  };
}

export function fetchSurveysForTestId(testId) {
  return {
    type: FETCH_SURVEYS_FOR_TEST_ID,
    meta: {
      offline: {
        effect: {
          url: `v1/tests/${testId}/surveys`,
          method: 'GET'
        },
        commit: {
          type: FETCH_SURVEYS_FOR_TEST_ID_COMMIT,
          meta: {
            testId
          }
        },
        rollback: {
          type: FETCH_SURVEYS_FOR_TEST_ID_ROLLBACK,
          meta: {
            testId
          }
        }
      }
    }
  };
}

export function fetchSurvey(id) {
  return {
    type: FETCH_SURVEY,
    meta: {
      offline: {
        effect: {
          url: `v1/surveys/${id}`,
          method: 'GET'
        },
        commit: {
          type: FETCH_SURVEY_COMMIT
        },
        rollback: {
          type: FETCH_SURVEY_ROLLBACK
        }
      }
    }
  };
}

export function fetchEditData(id) {
  return {
    type: FETCH_EDIT_DATA,
    meta: {
      offline: {
        effect: {
          url: `v1/surveys/${id}`,
          method: 'GET'
        },
        commit: {
          type: FETCH_EDIT_DATA_COMMIT
        },
        rollback: {
          type: FETCH_EDIT_DATA_ROLLBACK
        }
      }
    }
  };
}

export function setEditData(data) {
  return {
    type: SET_EDIT_DATA,
    payload: {
      data
    }
  };
}

export function setEditDataTestId(testId) {
  return {
    type: SET_EDIT_DATA_TEST_ID,
    payload: {
      id: testId
    }
  };
}

export function resetEditData() {
  return {
    type: RESET_EDIT_DATA
  };
}

export function postSurvey(survey) {
  return {
    type: POST_SURVEY,
    meta: {
      offline: {
        effect: {
          url: 'v1/surveys',
          method: 'POST',
          data: {
            ...survey
          }
        },
        commit: {
          type: POST_SURVEY_COMMIT
        },
        rollback: {
          type: POST_SURVEY_ROLLBACK
        }
      }
    }
  };
}

export function putSurvey(survey) {
  return {
    type: PUT_SURVEY,
    meta: {
      offline: {
        effect: {
          url: `v1/surveys/${survey.id}`,
          method: 'PUT',
          data: {
            ...survey
          }
        },
        commit: {
          type: PUT_SURVEY_COMMIT
        },
        rollback: {
          type: PUT_SURVEY_ROLLBACK
        }
      }
    }
  };
}

export function deleteSurvey(id) {
  return {
    type: DELETE_SURVEY,
    payload: {
      id
    },
    meta: {
      offline: {
        effect: {
          url: `v1/surveys/${id}`,
          method: 'DELETE'
        },
        commit: {
          type: DELETE_SURVEY_COMMIT,
          meta: {
            id
          }
        },
        rollback: {
          type: DELETE_SURVEY_ROLLBACK
        }
      }
    }
  };
}

export function openSurveyCreateDialog() {
  return {
    type: OPEN_SURVEY_CREATE_DIALOG
  };
}

export function closeSurveyCreateDialog() {
  return {
    type: CLOSE_SURVEY_CREATE_DIALOG
  };
}

export function addMemberToPermittedForId(id, memberIdentifier) {
  return {
    type: ADD_MEMBER_TO_PERMITTED_FOR_ID,
    meta: {
      offline: {
        effect: {
          url: `v1/surveys/${id}/permissions/${memberIdentifier}`,
          method: 'PUT'
        },
        commit: {
          type: ADD_MEMBER_TO_PERMITTED_FOR_ID_COMMIT,
          meta: {
            id
          }
        },
        rollback: {
          type: ADD_MEMBER_TO_PERMITTED_FOR_ID_ROLLBACK
        }
      }
    }
  };
}

export function removeMemberFromPermittedForId(id, memberIdentifier) {
  return {
    type: REMOVE_MEMBER_FROM_PERMITTED_FOR_ID,
    meta: {
      offline: {
        effect: {
          url: `v1/surveys/${id}/permissions/${memberIdentifier}`,
          method: 'DELETE'
        },
        commit: {
          type: REMOVE_MEMBER_FROM_PERMITTED_FOR_ID_COMMIT,
          meta: {
            id
          }
        },
        rollback: {
          type: REMOVE_MEMBER_FROM_PERMITTED_FOR_ID_ROLLBACK
        }
      }
    }
  };
}
