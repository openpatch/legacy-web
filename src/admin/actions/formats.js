/**
 *
 * Formats actions
 *
 */

import {
  DEFAULT_ACTION,
  FETCH_FORMATS,
  FETCH_FORMATS_COMMIT,
  FETCH_FORMATS_ROLLBACK,
  SET_ACTIVE
} from '../constants/formats';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION
  };
}

export function setActive(label) {
  return {
    type: SET_ACTIVE,
    payload: {
      label
    }
  };
}

export function fetchFormats() {
  return {
    type: FETCH_FORMATS,
    meta: {
      offline: {
        effect: {
          url: 'v1/formats',
          method: 'GET'
        },
        commit: {
          type: FETCH_FORMATS_COMMIT
        },
        rollback: {
          type: FETCH_FORMATS_ROLLBACK
        }
      }
    }
  };
}
