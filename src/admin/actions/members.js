/**
 *
 * Members actions
 *
 */

import {
  DEFAULT_ACTION,
  FETCH_MEMBERS,
  FETCH_MEMBERS_COMMIT,
  FETCH_MEMBERS_ROLLBACK,
  FILTER,
  SET_ACTIVE,
  SET_EDIT_DATA,
  RESET_EDIT_DATA,
  FETCH_EDIT_DATA,
  FETCH_EDIT_DATA_COMMIT,
  FETCH_EDIT_DATA_ROLLBACK,
  FETCH_MEMBER,
  FETCH_MEMBER_COMMIT,
  FETCH_MEMBER_ROLLBACK,
  POST_MEMBER,
  POST_MEMBER_COMMIT,
  POST_MEMBER_ROLLBACK,
  PUT_MEMBER,
  PUT_MEMBER_COMMIT,
  PUT_MEMBER_ROLLBACK,
  DELETE_MEMBER,
  DELETE_MEMBER_COMMIT,
  DELETE_MEMBER_ROLLBACK,
  OPEN_MEMBER_CREATE_DIALOG,
  CLOSE_MEMBER_CREATE_DIALOG,
  FETCH_MEMBERS_PERMITTED_FOR_ITEM_ID,
  FETCH_MEMBERS_PERMITTED_FOR_ITEM_ID_COMMIT,
  FETCH_MEMBERS_PERMITTED_FOR_ITEM_ID_ROLLBACK,
  FETCH_MEMBERS_PERMITTED_FOR_SURVEY_ID,
  FETCH_MEMBERS_PERMITTED_FOR_SURVEY_ID_COMMIT,
  FETCH_MEMBERS_PERMITTED_FOR_SURVEY_ID_ROLLBACK,
  FETCH_MEMBERS_PERMITTED_FOR_TEST_ID,
  FETCH_MEMBERS_PERMITTED_FOR_TEST_ID_COMMIT,
  FETCH_MEMBERS_PERMITTED_FOR_TEST_ID_ROLLBACK
} from '../constants/members';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION
  };
}

export function setFilter(filter) {
  return {
    type: FILTER,
    payload: {
      filter
    }
  };
}

export function setActive(id) {
  return {
    type: SET_ACTIVE,
    payload: {
      id
    }
  };
}

export function fetchEditData(id) {
  return {
    type: FETCH_EDIT_DATA,
    meta: {
      offline: {
        effect: {
          url: `v1/members/${id}`,
          method: 'GET'
        },
        commit: {
          type: FETCH_EDIT_DATA_COMMIT
        },
        rollback: {
          type: FETCH_EDIT_DATA_ROLLBACK
        }
      }
    }
  };
}

export function setEditData(data) {
  return {
    type: SET_EDIT_DATA,
    payload: {
      data
    }
  };
}

export function resetEditData() {
  return {
    type: RESET_EDIT_DATA
  };
}

export function fetchMember(id) {
  return {
    type: FETCH_MEMBER,
    meta: {
      offline: {
        effect: {
          url: `v1/members/${id}`,
          method: 'GET',
        },
        commit: {
          type: FETCH_MEMBER_COMMIT
        },
        rollback: {
          type: FETCH_MEMBER_ROLLBACK
        }
      }
    }
  };
}

export function postMember(member) {
  return {
    type: POST_MEMBER,
    meta: {
      offline: {
        effect: {
          url: 'v1/members',
          method: 'POST',
          data: {
            ...member
          }
        },
        commit: {
          type: POST_MEMBER_COMMIT
        },
        rollback: {
          type: POST_MEMBER_ROLLBACK
        }
      }
    }
  };
}

export function putMember(member) {
  return {
    type: PUT_MEMBER,
    meta: {
      offline: {
        effect: {
          url: `v1/members/${member.id}`,
          method: 'PUT',
          data: {
            ...member
          }
        },
        commit: {
          type: PUT_MEMBER_COMMIT
        },
        rollback: {
          type: PUT_MEMBER_ROLLBACK
        }
      }
    }
  };
}

export function openMemberCreateDialog() {
  return {
    type: OPEN_MEMBER_CREATE_DIALOG
  };
}

export function closeMemberCreateDialog() {
  return {
    type: CLOSE_MEMBER_CREATE_DIALOG
  };
}

export function deleteMember(id) {
  return {
    type: DELETE_MEMBER,
    payload: {
      id
    },
    meta: {
      offline: {
        effect: {
          url: `v1/members/${id}`,
          method: 'DELETE'
        },
        commit: {
          type: DELETE_MEMBER_COMMIT,
          meta: {
            id
          }
        },
        rollback: {
          type: DELETE_MEMBER_ROLLBACK
        }
      }
    }
  };
}

export function fetchMembers() {
  return {
    type: FETCH_MEMBERS,
    meta: {
      offline: {
        effect: {
          url: 'v1/members',
          method: 'GET'
        },
        commit: {
          type: FETCH_MEMBERS_COMMIT
        },
        rollback: {
          type: FETCH_MEMBERS_ROLLBACK
        }
      }
    }
  };
}

export function fetchMembersPermittedForItemId(itemId) {
  return {
    type: FETCH_MEMBERS_PERMITTED_FOR_ITEM_ID,
    meta: {
      offline: {
        effect: {
          url: `v1/items/${itemId}/permissions`,
          method: 'GET'
        },
        commit: {
          type: FETCH_MEMBERS_PERMITTED_FOR_ITEM_ID_COMMIT
        },
        rollback: {
          type: FETCH_MEMBERS_PERMITTED_FOR_ITEM_ID_ROLLBACK
        }
      }
    }
  };
}

export function fetchMembersPermittedForTestId(testId) {
  return {
    type: FETCH_MEMBERS_PERMITTED_FOR_TEST_ID,
    meta: {
      offline: {
        effect: {
          url: `v1/tests/${testId}/permissions`,
          method: 'GET'
        },
        commit: {
          type: FETCH_MEMBERS_PERMITTED_FOR_TEST_ID_COMMIT
        },
        rollback: {
          type: FETCH_MEMBERS_PERMITTED_FOR_TEST_ID_ROLLBACK
        }
      }
    }
  };
}

export function fetchMembersPermittedForSurveyId(surveyId) {
  return {
    type: FETCH_MEMBERS_PERMITTED_FOR_SURVEY_ID,
    meta: {
      offline: {
        effect: {
          url: `v1/surveys/${surveyId}/permissions`,
          method: 'GET'
        },
        commit: {
          type: FETCH_MEMBERS_PERMITTED_FOR_SURVEY_ID_COMMIT
        },
        rollback: {
          type: FETCH_MEMBERS_PERMITTED_FOR_SURVEY_ID_ROLLBACK
        }
      }
    }
  };
}
