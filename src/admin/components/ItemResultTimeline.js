import React from 'react';
import PropTypes from 'prop-types';
import { List } from 'immutable';
import { withStyles } from '@material-ui/core/styles';

import ItemResultTimelineComments from './ItemResultTimelineComments';
import ItemResultTimelineActions from './ItemResultTimelineActions';

const styles = theme => ({
  root: {
    display: 'flex',
    background: '#424242',
    height: 30,
    position: 'relative',
    width: '100%',
    overflow: 'hidden'
  },
  timeIndicator: {
    position: 'absolute',
    width: 2,
    height: 30,
    background: 'white'
  },
  commentIndicator: {
    position: 'absolute',
    width: 2,
    height: 30,
    background: 'white',
    opacity: 0.4
  }
});

class Timeline extends React.PureComponent {
  static propTypes = {
    time: PropTypes.number,
    endTime: PropTypes.number,
    commentDuration: PropTypes.number,
    commentCategory: PropTypes.string,
    comments: PropTypes.instanceOf(List),
    onTimeChange: PropTypes.func,
    actions: PropTypes.instanceOf(List)
  };
  static defaultProps = {
    time: 0,
    endTime: 0,
    comments: new List([]),
    commentDuration: 0,
    commentCategory: '#000',
    onTimeChange: () => {},
    actions: new List([])
  };

  state = {
    scale: 1
  };

  componentDidMount() {
    window.addEventListener('resize', this.onResize);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.onResize);
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      scale: nextProps.endTime / (this.root.clientWidth - 2)
    });
  }

  rootRef = ref => {
    this.root = ref;
    this.onResize();
  };

  onResize = e => {
    if (this.props.endTime && this.root) {
      this.setState({
        scale: this.props.endTime / (this.root.clientWidth - 2)
      });
    }
  };

  onTimeClick = e => {
    this.props.onTimeChange(e.nativeEvent.offsetX * this.state.scale);
  };
  render() {
    const {
      classes,
      comments,
      commentCategory,
      commentDuration,
      time,
      actions
    } = this.props;
    const scale = this.state.scale;
    return (
      <div
        ref={this.rootRef}
        className={classes.root}
        onClick={this.onTimeClick}
      >
        <ItemResultTimelineComments comments={comments} scale={scale} />
        <ItemResultTimelineActions actions={actions} scale={scale} />
        <div
          className={classes.timeIndicator}
          style={{
            left: time / scale,
            pointerEvents: 'none'
          }}
        />
        <div
          className={classes.commentIndicator}
          style={{
            left: time / scale,
            width: commentDuration / scale,
            backgroundColor: commentCategory,
            pointerEvents: 'none'
          }}
        />
      </div>
    );
  }
}

export default withStyles(styles)(Timeline);
