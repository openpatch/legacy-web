/**
 *
 * MembersPermittedPopup
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { List as IList } from 'immutable';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import IconButton from '@material-ui/core/IconButton';
import Button from '@material-ui/core/Button';
import DeleteIcon from '@material-ui/icons/Delete';
import AddIcon from '@material-ui/icons/Add';

import Avatar from '../../common/components/Avatar';
import PureTextField from '../../common/components/PureTextField';

class MembersPermittedPopup extends React.PureComponent {
  static propTypes = {
    members: PropTypes.instanceOf(IList),
    open: PropTypes.bool,
    onMemberDelete: PropTypes.func,
    onMemberAdd: PropTypes.func,
    onClose: PropTypes.func,
    errorAdding: PropTypes.bool
  };

  static defaultProps = {
    members: new IList([]),
    onMemberDelete: null,
    onMemberAdd: null,
    open: false,
    onClose: () => {},
    errorAdding: false
  };

  state = {
    addTextField: ''
  };

  onAddTextFieldChange = e => {
    const addTextField = e.currentTarget.value;
    this.setState({
      addTextField
    });
  };

  render() {
    const {
      members,
      open,
      errorAdding,
      onMemberAdd,
      onMemberDelete,
      onClose
    } = this.props;
    return (
      <Dialog
        open={open}
        onClose={onClose}
        aria-labelledby="member-permissions-popup"
      >
        <DialogTitle id="member-permissions-popup">
          Configure Member Permissions
        </DialogTitle>
        <List>
          {members.map(
            member =>
              member && (
                <ListItem key={member.get('id')}>
                  <ListItemAvatar>
                    <Avatar
                      role={member.get('role')}
                      name={member.get('username')}
                    />
                  </ListItemAvatar>
                  <ListItemText
                    primary={member.get('username')}
                    secondary={member.get('role')}
                  />
                  {onMemberDelete && (
                    <ListItemSecondaryAction>
                      <IconButton
                        aria-label="Delete"
                        onClick={() => onMemberDelete(member.get('id'))}
                      >
                        <DeleteIcon />
                      </IconButton>
                    </ListItemSecondaryAction>
                  )}
                </ListItem>
              )
          )}
        </List>
        {onMemberAdd && (
          <DialogContent>
            <PureTextField
              error={errorAdding}
              label="Username"
              margin="normal"
              value={this.state.addTextField}
              onChange={this.onAddTextFieldChange}
            />
            <IconButton
              aria-label="Add"
              onClick={() => onMemberAdd(this.state.addTextField)}
            >
              <AddIcon />
            </IconButton>
          </DialogContent>
        )}
        <DialogActions>
          <Button size="small" onClick={onClose}>
            Close
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

export default MembersPermittedPopup;
