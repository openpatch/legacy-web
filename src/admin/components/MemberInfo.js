/**
 *
 * MemberCard
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Map } from 'immutable';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import IconButton from '@material-ui/core/IconButton';
import AlternateEmailIcon from '@material-ui/icons/AlternateEmail';
import Paper from '@material-ui/core/Paper';
import Tooltip from '@material-ui/core/Tooltip';

import {
  PureListItem,
  PureListItemText,
  PureList
} from '../../common/components/PureList';
import Avatar from '../../common/components/Avatar';

class MemberInfo extends React.PureComponent {
  static propTypes = {
    member: PropTypes.instanceOf(Map)
  };

  static defaultProps = {
    member: new Map({})
  };

  onMailTo = () => {
    const { member } = this.props;
    window.location.href = `mailto:${member.get('email')}`;
  };

  render() {
    const { member } = this.props;
    return (
      <Paper>
        <PureList>
          <PureListItem>
            <Avatar name={member.get('username')} role={member.get('role')} />
            <PureListItemText
              primary={member.get('username')}
              secondary={member.get('role')}
            />
            {member.get('email') && (
              <ListItemSecondaryAction>
                <Tooltip title="Contact via E-Mail">
                  <IconButton onClick={this.onMailTo}>
                    <AlternateEmailIcon />
                  </IconButton>
                </Tooltip>
              </ListItemSecondaryAction>
            )}
          </PureListItem>
          <PureListItem>
            {member.get('first_name')} {member.get('last_name')}
          </PureListItem>
        </PureList>
      </Paper>
    );
  }
}

export default MemberInfo;
