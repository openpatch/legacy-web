/**
 *
 * ItemPreview
 *
 */

import React from 'react';
import { connect } from 'react-redux';
import { Map, List, fromJS } from 'immutable';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import { createStructuredSelector } from 'reselect';
import isEmpty from 'lodash/isEmpty';
import green from '@material-ui/core/colors/green';

import commonMessages from '../messages/common';
import ItemLogger from './ItemLogger';
import formats from '../../formats';
import ItemRenderer, {
  itemReducer
} from '../../common/components/ItemRenderer';
import { makeEvaluationSelector } from '../selectors/items';

const styles = theme => ({
  logger: {},
  wrapper: {
    display: 'flex',
    [theme.breakpoints.down('lg')]: {
      flexDirection: 'column',
      margin: 0
    }
  },
  assignment: {
    marginTop: theme.spacing.unit,
    padding: theme.spacing.unit
  },
  item: {
    padding: theme.spacing.unit,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    marginBottom: theme.spacing.unit,
    boxSizing: 'border-box'
  },
  title: {
    marginTop: theme.spacing.unit,
    marginBottom: theme.spacing.unit
  },
  content: {
    flex: 1,
    marginRight: theme.spacing.unit * 2,
    [theme.breakpoints.down('lg')]: {
      marginRight: 0
    }
  },
  sidebar: {
    flexShrink: 0,
    flexBasis: 300
  },
  evaluationFailed: {
    backgroundColor: theme.palette.error.main
  },
  evaluationSuccess: {
    backgroundColor: green[500]
  }
});

class ItemPreview extends React.PureComponent {
  static propTypes = {
    classes: PropTypes.object.isRequired,
    solution: PropTypes.object,
    evaluation: PropTypes.object,
    format: PropTypes.string.isRequired,
    showLog: PropTypes.bool,
    onStateSubmit: PropTypes.func,
    onStateEvaluate: PropTypes.func,
    showHeaders: PropTypes.bool
  };

  static defaultProps = {
    onStateSubmit: undefined,
    onStateEvaluate: undefined,
    solution: {},
    evaluation: {},
    showLog: true,
    showHeaders: true
  };

  constructor(props) {
    super(props);
    const { format } = this.props;
    const { reducer } = formats[format] || {};
    this.state = {
      state: reducer
        ? reducer(
            isEmpty(props.solution) ? undefined : fromJS(props.solution),
            {}
          )
        : fromJS({}),
      actionsHistory: new List([])
    };
  }

  dispatch = action => {
    const { state, actionsHistory } = this.state;
    const { format } = this.props;
    const reducer = formats[format].reducer;
    const nextState = itemReducer(state, action, reducer);

    this.setState({
      state: nextState,
      actionsHistory: actionsHistory.push({
        prevState: state,
        nextState,
        timestamp: Date.now(),
        action
      })
    });
  };

  onStateSubmit = () => {
    const { format, onStateSubmit } = this.props;
    const { state } = this.state;
    const pureSolutionSelector = formats[format].selectors;
    const solution = pureSolutionSelector(state.delete('note'));
    onStateSubmit(solution.toJS());
  };

  onStateEvaluate = () => {
    const { format, onStateEvaluate } = this.props;
    const { state } = this.state;
    const pureSolutionSelector = formats[format].selectors;
    const solution = pureSolutionSelector(state.delete('note'));
    onStateEvaluate(solution.toJS());
  };

  onStateReset = () => {
    const { onStateSubmit } = this.props;
    this.setState(
      {
        state: new Map({})
      },
      () => onStateSubmit(new Map({}))
    );
  };

  render() {
    const {
      item,
      format,
      classes,
      onStateSubmit,
      evaluation,
      onStateEvaluate,
      showLog,
      showHeaders
    } = this.props;
    const { state, actionsHistory } = this.state;
    if (!(format in formats)) {
      return <div>Unsupported</div>;
    }
    return (
      <div className={classes.wrapper}>
        <div className={classes.content}>
          {showHeaders && (
            <Typography className={classes.title} type="display1">
              <FormattedMessage {...commonMessages.preview} />
            </Typography>
          )}
          <ItemRenderer item={item} dispatch={this.dispatch} state={state} />
          {onStateSubmit && (
            <Button variant="contained" onClick={this.onStateSubmit}>
              Set Solution
            </Button>
          )}
          {onStateSubmit && (
            <Button style={{}} variant="contained" onClick={this.onStateReset}>
              Reset Solution
            </Button>
          )}
          {onStateEvaluate && (
            <Button
              className={
                evaluation.getIn(['evaluation', 'correct']) !== undefined
                  ? evaluation.getIn(['evaluation', 'correct'])
                    ? classes.evaluationSuccess
                    : classes.evaluationFailed
                  : null
              }
              variant="contained"
              disabled={evaluation.get('evaluating')}
              onClick={this.onStateEvaluate}
            >
              Evaluate
            </Button>
          )}
        </div>
        {showLog && (
          <div className={classes.sidebar}>
            {showHeaders && (
              <Typography className={classes.title} type="display1">
                <FormattedMessage {...commonMessages.log} />
              </Typography>
            )}
            <div className={classes.logger}>
              <ItemLogger actions={actionsHistory.toJS()} />
            </div>
          </div>
        )}
      </div>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  evaluation: makeEvaluationSelector()
});

export default connect(mapStateToProps)(withStyles(styles)(ItemPreview));
