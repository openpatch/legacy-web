import React from 'react';
import TextField from '@material-ui/core/TextField';

class DatePickers extends React.PureComponent {
  render() {
    return (
      <TextField
        id="date"
        {...this.props}
        type="date"
        InputLabelProps={{
          shrink: true
        }}
      />
    );
  }
}

export default DatePickers;
