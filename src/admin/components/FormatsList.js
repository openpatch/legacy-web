import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import isEqual from 'lodash/isEqual';

import FormatEntry from './FormatChip';

const styles = theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap'
  }
});

export class FormatsList extends React.Component {
  static propTypes = {
    classes: PropTypes.object.isRequired,
    formats: PropTypes.arrayOf(
      PropTypes.shape({
        type: PropTypes.string,
        color_hex: PropTypes.string,
        code: PropTypes.string
      })
    ),
    active: PropTypes.object,
    onChange: PropTypes.func
  };

  static defaultProps = {
    formats: [],
    active: {},
    onChange: () => {}
  };

  shouldComponentUpdate(nextProps) {
    return !(
      isEqual(nextProps.formats, this.props.formats) &&
      isEqual(nextProps.active, this.props.active)
    );
  }

  render() {
    const { classes, active, onChange, formats } = this.props;
    return (
      <div className={classes.root}>
        {formats.map(format => (
          <FormatEntry
            onClick={() => onChange(format.type)}
            active={format.type === active.type}
            format={format.type}
            color={`#${format.color_hex}`}
            key={format.code}
          />
        ))}
      </div>
    );
  }
}

export default withStyles(styles)(FormatsList);
