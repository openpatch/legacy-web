/**
 *
 * FavoriteCard
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Map } from 'immutable';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import Tooltip from '@material-ui/core/Tooltip';

import FavoriteIcon from './FavoriteIcon';

const styles = theme => ({
  container: {
    padding: theme.spacing.unit
  },
  nameContainer: {
    display: 'flex',
    alignItems: 'center'
  },
  name: {
    cursor: 'pointer'
  },
  favIcon: {
    paddingRight: theme.spacing.unit,
    cursor: 'pointer'
  }
});

class FavoriteCard extends React.Component {
  static propTypes = {
    classes: PropTypes.object.isRequired,
    name: PropTypes.string.isRequired,
    onClick: PropTypes.func,
    onToggleFavorite: PropTypes.func,
    stats: PropTypes.instanceOf(Map),
    children: PropTypes.element
  };

  static defaultProps = {
    onClick: () => {},
    onToggleFavorite: () => {}
  };

  render() {
    const { classes, children, name, onClick, onToggleFavorite } = this.props;
    return (
      <Paper className={classes.container}>
        <Typography className={classes.nameContainer}>
          <Tooltip title="Remove from favorites">
            <FavoriteIcon
              onClick={onToggleFavorite}
              isFavorized={true}
              className={classes.favIcon}
            />
          </Tooltip>
          <Tooltip title="Open">
            <span
              className={classes.name}
              onClick={onClick}
              tabIndex={0}
              role="button"
            >
              {name}
            </span>
          </Tooltip>
        </Typography>
        {children}
      </Paper>
    );
  }
}

export default withStyles(styles)(FavoriteCard);
