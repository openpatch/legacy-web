/* eslint-disable flowtype/require-valid-file-annotation */
/* eslint-disable react/no-array-index-key */

import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import MenuItem from '@material-ui/core/MenuItem';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import Autosuggest from 'react-autosuggest';
import match from 'autosuggest-highlight/match';
import parse from 'autosuggest-highlight/parse';

function renderInput(inputProps) {
  const { classes, inputRef = () => {}, ref, ...other } = inputProps;

  return (
    <TextField
      fullWidth
      label="Test"
      InputProps={{
        inputRef: node => {
          ref(node);
          inputRef(node);
        },
        classes: {
          input: classes.input
        }
      }}
      {...other}
    />
  );
}

function renderSuggestion(suggestion, { query, isHighlighted }) {
  const matches = match(suggestion.label, query);
  const parts = parse(suggestion.label, matches);

  return (
    <MenuItem selected={isHighlighted} component="div">
      <div>
        {parts.map((part, index) => {
          return part.highlight ? (
            <span key={index} style={{ fontWeight: 500 }}>
              {part.text}
            </span>
          ) : (
            <strong key={index} style={{ fontWeight: 300 }}>
              {part.text}
            </strong>
          );
        })}
      </div>
    </MenuItem>
  );
}

const renderSuggestionsContainer = options => {
  const { containerProps, children } = options;

  return (
    <Paper square {...containerProps}>
      {children}
    </Paper>
  );
};

function getSuggestionValue(suggestion) {
  return suggestion.label;
}

function getSuggestions(value, suggestions) {
  const inputValue = value.trim().toLowerCase();
  const inputLength = inputValue.length;
  let count = 0;

  return inputLength === 0
    ? []
    : suggestions.filter(suggestion => {
        const keep =
          count < 15 &&
          suggestion.label.toLowerCase().slice(0, inputLength) === inputValue;

        if (keep) {
          count += 1;
        }

        return keep;
      });
}

const styles = theme => ({
  container: {
    position: 'relative',
    zIndex: 99
  },
  suggestion: {
    display: 'block'
  },
  suggestionsList: {
    margin: 0,
    padding: 0,
    listStyleType: 'none'
  },
  suggestionsContainerOpen: {
    position: 'absolute',
    zIndex: 1,
    marginTop: theme.spacing.unit,
    left: 0,
    right: 0
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 200
  }
});

class IntegrationAutosuggest extends React.PureComponent {
  popperNode = null;

  static propTypes = {
    value: PropTypes.string,
    suggestions: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.number,
        label: PropTypes.string
      })
    )
  };

  state = {
    single: '',
    value: '',
    popper: '',
    suggestions: []
  };

  componentWillReceiveProps(nextProps) {
    let newValue = '';
    for (let suggestion of nextProps.suggestions) {
      if (suggestion.id === nextProps.value) {
        newValue = suggestion.label;
      }
    }
    if (newValue !== this.state.value) {
      this.setState({
        value: newValue
      });
    }
  }

  handleSuggestionsFetchRequested = ({ value }, suggestions) => {
    this.setState({
      suggestions: getSuggestions(value, suggestions)
    });
  };

  handleSuggestionsClearRequested = () => {
    this.setState({
      suggestions: []
    });
  };

  handleChange = (event, { newValue }) => {
    this.setState({
      value: newValue
    });
  };

  render() {
    const { classes, suggestions, onSuggestionSelected, value } = this.props;

    return (
      <Autosuggest
        inputProps={{
          classes,
          placeholder: 'Search for a test',
          value: this.state.value,
          onChange: this.handleChange,
          inputRef: node => {
            this.popperNode = node;
          },
          InputLabelProps: {
            shrink: true
          }
        }}
        theme={{
          container: classes.container,
          suggestionsList: classes.suggestionsList,
          suggestion: classes.suggestion,
          suggestionsContainerOpen: classes.suggestionsContainerOpen
        }}
        onSuggestionSelected={onSuggestionSelected}
        onSuggestionsClearRequested={this.handleSuggestionsClearRequested}
        renderInputComponent={renderInput}
        suggestions={this.state.suggestions}
        onSuggestionsFetchRequested={e =>
          this.handleSuggestionsFetchRequested(e, suggestions)
        }
        renderSuggestionsContainer={renderSuggestionsContainer}
        getSuggestionValue={getSuggestionValue}
        renderSuggestion={renderSuggestion}
      />
    );
  }
}

IntegrationAutosuggest.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(IntegrationAutosuggest);
