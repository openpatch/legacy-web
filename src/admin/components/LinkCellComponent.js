import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import Button from '@material-ui/core/Button';
import { Table } from '@devexpress/dx-react-grid-material-ui';

const LinkCellComponent = ({ value, column, row, ...props }) => (
  <Table.Cell {...props}>
    <Button size="small" component={Link} to={column.getLink(row)}>
      {value}
    </Button>
  </Table.Cell>
);

LinkCellComponent.propTypes = {
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  column: PropTypes.shape({
    getLink: PropTypes.func.isRequired
  }),
  row: PropTypes.object
};

LinkCellComponent.defaultProps = {
  value: ''
};

export default LinkCellComponent;
