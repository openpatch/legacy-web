/**
 *
 * ItemCompactCardList
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { List } from 'immutable';
import { withStyles } from '@material-ui/core/styles';

import ItemCompactCardDrag from './ItemCompactCardDrag';

const styles = theme => ({
  container: {
    display: 'flex'
  }
});

class ItemCompactCardDragList extends React.PureComponent {
  static propTypes = {
    classes: PropTypes.object.isRequired,
    items: PropTypes.instanceOf(List),
    prefix: PropTypes.number,
    onPreviewItem: PropTypes.func,
    onDeleteItem: PropTypes.func
  };

  render() {
    const { classes, items, prefix, onPreviewItem, onDeleteItem } = this.props;
    return (
      <div className={classes.container}>
        {items.map((item, k) => (
          <ItemCompactCardDrag
            item={item}
            index={k}
            key={k}
            draggableId={prefix === undefined ? `${prefix}_${k}` : k}
            onPreview={onPreviewItem}
            onDelete={onDeleteItem}
          />
        ))}
      </div>
    );
  }
}

export default withStyles(styles)(ItemCompactCardDragList);
