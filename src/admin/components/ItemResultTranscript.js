/**
 *
 * ItemResultTranscript
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { List as IList } from 'immutable';
import List from '@material-ui/core/List';

import ItemResultTranscriptEntry from './ItemResultTranscriptEntry';

class ItemResultTranscript extends React.PureComponent {
  static propTypes = {
    actions: PropTypes.instanceOf(IList),
    onClick: PropTypes.func.isRequired,
    currentAction: PropTypes.number
  };

  static defaultProps = {
    actions: new IList([]),
    currentAction: -1
  };

  render() {
    const { actions, onClick, currentAction } = this.props;
    return (
      <List>
        {actions.map((action, i) => (
          <ItemResultTranscriptEntry
            highlight={currentAction === i}
            onClick={onClick}
            key={action.get('time')}
            {...action.toJS()}
          />
        ))}
      </List>
    );
  }
}

export default ItemResultTranscript;
