import React from 'react';
import PropTypes from 'prop-types';
import { Map, List } from 'immutable';
import { withStyles } from '@material-ui/core/styles';

import Timeline from './ItemResultTimeline';
import PlaybackControls from './ItemResultPlaybackControls';
import ActionConstantsLegend from './ActionConstantsLegend';
import ItemRenderer from '../../common/components/ItemRenderer';

const styles = theme => ({
  control: {
    background: '#212121'
  }
});

class ItemResultPlayer extends React.PureComponent {
  static propTypes = {
    classes: PropTypes.object.isRequired,
    actions: PropTypes.instanceOf(List),
    item: PropTypes.instanceOf(Map).isRequired,
    state: PropTypes.instanceOf(Map),
    commentDuration: PropTypes.number,
    commentCategory: PropTypes.string,
    comments: PropTypes.instanceOf(List),
    startDate: PropTypes.number,
    time: PropTypes.number,
    endTime: PropTypes.number,
    playbackStatus: PropTypes.string,
    onTimeChange: PropTypes.func,
    onPlay: PropTypes.func,
    onPause: PropTypes.func,
    onStop: PropTypes.func
  };

  static defaultProps = {
    actions: new List([]),
    commentDuration: 0,
    commentCategory: '#000',
    comments: new List([]),
    time: 0,
    endTime: 0,
    state: new Map({}),
    playbackStatus: 'stop',
    onTimeChange: () => {},
    onPlay: () => {},
    onPause: () => {},
    onStop: () => {},
    onSpeedChange: () => {}
  };

  dispatch = () => {};

  render() {
    const {
      actions,
      classes,
      item,
      state,
      time,
      endTime,
      commentDuration,
      commentCategory,
      comments,
      speed,
      playbackStatus,
      onTimeChange,
      onPlay,
      onPause,
      onStop,
      onSpeedChange
    } = this.props;
    return (
      <div>
        <div className={classes.control}>
          <PlaybackControls
            onPlay={onPlay}
            onPause={onPause}
            onStop={onStop}
            playbackStatus={playbackStatus}
            onSpeedChange={onSpeedChange}
            time={time}
            speed={speed}
            endTime={endTime}
          />
          <Timeline
            comments={comments}
            commentDuration={commentDuration}
            commentCategory={commentCategory}
            time={time}
            endTime={endTime}
            onTimeChange={onTimeChange}
            actions={actions}
          />
          <ActionConstantsLegend
            constants={actions
              .map(action => action.getIn(['action', 'type']))
              .toSet()
              .toJS()}
          />
        </div>
        <ItemRenderer item={item} state={state} dispatch={this.dispatch} />
      </div>
    );
  }
}

export default withStyles(styles)(ItemResultPlayer);
