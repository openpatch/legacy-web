import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import format from 'date-fns/format';

const styles = theme => ({
  wrapper: {
    marginBottom: theme.spacing.unit
  },
  entry: {
    padding: theme.spacing.unit,
    boxSizing: 'border-box',
    wordWrap: 'break-word'
  },
  timestamp: {
    color: 'grey',
    float: 'right'
  }
});

export class ActionLogEntry extends React.PureComponent {
  static propTypes = {
    classes: PropTypes.object.isRequired,
    action: PropTypes.object.isRequired,
    nextState: PropTypes.object,
    prevState: PropTypes.object,
    timestamp: PropTypes.number.isRequired
  };

  render() {
    const { action, timestamp, classes } = this.props;
    const { type, ...payload } = action;
    return (
      <div className={classes.wrapper}>
        <Paper className={classes.entry}>
          <Typography type="caption">
            <span>
              <strong>{type.split('.').pop()}</strong>
            </span>
            <span className={classes.timestamp}>
              {format(timestamp, 'mm:ss')}
            </span>
          </Typography>
          <Typography type="caption">
            {JSON.stringify(payload, null, 2)}
          </Typography>
        </Paper>
      </div>
    );
  }
}

export default withStyles(styles)(ActionLogEntry);
