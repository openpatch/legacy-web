/**
 *
 * ItemForm
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Map, fromJS } from 'immutable';
import { FormattedMessage } from 'react-intl';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Switch from '@material-ui/core/Switch';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Button from '@material-ui/core/Button/Button';

import ResponseFieldForm from './ReponseFieldForm';
import MarkdownEditor from './MarkdownEditor';
import formats from '../../formats';
import formatsMessages from '../messages/formats';

const styles = theme => ({
  title: {
    marginTop: theme.spacing.unit,
    marginBottom: theme.spacing.unit
  },
  wrapper: {
    display: 'flex',
    flexDirection: 'column'
  },
  form: {
    padding: theme.spacing.unit
  }
});

class ItemForm extends React.PureComponent {
  static propTypes = {
    format: PropTypes.string,
    name: PropTypes.string.isRequired,
    assignment: PropTypes.string,
    content: PropTypes.instanceOf(Map).isRequired,
    responses: PropTypes.instanceOf(Map)
  };

  static defaultProps = {
    responses: new Map({}),
    format: 'common',
    assignment: ''
  };

  constructor(props) {
    super(props);
    const { format } = props;
    if (!formats.hasOwnProperty(format)) {
      this.state = {
        unsupported: true
      };
    } else {
      this.state = {
        unsupported: false
      };
    }
  }

  onNameChange = e => {
    const name = e.currentTarget.value;
    this.props.onChange(['name'], name);
  };

  onAssignmentChange = value => {
    this.props.onChange(['assignment'], value);
  };

  onAllowNoteChange = (e, checked) => {
    this.props.onChange(['allow_note'], checked);
  };

  onResponseChange = (value, key, id) => {
    this.props.onChange(['responses', id, key], value);
  };

  onResponseTypeChange = (type, id) => {
    this.props.onChange(
      ['responses', id],
      fromJS({
        type
      })
    );
  };

  onPrivateContentChange = content => {
    this.props.onChange(['content', 'private'], content);
  };

  onPublicContentChange = content => {
    this.props.onChange(['content', 'public'], content);
  };

  onResponseDelete = id => {
    const { onChange, responses } = this.props;
    onChange(['responses'], responses.delete(id));
  };

  addResponse = () => {
    const { onChange, responses } = this.props;
    onChange(
      ['responses'],
      responses.set(
        responses.size,
        fromJS({
          type: 'multiple-choice',
          label: ''
        })
      )
    );
  };

  render() {
    const {
      format,
      name,
      allowNote,
      assignment,
      content,
      responses,
      classes
    } = this.props;
    const { unsupported } = this.state;
    if (unsupported) {
      return <FormattedMessage {...formatsMessages.unsupported} />;
    }
    const Form = formats[format].Form;
    return (
      <div className={classes.wrapper}>
        <Paper className={classes.form}>
          <Typography type="headline">Meta</Typography>
          <TextField
            label="Name"
            fullWidth
            margin="normal"
            value={name}
            onChange={this.onNameChange}
          />
          <FormControlLabel
            control={
              <Switch checked={allowNote} onChange={this.onAllowNoteChange} />
            }
            label="Allow Note"
          />
          <Typography>Assignment</Typography>
          <MarkdownEditor
            value={assignment || ''}
            height="200px"
            onChange={this.onAssignmentChange}
          />
          <Form
            onPublicChange={this.onPublicContentChange}
            onPrivateChange={this.onPrivateContentChange}
            publicData={content.get('public', new Map({}))}
            privateData={content.get('private', new Map({}))}
          />
          {responses &&
            responses
              .map((response, i) => (
                <ResponseFieldForm
                  {...response.toJS()}
                  key={i}
                  onDelete={() => this.onResponseDelete(i)}
                  onChange={(key, value) =>
                    this.onResponseChange(value, key, i)
                  }
                  onTypeChange={type => this.onResponseTypeChange(type, i)}
                />
              ))
              .toList()}
          <Button variant="contained" onClick={this.addResponse}>
            Add Response
          </Button>
        </Paper>
      </div>
    );
  }
}

export default withStyles(styles)(ItemForm);
