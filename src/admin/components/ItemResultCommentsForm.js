/**
 *
 * ItemResultCommentsForm
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';
import ColorPicker from '../../common/components/ColorPicker';

const styles = theme => ({
  root: {
    display: 'flex',
    alignItems: 'baseline'
  },
  time: {
    width: 100,
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit
  },
  comment: {
    flex: 1,
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit
  },
  category: {
    width: 150,
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit
  }
});

class ItemResultCommentsForm extends React.Component {
  static propTypes = {
    classes: PropTypes.object.isRequired,
    time: PropTypes.number,
    comment: PropTypes.string,
    reply: PropTypes.bool,
    duration: PropTypes.number,
    category: PropTypes.string,
    onSubmit: PropTypes.func,
    onChange: PropTypes.func
  };

  static defaultProps = {
    comment: '',
    duration: 0,
    category: '',
    reply: false,
    onSubmit: () => {},
    onChange: () => {}
  };

  onChangeComment = event => {
    const comment = event.target.value;
    const { onChange } = this.props;
    onChange('comment', comment);
  };

  onChangeDuration = event => {
    const duration = Number(event.target.value);
    const { onChange } = this.props;
    onChange('duration', duration);
  };

  onChangeCategory = color => {
    const { onChange } = this.props;
    onChange('category', color);
  };

  onChangeTime = event => {
    const time = Number(event.target.value);
    const { onChange } = this.props;
    onChange('time', time);
  };
  render() {
    const {
      classes,
      reply,
      time,
      onSubmit,
      comment,
      duration,
      category
    } = this.props;
    return (
      <div className={classes.root}>
        {!reply && (
          <ColorPicker onChange={this.onChangeCategory} color={category} />
        )}
        <TextField
          className={classes.comment}
          label="Comment"
          value={comment}
          onChange={this.onChangeComment}
          margin="normal"
        />
        {!reply && (
          <React.Fragment>
            <TextField
              className={classes.time}
              label="Time (ms)"
              type="number"
              value={Math.round(time)}
              onChange={this.onChangeTime}
              margin="normal"
            />
            <TextField
              className={classes.time}
              label="Duration (ms)"
              value={duration}
              type="number"
              onChange={this.onChangeDuration}
              margin="normal"
            />
          </React.Fragment>
        )}
        <Button color="primary" onClick={onSubmit}>
          Comment
        </Button>
      </div>
    );
  }
}

export default withStyles(styles)(ItemResultCommentsForm);
