/**
 *
 * ActionConstantsLegend
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import ActionIndicator from './ActionIndicator';

const styles = theme => ({
  constants: {
    display: 'flex'
  },
  constant: {
    display: 'flex',
    color: 'white',
    fontFamily: 'Roboto',
    margin: theme.spacing.unit
  }
});

class ActionConstantsLegend extends React.Component {
  static propTypes = {
    classes: PropTypes.object.isRequired,
    constants: PropTypes.array
  };

  static defaultProps = {
    constants: []
  };

  shouldComponentUpdate(nextProps) {
    return this.props.constants.length !== nextProps.constants.length;
  }

  render() {
    const { classes, constants } = this.props;
    return (
      <div className={classes.constants}>
        {constants.map(constant => (
          <div key={constant} className={classes.constant}>
            <ActionIndicator type={constant} style={{ marginRight: 4 }} />
            {constant
              .split('.')
              .slice(-1)
              .pop()}
          </div>
        ))}
      </div>
    );
  }
}

export default withStyles(styles)(ActionConstantsLegend);
