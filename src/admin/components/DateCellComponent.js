import React from 'react';
import PropTypes from 'prop-types';
import formatFns from 'date-fns/format';
import isFuture from 'date-fns/is_future';
import isPast from 'date-fns/is_past';
import parse from 'date-fns/parse';
import distanceInWords from 'date-fns/distance_in_words';
import distanceInWordsToNow from 'date-fns/distance_in_words_to_now';
import { Table } from '@devexpress/dx-react-grid-material-ui';

const DateCellComponent = ({ value, style, column }) => {
  let date = parse(value);
  const {
    relativeToDateString,
    relativeToNow,
    relativeToPast,
    relativeToFuture,
    format
  } = column;
  let displayedDate;
  if (!value) {
    displayedDate = '';
  } else if (relativeToDateString) {
    displayedDate = distanceInWords(date, relativeToDateString);
  } else if (relativeToNow) {
    displayedDate = distanceInWordsToNow(date);
  } else if (relativeToFuture && isFuture(date)) {
    displayedDate = distanceInWordsToNow(date);
  } else if (relativeToPast && isPast(date)) {
    displayedDate = distanceInWordsToNow(date);
  } else {
    displayedDate = formatFns(date, format || 'DD. MMM YYYY hh:mm');
  }

  return <Table.Cell style={{ ...style }}>{displayedDate}</Table.Cell>;
};

DateCellComponent.propTypes = {
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  style: PropTypes.object,
  column: PropTypes.shape({
    relativeToPast: PropTypes.bool,
    relativeToFuture: PropTypes.bool,
    relativeToNow: PropTypes.bool,
    relativeToDateString: PropTypes.string,
    format: PropTypes.string
  }).isRequired
};

DateCellComponent.defaultProps = {
  style: {},
  value: ''
};

export default DateCellComponent;
