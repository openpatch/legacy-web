import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Chip from '@material-ui/core/Chip';
import { injectIntl } from 'react-intl';

import formats from '../../formats';

const styles = theme => ({
  root: {
    padding: theme.spacing.unit,
    display: 'flex'
  },
  chip: {
    '&:focus': {
      border: `2px solid #000!important`
    },
    color: 'white'
  }
});

export class FormatChip extends React.PureComponent {
  static propTypes = {
    classes: PropTypes.object.isRequired,
    format: PropTypes.string.isRequired,
    color: PropTypes.string,
    key: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    onClick: PropTypes.func,
    active: PropTypes.bool
  };
  static defaultProps = {
    color: 'grey',
    key: 0,
    active: false,
    onClick: () => {}
  };
  render() {
    const { classes, format, active, color, key, onClick, intl } = this.props;
    return (
      <div className={classes.root}>
        <Chip
          className={classes.chip}
          style={{
            backgroundColor: color,
            opacity: active ? 1 : 0.3
          }}
          label={intl.formatMessage(formats[format].messages.formatName)}
          key={key}
          onClick={onClick}
        />
      </div>
    );
  }
}

export default withStyles(styles)(injectIntl(FormatChip));
