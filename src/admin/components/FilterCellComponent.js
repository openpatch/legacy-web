import React from 'react';
import IconButton from '@material-ui/core/IconButton';
import { Table, TableFilterRow } from '@devexpress/dx-react-grid-material-ui';

import FavoriteIcon from './FavoriteIcon';

/**
 * FilterCellComponent. Enhanced TableFilterRow.Cell with the ability to
 * disable the filtering of columns individually. Just add `hideFilterControls:
 * true` to the column object. Also columns with the name 'favorite' will be
 * filtered by cycling through three color/stages of a heart.
 *
 * 0: Lightgrey heart (all)
 * 1: Red heart (favorized)
 * 2: Grey heart (not favorized)
 */
const FilterCellComponent = props => {
  const { hideFilterControls, name } = props.column;
  if (hideFilterControls) {
    return <Table.Cell />;
  } else if (name === 'favorite') {
    const filterValue = props.filter ? props.filter.value : 0;
    const nextFilterValue = (filterValue + 1) % 3;
    let fill = 'lightgrey';
    if (filterValue === 1) {
      fill = 'red';
    } else if (filterValue === 2) {
      fill = undefined;
    }
    return (
      <Table.Cell>
        <IconButton
          tooltip="Favorize"
          onClick={() =>
            props.onFilter({ columnName: 'favorite', value: nextFilterValue })
          }
        >
          <FavoriteIcon
            style={{
              fill
            }}
          />
        </IconButton>
      </Table.Cell>
    );
  } else {
    return <TableFilterRow.Cell {...props} />;
  }
};

/**
 * Predicate for the FilterCellComponent to filter the favorite column
 * @param {object} filter object which contains a value property
 * @param {object} row object which contains an id property
 * @param {array} favorites Array of ids
 */
const favoritePredicate = (filter, row, favorites) => {
  const isFavorite = favorites.indexOf(row.id) > -1;
  if (filter.value === 0) {
    return true;
  }
  return isFavorite === (filter.value === 1);
};

export { favoritePredicate };

export default FilterCellComponent;
