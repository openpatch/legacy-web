import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import MenuItem from '@material-ui/core/MenuItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import IconButton from '@material-ui/core/IconButton';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import {
  PagingState,
  IntegratedPaging,
  SortingState,
  IntegratedSorting,
  FilteringState,
  IntegratedFiltering
} from '@devexpress/dx-react-grid';
import {
  Grid,
  Table,
  TableHeaderRow,
  PagingPanel,
  TableFilterRow
} from '@devexpress/dx-react-grid-material-ui';

import HeaderCellComponent from './HeaderCellComponent';
import FilterCellComponent, { favoritePredicate } from './FilterCellComponent';
import FavoriteCellComponent from './FavoriteCellComponent';
import DateCellComponent from './DateCellComponent';
import LinkCellComponent from './LinkCellComponent';
import FavoriteGridTableMenu from './FavoriteGridTableMenu';

const styles = theme => ({
  table: {
    '& tr:hover': {
      backgroundColor: 'whitesmoke'
    }
  }
});

const Cell = props => {
  if (props.column.type === 'date') {
    return <DateCellComponent {...props} />;
  } else if (props.column.type === 'link') {
    return <LinkCellComponent {...props} />;
  }
  return <Table.Cell {...props} />;
};

class FavoriteGridTable extends React.Component {
  static propTypes = {
    rows: PropTypes.array,
    favorites: PropTypes.array,
    columns: PropTypes.array.isRequired,
    additionalMenuItems: PropTypes.array,
    onViewEntry: PropTypes.func,
    onDeleteEntry: PropTypes.func,
    onEditEntry: PropTypes.func,
    onCopyEntry: PropTypes.func,
    menuOpen: PropTypes.func,
    onOpenMenu: PropTypes.func,
    onCloseMenu: PropTypes.func,
    onToggleFavorite: PropTypes.func,
    columnExtensions: PropTypes.array,
    integratedFilteringColumnExtensions: PropTypes.array,
    integratedSortingExtensions: PropTypes.array,
    pageSizes: PropTypes.array
  };

  static defaultProps = {
    onOpenMenu: () => {},
    onCloseMenu: () => {},
    onToggleFavorite: () => {},
    additionalMenuItems: [],
    rows: [],
    favorites: [],
    columnExtensions: [],
    integratedFilteringColumnExtensions: [],
    integratedSortingExtensions: [],
    pageSizes: [5, 10, 25, 50, 100]
  };

  state = {
    anchorElementForMenu: undefined,
    menuOpen: false
  };

  onOpenMenu = (id, event) => {
    this.props.onOpenMenu(id);
    this.setState({
      anchorElementForMenu: event.currentTarget,
      menuOpen: true,
      menuActive: id
    });
  };

  onCloseMenu = () => {
    this.props.onCloseMenu();
    this.setState({
      menuOpen: false,
      menuActive: -1
    });
  };

  render() {
    const {
      classes,
      favorites,
      columns,
      columnExtensions,
      pageSizes,
      additionalMenuItems,
      integratedFilteringColumnExtensions,
      integratedSortingExtensions,
      onToggleFavorite,
      onViewEntry,
      onDeleteEntry,
      onEditEntry,
      onCopyEntry,
      rows
    } = this.props;
    const favoriteColumn = {
      name: 'favorite',
      title: 'Favorite',
      hideSortingControls: true,
      getCellValue: row => (
        <FavoriteCellComponent
          onClick={() => onToggleFavorite(row.id)}
          isFavorized={favorites.indexOf(row.id) > -1}
        />
      )
    };

    const menuColumn = {
      name: 'menu',
      title: ' ',
      hideSortingControls: true,
      hideFilterControls: true,
      getCellValue: row => (
        <IconButton
          tooltip="Menu"
          aria-owns={this.state.menuOpen ? 'favorite-grid-menu' : null}
          aria-haspopup="true"
          onClick={e => this.onOpenMenu(row.id, e)}
        >
          <MoreVertIcon />
        </IconButton>
      )
    };
    let enhancedColumns = [favoriteColumn, ...columns, menuColumn];

    const enhancedIntegratedSortingExtensions = [
      ...integratedSortingExtensions
    ];

    const enhancedIntegratedFilteringColumnExtensions = [
      {
        columnName: 'favorite',
        predicate: (value, filter, row) =>
          favoritePredicate(filter, row, favorites)
      },
      ...integratedFilteringColumnExtensions
    ];

    const enhancedColumnExtensions = [
      { columnName: 'favorite', width: 80, align: 'left' },
      { columnName: 'menu', width: 80, align: 'right' },
      ...columnExtensions
    ];

    return (
      <div className={classes.table}>
        <FavoriteGridTableMenu
          id="favorite-grid-menu"
          open={this.state.menuOpen}
          onView={onViewEntry}
          onClose={this.onCloseMenu}
          onDelete={onDeleteEntry}
          onEdit={onEditEntry}
          onCopy={onCopyEntry}
          entryId={this.state.menuActive}
          anchorEl={this.state.anchorElementForMenu}
        >
          {additionalMenuItems.map(menuItem => (
            <MenuItem
              key={menuItem.key}
              onClick={() => {
                this.onCloseMenu();
                menuItem.onClick();
              }}
            >
              <ListItemIcon tooltip={menuItem.tooltip}>
                <menuItem.Icon />
              </ListItemIcon>
              <ListItemText inset primary={menuItem.label} />
            </MenuItem>
          ))}
        </FavoriteGridTableMenu>
        <Grid getRowId={row => row.id} columns={enhancedColumns} rows={rows}>
          <FilteringState />
          <IntegratedFiltering
            columnExtensions={enhancedIntegratedFilteringColumnExtensions}
          />
          <SortingState />
          <IntegratedSorting
            columnExtensions={enhancedIntegratedSortingExtensions}
          />
          <PagingState defaultCurrentPage={0} defaultPageSize={10} />
          <IntegratedPaging />
          <Table
            cellComponent={Cell}
            columnExtensions={enhancedColumnExtensions}
          />
          <TableFilterRow cellComponent={FilterCellComponent} />
          <TableHeaderRow
            cellComponent={HeaderCellComponent}
            showSortingControls
          />
          <PagingPanel pageSizes={pageSizes} />
        </Grid>
      </div>
    );
  }
}

export default withStyles(styles)(FavoriteGridTable);
