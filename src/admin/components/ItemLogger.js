/**
 *
 * ItemLogger
 *
 */

import React from 'react';
import PropTypes from 'prop-types';

import ActionLogEntry from './ActionLogEntry';

class ItemLogger extends React.PureComponent {
  static propTypes = {
    actions: PropTypes.array
  };

  render() {
    const { actions } = this.props;
    let startTimestamp = 0;
    if (actions.length > 0) {
      startTimestamp = actions[0].timestamp;
    }
    return actions.map(action => (
      <ActionLogEntry
        key={action.timestamp - startTimestamp}
        {...action}
        timestamp={action.timestamp - startTimestamp}
      />
    ));
  }
}

export default ItemLogger;
