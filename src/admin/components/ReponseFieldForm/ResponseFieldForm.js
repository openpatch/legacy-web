import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import Select from '@material-ui/core/Select/Select';
import MenuItem from '@material-ui/core/MenuItem';
import Paper from '@material-ui/core/Paper/Paper';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';

import ChoiceForm from './ChoiceForm';
import MatrixForm from './MatrixForm';
import TextForm from './TextForm';
import RankingForm from './RankingForm';
import SelectForm from './SelectForm';
import PureTextField from '../../../common/components/PureTextField';

const styles = theme => ({
  container: {
    padding: theme.spacing.unit,
    margin: theme.spacing.unit,
    marginBottom: theme.spacing.unit * 2,
    '& > div': {
      marginBottom: theme.spacing.unit * 2
    }
  },
  head: {
    display: 'flex',
    alignItems: 'center'
  }
});

class ResponseFieldForm extends React.Component {
  static propTypes = {
    onChange: PropTypes.func,
    onTypeChange: PropTypes.func,
    onDelete: PropTypes.func,
    type: PropTypes.string,
    label: PropTypes.string
  };

  static defaultProps = {
    onDelete: () => {},
    onChange: () => {},
    onTypeChange: () => {},
    edit: false,
    type: 'single-choice',
    label: ''
  };

  onLabelChange = event => {
    const { onChange } = this.props;
    const value = event.target.value;
    onChange('label', value);
  };

  onTypeChange = event => {
    const { onTypeChange } = this.props;
    const value = event.target.value;
    onTypeChange(value);
  };

  render() {
    const { classes, type, onDelete, label, ...props } = this.props;
    let form = <div />;
    if (type === 'multiple-choice') {
      form = <ChoiceForm {...props} multiple={true} />;
    } else if (type === 'single-choice') {
      form = <ChoiceForm {...props} />;
    } else if (type === 'matrix') {
      form = <MatrixForm {...props} />;
    } else if (type === 'text') {
      form = <TextForm {...props} />;
    } else if (type === 'ranking') {
      form = <RankingForm {...props} />;
    } else if (type === 'select') {
      form = <SelectForm {...props} />;
    }

    return (
      <Paper className={classes.container}>
        <div className={classes.head}>
          <IconButton onClick={onDelete}>
            <DeleteIcon />
          </IconButton>
          <div>
            <Select value={type} onChange={this.onTypeChange}>
              <MenuItem value="multiple-choice">Multiple Choice</MenuItem>
              <MenuItem value="single-choice">Single Choice</MenuItem>
              <MenuItem value="matrix">Matrix</MenuItem>
              <MenuItem value="text">Text</MenuItem>
              <MenuItem value="select">Select</MenuItem>
            </Select>
          </div>
        </div>
        <PureTextField
          fullWidth
          value={label}
          label="Response Text"
          margin="normal"
          onChange={this.onLabelChange}
        />
        {form}
      </Paper>
    );
  }
}

export default withStyles(styles)(ResponseFieldForm);
