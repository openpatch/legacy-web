import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton/IconButton';
import Button from '@material-ui/core/Button';
import DeleteIcon from '@material-ui/icons/Delete';

import MarkdownEditor from '../MarkdownEditor';

const styles = theme => ({
  editor: {
    flexGrow: 1
  },
  choice: {
    display: 'flex',
    width: '100%'
  }
});

class ChoiceForm extends React.Component {
  static propTypes = {
    onChange: PropTypes.func,
    multiple: PropTypes.bool,
    choices: PropTypes.object
  };

  static defaultProps = {
    onChange: () => {},
    multiple: false,
    choices: {}
  };

  onChoiceChange = (value, id) => {
    const { choices, onChange } = this.props;
    const nextChoices = { ...choices };
    nextChoices[id] = value;
    onChange('choices', nextChoices);
  };

  onChoiceAdd = () => {
    const { choices, onChange } = this.props;
    const nextChoices = { ...choices };
    nextChoices[Object.keys(nextChoices).length] = '';
    onChange('choices', nextChoices);
  };

  onChoiceDelete = id => {
    const { choices, onChange } = this.props;
    const nextChoices = { ...choices };
    delete nextChoices[id];
    onChange('choices', nextChoices);
  };

  render() {
    const { classes, choices } = this.props;
    return (
      <div>
        {Object.values(choices).map((choice, i) => (
          <div key={i} className={classes.choice}>
            <IconButton onClick={() => this.onChoiceDelete(i)}>
              <DeleteIcon />
            </IconButton>
            <div className={classes.editor}>
              <MarkdownEditor
                height="100px"
                value={choice}
                onChange={value => this.onChoiceChange(value, i)}
              />
            </div>
          </div>
        ))}
        <Button variant="contained" onClick={this.onChoiceAdd}>
          Add Choice
        </Button>
      </div>
    );
  }
}

export default withStyles(styles)(ChoiceForm);
