import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton/IconButton';
import Button from '@material-ui/core/Button';
import DeleteIcon from '@material-ui/icons/Delete';

import PureTextField from '../../../common/components/PureTextField';

const styles = theme => ({
  editor: {
    flexGrow: 1
  },
  option: {
    display: 'flex',
    width: '100%'
  }
});

class SelectForm extends React.Component {
  static propTypes = {
    onChange: PropTypes.func,
    options: PropTypes.arrayOf(PropTypes.string)
  };

  static defaultProps = {
    onChange: () => {},
    options: []
  };

  onSelectChange = (value, id) => {
    const { options, onChange } = this.props;
    const nextSelects = [...options];
    nextSelects[id] = value;
    onChange('options', nextSelects);
  };

  onSelectAdd = () => {
    const { options, onChange } = this.props;
    const nextSelects = [...options];
    nextSelects.push('');
    onChange('options', nextSelects);
  };

  onSelectDelete = id => {
    const { options, onChange } = this.props;
    const nextSelects = [...options];
    nextSelects.splice(id, 1);
    onChange('options', nextSelects);
  };

  render() {
    const { classes, options } = this.props;
    return (
      <div>
        {options.map((option, i) => (
          <div key={i} className={classes.option}>
            <IconButton onClick={() => this.onSelectDelete(i)}>
              <DeleteIcon />
            </IconButton>
            <div className={classes.editor}>
              <PureTextField
                value={option}
                fullWidth
                onChange={e => this.onSelectChange(e.target.value, i)}
              />
            </div>
          </div>
        ))}
        <Button variant="contained" onClick={this.onSelectAdd}>
          Add Select
        </Button>
      </div>
    );
  }
}

export default withStyles(styles)(SelectForm);
