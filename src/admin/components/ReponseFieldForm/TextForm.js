import React from 'react';
import PropTypes from 'prop-types';
import Switch from '@material-ui/core/Switch';
import FormControlLabel from '@material-ui/core/FormControlLabel';

import PureTextField from '../../../common/components/PureTextField';

class Text extends React.Component {
  static propTypes = {
    onChange: PropTypes.func,
    input: PropTypes.shape({
      multiline: PropTypes.bool,
      type: PropTypes.oneOf(['number', 'text', 'email', 'password']),
      defaultValue: PropTypes.string,
      rows: PropTypes.number
    }).isRequired
  };

  static defaultProps = {
    onChange: () => {},
    input: {},
    content: {}
  };

  onChange = (key, value) => {
    const { input } = this.props;

    const nextInput = { ...input };
    nextInput[key] = value;

    this.props.onChange('input', nextInput);
  };

  render() {
    const { input } = this.props;
    return (
      <div>
        <PureTextField
          label="Default Value"
          fullWidth
          value={input.defaultValue || ''}
          onChange={e => this.onChange('defaultValue', e.target.value)}
        />
        <FormControlLabel
          control={
            <Switch
              checked={input.multiline}
              onChange={(e, checked) => this.onChange('multiline', checked)}
            />
          }
          label="Multiline"
        />
      </div>
    );
  }
}

export default Text;
