import React from 'react';
import PropTypes from 'prop-types';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Radio from '@material-ui/core/Radio/Radio';
import Checkbox from '@material-ui/core/Checkbox/Checkbox';
import IconButton from '@material-ui/core/IconButton';
import MenuItem from '@material-ui/core/MenuItem';
import DeleteIcon from '@material-ui/icons/Delete';
import AddIcon from '@material-ui/icons/Add';

import PureTextField from '../../../common/components/PureTextField';
import Select from '@material-ui/core/Select/Select';
import LabeledSwitch from '../../../common/components/LabeledSwitch';

class Matrix extends React.Component {
  static propTypes = {
    onChange: PropTypes.func,
    input: PropTypes.shape({
      type: PropTypes.oneOf(['single-choice', 'multiple-choice', 'text'])
    }),
    columns: PropTypes.arrayOf(PropTypes.string),
    rows: PropTypes.arrayOf(PropTypes.string),
    allowAdditional: PropTypes.bool
  };

  static defaultProps = {
    onChange: () => {},
    edit: false,
    columns: [],
    input: {
      type: 'single-choice'
    },
    rows: [],
    allowAdditional: false
  };

  onChange = (e, rowId, columnId) => {
    const { value, onChange, input } = this.props;
    let nextValue = { ...value };
    let nextRow = nextValue[rowId] || {};

    // standard input should be single-choice, therefore undefined is handled as single-choice
    if (input.type === 'single-choice' || input.type === undefined) {
      nextRow = {};
    }

    nextRow[columnId] = e;
    nextValue[rowId] = nextRow;
    onChange(nextValue);
  };

  renderCell = (rowId, columnId) => {
    const { input } = this.props;

    switch (input.type) {
      case 'multiple-choice':
        return <Checkbox checked={false} />;
      case 'text':
        return <PureTextField value={''} />;
      default:
        return <Radio checked={false} />;
    }
  };

  onColumnChange = (e, i) => {
    const { onChange, columns } = this.props;
    const value = e.target.value;
    const nextColumns = [...columns];
    nextColumns[i] = value;
    onChange('columns', nextColumns);
  };

  onColumnDelete = i => {
    const { onChange, columns } = this.props;
    const nextColumns = [...columns];
    nextColumns.splice(i, 1);
    onChange('columns', nextColumns);
  };

  onColumnAdd = () => {
    const { onChange, columns } = this.props;
    const nextColumns = [...columns];
    nextColumns.push('');
    onChange('columns', nextColumns);
  };

  onRowChange = (e, i) => {
    const { onChange, rows } = this.props;
    const value = e.target.value;
    const nextRows = [...rows];
    nextRows[i] = value;
    onChange('rows', nextRows);
  };

  onRowDelete = i => {
    const { onChange, rows } = this.props;
    const nextRows = [...rows];
    nextRows.splice(i, 1);
    onChange('rows', nextRows);
  };

  onRowAdd = () => {
    const { onChange, rows } = this.props;
    const nextRows = [...rows];
    nextRows.push('');
    onChange('rows', nextRows);
  };

  onTypeChange = event => {
    const { onChange } = this.props;
    const value = event.target.value;
    onChange('input', { type: value });
  };

  onToggleAllowAdditional = () => {
    const { onChange, allowAdditional } = this.props;
    onChange('allowAdditional', !allowAdditional);
  };

  render() {
    const { columns, rows, input, allowAdditional } = this.props;
    return (
      <Table>
        <TableHead>
          <TableRow>
            <TableCell>
              <Select value={input.type} onChange={this.onTypeChange}>
                <MenuItem value="single-choice">Single Choice</MenuItem>
                <MenuItem value="multiple-choice">Multiple Choice</MenuItem>
                <MenuItem value="text">Text</MenuItem>
              </Select>
            </TableCell>
            {columns.map((column, i) => (
              <TableCell key={i}>
                <IconButton onClick={() => this.onColumnDelete(i)}>
                  <DeleteIcon />
                </IconButton>
                <PureTextField
                  value={column}
                  onChange={e => this.onColumnChange(e, i)}
                />
              </TableCell>
            ))}
            <TableCell>
              <IconButton onClick={this.onColumnAdd}>
                <AddIcon />
              </IconButton>
            </TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row, i) => (
            <TableRow key={i}>
              <TableCell key={i + '.head'}>
                <IconButton onClick={() => this.onRowDelete(i)}>
                  <DeleteIcon />
                </IconButton>
                <PureTextField
                  value={row}
                  onChange={e => this.onRowChange(e, i)}
                />
              </TableCell>
              {columns.map((columns, k) => (
                <TableCell key={i + '.' + k}>{this.renderCell(i, k)}</TableCell>
              ))}
              <TableCell />
            </TableRow>
          ))}
          <TableRow>
            <TableCell>
              <IconButton onClick={this.onRowAdd}>
                <AddIcon />
              </IconButton>
            </TableCell>
            {columns.map((columns, k) => <TableCell key={k} />)}
            <TableCell />
          </TableRow>
          <TableRow>
            <TableCell>
              <LabeledSwitch
                label="Allow Additional"
                checked={allowAdditional}
                onClick={this.onToggleAllowAdditional}
              />
            </TableCell>
            {columns.map((columns, k) => <TableCell key={k} />)}
            <TableCell />
          </TableRow>
        </TableBody>
      </Table>
    );
  }
}

export default Matrix;
