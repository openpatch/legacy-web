/**
 *
 * ItemResultSolution
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Map } from 'immutable';

class ItemResultSolution extends React.PureComponent {
  static propTypes = {
    solution: PropTypes.instanceOf(Map)
  };

  static defaultProps = {
    solution: new Map({})
  };

  render() {
    const { solution } = this.props;
    return <pre>{JSON.stringify(solution.toJS(), null, 2)}</pre>;
  }
}

export default ItemResultSolution;
