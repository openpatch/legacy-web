/**
 *
 * ItemBasicStats
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Map } from 'immutable';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  container: {
    display: 'flex',
    alignItems: 'top'
  },
  stat: {
    flex: 1,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    textAlign: 'center'
  },
  statHeading: {
    fontSize: 14,
    fontWeight: 500,
    textOverflow: 'ellipsis',
    whiteSpace: 'nowrap'
  },
  statValue: {
    fontSize: 12
  }
});

class ItemBasicStats extends React.Component {
  static propTypes = {
    stats: PropTypes.instanceOf(Map),
    classes: PropTypes.object.isRequired
  };

  static defaultProps = {
    stats: new Map({})
  };
  render() {
    const { classes, stats } = this.props;
    return (
      <div className={classes.container}>
        {stats.get('num_results') !== 0 && (
          <div className={classes.stat}>
            <div className={classes.statHeading}>Ø Duration</div>
            <div className={classes.statValue}>
              <span style={{ color: 'blue' }}>{stats.get('time_min')}</span> -{' '}
              {Math.round(stats.get('time_mean'))} -{' '}
              <span style={{ color: 'red' }}>{stats.get('time_max')}</span> (in
              s)
            </div>
          </div>
        )}
        <div className={classes.stat}>
          <div className={classes.statHeading}># Results</div>
          <div className={classes.statValue}>
            {Math.round(stats.get('num_results', 0))}
          </div>
        </div>
        <div className={classes.stat}>
          <div className={classes.statHeading}># Tests</div>
          <div className={classes.statValue}>
            {Math.round(stats.get('num_tests', 0))}
          </div>
        </div>
        {stats.get('num_correct') !== undefined && (
          <div className={classes.stat}>
            <div className={classes.statHeading}># Correct</div>
            <div className={classes.statValue}>
              {Math.round(stats.get('num_correct', 0))}
            </div>
          </div>
        )}
      </div>
    );
  }
}

export default withStyles(styles)(ItemBasicStats);
