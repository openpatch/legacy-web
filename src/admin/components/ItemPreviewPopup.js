import React from 'react';
import PropTypes from 'prop-types';
import { Map } from 'immutable';
import { connect } from 'react-redux';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import withMobileDialog from '@material-ui/core/withMobileDialog';
import Button from '@material-ui/core/Button';

import ItemPreview from './ItemPreview';
import { evaluateItem } from '../actions/items';

class ItemPreviewPopup extends React.PureComponent {
  static propTypes = {
    fullScreen: PropTypes.bool.isRequired,
    onClose: PropTypes.func.isRequired,
    open: PropTypes.bool,
    item: PropTypes.instanceOf(Map)
  };

  static defaultProps = {
    open: false
  };

  onSolutionEvaluate = solution => {
    const { item, dispatch } = this.props;
    dispatch(
      evaluateItem(
        {
          ...item.toJS(),
          format: item.getIn(['format', 'type'])
        },
        solution
      )
    );
  };

  render() {
    const { open, onClose, fullScreen, item } = this.props;
    return (
      <Dialog
        open={open}
        fullScreen={fullScreen}
        onClose={onClose}
        maxWidth={false}
        aria-labelledby="item-preview-popup-title"
      >
        <DialogTitle id="item-preview-popup-title">
          {item.get('name')}
        </DialogTitle>
        <DialogContent>
          {item && (
            <ItemPreview
              item={item}
              format={item.getIn(['format', 'type'])}
              showLog={false}
              onStateEvaluate={this.onSolutionEvaluate}
              showHeaders={false}
            />
          )}
        </DialogContent>
        <DialogActions>
          <Button onClick={onClose}>Close</Button>
        </DialogActions>
      </Dialog>
    );
  }
}

export default connect()(withMobileDialog()(ItemPreviewPopup));
