/**
 *
 * ItemResultTranscriptEntry
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Typography from '@material-ui/core/Typography';
import _omit from 'lodash/omit';

import ActionIndicator from './ActionIndicator';

const styles = theme => ({
  item: {
    cursor: 'pointer',
    '&:hover': {
      backgroundColor: '#F5F5F5'
    }
  }
});

class ItemResultTranscriptEntry extends React.PureComponent {
  static propTypes = {
    classes: PropTypes.object.isRequired,
    action: PropTypes.shape({
      type: PropTypes.string.isRequired,
      payload: PropTypes.object
    }),
    time: PropTypes.number.isRequired,
    payload: PropTypes.object,
    highlight: PropTypes.bool,
    onClick: PropTypes.func.isRequired
  };

  static defaultProps = {
    payload: {},
    highlight: false
  };

  static defaultProps = {};

  render() {
    const { classes, action, time, highlight, onClick } = this.props;
    return (
      <ListItem
        className={classes.item}
        style={{
          backgroundColor: highlight ? '#F5F5F5' : null
        }}
        onClick={() => onClick(time)}
      >
        <ListItemAvatar>
          <ActionIndicator
            type={action.type}
            style={{
              marginRight: 4,
              marginTop: 0,
              border: '1px solid #000'
            }}
          />
        </ListItemAvatar>
        <ListItemText
          disableTypography={true}
          primary={
            <div style={{ display: 'flex' }}>
              <div style={{ flex: 1 }}>
                <Typography>
                  {action.type
                    .split('.')
                    .slice(-1)
                    .pop()}
                </Typography>
              </div>
              <Typography variant="caption">
                {JSON.stringify(_omit(action, ['type']))}
              </Typography>
            </div>
          }
          secondary={<Typography variant="caption">{time}</Typography>}
        />
      </ListItem>
    );
  }
}

export default withStyles(styles)(ItemResultTranscriptEntry);
