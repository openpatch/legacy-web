import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { List as IList, Map } from 'immutable';
import List from '@material-ui/core/List';
import ItemResultCommentsForm from './ItemResultCommentsForm';
import ItemResultCommentsEntry from './ItemResultCommentsEntry';

const styles = theme => ({});

class Comments extends React.PureComponent {
  static propTypes = {
    currentAction: PropTypes.number,
    time: PropTypes.number.isRequired,
    onClick: PropTypes.func.isRequired,
    onChange: PropTypes.func.isRequired,
    onReplyChange: PropTypes.func.isRequired,
    comments: PropTypes.instanceOf(IList),
    memberId: PropTypes.number.isRequired,
    replyForm: PropTypes.instanceOf(Map).isRequired,
    form: PropTypes.instanceOf(Map).isRequired,
    onSubmit: PropTypes.func.isRequired,
    onReplySubmit: PropTypes.func.isRequired,
    onDelete: PropTypes.func.isRequired
  };

  static defaultProps = {
    comments: new IList([]),
    currentAction: -1
  };

  onReplyChange = id => {
    const { onReplyChange } = this.props;
    onReplyChange('parent_id', id);
  };

  render() {
    const {
      onClick,
      onChange,
      form,
      comments,
      time,
      replyForm,
      memberId,
      onDelete,
      onReplyChange,
      onSubmit,
      onReplySubmit
    } = this.props;
    const sortedComments = comments.sort((c1, c2) => {
      if (c1.get('start_time') > c2.get('start_time')) {
        return 1;
      } else if (c1.get('start_time') === c2.get('start_time')) {
        return c1.get('end_time') > c2.get('end_time');
      }
      return -1;
    });
    return (
      <div>
        <List>
          {sortedComments.map(comment => (
            <React.Fragment key={comment.get('id')}>
              <ItemResultCommentsEntry
                {...comment.toJS()}
                onReply={this.onReplyChange}
                memberId={memberId}
                onClick={onClick}
                active={
                  time >= comment.get('start_time') &&
                  time <= comment.get('end_time')
                }
                onDelete={onDelete}
              >
                {replyForm.get('parent_id') === comment.get('id') ? (
                  <li>
                    <ItemResultCommentsForm
                      comment={replyForm.get('comment')}
                      onChange={onReplyChange}
                      reply={true}
                      onSubmit={onReplySubmit}
                    />
                  </li>
                ) : null}
              </ItemResultCommentsEntry>
            </React.Fragment>
          ))}
        </List>
        <ItemResultCommentsForm
          time={time}
          comment={form.get('comment')}
          category={form.get('category')}
          duration={form.get('duration')}
          onChange={onChange}
          onClick={onClick}
          onSubmit={onSubmit}
        />
      </div>
    );
  }
}

export default withStyles(styles)(Comments);
