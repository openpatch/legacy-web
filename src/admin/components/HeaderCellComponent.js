import React from 'react';
import { TableHeaderRow } from '@devexpress/dx-react-grid-material-ui';

/**
 * HeaderCellComponent. Enhanced TableHeaderRow.Cell with the ability to
 * disable the sorting of columns individually. Just add `hideSortingControls:
 * true` property to the column object.
 */
const HeaderCellComponent = ({ showSortingControls, ...props }) => {
  const { hideSortingControls } = props.column;
  return (
    <TableHeaderRow.Cell
      showSortingControls={showSortingControls && !hideSortingControls}
      {...props}
    />
  );
};

export default HeaderCellComponent;
