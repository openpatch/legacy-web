/**
*
* ItemSource
*
*/

import React from 'react';
import PropTypes from 'prop-types';

class ItemSource extends React.PureComponent {
  static propTypes = {
    source: PropTypes.object.isRequired
  };

  render() {
    const { source } = this.props;
    return <pre>{JSON.stringify(source, null, 2)}</pre>;
  }
}

export default ItemSource;
