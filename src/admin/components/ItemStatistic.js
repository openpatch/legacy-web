/**
 *
 * ItemStatistic
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import Divider from '@material-ui/core/Divider';
import { Map } from 'immutable';

import formats from '../../formats';
import Loading from '../../common/components/Loading';
import ResponseFieldStatistic from './ResponseFieldStatistic/ResponseFieldStatistic';
import StatisticSummaryTable from '../../common/components/StatisticSummaryTable';

class ItemStatistic extends React.Component {
  static propTypes = {
    format: PropTypes.shape({
      type: PropTypes.string.isRequired
    }),
    data: PropTypes.object.isRequired
  };

  render() {
    const { data, format } = this.props;
    let Statistic = null;
    if (format.type in formats) {
      Statistic = formats[format.type].Statistic;
    }
    return (
      <div>
        {data && data.get('responses') ? (
          <React.Fragment>
            <StatisticSummaryTable
              rows={new Map({ 'times in s': data.get('times') })}
            />
            {data
              .get('responses')
              .toList()
              .map((response, i) => (
                <React.Fragment>
                  <Divider />
                  <ResponseFieldStatistic key={i} {...response.toJS()} />
                </React.Fragment>
              ))}
            <Divider />
            {Statistic ? (
              <React.Fragment>
                <Divider />
                <Statistic data={data.get('format')} />
              </React.Fragment>
            ) : null}
          </React.Fragment>
        ) : (
          <Loading />
        )}
      </div>
    );
  }
}

export default ItemStatistic;
