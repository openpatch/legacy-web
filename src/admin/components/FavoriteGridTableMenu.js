import React from 'react';
import PropTypes from 'prop-types';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import LaunchIcon from '@material-ui/icons/Launch';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import ContentCopyIcon from '@material-ui/icons/FileCopy';

class FavoriteGridTableMenu extends React.PureComponent {
  static propTypes = {
    id: PropTypes.string,
    open: PropTypes.bool,
    entryId: PropTypes.number,
    anchorEl: PropTypes.object,
    onView: PropTypes.func.isRequired,
    onDelete: PropTypes.func,
    onClose: PropTypes.func,
    onEdit: PropTypes.func,
    onCopy: PropTypes.func
  };

  static defaultProps = {
    open: false,
    onClose: () => {}
  };

  onCopy = e => {
    this.props.onClose();
    this.props.onCopy(this.props.entryId, e);
  };

  onView = e => {
    this.props.onClose();
    this.props.onView(this.props.entryId, e);
  };

  onDelete = e => {
    this.props.onClose();
    this.props.onDelete(this.props.entryId, e);
  };

  onEdit = e => {
    this.props.onClose();
    this.props.onEdit(this.props.entryId, e);
  };

  render() {
    const {
      id,
      children,
      open,
      anchorEl,
      onDelete,
      onClose,
      onEdit,
      onCopy
    } = this.props;
    return (
      <Menu id={id} open={open} anchorEl={anchorEl} onClose={onClose}>
        <MenuItem onClick={this.onView}>
          <ListItemIcon tooltip="Open">
            <LaunchIcon />
          </ListItemIcon>
          <ListItemText inset primary="Open" />
        </MenuItem>
        {onCopy && (
          <MenuItem onClick={this.onCopy}>
            <ListItemIcon>
              <ContentCopyIcon />
            </ListItemIcon>
            <ListItemText inset primary="Duplicate" />
          </MenuItem>
        )}
        {onEdit && (
          <MenuItem onClick={this.onEdit}>
            <ListItemIcon>
              <EditIcon />
            </ListItemIcon>
            <ListItemText inset primary="Edit" />
          </MenuItem>
        )}
        {onDelete && (
          <MenuItem onClick={this.onDelete}>
            <ListItemIcon>
              <DeleteIcon />
            </ListItemIcon>
            <ListItemText inset primary="Delete" />
          </MenuItem>
        )}
        {children}
      </Menu>
    );
  }
}

export default FavoriteGridTableMenu;
