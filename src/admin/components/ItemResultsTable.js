/*
 *
 * ItemResultsTable
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { TableCell } from '@material-ui/core/Table';
import CheckIcon from '@material-ui/icons/Check';
import ClearIcon from '@material-ui/icons/Clear';
import {
  PagingState,
  IntegratedPaging,
  SortingState,
  IntegratedSorting,
  FilteringState,
  IntegratedFiltering,
  GroupingState,
  IntegratedGrouping
} from '@devexpress/dx-react-grid';
import {
  Grid,
  Table,
  TableHeaderRow,
  PagingPanel,
  TableFilterRow,
  TableGroupRow
} from '@devexpress/dx-react-grid-material-ui';
import distanceInWordsStrict from 'date-fns/distance_in_words_strict';
import getTime from 'date-fns/get_time';

import LinkCellComponent from './LinkCellComponent';

const Cell = props => {
  if (props.column.type === 'correct') {
    return (
      <Table.Cell {...props}>
        {props.row.correct ? (
          <CheckIcon style={{ color: 'green' }} />
        ) : (
          <ClearIcon />
        )}
      </Table.Cell>
    );
  } else if (props.column.type === 'link') {
    return <LinkCellComponent {...props} />;
  } else if (props.column.type === 'func') {
    return <Table.Cell {...props}>{props.column.func(props.row)}</Table.Cell>;
  } else {
    return <Table.Cell {...props} />;
  }
};

export class ItemResultsTable extends React.Component {
  static propTypes = {
    results: PropTypes.array,
    columns: PropTypes.arrayOf(PropTypes.object),
    allowedPageSizes: PropTypes.arrayOf(PropTypes.number),
    defaultGrouping: PropTypes.arrayOf(
      PropTypes.shape({
        columnName: PropTypes.string
      })
    )
  };

  static defaultProps = {
    results: [],
    columns: [
      {
        name: 'id',
        title: 'Id',
        type: 'link',
        getLink: row => `/admin/items/${row.item_id}/results/${row.id}`
      },
      {
        name: 'survey_id',
        title: 'Survey',
        type: 'link',
        getLink: row => `/admin/surveys/${row.survey_id}`
      },
      {
        name: 'session_hash',
        title: 'Session'
      },
      {
        name: 'correct',
        title: 'Correct',
        type: 'correct'
      },
      {
        name: 'duration',
        title: 'Duration',
        type: 'func',
        getCellValue: row => getTime(row.created_on) - getTime(row.updated_on),
        func: row => distanceInWordsStrict(row.created_on, row.updated_on)
      }
    ],
    pageSizes: [5, 10, 25, 50, 100],
    defaultGrouping: []
  };
  render() {
    const { results, columns, pageSizes, defaultGrouping } = this.props;
    return (
      <Grid getRowId={row => row.id} rows={results} columns={columns}>
        {defaultGrouping.length === 0
          ? [
              <FilteringState defaultFilters={[]} key="filter" />,
              <IntegratedFiltering key="ifilter" />,
              <SortingState key="sorting" />,
              <IntegratedSorting key="isorting" />,
              <PagingState
                defaultCurrentPage={0}
                defaultPageSize={10}
                key="paging"
              />,
              <IntegratedPaging key="ipaging" />
            ]
          : [
              <GroupingState
                defaultGrouping={defaultGrouping}
                key="grouping"
              />,
              <IntegratedGrouping key="igrouping" />
            ]}
        <Table cellComponent={Cell} />
        {defaultGrouping.length === 0 && (
          <TableFilterRow
            filterCellTemplate={({ column }) => {
              if (column.name === 'actions') {
                return <TableCell />;
              }
              return undefined;
            }}
          />
        )}
        <TableHeaderRow showSortingControls={defaultGrouping.length === 0} />
        {defaultGrouping.length === 0 ? (
          <PagingPanel pageSizes={pageSizes} />
        ) : (
          <TableGroupRow />
        )}
      </Grid>
    );
  }
}

export default ItemResultsTable;
