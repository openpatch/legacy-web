import React from 'react';
import PropTypes from 'prop-types';
import IconButton from '@material-ui/core/IconButton';

import FavoriteIcon from './FavoriteIcon';

const FavoriteCellComponent = ({ onClick, isFavorized }) => (
  <div style={{ width: '100%', textAlign: 'center' }}>
    <IconButton onClick={onClick}>
      <FavoriteIcon isFavorized={isFavorized} />
    </IconButton>
  </div>
);

FavoriteCellComponent.propTypes = {
  onClick: PropTypes.func,
  isFavorized: PropTypes.bool
};

FavoriteCellComponent.defaultProps = {
  onClick: () => {},
  isFavorized: false
};

export default FavoriteCellComponent;
