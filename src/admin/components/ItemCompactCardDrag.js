import React from 'react';
import PropTypes from 'prop-types';
import { Map } from 'immutable';
import { withStyles } from '@material-ui/core/styles';
import { Draggable } from 'react-beautiful-dnd';

import ItemCompactCard from './ItemCompactCard';

const styles = theme => ({
  root: {
    display: 'flex',
    userSelect: 'none',
    width: 160,
    height: 160,
    margin: theme.spacing.unit,
    '&:focus': {
      border: '2px solid #000'
    }
  }
});

class ItemCompactCardDrag extends React.PureComponent {
  static propTypes = {
    classes: PropTypes.object.isRequired,
    index: PropTypes.number.isRequired,
    item: PropTypes.instanceOf(Map),
    draggableId: PropTypes.oneOfType([PropTypes.number, PropTypes.string])
      .isRequired,
    onPreview: PropTypes.func,
    onDelete: PropTypes.func,
    background: PropTypes.string
  };

  static defaultProps = {
    background: 'white',
    item: new Map({})
  };

  onDelete = () => {
    this.props.onDelete(this.props.id);
  };

  onPreview = () => {
    this.props.onPreview(this.props.item.get('id'));
  };

  render() {
    const {
      classes,
      item,
      index,
      onDelete,
      onPreview,
      draggableId
    } = this.props;
    return (
      <Draggable index={index} draggableId={`item-${draggableId}`} type="ITEM">
        {(provided, snapshot) => (
          <div>
            <div
              className={classes.root}
              ref={provided.innerRef}
              {...provided.draggableProps}
              {...provided.dragHandleProps}
            >
              <ItemCompactCard
                item={item}
                onDelete={onDelete ? this.onDelete : undefined}
                onPreview={onPreview ? this.onPreview : undefined}
              />
            </div>
            {provided.placeholder}
          </div>
        )}
      </Draggable>
    );
  }
}

export default withStyles(styles)(ItemCompactCardDrag);
