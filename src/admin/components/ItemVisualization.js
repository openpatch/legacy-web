/**
 *
 * ItemVisualization
 *
 */
import React from 'react';
import PropTypes from 'prop-types';

import formats from '../../formats';
import Loading from '../../common/components/Loading';
import NotSupported from '../../common/components/NotSupported';

class ItemVisualization extends React.Component {
  static props = {
    format: PropTypes.shape({
      type: PropTypes.string.isRequired
    }),
    data: PropTypes.object.isRequired
  };
  render() {
    const { data, format } = this.props;
    let Visualization = null;
    if (format.type in formats) {
      Visualization = formats[format.type].Visualization;
    }
    return (
      <div>
        {Visualization ? (
          data ? (
            <Visualization data={data} />
          ) : (
            <Loading />
          )
        ) : (
          <NotSupported />
        )}
      </div>
    );
  }
}

export default ItemVisualization;
