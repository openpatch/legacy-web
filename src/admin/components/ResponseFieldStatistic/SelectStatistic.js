/**
 *
 * SelectStatistic
 *
 */

import React from 'react';
import SimpleBarChart from './SimpleBarChart';

class SelectStatistic extends React.Component {
  render() {
    const { frequency } = this.props;
    return <SimpleBarChart data={frequency} />;
  }
}

export default SelectStatistic;
