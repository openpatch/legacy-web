/**
 *
 * SimpleBarChart
 *
 */

import React from 'react';
import {
  VictoryChart,
  VictoryTooltip,
  VictoryBar,
  VictoryTheme
} from 'victory';
import _map from 'lodash/map';

class SimpleBarChart extends React.Component {
  render() {
    let { data } = this.props;
    data = _map(data, (value, key) => ({
      x: key,
      y: value,
      label: value
    }));
    return (
      <div>
        <VictoryChart
          animate={{
            duration: 2000,
            onLoad: { duration: 1000 }
          }}
          width={800}
          theme={VictoryTheme.material}
          domainPadding={20}
        >
          <VictoryBar barWidth={30} data={data} labelComponent={<VictoryTooltip />} />
        </VictoryChart>
      </div>
    );
  }
}

export default SimpleBarChart;
