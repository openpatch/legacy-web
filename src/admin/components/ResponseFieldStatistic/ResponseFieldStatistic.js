/**
 *
 * ResponseFieldStatistic
 *
 */

import React from 'react';
import Typography from '@material-ui/core/Typography/Typography';
import { withStyles } from '@material-ui/core/styles';
import ChoiceStatistic from './ChoiceStatistic';
import MatrixStatistic from './MatrixStatistic';
import SelectStatistic from './SelectStatistic';
import TextStatistic from './TextStatistic';

const styles = theme => ({
  header: {
    padding: theme.spacing.unit * 2,
    backgroundColor: theme.palette.primary[100]
  },
  body: {
    padding: theme.spacing.unit * 2,
    display: 'flex',
    justifyContent: 'center'
  }
});

class ResponseFieldStatistic extends React.Component {
  render() {
    const { classes, type, label, ...props } = this.props;
    let statistic = <div />;
    if (type === 'multiple-choice' || type === 'single-choice') {
      statistic = <ChoiceStatistic {...props} />;
    } else if (type === 'matrix') {
      statistic = <MatrixStatistic {...props} />;
    } else if (type === 'select') {
      statistic = <SelectStatistic {...props} />;
    } else if (type === 'text') {
      statistic = <TextStatistic {...props} />;
    }
    return (
      <div>
        <div className={classes.header}>
          <Typography>{label}</Typography>
        </div>
        <div className={classes.body}>{statistic}</div>
      </div>
    );
  }
}

export default withStyles(styles)(ResponseFieldStatistic);
