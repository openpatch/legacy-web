/**
 *
 * ChoiceStatistic
 *
 */

import React from 'react';
import SimpleBarChart from './SimpleBarChart';

class ChoiceStatistic extends React.Component {
  render() {
    const { frequency } = this.props;
    return <SimpleBarChart data={frequency} />;
  }
}

export default ChoiceStatistic;
