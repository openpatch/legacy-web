/**
 *
 * TextStatistic
 *
 */

import React from 'react';
import TagCloud from 'react-tag-cloud';
import _reduce from 'lodash/reduce';
import _map from 'lodash/map';
import Typography from '@material-ui/core/Typography';
import Tooltip from '@material-ui/core/Tooltip';

class TextStatistic extends React.Component {
  render() {
    const { frequency, num_words, avg_words } = this.props;
    const fontSize = 32;
    const max = _reduce(
      frequency,
      (max = 0, value) => (max < value ? value : max)
    );
    const tags = _map(frequency, (value, key) => (
      <div
        key={key}
        style={{
          fontSize: Math.max(Math.round((fontSize * value) / max), 6)
        }}
      >
        <Tooltip title={`${key}: ${value}`}>
          <div>{key}</div>
        </Tooltip>
      </div>
    ));

    return (
      <div>
        <Typography>Number of words: {num_words}</Typography>
        <Typography>
          Average number of words: {Math.round(avg_words * 10) / 10}
        </Typography>
        <TagCloud
          style={{ width: 800, height: 300, padding: 5, position: 'relative' }}
        >
          {tags}
        </TagCloud>
      </div>
    );
  }
}

export default TextStatistic;
