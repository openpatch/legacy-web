/**
 *
 * MatrixStatistic
 *
 */

import React from 'react';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import _map from 'lodash/map';
import ChoiceStatistic from './ChoiceStatistic';
import TextStatistic from './TextStatistic';

class MatrixStatistic extends React.Component {
  state = {};
  onRowChange = event => {
    this.setState({
      row: event.target.value
    });
  };
  render() {
    const { frequencies } = this.props;
    let row;
    const rowSelect = _map(frequencies, (frequency, key) => {
      row = key;
      return (
        <MenuItem key={key} value={key}>
          {key}
        </MenuItem>
      );
    });

    const selectedFrequency = frequencies[this.state.row || row];
    if (!selectedFrequency) {
      return null;
    }
    const type = selectedFrequency['type'];
    let statistic = null;
    if (type === 'multiple-choice' || type === 'single-choice') {
      statistic = <ChoiceStatistic {...selectedFrequency} />;
    } else if (type === 'text') {
      statistic = <TextStatistic {...selectedFrequency} />;
    }

    return (
      <div>
        <Select onChange={this.onRowChange} value={this.state.row || row}>
          {rowSelect}
        </Select>
        <br />
        {statistic}
      </div>
    );
  }
}

export default MatrixStatistic;
