import React from 'react';
import PropTypes from 'prop-types';
import { List } from 'immutable';
import { withStyles } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import ShuffleIcon from '@material-ui/icons/Shuffle';
import RepeatIcon from '@material-ui/icons/Repeat';
import DoneIcon from '@material-ui/icons/Done';

import ItemCompactCard from './ItemCompactCard';

const styles = theme => ({
  scroll: {
    overflow: 'auto'
  },
  inner: {
    minHeight: 175,
    minWidth: 150,
    padding: theme.spacing.unit,
    paddingRight: 150,
    display: 'inline-flex',
    flexDirection: 'row'
  },
  outer: {
    display: 'inline-flex',
    flexGrow: 1
  },
  container: {
    display: 'flex',
    alignItems: 'center'
  },
  settings: {
    display: 'inline-flex',
    flexDirection: 'column',
    float: 'left'
  },
  card: {
    userSelect: 'none',
    width: 160,
    height: 160,
    margin: theme.spacing.unit,
    '&:focus': {
      border: '2px solid #000'
    }
  }
});

class ItemGroup extends React.PureComponent {
  static propTypes = {
    classes: PropTypes.object.isRequired,
    randomized: PropTypes.bool,
    jumpable: PropTypes.bool,
    needsToBeCorrect: PropTypes.bool,
    onPreviewItem: PropTypes.func.isRequired,
    items: PropTypes.instanceOf(List)
  };
  static defaultProps = {
    randomized: false,
    jumpable: false,
    needsToBeCorrect: false,
    items: new List([])
  };
  render() {
    const {
      classes,
      randomized,
      jumpable,
      needsToBeCorrect,
      onPreviewItem,
      items
    } = this.props;
    return (
      <div className={classes.container}>
        <div className={classes.settings}>
          <Tooltip title="Jumpable" placement="right">
            <IconButton
              color={jumpable ? 'primary' : 'default'}
              aria-label="Jumpable"
            >
              <RepeatIcon />
            </IconButton>
          </Tooltip>
          <Tooltip title="Randomized" placement="right">
            <IconButton
              color={randomized ? 'primary' : 'default'}
              aria-label="Randomized"
            >
              <ShuffleIcon />
            </IconButton>
          </Tooltip>
          <Tooltip title="Needs to be correct" placement="right">
            <IconButton
              color={needsToBeCorrect ? 'primary' : 'default'}
              aria-label="Needs to be correct"
            >
              <DoneIcon />
            </IconButton>
          </Tooltip>
        </div>
        <div className={classes.scroll}>
          <div className={classes.outer}>
            <div className={classes.inner}>
              {items.map((item, k) => (
                <div className={classes.card} key={k}>
                  <ItemCompactCard
                    item={item}
                    onPreview={() => onPreviewItem(item.get('id'))}
                  />
                </div>
              ))}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default withStyles(styles)(ItemGroup);
