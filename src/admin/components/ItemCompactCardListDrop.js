import React from 'react';
import PropTypes from 'prop-types';
import { List } from 'immutable';
import { withStyles } from '@material-ui/core/styles';
import { Droppable } from 'react-beautiful-dnd';

import ItemCompactCardDragList from './ItemCompactCardDragList';

const styles = theme => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    position: 'relative'
  },
  scroll: {
    overflow: 'auto'
  },
  inner: {
    display: 'flex',
    minWidth: 600,
    padding: theme.spacing.unit
  },
  outer: {
    display: 'inline-flex',
    flexGrow: 1
  }
});

class ItemCompactCardListDrop extends React.PureComponent {
  static propTypes = {
    items: PropTypes.instanceOf(List),
    id: PropTypes.string,
    onPreviewItem: PropTypes.func
  };

  static defaultProps = {
    items: new List([]),
    id: 'itemsList',
    onPreviewItem: undefined
  };
  render() {
    const { classes, items, id, prefix, onPreviewItem } = this.props;
    return (
      <Droppable
        droppableId={id}
        direction="horizontal"
        type="ITEM"
        isDropDisabled={true}
      >
        {(provided, snapshot) => (
          <div className={classes.root}>
            <div className={classes.scroll}>
              <div className={classes.outer}>
                <div
                  ref={provided.innerRef}
                  className={classes.inner}
                  {...provided.droppableProps}
                >
                  <ItemCompactCardDragList
                    items={items}
                    prefix={prefix}
                    onPreviewItem={onPreviewItem}
                  />
                  {provided.placeholder}
                </div>
              </div>
            </div>
          </div>
        )}
      </Droppable>
    );
  }
}

export default withStyles(styles)(ItemCompactCardListDrop);
