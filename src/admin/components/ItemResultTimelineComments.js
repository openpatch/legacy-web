import React from 'react';
import PropTypes from 'prop-types';
import { List } from 'immutable';
import _isEqual from 'lodash/isEqual';

class ItemResultTimelineComments extends React.Component {
  static propTypes = {
    comments: PropTypes.instanceOf(List),
    scale: PropTypes.number
  };

  static defaultProps = {
    comments: new List([]),
    scale: 1
  };

  shouldComponentUpdate(nextProps) {
    return !_isEqual(nextProps, this.props);
  }

  render() {
    const { comments, scale } = this.props;
    return (
      <React.Fragment>
        {comments.map((comment, i) => (
          <div
            key={i}
            style={{
              position: 'absolute',
              left: comment.get('start_time') / scale || 0,
              bottom: 0,
              width:
                (comment.get('end_time') - comment.get('start_time')) / scale ||
                0,
              background: comment.get('category'),
              height: 8,
              opacity: 0.4,
              pointerEvents: 'none'
            }}
          />
        ))}
      </React.Fragment>
    );
  }
}

export default ItemResultTimelineComments;
