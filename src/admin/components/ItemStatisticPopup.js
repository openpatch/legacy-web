/**
 *
 * ItemStatisticPopup
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import withMobileDialog from '@material-ui/core/withMobileDialog';
import Button from '@material-ui/core/Button';

import ItemStatistic from './ItemStatistic.js';

class ItemStatisticPopup extends React.PureComponent {
  static propTypes = {
    format: PropTypes.shape({
      type: PropTypes.string.isRequired
    }),
    data: PropTypes.object.isRequired,
    fullScreen: PropTypes.bool,
    onClose: PropTypes.func.isRequired,
    open: PropTypes.bool
  };

  static defaultProps = {
    open: false,
    fullScreen: false
  };

  render() {
    const { open, onClose, fullScreen, data, format } = this.props;
    return (
      <Dialog
        open={open}
        fullScreen={fullScreen}
        onClose={onClose}
        maxWidth={false}
        aria-labelledby="item-statistic-popup-title"
      >
        <DialogTitle id="item-statistic-popup-title">Statistic</DialogTitle>
        <DialogContent>
          <ItemStatistic format={format} data={data} />
        </DialogContent>
        <DialogActions>
          <Button onClick={onClose}>Close</Button>
        </DialogActions>
      </Dialog>
    );
  }
}

export default withMobileDialog()(ItemStatisticPopup);
