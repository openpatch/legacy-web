import React from 'react';
import PropTypes from 'prop-types';
import {
  SelectionState,
  IntegratedSelection,
  PagingState,
  IntegratedPaging,
  SortingState,
  IntegratedSorting,
  FilteringState,
  IntegratedFiltering
} from '@devexpress/dx-react-grid';
import {
  Grid,
  Table,
  TableHeaderRow,
  PagingPanel,
  TableSelection,
  TableFilterRow
} from '@devexpress/dx-react-grid-material-ui';

import HeaderCellComponent from './HeaderCellComponent';
import FilterCellComponent from './FilterCellComponent';
import DateCellComponent from './DateCellComponent';

const Cell = props => {
  if (props.column.type === 'date') {
    return <DateCellComponent {...props} />;
  }
  return <Table.Cell {...props} />;
};

class FavoriteGridTable extends React.Component {
  static propTypes = {
    rows: PropTypes.array,
    favorites: PropTypes.array,
    columns: PropTypes.array.isRequired,
    onToggleFilter: PropTypes.func,
    columnExtensions: PropTypes.array,
    integratedFilteringColumnExtensions: PropTypes.array,
    pageSizes: PropTypes.array
  };

  static defaultProps = {
    rows: [],
    filter: [],
    columnExtensions: [],
    integratedFilteringColumnExtensions: [],
    pageSizes: [5, 10, 25, 50, 100]
  };

  state = {
    anchorElementForMenu: undefined,
    menuOpen: false
  };

  render() {
    const {
      rows,
      selection,
      columns,
      columnExtensions,
      pageSizes,
      integratedFilteringColumnExtensions,
      onSelectionChange
    } = this.props;

    return (
      <div>
        <Grid getRowId={row => row.id} columns={columns} rows={rows}>
          <FilteringState />
          <IntegratedFiltering
            columnExtensions={integratedFilteringColumnExtensions}
          />
          <SortingState />
          <IntegratedSorting />
          <SelectionState
            selection={selection}
            onSelectionChange={onSelectionChange}
          />
          <IntegratedSelection />
          <PagingState defaultCurrentPage={0} defaultPageSize={10} />
          <IntegratedPaging />
          <Table cellComponent={Cell} columnExtensions={columnExtensions} />
          <TableSelection showSelectAll />
          <TableFilterRow cellComponent={FilterCellComponent} />
          <TableHeaderRow
            cellComponent={HeaderCellComponent}
            showSortingControls
          />
          <PagingPanel pageSizes={pageSizes} />
        </Grid>
      </div>
    );
  }
}

export default FavoriteGridTable;
