import React from 'react';
import PropTypes from 'prop-types';
import FavoriteMuiIcon from '@material-ui/icons/Favorite';

/**
 * Placeholder incase of changing the favorite icon.
 */
const FavoriteIcon = ({ isFavorized, ...props }) => (
  <FavoriteMuiIcon
    style={{
      fill: isFavorized ? 'red' : undefined
    }}
    {...props}
  />
);

FavoriteIcon.propTypes = {
  isFavorized: PropTypes.bool
};

FavoriteIcon.defaultProps = {
  isFavorized: false
};

export default FavoriteIcon;
