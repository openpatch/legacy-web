import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import _isEqual from 'lodash/isEqual';

import stringToColor from '../../common/utils/stringToColor';

export const styles = theme => ({
  root: {
    borderRadius: '50%',
    width: 10,
    height: 10,
    marginLeft: -6,
    marginTop: 5,
    background: 'whitesmoke'
  }
});

export class ActionIndicator extends React.Component {
  static propTypes = {
    style: PropTypes.object,
    type: PropTypes.string.isRequired
  };

  shouldComponentUpdate(nextProps) {
    return !_isEqual(nextProps, this.props);
  }

  render() {
    const { classes, style, type } = this.props;
    return (
      <div
        className={classes.root}
        style={{
          background: `#${stringToColor(
            type
              .split('.')
              .slice(-1)
              .pop(),
            25
          )}`,
          ...style
        }}
      />
    );
  }
}

export default withStyles(styles)(ActionIndicator);
