/**
 *
 * FavoriteItemCard
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Map } from 'immutable';

import FavoriteCard from './FavoriteCard';
import ItemBasicStats from './ItemBasicStats';

class FavoriteItemCard extends React.Component {
  static propTypes = {
    name: PropTypes.string.isRequired,
    onClick: PropTypes.func,
    onToggleFavorite: PropTypes.func,
    stats: PropTypes.instanceOf(Map)
  };

  static defaultProps = {
    onClick: () => {},
    onToggleFavorite: () => {},
    stats: new Map({})
  };

  render() {
    const { stats, ...props } = this.props;
    return (
      <FavoriteCard {...props}>
        <ItemBasicStats stats={stats} />
      </FavoriteCard>
    );
  }
}

export default FavoriteItemCard;
