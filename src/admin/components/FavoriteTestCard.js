/**
 *
 * FavoriteTestCard
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Map } from 'immutable';

import FavoriteCard from './FavoriteCard';

class FavoriteTestCard extends React.Component {
  static propTypes = {
    name: PropTypes.string.isRequired,
    onClick: PropTypes.func,
    onToggleFavorite: PropTypes.func,
    stats: PropTypes.instanceOf(Map)
  };

  static defaultProps = {
    onClick: () => {},
    onToggleFavorite: () => {},
    stats: new Map({})
  };

  render() {
    const { stats, ...props } = this.props;
    return <FavoriteCard {...props} />;
  }
}

export default FavoriteTestCard;
