/**
 *
 * MarkdownEditor
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import CodeEditor from '../../common/components/CodeEditor';
import Markdown from '../../common/components/Markdown';

const styles = theme => ({
  container: {
    '@supports (display: grid)': {
      display: 'grid',
      gridTemplateColumns: '1fr 1fr',
      gridTemplateRows: 'auto',
      gridTemplateAreas: '"editor preview"',
      gridGap: `${theme.spacing.unit * 2}px`,

      [theme.breakpoints.down('lg')]: {
        display: 'block'
      }
    }
  },
  editor: {
    '@supports (display: grid)': {
      gridArea: 'editor'
    },
    padding: theme.spacing.unit
  },
  preview: {
    '@supports (display: grid)': {
      gridArea: 'preview'
    },
    overflow: 'auto',
    maxHeight: 508,
    padding: theme.spacing.unit
  }
});

export class MarkdownEditor extends React.PureComponent {
  static propTypes = {
    classes: PropTypes.object.isRequired,
    onChange: PropTypes.func,
    value: PropTypes.string,
    height: PropTypes.string
  };

  static defaultProps = {
    value: '',
    height: '500px'
  };

  render() {
    const { classes, height, value, onChange, renderers } = this.props;
    return (
      <div className={classes.container}>
        <div className={classes.editor}>
          <CodeEditor
            width="100%"
            height={height}
            mode="markdown"
            value={value || ''}
            onChange={onChange}
          />
        </div>
        <div className={classes.preview}>
          <Markdown source={value} escapeHtml={false} renderers={renderers} />
        </div>
      </div>
    );
  }
}

export default withStyles(styles)(MarkdownEditor);
