import React from 'react';
import PropTypes from 'prop-types';
import { List } from 'immutable';
import { withStyles } from '@material-ui/core/styles';
import { Droppable } from 'react-beautiful-dnd';

import ItemGroupEntry from './ItemGroupDnDEntry';

const styles = theme => ({});

class ItemGroupsList extends React.PureComponent {
  static propTypes = {
    id: PropTypes.string.isRequired,
    itemGroups: PropTypes.instanceOf(List),
    classes: PropTypes.object.isRequired,
    onDelete: PropTypes.func.isRequired,
    onJumpable: PropTypes.func.isRequired,
    onRandomized: PropTypes.func.isRequired,
    onNeedsToBeCorrect: PropTypes.func.isRequired,
    onDeleteItem: PropTypes.func.isRequired,
    onPreviewItem: PropTypes.func.isRequired
  };

  static defaultProps = {
    itemGroups: new List([])
  };
  render() {
    const {
      classes,
      itemGroups,
      onDelete,
      onDeleteItem,
      onJumpable,
      onNeedsToBeCorrect,
      onRandomized,
      onPreviewItem,
      id
    } = this.props;
    return (
      <Droppable droppableId={id} type="ITEM_GROUP" direction="vertical">
        {(provided, snapshot) => (
          <div className={classes.root}>
            <div ref={provided.innerRef} {...provided.droppableProps}>
              {itemGroups.map((itemGroup, i) => (
                <ItemGroupEntry
                  id={i}
                  key={i}
                  items={itemGroup.get('items')}
                  onDelete={onDelete}
                  onJumpable={onJumpable}
                  onRandomized={onRandomized}
                  onNeedsToBeCorrect={onNeedsToBeCorrect}
                  onPreviewItem={onPreviewItem}
                  onDeleteItem={onDeleteItem}
                  jumpable={itemGroup.get('is_jumpable')}
                  randomized={itemGroup.get('is_randomized')}
                  needsToBeCorrect={itemGroup.get('needs_to_be_correct')}
                />
              ))}
              {provided.placeholder}
            </div>
          </div>
        )}
      </Droppable>
    );
  }
}

export default withStyles(styles)(ItemGroupsList);
