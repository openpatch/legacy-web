import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import Select from '@material-ui/core/Select/Select';
import MenuItem from '@material-ui/core/MenuItem';
import PlayIcon from '@material-ui/icons/PlayArrow';
import PauseIcon from '@material-ui/icons/Pause';
import StopIcon from '@material-ui/icons/Stop';

const styles = theme => ({
  root: {
    display: 'flex'
  },
  spacer: {
    flex: '1 1 auto'
  },
  select: {
    color: 'white'
  },
  time: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    padding: theme.spacing.unit,
    height: 48,
    fontFamily: 'Roboto',
    fontWeight: 'bold',
    fontSize: 14,
    color: 'white'
  },
  icon: {
    color: 'white'
  }
});

class PlaybackControls extends React.PureComponent {
  static propTypes = {
    classes: PropTypes.object.isRequired,
    playbackStatus: PropTypes.string.isRequired,
    onPlay: PropTypes.func.isRequired,
    onPause: PropTypes.func.isRequired,
    onStop: PropTypes.func.isRequired,
    onSpeedChange: PropTypes.func.isRequired,
    time: PropTypes.number,
    endTime: PropTypes.number,
    speed: PropTypes.number
  };
  render() {
    const {
      classes,
      time,
      endTime,
      speed,
      playbackStatus,
      onSpeedChange,
      onPause,
      onPlay,
      onStop
    } = this.props;
    return (
      <div className={classes.root}>
        <IconButton
          color="inherit"
          classes={{
            colorInherit: classes.icon
          }}
          aria-label={playbackStatus === 'play' ? 'Pause' : 'Play'}
          onClick={playbackStatus === 'play' ? onPause : onPlay}
        >
          {playbackStatus === 'play' ? <PauseIcon /> : <PlayIcon />}
        </IconButton>
        <IconButton
          aria-label="Stop"
          color="inherit"
          classes={{
            colorInherit: classes.icon
          }}
          onClick={onStop}
        >
          <StopIcon />
        </IconButton>
        <div className={classes.time}>
          {Math.round(time / 100) / 10} / {Math.round(endTime / 100) / 10}
        </div>
        <div className={classes.spacer} />
        <Select
          onChange={onSpeedChange}
          value={speed}
          classes={{
            root: classes.time,
            icon: classes.icon,
            select: classes.select
          }}
        >
          <MenuItem value={0.25}>x0.25</MenuItem>
          <MenuItem value={0.5}>x0.5</MenuItem>
          <MenuItem value={1}>x1</MenuItem>
          <MenuItem value={2}>x2</MenuItem>
          <MenuItem value={5}>x5</MenuItem>
        </Select>
      </div>
    );
  }
}

export default withStyles(styles)(PlaybackControls);
