import React from 'react';
import PropTypes from 'prop-types';
import _isEqual from 'lodash/isEqual';

import ActionIndicator from './ActionIndicator';

class ItemResultTimelineActions extends React.Component {
  static propTypes = {
    comments: PropTypes.array,
    scale: PropTypes.number
  };

  static defaultProps = {
    actions: [],
    scale: 1
  };

  shouldComponentUpdate(nextProps) {
    return !_isEqual(nextProps, this.props);
  }

  render() {
    const { actions, scale } = this.props;
    return (
      <React.Fragment>
        {actions.map((action, i) => (
          <ActionIndicator
            key={i}
            style={{
              left: action.get('time') / scale,
              position: 'absolute',
              zIndex: 1,
              pointerEvents: 'none'
            }}
            type={action.getIn(['action', 'type'])}
          />
        ))}
      </React.Fragment>
    );
  }
}

export default ItemResultTimelineActions;
