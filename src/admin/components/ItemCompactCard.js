import React from 'react';
import PropTypes from 'prop-types';
import { Map } from 'immutable';
import { FormattedMessage } from 'react-intl';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';

import { getFormatNameMessage } from '../../formats/utils';
import PreviewButton from '../../common/components/InfoButton';

const styles = theme => ({
  title: {
    fontSize: 14,
    textOverflow: 'ellipsis',
    whiteSpace: 'nowrap',
    overflow: 'hidden'
  },
  actionSpacer: {
    flex: '1 1 auto'
  },
  format: {
    marginBottom: 12,
    color: theme.palette.text.secondary
  },
  card: {
    width: '100%',
    height: '100%',
    display: 'flex',
    flexDirection: 'column'
  },
  id: {
    color: theme.palette.text.secondary
  }
});

class ItemCompactCard extends React.PureComponent {
  static propTypes = {
    item: PropTypes.instanceOf(Map),
    onDelete: PropTypes.func,
    onPreview: PropTypes.func,
    classes: PropTypes.object.isRequired,
    disabled: PropTypes.bool
  };
  static defaultProps = {
    item: new Map({}),
    disabled: false
  };
  render() {
    const { item, onDelete, onPreview, classes, disabled } = this.props;
    return (
      <Card
        className={classes.card}
        style={{
          border: `2px solid #${item.getIn(['format', 'color_hex'], '000')}`,
          opacity: disabled ? 0.5 : 1
        }}
      >
        {item && item.getIn(['format', 'type']) ? (
          <React.Fragment>
            <CardContent style={{ paddingBottom: 0 }}>
              <Typography variant="headline" className={classes.title}>
                {item.get('name')}
              </Typography>
              <Typography variant="body1" className={classes.format}>
                <FormattedMessage
                  {...getFormatNameMessage(item.getIn(['format', 'type']))}
                />
              </Typography>
              {item.getIn(['stats', 'num_results']) !== 0 ? (
                <React.Fragment>
                  <Typography>
                    {'⏱️ '}
                    {Math.round(item.getIn(['stats', 'time_mean']))}s
                    {item.get('solution', new Map({})).size !== 0
                      ? ` ✅ ${Math.round(
                          (item.getIn(['stats', 'num_correct']) /
                            item.getIn(['stats', 'num_results'])) *
                            100
                        )}%`
                      : ''}
                  </Typography>
                </React.Fragment>
              ) : null}
            </CardContent>
            <div className={classes.actionSpacer} />
            <CardActions>
              <Typography variant="body1" className={classes.id}>
                {item.get('id')}
              </Typography>
              <div className={classes.actionSpacer} />
              {onPreview && (
                <PreviewButton onClick={() => onPreview(item.get('id'))} />
              )}
              {onDelete && (
                <IconButton onClick={onDelete}>
                  <DeleteIcon />
                </IconButton>
              )}
            </CardActions>
          </React.Fragment>
        ) : (
          item.get('id')
        )}
      </Card>
    );
  }
}

export default withStyles(styles)(ItemCompactCard);
