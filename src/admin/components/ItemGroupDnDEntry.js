import React from 'react';
import PropTypes from 'prop-types';
import { List } from 'immutable';
import { withStyles } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import ShuffleIcon from '@material-ui/icons/Shuffle';
import RepeatIcon from '@material-ui/icons/Repeat';
import DoneIcon from '@material-ui/icons/Done';
import DeleteIcon from '@material-ui/icons/Delete';
import { Draggable, Droppable } from 'react-beautiful-dnd';

import ItemCompactCardDrag from './ItemCompactCardDrag';

const styles = theme => ({
  inner: {
    minHeight: 175,
    minWidth: 150,
    padding: theme.spacing.unit,
    overflow: 'auto',
    width: '100%',
    display: 'inline-flex',
    flexDirection: 'row'
  },
  outer: {
    display: 'inline-flex',
    flexGrow: 1
  },
  container: {
    borderLeft: '10px solid grey',
    display: 'flex',
    alignItems: 'center',
    background: 'white'
  },
  settings: {
    display: 'inline-flex',
    flexDirection: 'column',
    float: 'left'
  }
});

class ItemGroupEntry extends React.PureComponent {
  static propTypes = {
    classes: PropTypes.object.isRequired,
    id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
    randomized: PropTypes.bool,
    jumpable: PropTypes.bool,
    needsToBeCorrect: PropTypes.bool,
    onRandomized: PropTypes.func,
    onJumpable: PropTypes.func,
    onNeedsToBeCorrect: PropTypes.func,
    onDelete: PropTypes.func,
    onDeleteItem: PropTypes.func,
    onPreviewItem: PropTypes.func.isRequired,
    items: PropTypes.instanceOf(List)
  };
  static defaultProps = {
    randomized: false,
    jumpable: false,
    needsToBeCorrect: false,
    onRandomized: null,
    onJumpable: null,
    onNeedsToBeCorrect: null,
    onDelete: null,
    onDeleteItem: null,
    items: new List([])
  };
  render() {
    const {
      classes,
      id,
      randomized,
      jumpable,
      needsToBeCorrect,
      onJumpable,
      onRandomized,
      onNeedsToBeCorrect,
      onDelete,
      onDeleteItem,
      onPreviewItem,
      items
    } = this.props;
    return (
      <Draggable
        index={id}
        draggableId={id}
        direction="horizontal"
        type="ITEM_GROUP"
      >
        {(provided, snapshot) => (
          <div
            ref={provided.innerRef}
            {...provided.draggableProps}
            {...provided.dragHandleProps}
          >
            <div className={classes.container}>
              <div className={classes.settings}>
                {onJumpable && (
                  <Tooltip title="Jumpable" placement="right">
                    <IconButton
                      onClick={() => onJumpable(id)}
                      color={jumpable ? 'primary' : 'default'}
                      aria-label="Jumpable"
                    >
                      <RepeatIcon />
                    </IconButton>
                  </Tooltip>
                )}
                {onRandomized && (
                  <Tooltip title="Randomized" placement="right">
                    <IconButton
                      onClick={() => onRandomized(id)}
                      color={randomized ? 'primary' : 'default'}
                      aria-label="Randomized"
                    >
                      <ShuffleIcon />
                    </IconButton>
                  </Tooltip>
                )}
                {onNeedsToBeCorrect && (
                  <Tooltip title="Needs to be correct" placement="right">
                    <IconButton
                      onClick={() => onNeedsToBeCorrect(id)}
                      color={needsToBeCorrect ? 'primary' : 'default'}
                      aria-label="Needs to be correct"
                    >
                      <DoneIcon />
                    </IconButton>
                  </Tooltip>
                )}
                {onDelete && (
                  <Tooltip title="Delete" placement="right">
                    <IconButton
                      onClick={() => onDelete(id)}
                      aria-label="Delete"
                    >
                      <DeleteIcon />
                    </IconButton>
                  </Tooltip>
                )}
              </div>
              <Droppable
                droppableId={`${id}`}
                direction="horizontal"
                type="ITEM"
              >
                {(provided, snapshot) => (
                  <div
                    ref={provided.innerRef}
                    {...provided.droppableProps}
                    className={classes.inner}
                  >
                    {items.map((item, k) => (
                      <ItemCompactCardDrag
                        item={item}
                        key={k}
                        index={k}
                        draggableId={`${id}-${k}`}
                        onDelete={() => onDeleteItem(id, k)}
                        onPreview={() => onPreviewItem(item.get('id'))}
                      />
                    ))}
                    {provided.placeholder}
                  </div>
                )}
              </Droppable>
            </div>
            {provided.placeholder}
          </div>
        )}
      </Draggable>
    );
  }
}

export default withStyles(styles)(ItemGroupEntry);
