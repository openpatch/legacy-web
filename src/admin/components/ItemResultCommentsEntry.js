/**
 *
 * ItemResultCommentsEntry
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import distanceInWordsToNow from 'date-fns/distance_in_words_to_now';
import Divider from '@material-ui/core/Divider';
import Typography from '@material-ui/core/Typography';

class ItemResultCommentsEntry extends React.Component {
  static propTypes = {
    children: PropTypes.element,
    active: PropTypes.bool,
    id: PropTypes.number.isRequired,
    category: PropTypes.string,
    text: PropTypes.string.isRequired,
    start_time: PropTypes.number,
    end_time: PropTypes.number,
    parent_id: PropTypes.number,
    replies: PropTypes.arrayOf(PropTypes.object),
    member_id: PropTypes.number.isRequired,
    member: PropTypes.shape({
      username: PropTypes.string.isRequired
    }),
    memberId: PropTypes.number.isRequired,
    created_on: PropTypes.string.isRequired,
    updated_on: PropTypes.string.isRequired,
    onReply: PropTypes.func,
    onDelete: PropTypes.func.isRequired,
    onClick: PropTypes.func
  };

  static defaultProps = {
    category: '',
    start_time: 0,
    active: false,
    end_time: 0,
    replies: [],
    onClick: () => {}
  };

  onReply = () => {
    const { id, onReply } = this.props;
    onReply(id);
  };

  onEdit = () => {
    const { id, onEdit } = this.props;
    onEdit(id);
  };

  onDelete = () => {
    const { id, parent_id, onDelete } = this.props;
    onDelete(id, parent_id);
  };
  onTimeClick = () => {
    const { start_time, onClick } = this.props;
    onClick(start_time);
  };

  render() {
    const {
      text,
      active,
      children,
      category,
      parent_id,
      start_time,
      memberId,
      member,
      end_time,
      replies,
      member_id,
      created_on
    } = this.props;

    const flatButton = {
      background: 'none',
      outline: 'none',
      border: 'none',
      fontSize: '0.75rem',
      color: 'rgba(0,0,0,0.54)',
      textDecoration: 'underline',
      cursor: 'pointer',
      fontWeigth: 400
    };

    return (
      <React.Fragment>
        <ListItem style={{ backgroundColor: active ? '#F5F5F5' : null }}>
          <ListItemText
            inset={parent_id !== null}
            disableTypography={true}
            secondary={
              <div style={{ display: 'flex', alignItems: 'center' }}>
                {category && (
                  <div
                    style={{
                      borderRadius: '50%',
                      backgroundColor: category,
                      marginRight: 5,
                      width: 10,
                      height: 10
                    }}
                  />
                )}
                <div style={{ flex: 1 }}>
                  <Typography variant="caption">
                    {member.username} - {distanceInWordsToNow(created_on)} ago
                    {!parent_id && (
                      <React.Fragment>
                        {' - '}
                        <button style={flatButton} onClick={this.onReply}>
                          Reply
                        </button>
                      </React.Fragment>
                    )}
                    {memberId === member_id && (
                      <React.Fragment>
                        {' - '}
                        <button style={flatButton} onClick={this.onDelete}>
                          Delete
                        </button>
                      </React.Fragment>
                    )}
                  </Typography>
                </div>
                {!parent_id && (
                  <button
                    style={{
                      display: 'flex',
                      flexDirection: 'column',
                      ...flatButton
                    }}
                    onClick={this.onTimeClick}
                  >
                    <Typography variant="caption" align="right">
                      {Math.round(start_time / 100) / 10} s
                    </Typography>
                    <Typography variant="caption" align="right">
                      {Math.round(end_time / 100) / 10} s
                    </Typography>
                  </button>
                )}
              </div>
            }
          >
            <Typography>{text}</Typography>
          </ListItemText>
        </ListItem>
        {replies.map(reply => (
          <ItemResultCommentsEntry
            key={reply.id}
            {...reply}
            memberId={memberId}
            onDelete={this.props.onDelete}
          />
        ))}
        {children}
        {!parent_id && (
          <li>
            <Divider />
          </li>
        )}
      </React.Fragment>
    );
  }
}

export default ItemResultCommentsEntry;
