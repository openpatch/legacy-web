import { createSelector } from 'reselect';
import { activeSelector as getActiveFormat } from './formats';
import { makeActiveTestResultSelector } from './tests';
import { dataSelector as membersDataSelector } from './members';
import { Map, fromJS } from 'immutable';

/**
 * Direct selector to the Items state domain
 */
const ItemsSelector = state => state.admin.get('items');

/**
 * Other specific selectors
 */

export const dataSelector = createSelector(ItemsSelector, substate =>
  substate.get('data')
);

export const activeSelector = createSelector(ItemsSelector, substate =>
  substate.get('active')
);

export const activeResultSelector = createSelector(ItemsSelector, substate =>
  substate.get('active_result')
);

export const resultsSelector = createSelector(ItemsSelector, substate =>
  substate.get('results')
);

export const statisticsSelector = createSelector(ItemsSelector, substate =>
  substate.get('statistics')
);

export const editSelector = createSelector(ItemsSelector, substate =>
  substate.get('edit')
);

export const filterSelector = createSelector(ItemsSelector, substate =>
  substate.get('filter')
);

export const unsavedSelector = createSelector(ItemsSelector, substate =>
  substate.get('unsaved')
);

export const itemPreviewPopupOpenSelector = createSelector(
  ItemsSelector,
  substate => substate.get('item_preview_popup_open')
);

export const itemVisualizationPopupOpenSelector = createSelector(
  ItemsSelector,
  substate => substate.get('item_visualization_popup_open')
);

export const itemStatisticPopupOpenSelector = createSelector(
  ItemsSelector,
  substate => substate.get('item_statistic_popup_open')
);

export const itemCreateStatusSelector = createSelector(
  ItemsSelector,
  substate =>
    fromJS({
      posting: substate.get('posting'),
      posting_failed: substate.get('posting_failed'),
      post: substate.get('post')
    })
);

export const itemMembersPermittedStatusSelector = createSelector(
  ItemsSelector,
  substate =>
    fromJS({
      putting: substate.get('permitted_putting'),
      putting_failed: substate.get('permitted_putting_failed'),
      deleting: substate.get('permitted_deleting'),
      deleting_failed: substate.get('permitted_deleting_failed')
    })
);

export const makeEvaluationSelector = () =>
  createSelector(ItemsSelector, substate =>
    fromJS({
      evaluation: substate.get('evaluation'),
      evaluating: substate.get('evaluating'),
      evaluating_failed: substate.get('evaluating_failed')
    })
  );

export const makeEditStatusSelector = () =>
  createSelector(ItemsSelector, substate =>
    fromJS({
      unsaved: substate.get('unsaved'),
      saving: substate.get('posting') || substate.get('putting'),
      savingFailed:
        substate.get('posting_failed') || substate.get('putting_failed'),
      existing: substate.get('existing'),
      fetching: substate.get('fetching')
    })
  );

export const makeItemsActiveFormatSelector = () =>
  createSelector([dataSelector, getActiveFormat], (data, activeFormat) => {
    return data
      .filter(value => value.getIn(['format', 'type']) === activeFormat)
      .toList();
  });

export const makeItemsActiveFormatFilterSelector = () =>
  createSelector(
    [dataSelector, getActiveFormat, filterSelector],
    (data, activeFormat, filter) =>
      data
        .filter(value => value.getIn(['format', 'type']) === activeFormat)
        .filter(
          value =>
            filter && filter !== '' ? value.get('name').includes(filter) : true
        )
        .toList()
  );

export const makeActiveItemSelector = () =>
  createSelector([dataSelector, activeSelector], (data, active) =>
    data.get(active, new Map({}))
  );

export const makeActiveItemResultSelector = () =>
  createSelector([resultsSelector, activeResultSelector], (results, active) =>
    results.get(active, new Map({}))
  );

export const makeActiveItemResultsSelector = () =>
  createSelector([resultsSelector, activeSelector], (results, active) =>
    results.filter(value => value.get('item_id') === active).toList()
  );

export const makeActiveItemStatisticSelector = () =>
  createSelector([statisticsSelector, activeSelector], (statistics, active) =>
    statistics.get(active, fromJS({ format: {}, responses: {} }))
  );

export const makeActiveItemPermittedMembersSelector = () =>
  createSelector(
    [dataSelector, activeSelector, membersDataSelector],
    (data, active, members) =>
      data
        .getIn([active, 'members_permitted'], fromJS([]))
        .map(memberId => members.get(memberId))
  );

export const makeActiveTestResultItemsSelector = () =>
  createSelector(
    [resultsSelector, makeActiveTestResultSelector()],
    (results, activeTestResult) => {
      return results
        .filter(
          value =>
            value.get('session_hash') === activeTestResult.get('session_hash')
        )
        .toList();
    }
  );

/**
 * Default selector used by Items
 */

export default () => createSelector(ItemsSelector, substate => substate);
