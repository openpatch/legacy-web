import { fromJS, Map } from 'immutable';
import { createSelector } from 'reselect';
import { dataSelector as getItemsData } from './items';
import { dataSelector as membersDataSelector } from './members';
import { dataSelector as surveysDataSelector } from './surveys';

/**
 * Direct selector to the Tests state domain
 */
const TestsSelector = state => state.admin.get('tests');

/**
 * Other specific selectors
 */
export const dataSelector = createSelector(TestsSelector, substate =>
  substate.get('data')
);

export const activeSelector = createSelector(TestsSelector, substate =>
  substate.get('active')
);

export const resultsSelector = createSelector(TestsSelector, substate =>
  substate.get('results')
);

export const editSelector = createSelector(TestsSelector, substate =>
  substate.get('edit')
);

export const editDataSelector = createSelector(TestsSelector, substate =>
  substate.get('edit')
);

export const resultsFilterSelector = createSelector(TestsSelector, substate =>
  substate.get('results_filter')
);

export const resultsSelectedSelector = createSelector(TestsSelector, substate =>
  substate.get('results_selected')
);

export const activeResultSelector = createSelector(TestsSelector, substate =>
  substate.get('active_result')
);

export const testMembersPermittedStatusSelector = createSelector(
  TestsSelector,
  substate =>
    fromJS({
      putting: substate.get('permitted_putting'),
      putting_failed: substate.get('permitted_putting_failed'),
      deleting: substate.get('permitted_deleting'),
      deleting_failed: substate.get('permitted_deleting_failed')
    })
);

export const makeEditStatusSelector = () =>
  createSelector(TestsSelector, substate => ({
    saving: substate.get('posting') || substate.get('putting'),
    savingFailed:
      substate.get('posting_failed') || substate.get('putting_failed'),
    existing: substate.get('existing'),
    fetching: substate.get('fetching')
  }));

export const makeEditItemGroupSelector = () =>
  createSelector([editSelector, getItemsData], (edit, itemsData) =>
    edit
      .get('item_groups')
      .map(itemGroup =>
        itemGroup.update('items', items =>
          items.map(itemId => itemsData.get(itemId))
        )
      )
  );

export const makeDataSelector = () =>
  createSelector(dataSelector, substate => substate.toList());

export const makeActiveTestSelector = () =>
  createSelector([dataSelector, activeSelector], (data, active) =>
    data.get(active, new Map({}))
  );

export const makeActiveTestResultsSelector = () =>
  createSelector([resultsSelector, activeSelector], (results, active) =>
    results.filter(value => value.get('test_id') === active).toList()
  );

export const makeActiveTestFilteredResultsSelector = () =>
  createSelector(
    [resultsSelector, activeSelector, resultsFilterSelector],
    (results, active, resultsFilter) =>
      results
        .filter(result => result.get('test_id') === active)
        .filter(
          result =>
            resultsFilter.get('survey_ids') &&
            resultsFilter.get('survey_ids').size > 0
              ? resultsFilter
                  .get('survey_ids')
                  .includes(result.get('survey_id'))
              : true
        )
        .toList()
  );

export const makeActiveTestSurveysSelector = () =>
  createSelector([activeSelector, surveysDataSelector], (active, surveysData) =>
    surveysData.filter(survey => survey.get('test_id') === active).toList()
  );

export const makeActiveTestItemGroupsSelector = () =>
  createSelector(
    [dataSelector, activeSelector, getItemsData],
    (data, active, itemsData) =>
      data
        .getIn([active, 'item_groups'], fromJS([]))
        .map(itemGroup =>
          itemGroup.update('items', items =>
            items.map(itemId => itemsData.get(itemId))
          )
        )
  );

export const makeActiveTestPermittedMembersSelector = () =>
  createSelector(
    [dataSelector, activeSelector, membersDataSelector],
    (data, active, members) =>
      data
        .getIn([active, 'members_permitted'], fromJS([]))
        .map(memberId => members.get(memberId))
  );

export const makeActiveTestResultSelector = () =>
  createSelector([resultsSelector, activeResultSelector], (results, active) =>
    results.get(active, new Map({}))
  );

/**
 * Default selector used by Tests
 */

export default () => createSelector(TestsSelector, substate => substate);
