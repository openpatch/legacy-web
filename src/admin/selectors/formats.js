import { createSelector } from 'reselect';
import { fromJS } from 'immutable';

import formats from '../../formats';
/**
 * Direct selector to the Formats state domain
 */
const FormatsSelector = state => state.admin.get('formats');

/**
 * Other specific selectors
 */

// only select formats which are currently implemented
export const dataSelector = createSelector(FormatsSelector, substate =>
  substate
    .get('data')
    .filter(value => formats.hasOwnProperty(value.get('type')))
);

export const activeSelector = createSelector(FormatsSelector, substate =>
  substate.get('active')
);

export const makeActiveFormatSelector = () =>
  createSelector([dataSelector, activeSelector], (data, active) =>
    data.filter(value => value.get('type') === active).get(0, fromJS({}))
  );

/**
 * Default selector used by Formats
 */

export default () => createSelector(FormatsSelector, substate => substate);
