import { createSelector } from 'reselect';
import { fromJS } from 'immutable';

/**
 * Direct selector to the Members state domain
 */
const MembersSelector = state => state.admin.get('members');

/**
 * Other specific selectors
 */

export const dataSelector = createSelector(MembersSelector, substate =>
  substate.get('data')
);

export const activeSelector = createSelector(MembersSelector, substate =>
  substate.get('active')
);

export const editSelector = createSelector(MembersSelector, substate =>
  substate.get('edit')
);

export const filterSelector = createSelector(MembersSelector, substate =>
  substate.get('filter')
);

export const makeFilteredDataSelector = () =>
  createSelector([dataSelector, filterSelector], (data, filter) =>
    data
      .filter(value => (filter ? value.get('username').includes(filter) : true))
      .toList()
  );

export const makeActiveMemberSelector = () =>
  createSelector([dataSelector, activeSelector], (data, active) =>
    data.get(active, fromJS({}))
  );

export const makeEditStatusSelector = () =>
  createSelector(MembersSelector, substate => ({
    saving: substate.get('posting') || substate.get('putting'),
    savingFailed:
      substate.get('posting_failed') || substate.get('putting_failed'),
    existing: substate.get('existing'),
    fetching: substate.get('fetching')
  }));

export const makeCreateDialogOpenSelector = () =>
  createSelector(MembersSelector, substate =>
    substate.get('create_dialog_open')
  );

/**
 * Default selector used by Members
 */

export default () => createSelector(MembersSelector, substate => substate);
