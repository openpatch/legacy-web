import { Map, fromJS } from 'immutable';
import { createSelector } from 'reselect';
import { dataSelector as membersDataSelector } from './members';
import { resultsSelector as testResultsSelector } from './tests';

/**
 * Direct selector to the Surveys state domain
 */
const SurveysSelector = state => state.admin.get('surveys');

/**
 * Other specific selectors
 */

export const dataSelector = createSelector(SurveysSelector, substate =>
  substate.get('data')
);

export const activeSelector = createSelector(SurveysSelector, substate =>
  substate.get('active')
);

export const editDataSelector = createSelector(SurveysSelector, substate =>
  substate.get('edit')
);

export const surveyMembersPermittedStatusSelector = createSelector(
  SurveysSelector,
  substate =>
    fromJS({
      putting: substate.get('permitted_putting'),
      putting_failed: substate.get('permitted_putting_failed'),
      deleting: substate.get('permitted_deleting'),
      deleting_failed: substate.get('permitted_deleting_failed')
    })
);

export const makeDataSelector = () =>
  createSelector(dataSelector, substate => substate.toList());

export const makeActiveSurveySelector = () =>
  createSelector([dataSelector, activeSelector], (data, active) =>
    data.get(active, new Map({}))
  );

export const makeActiveSurveyPermittedMembersSelector = () =>
  createSelector(
    [dataSelector, activeSelector, membersDataSelector],
    (data, active, members) =>
      data
        .getIn([active, 'members_permitted'], fromJS([]))
        .map(memberId => members.get(memberId))
  );

export const makeActiveSurveyResultsSelector = () =>
  createSelector([activeSelector, testResultsSelector], (active, testResults) =>
    testResults
      .filter(testResult => testResult.get('survey_id') === active)
      .toList()
  );

export const makeEditStatusSelector = () =>
  createSelector(SurveysSelector, substate =>
    fromJS({
      saving: substate.get('posting') || substate.get('putting'),
      savingFailed:
        substate.get('posting_failed') || substate.get('putting_failed'),
      existing: substate.get('existing'),
      fetching: substate.get('fetching')
    })
  );

export const makeCreateDialogOpenSelector = () =>
  createSelector(SurveysSelector, substate =>
    substate.get('create_dialog_open')
  );

/**
 * Default selector used by Surveys
 */

export default () =>
  createSelector(SurveysSelector, substate => substate.toJS());
