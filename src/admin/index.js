import React from 'react';
import PropTypes from 'prop-types';
import { Switch, Route } from 'react-router-dom';
import { connect } from 'react-redux';
import Loadable from 'react-loadable';

import AdminLayout from './containers/AdminLayout';
import {
  AdminRole,
  AuthorRole,
  DataAnalystRole
} from '../common/containers/Authorization';
import Loading from '../common/components/Loading';

// import Dashboard from './containers/Dashboard';
// import Test from './containers/Test';
// import Tests from './containers/Tests';
// import TestResult from './containers/TestResult';
// import TestCreate from './containers/TestCreate';
// import Format from './containers/Format';
// import Members from './containers/Members';
// import Member from './containers/Member';
// import MemberCreate from './containers/MemberCreate';
// import Item from './containers/Item';
// import ItemEdit from './containers/ItemEdit';
// import ItemResult from './containers/ItemResult';
// import Survey from './containers/Survey';
// import Surveys from './containers/Surveys';
// import SurveyCreate from './containers/SurveyCreate';

const Dashboard = Loadable({
  loader: () => import('./containers/Dashboard'),
  loading: Loading
});

const Test = Loadable({
  loader: () => import('./containers/Test'),
  loading: Loading
});

const Tests = Loadable({
  loader: () => import('./containers/Tests'),
  loading: Loading
});

const TestResult = Loadable({
  loader: () => import('./containers/TestResult'),
  loading: Loading
});

const TestCreate = Loadable({
  loader: () => import('./containers/TestCreate'),
  loading: Loading
});

const Format = Loadable({
  loader: () => import('./containers/Format'),
  loading: Loading
});

const Members = Loadable({
  loader: () => import('./containers/Members'),
  loading: Loading
});

const Member = Loadable({
  loader: () => import('./containers/Member'),
  loading: Loading
});

const MemberCreate = Loadable({
  loader: () => import('./containers/MemberCreate'),
  loading: Loading
});

const Item = Loadable({
  loader: () => import('./containers/Item'),
  loading: Loading
});

const ItemEdit = Loadable({
  loader: () => import('./containers/ItemEdit'),
  loading: Loading
});

const ItemResult = Loadable({
  loader: () => import('./containers/ItemResult'),
  loading: Loading
});

const Surveys = Loadable({
  loader: () => import('./containers/Surveys'),
  loading: Loading
});

const Survey = Loadable({
  loader: () => import('./containers/Survey'),
  loading: Loading
});

const SurveyCreate = Loadable({
  loader: () => import('./containers/SurveyCreate'),
  loading: Loading
});

class Admin extends React.Component {
  static propTypes = {
    match: PropTypes.object.isRequired
  };

  render() {
    const { match } = this.props;
    return (
      <AdminLayout url={match.url}>
        <Switch>
          <Route
            exact
            path={`${match.url}/`}
            component={DataAnalystRole(Dashboard)}
          />
          <Route
            exact
            path={`${match.url}/formats/:format`}
            component={DataAnalystRole(Format)}
          />
          <Route
            exact
            path={`${match.url}/formats/:format/:itemId/edit`}
            component={DataAnalystRole(ItemEdit)}
          />
          <Route
            exact
            path={`${match.url}/formats/:format/results/:resultId`}
            component={DataAnalystRole(ItemResult)}
          />
          <Route
            exact
            path={`${match.url}/formats/:format/create`}
            component={AuthorRole(ItemEdit)}
          />
          <Route
            exact
            path={`${match.url}/items/:itemId`}
            component={DataAnalystRole(Item)}
          />
          <Route
            exact
            path={`${match.url}/items/:itemId/results/:resultId`}
            component={DataAnalystRole(ItemResult)}
          />
          <Route
            exact
            path={`${match.url}/tests`}
            component={DataAnalystRole(Tests)}
          />
          <Route
            exact
            path={`${match.url}/tests/create`}
            component={AuthorRole(TestCreate)}
          />
          <Route
            exact
            path={`${match.url}/tests/:testId/edit`}
            component={AuthorRole(TestCreate)}
          />
          <Route
            exact
            path={`${match.url}/tests/:testId`}
            component={DataAnalystRole(Test)}
          />
          <Route
            exact
            path={`${match.url}/tests/:testId/results/:resultId`}
            component={DataAnalystRole(TestResult)}
          />
          <Route
            exact
            path={`${match.url}/members`}
            component={AdminRole(Members)}
          />
          <Route
            path={`${match.url}/members/me`}
            component={DataAnalystRole(Member)}
          />
          <Route
            exact
            path={`${match.url}/members/create`}
            component={AdminRole(MemberCreate)}
          />
          <Route
            path={`${match.url}/members/:member`}
            component={AdminRole(Member)}
          />
          <Route
            exact
            path={`${match.url}/members/:member/edit`}
            component={AdminRole(MemberCreate)}
          />
          <Route
            exact
            path={`${match.url}/surveys`}
            component={DataAnalystRole(Surveys)}
          />
          <Route
            exact
            path={`${match.url}/surveys/create`}
            component={AuthorRole(SurveyCreate)}
          />
          <Route
            path={`${match.url}/surveys/:surveyId`}
            component={DataAnalystRole(Survey)}
          />
        </Switch>
      </AdminLayout>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return {
    dispatch
  };
}

export default connect(
  null,
  mapDispatchToProps
)(Admin);
