/**
 *
 * Members messages
 *
 * This contains all the text for the domain
 *
 */

import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'commoop.admin.members.header',
    defaultMessage: 'This is members container!'
  }
});
