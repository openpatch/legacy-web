/**
 *
 * Formats messages
 *
 * This contains all the text for the domain
 *
 */

import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'commoop.admin.formats.header',
    defaultMessage: 'This is formats container!'
  },
  unsupported: {
    id: 'commoop.admin.formats.unsupported',
    defaultMessage: 'Unsupported format'
  }
});
