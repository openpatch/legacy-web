/**
 *
 * Surveys messages
 *
 * This contains all the text for the domain
 *
 */

import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'commoop.admin.surveys.header',
    defaultMessage: 'This is surveys container!'
  },
  resetSurveyResultsTitle: {
    id: 'commoop.admin.suvreys.resetSurveyResultsTitle',
    defaultMessage: 'Reset Survey Results'
  },
  resetSurveyResultsContent: {
    id: 'commoop.admin.survey.resetSurveyResultsContent',
    defaultMessage:
      'Do you really want to reset the survey results? The data will be lost after you confirm this operation!'
  }
});
