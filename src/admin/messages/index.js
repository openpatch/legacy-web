import { defineMessages } from 'react-intl';

export default defineMessages({
  dashboard: {
    id: 'commoop.admin.dashboard',
    defaultMessage: 'Dashboard'
  },
  formats: {
    id: 'commoop.admin.formats',
    defaultMessage: 'Formats'
  },
  tests: {
    id: 'commoop.admin.tests',
    defaultMessage: 'Tests'
  },
  surveys: {
    id: 'commoop.admin.surveys',
    defaultMessage: 'Surveys'
  },
  members: {
    id: 'commoop.admin.members',
    defaultMessage: 'Members'
  },
  settings: {
    id: 'commoop.admin.settings',
    defaultMessage: 'Settings'
  },
  myProfile: {
    id: 'commoop.admin.myProfile',
    defaultMessage: 'My Profile'
  },
  reportABug: {
    id: 'commoop.admin.reportABug',
    defaultMessage: 'Report a Bug'
  },
  help: {
    id: 'commoop.admin.help',
    defaultMessage: 'Help'
  }
});
