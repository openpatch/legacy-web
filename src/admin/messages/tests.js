/**
 *
 * Tests messages
 *
 * This contains all the text for the domain
 *
 */

import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'commoop.admin.tests.header',
    defaultMessage: 'This is tests container!'
  },
  deleteSelectedResultsTitle: {
    id: 'commoop.admin.tests.deleteSelectedResults',
    defaultMessage: 'Delete Selected Results'
  },
  deleteSelectedResultsContent: {
    id: 'commoop.admin.test.deleteSelectedResultsContent',
    defaultMessage:
      'Do you really want to delete {resultsCount, number} {resultsCount, plural, one {result} other{results}}? The data will be lost after you confirm this operation!'
  }
});
