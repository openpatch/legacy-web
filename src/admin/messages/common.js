/**
 *
 * Common messages
 *
 * This contains all the text for the domain
 *
 */

import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'commoop.admin.common.save',
    defaultMessage: 'Save'
  },
  preview: {
    id: 'commoop.admin.common.preview',
    defaultMessage: 'Preview'
  },
  log: {
    id: 'commoop.admin.common.log',
    defaultMessage: 'Log'
  },
  form: {
    id: 'commoop.admin.common.form',
    defaultMessage: 'Form'
  },
  source: {
    id: 'commoop.admin.common.source',
    defaultMessage: 'Source'
  }
});
