/**
 *
 * Items messages
 *
 * This contains all the text for the domain
 *
 */

import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'commoop.admin.items.header',
    defaultMessage: 'This is items container!'
  }
});
