import { combineReducers } from 'redux-immutable';

import tests from './tests';
import members from './members';
import surveys from './surveys';
import formats from './formats';
import items from './items';

const adminReducers = combineReducers({
  tests,
  members,
  surveys,
  formats,
  items
});

export default adminReducers;
