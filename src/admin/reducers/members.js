/*
 *
 * Members reducer
 *
 */

import { fromJS } from 'immutable';
import {
  DEFAULT_ACTION,
  FETCH_MEMBERS,
  FETCH_MEMBERS_COMMIT,
  FETCH_MEMBERS_ROLLBACK,
  FILTER,
  SET_ACTIVE,
  SET_EDIT_DATA,
  RESET_EDIT_DATA,
  FETCH_EDIT_DATA,
  FETCH_EDIT_DATA_COMMIT,
  FETCH_EDIT_DATA_ROLLBACK,
  FETCH_MEMBER,
  FETCH_MEMBER_COMMIT,
  FETCH_MEMBER_ROLLBACK,
  POST_MEMBER,
  POST_MEMBER_COMMIT,
  POST_MEMBER_ROLLBACK,
  DELETE_MEMBER,
  DELETE_MEMBER_COMMIT,
  DELETE_MEMBER_ROLLBACK,
  PUT_MEMBER,
  PUT_MEMBER_COMMIT,
  PUT_MEMBER_ROLLBACK,
  OPEN_MEMBER_CREATE_DIALOG,
  CLOSE_MEMBER_CREATE_DIALOG,
  FETCH_MEMBERS_PERMITTED_FOR_ITEM_ID,
  FETCH_MEMBERS_PERMITTED_FOR_ITEM_ID_COMMIT,
  FETCH_MEMBERS_PERMITTED_FOR_ITEM_ID_ROLLBACK,
  FETCH_MEMBERS_PERMITTED_FOR_SURVEY_ID,
  FETCH_MEMBERS_PERMITTED_FOR_SURVEY_ID_COMMIT,
  FETCH_MEMBERS_PERMITTED_FOR_SURVEY_ID_ROLLBACK,
  FETCH_MEMBERS_PERMITTED_FOR_TEST_ID,
  FETCH_MEMBERS_PERMITTED_FOR_TEST_ID_COMMIT,
  FETCH_MEMBERS_PERMITTED_FOR_TEST_ID_ROLLBACK
} from '../constants/members';

const initialState = fromJS({
  data: {},
  edit: {}
});

function MembersReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;
    case FETCH_MEMBER:
    case FETCH_MEMBERS_PERMITTED_FOR_ITEM_ID:
    case FETCH_MEMBERS_PERMITTED_FOR_TEST_ID:
    case FETCH_MEMBERS_PERMITTED_FOR_SURVEY_ID:
    case FETCH_MEMBERS:
      return state.set('fetching').delete('fetching_failed');
    case FETCH_MEMBER_ROLLBACK:
    case FETCH_MEMBERS_PERMITTED_FOR_ITEM_ID_ROLLBACK:
    case FETCH_MEMBERS_PERMITTED_FOR_TEST_ID_ROLLBACK:
    case FETCH_MEMBERS_PERMITTED_FOR_SURVEY_ID_ROLLBACK:
    case FETCH_MEMBERS_ROLLBACK:
      return state.delete('fetching').set('fetching_failed');
    case FETCH_MEMBER_COMMIT:
      return state.setIn(
        ['data', action.payload.member.id],
        fromJS(action.payload.member)
      );
    case FETCH_MEMBERS_COMMIT:
      let data = fromJS({});
      let members = action.payload.members;
      members.forEach(member => (data = data.set(member.id, fromJS(member))));
      return state.delete('fetching').set('data', data);
    case FETCH_MEMBERS_PERMITTED_FOR_ITEM_ID_COMMIT:
    case FETCH_MEMBERS_PERMITTED_FOR_TEST_ID_COMMIT:
    case FETCH_MEMBERS_PERMITTED_FOR_SURVEY_ID_COMMIT:
      data = state.get('data');
      members = action.payload.members;
      members.forEach(member => (data = data.set(member.id, fromJS(member))));
      return state.delete('fetching').set('data', data);
    case POST_MEMBER:
      return state.set('posting', true).delete('posting_failed');
    case POST_MEMBER_COMMIT:
      return state
        .delete('posting')
        .setIn(['edit', 'id'], action.payload.member_id);
    case POST_MEMBER_ROLLBACK:
      return state.delete('posting').set('posting_failed', true);
    case PUT_MEMBER:
      return state.set('putting', true).delete('putting_failed');
    case PUT_MEMBER_COMMIT:
      return state.delete('putting');
    case PUT_MEMBER_ROLLBACK:
      return state.delete('putting').set('putting_failed', true);
    case DELETE_MEMBER:
      return state.set('deleting', action.payload.id).delete('deleting_failed');
    case DELETE_MEMBER_COMMIT:
      return state.delete('deleting').deleteIn(['data', action.meta.id]);
    case DELETE_MEMBER_ROLLBACK:
      return state.set('deleting_failed', true).delete('deleting');
    case OPEN_MEMBER_CREATE_DIALOG:
      return state.set('create_dialog_open', true);
    case CLOSE_MEMBER_CREATE_DIALOG:
      return state.set('create_dialog_open', false);
    case FETCH_EDIT_DATA:
      return state.set('fetching', true);
    case FETCH_EDIT_DATA_COMMIT:
      return state
        .delete('fetching')
        .set('edit', fromJS(action.payload.test).delete('member'));
    case FETCH_EDIT_DATA_ROLLBACK:
      return state.delete('fetching').set('edit', initialState.get('edit'));
    case SET_EDIT_DATA:
      return state.set('edit', fromJS(action.payload.data));
    case RESET_EDIT_DATA:
      return state.set('edit', initialState.get('edit'));
    case FILTER:
      return state.set('filter', action.payload.filter);
    case SET_ACTIVE:
      return state.set('active', action.payload.id);
    default:
      return state;
  }
}

export default MembersReducer;
