/*
 *
 * Items reducer
 *
 */

import { fromJS } from 'immutable';
import {
  DEFAULT_ACTION,
  FETCH_ITEMS,
  FETCH_ITEMS_COMMIT,
  FETCH_ITEMS_ROLLBACK,
  POST_ITEM,
  POST_ITEM_COMMIT,
  POST_ITEM_ROLLBACK,
  SET_ACTIVE,
  FETCH_RESULTS_FOR_ID,
  FETCH_RESULTS_FOR_ID_COMMIT,
  FETCH_RESULTS_FOR_ID_ROLLBACK,
  FETCH_STATISTIC_FOR_ID,
  FETCH_STATISTIC_FOR_ID_COMMIT,
  FETCH_STATISTIC_FOR_ID_ROLLBACK,
  FETCH_ITEM,
  FETCH_ITEM_COMMIT,
  FETCH_ITEM_ROLLBACK,
  FETCH_ITEMS_FOR_FORMAT,
  FETCH_ITEMS_FOR_FORMAT_COMMIT,
  FETCH_ITEMS_FOR_FORMAT_ROLLBACK,
  FETCH_FAVORITE_ITEMS,
  FETCH_FAVORITE_ITEMS_COMMIT,
  FETCH_FAVORITE_ITEMS_ROLLBACK,
  PUT_ITEM,
  PUT_ITEM_COMMIT,
  PUT_ITEM_ROLLBACK,
  DELETE_ITEM,
  DELETE_ITEM_COMMIT,
  DELETE_ITEM_ROLLBACK,
  SET_EDIT_DATA,
  FETCH_EDIT_DATA,
  FETCH_EDIT_DATA_COMMIT,
  FETCH_EDIT_DATA_ROLLBACK,
  OPEN_ITEM_PREVIEW_POPUP,
  CLOSE_ITEM_PREVIEW_POPUP,
  OPEN_ITEM_STATISTIC_POPUP,
  CLOSE_ITEM_STATISTIC_POPUP,
  OPEN_ITEM_VISUALIZATION_POPUP,
  CLOSE_ITEM_VISUALIZATION_POPUP,
  FETCH_ITEMS_FOR_TEST_ID_COMMIT,
  FETCH_ITEMS_FOR_TEST_ID,
  FETCH_ITEMS_FOR_TEST_ID_ROLLBACK,
  EVALUATE_ITEM,
  EVALUATE_ITEM_COMMIT,
  EVALUATE_ITEM_ROLLBACK,
  RESET_EDIT_DATA,
  SET_FILTER,
  ADD_MEMBER_TO_PERMITTED_FOR_ID,
  ADD_MEMBER_TO_PERMITTED_FOR_ID_COMMIT,
  ADD_MEMBER_TO_PERMITTED_FOR_ID_ROLLBACK,
  REMOVE_MEMBER_FROM_PERMITTED_FOR_ID,
  REMOVE_MEMBER_FROM_PERMITTED_FOR_ID_COMMIT,
  REMOVE_MEMBER_FROM_PERMITTED_FOR_ID_ROLLBACK,
  SET_ACTIVE_RESULT,
  FETCH_RESULT,
  FETCH_RESULT_COMMIT,
  FETCH_RESULT_ROLLBACK,
  FETCH_RESULTS_FOR_TEST_RESULT_ID,
  FETCH_RESULTS_FOR_TEST_RESULT_ID_COMMIT,
  FETCH_RESULTS_FOR_TEST_RESULT_ID_ROLLBACK,
  PUT_ITEM_RESULT_COMMENT,
  PUT_ITEM_RESULT_COMMENT_COMMIT,
  PUT_ITEM_RESULT_COMMENT_ROLLBACK,
  DELETE_ITEM_RESULT_COMMENT,
  DELETE_ITEM_RESULT_COMMENT_ROLLBACK,
  DELETE_ITEM_RESULT_COMMENT_COMMIT
} from '../constants/items';

const initialState = fromJS({
  data: {},
  results: {},
  statistics: {},
  edit: {
    content: {},
    solution: {}
  },
  unsaved: false
});

function ItemsReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;
    case SET_FILTER:
      return state.set('filter', action.payload.filter);
    case SET_ACTIVE_RESULT:
      return state.set('active_result', Number(action.payload.active));
    case SET_ACTIVE:
      return state
        .set('active', Number(action.payload.active))
        .delete('evaluation')
        .delete('fetching');
    case FETCH_ITEMS_FOR_TEST_ID:
    case FETCH_ITEMS_FOR_FORMAT:
    case FETCH_FAVORITE_ITEMS:
    case FETCH_ITEMS:
      return state.set('fetching', true).delete('fetching_failed');
    case FETCH_ITEMS_COMMIT:
      let data = fromJS({});
      let items = action.payload.items;
      items.forEach(item => (data = data.set(item.id, fromJS(item))));
      return state.delete('fetching').set('data', data);
    case FETCH_FAVORITE_ITEMS_COMMIT:
    case FETCH_ITEMS_FOR_FORMAT_COMMIT:
    case FETCH_ITEMS_FOR_TEST_ID_COMMIT:
      return state.delete('fetching').set(
        'data',
        state.get('data').withMutations(data => {
          for (let i in action.payload.items) {
            const item = action.payload.items[i];
            data.set(item.id, fromJS(item));
          }
        })
      );
    case FETCH_ITEMS_FOR_TEST_ID_ROLLBACK:
    case FETCH_FAVORITE_ITEMS_ROLLBACK:
    case FETCH_ITEMS_FOR_FORMAT_ROLLBACK:
    case FETCH_ITEMS_ROLLBACK:
      return state.delete('fetching').set('fetching_failed', true);
    case FETCH_ITEM:
      return state.set('fetching', true).delete('fetching_failed');
    case FETCH_ITEM_COMMIT:
      const item = action.payload.item;
      return state
        .delete('fetching')
        .setIn(['data', item.id], fromJS(item))
        .set('edit', fromJS(item));
    case FETCH_ITEM_ROLLBACK:
      return state.delete('fetching').set('fetching_failed', true);
    case POST_ITEM:
      return state.set('posting', true).delete('posting_failed');
    case POST_ITEM_COMMIT:
      if (action.payload.status === 'existing') {
        return state
          .delete('posting')
          .set('posting_failed', true)
          .set('existing', action.payload.item_id);
      }
      return state
        .delete('posting')
        .set('unsaved', false)
        .setIn(['edit', 'id'], action.payload.item_id);
    case POST_ITEM_ROLLBACK:
      return state.delete('posting').set('posting_failed', true);
    case PUT_ITEM:
      return state.set('putting', true).delete('putting_failed');
    case PUT_ITEM_COMMIT:
      return state.delete('putting').set('unsaved', false);
    case PUT_ITEM_ROLLBACK:
      return state.delete('putting').set('putting_failed', true);
    case DELETE_ITEM:
      return state.set('deleting', action.payload.id).delete('deleting_failed');
    case DELETE_ITEM_COMMIT:
      return state.delete('deleting').deleteIn(['data', action.meta.id]);
    case DELETE_ITEM_ROLLBACK:
      return state.set('deleting_failed', true).delete('deleting');
    case DELETE_ITEM_RESULT_COMMENT:
      return state.set('deleting', true).delete('deleting_failed');
    case DELETE_ITEM_RESULT_COMMENT_ROLLBACK:
      return state.set('deleting_failed', true).delete('deleting');
    case DELETE_ITEM_RESULT_COMMENT_COMMIT:
      return state.updateIn(
        ['results', action.meta.resultId, 'item_result_comments'],
        itemResultComments => {
          if (action.meta.parentId !== null) {
            let replyId;
            const commentId = itemResultComments.findKey(comment => {
              const tmpReplyId = comment
                .get('replies')
                .findKey(reply => reply.get('id') === action.meta.commentId);
              if (tmpReplyId !== null) {
                replyId = tmpReplyId;
                return true;
              } else {
                return false;
              }
            });
            return itemResultComments.deleteIn([commentId, 'replies', replyId]);
          }
          const commentId = itemResultComments.findKey(
            comment => comment.get('id') === action.meta.commentId
          );
          return itemResultComments.delete(commentId);
        }
      );
    case SET_EDIT_DATA:
      return state
        .set('edit', fromJS(action.payload.data))
        .delete('evaluation')
        .set('unsaved', true);
    case RESET_EDIT_DATA:
      return state.set('edit', initialState.get('edit')).delete('evaluation');
    case FETCH_EDIT_DATA:
      return state.set('fetching', true);
    case FETCH_EDIT_DATA_COMMIT:
      return state
        .delete('fetching')
        .set('edit', fromJS(action.payload.item).delete('member'));
    case FETCH_EDIT_DATA_ROLLBACK:
      return state.delete('fetching').set('edit', initialState.get('edit'));
    case FETCH_STATISTIC_FOR_ID:
      return state
        .set('fetching_statistic', true)
        .delete('fetching_statistic_failed');
    case FETCH_STATISTIC_FOR_ID_COMMIT:
      return state
        .delete('fetching_statistic')
        .setIn(
          ['statistics', action.meta.id],
          fromJS(action.payload.item_statistic)
        );
    case FETCH_STATISTIC_FOR_ID_ROLLBACK:
      return state
        .delete('fetching_statistic')
        .set('fetching_statistic_failed', true);
    case FETCH_RESULT:
    case FETCH_RESULTS_FOR_ID:
    case FETCH_RESULTS_FOR_TEST_RESULT_ID:
      return state.set('fetching', true).delete('fetching_failed');
    case FETCH_RESULT_COMMIT:
      return state
        .setIn(
          ['results', action.payload.result.id],
          fromJS(action.payload.result)
        )
        .delete('fetching');
    case FETCH_RESULTS_FOR_ID_COMMIT:
    case FETCH_RESULTS_FOR_TEST_RESULT_ID_COMMIT:
      let results = state.get('results');
      const itemResults = action.payload.results;
      itemResults.forEach(
        itemResult => (results = results.set(itemResult.id, fromJS(itemResult)))
      );
      return state.delete('fetching').set('results', results);
    case FETCH_RESULT_ROLLBACK:
    case FETCH_RESULTS_FOR_TEST_RESULT_ID_ROLLBACK:
    case FETCH_RESULTS_FOR_ID_ROLLBACK:
      return state.delete('fetching').set('fetching_failed', true);
    case OPEN_ITEM_PREVIEW_POPUP:
      return state.set('item_preview_popup_open', true);
    case CLOSE_ITEM_PREVIEW_POPUP:
      return state.delete('item_preview_popup_open');
    case OPEN_ITEM_VISUALIZATION_POPUP:
      return state.set('item_visualization_popup_open', true);
    case CLOSE_ITEM_VISUALIZATION_POPUP:
      return state.delete('item_visualization_popup_open');
    case OPEN_ITEM_STATISTIC_POPUP:
      return state.set('item_statistic_popup_open', true);
    case CLOSE_ITEM_STATISTIC_POPUP:
      return state.delete('item_statistic_popup_open');
    case ADD_MEMBER_TO_PERMITTED_FOR_ID:
      return state
        .set('permitted_putting', true)
        .delete('permitted_putting_failed');
    case ADD_MEMBER_TO_PERMITTED_FOR_ID_COMMIT:
      return state.delete('permitted_putting').updateIn(
        ['data', action.meta.id, 'members_permitted'],
        members =>
          members
            ? members
                .push(action.payload.member_id)
                .toSet()
                .toList()
            : members
      );
    case ADD_MEMBER_TO_PERMITTED_FOR_ID_ROLLBACK:
      return state
        .set('permitted_putting_failed', true)
        .delete('permitted_putting');
    case REMOVE_MEMBER_FROM_PERMITTED_FOR_ID:
      return state
        .set('permitted_deleting', true)
        .delete('permitted_deleting_failed');
    case REMOVE_MEMBER_FROM_PERMITTED_FOR_ID_COMMIT:
      return state
        .delete('permitted_deleting')
        .updateIn(
          ['data', action.meta.id, 'members_permitted'],
          members =>
            members
              ? members.filter(member => member !== action.payload.member_id)
              : members
        );
    case REMOVE_MEMBER_FROM_PERMITTED_FOR_ID_ROLLBACK:
      return state
        .set('permitted_deleting_failed', true)
        .delete('permitted_deleting');
    case PUT_ITEM_RESULT_COMMENT:
      return state.set('commeting', true).delete('commeting_failed');
    case PUT_ITEM_RESULT_COMMENT_COMMIT:
      return state
        .delete('commeting')
        .updateIn(
          ['results', action.meta.resultId, 'item_result_comments'],
          itemResultComments => {
            if (action.payload.item_result_comment.parent_id) {
              const commentId = itemResultComments.findKey(comment => {
                return (
                  comment.get('id') ===
                  action.payload.item_result_comment.parent_id
                );
              });
              return itemResultComments.updateIn(
                [commentId, 'replies'],
                replies =>
                  replies.push(
                    fromJS({
                      ...action.payload.item_result_comment
                    })
                  )
              );
            } else {
              return itemResultComments.push(
                fromJS({
                  ...action.payload.item_result_comment
                })
              );
            }
          }
        );
    case PUT_ITEM_RESULT_COMMENT_ROLLBACK:
      return state.delete('commeting').set('commeting_failed', true);
    case EVALUATE_ITEM:
      return state
        .set('evaluating', true)
        .delete('evaluating_failed')
        .delete('evaluation');
    case EVALUATE_ITEM_COMMIT:
      return state
        .delete('evaluating')
        .set('evaluation', action.payload.evaluation);
    case EVALUATE_ITEM_ROLLBACK:
      return state.delete('evaluating').set('evaluating_failed', true);
    default:
      return state;
  }
}

export default ItemsReducer;
