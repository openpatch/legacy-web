/*
 *
 * Surveys reducer
 *
 */

import { fromJS } from 'immutable';
import format from 'date-fns/format';
import addDays from 'date-fns/add_days';

import {
  DEFAULT_ACTION,
  FETCH_SURVEYS,
  FETCH_SURVEYS_COMMIT,
  FETCH_SURVEYS_ROLLBACK,
  FETCH_FAVORITE_SURVEYS,
  FETCH_FAVORITE_SURVEYS_COMMIT,
  FETCH_FAVORITE_SURVEYS_ROLLBACK,
  SET_FILTER,
  POST_SURVEY,
  POST_SURVEY_COMMIT,
  POST_SURVEY_ROLLBACK,
  SET_ACTIVE,
  FETCH_EDIT_DATA,
  FETCH_EDIT_DATA_COMMIT,
  FETCH_EDIT_DATA_ROLLBACK,
  FETCH_SURVEY,
  FETCH_SURVEY_COMMIT,
  FETCH_SURVEY_ROLLBACK,
  SET_EDIT_DATA,
  PUT_SURVEY,
  PUT_SURVEY_COMMIT,
  PUT_SURVEY_ROLLBACK,
  DELETE_SURVEY,
  DELETE_SURVEY_COMMIT,
  DELETE_SURVEY_ROLLBACK,
  RESET_EDIT_DATA,
  OPEN_SURVEY_CREATE_DIALOG,
  CLOSE_SURVEY_CREATE_DIALOG,
  SET_EDIT_DATA_TEST_ID,
  ADD_MEMBER_TO_PERMITTED_FOR_ID,
  ADD_MEMBER_TO_PERMITTED_FOR_ID_COMMIT,
  ADD_MEMBER_TO_PERMITTED_FOR_ID_ROLLBACK,
  REMOVE_MEMBER_FROM_PERMITTED_FOR_ID,
  REMOVE_MEMBER_FROM_PERMITTED_FOR_ID_COMMIT,
  REMOVE_MEMBER_FROM_PERMITTED_FOR_ID_ROLLBACK,
  FETCH_SURVEYS_FOR_TEST_ID,
  FETCH_SURVEYS_FOR_TEST_ID_COMMIT,
  FETCH_SURVEYS_FOR_TEST_ID_ROLLBACK
} from '../constants/surveys';

const initialState = fromJS({
  data: {},
  edit: {
    name: '',
    city: '',
    term: '',
    starts_on: format(new Date(), 'YYYY-MM-DD'),
    ends_on: format(addDays(new Date(), 30), 'YYYY-MM-DD')
  }
});

function SurveysReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;
    case SET_ACTIVE:
      return state
        .set('active', Number(action.payload.active))
        .delete('fetching');
    case SET_FILTER:
      return state.set('filter', action.payload.filter);
    case FETCH_SURVEYS:
    case FETCH_FAVORITE_SURVEYS:
    case FETCH_SURVEYS_FOR_TEST_ID:
      return state.set('fetching', true).delete('fetching_failed');
    case FETCH_SURVEYS_COMMIT:
    case FETCH_FAVORITE_SURVEYS_COMMIT:
      let data = fromJS({});
      let surveys = action.payload.surveys;
      surveys.forEach(survey => (data = data.set(survey.id, fromJS(survey))));
      return state.set('data', data).delete('fetching');
    case FETCH_SURVEYS_FOR_TEST_ID_COMMIT:
      data = state
        .get('data')
        .filterNot(survey => survey.get('test_id') === action.meta.testId);
      surveys = action.payload.surveys;
      surveys.forEach(survey => (data = data.set(survey.id, fromJS(survey))));
      return state.set('data', data).delete('fetching');
    case FETCH_SURVEYS_ROLLBACK:
    case FETCH_FAVORITE_SURVEYS_ROLLBACK:
    case FETCH_SURVEYS_FOR_TEST_ID_ROLLBACK:
      return state.delete('fetching').set('fetching_failed');
    case FETCH_SURVEY:
      return state.set('fetching', true).delete('fetching_failed');
    case FETCH_SURVEY_COMMIT:
      const survey = action.payload.survey;
      return state
        .delete('fetching')
        .setIn(['data', survey.id], fromJS(survey));
    case FETCH_SURVEY_ROLLBACK:
      return state.delete('fetching').set('fetching_failed', true);
    case POST_SURVEY:
      return state.set('posting', true).delete('posting_failed');
    case POST_SURVEY_COMMIT:
      return state
        .delete('posting')
        .setIn(['edit', 'id'], action.payload.survey_id);
    case POST_SURVEY_ROLLBACK:
      return state.delete('posting').set('posting_failed', true);
    case PUT_SURVEY:
      return state.set('putting', true).delete('putting_failed');
    case PUT_SURVEY_COMMIT:
      return state.delete('putting');
    case PUT_SURVEY_ROLLBACK:
      return state.delete('putting').set('putting_failed', true);
    case DELETE_SURVEY:
      return state.set('deleting', action.payload.id).delete('deleting_failed');
    case DELETE_SURVEY_COMMIT:
      return state.delete('deleting').deleteIn(['data', action.meta.id]);
    case DELETE_SURVEY_ROLLBACK:
      return state.set('deleting_failed', true).delete('deleting');
    case FETCH_EDIT_DATA:
      return state.set('fetching', true);
    case FETCH_EDIT_DATA_COMMIT:
      return state
        .delete('fetching')
        .set('edit', fromJS(action.payload.survey).delete('member'));
    case FETCH_EDIT_DATA_ROLLBACK:
      return state.delete('fetching').set('edit', initialState.get('edit'));
    case SET_EDIT_DATA:
      return state.set('edit', fromJS(action.payload.data));
    case SET_EDIT_DATA_TEST_ID:
      return state.setIn(['edit', 'test_id'], action.payload.id);
    case RESET_EDIT_DATA:
      return state.set('edit', initialState.get('edit'));
    case OPEN_SURVEY_CREATE_DIALOG:
      return state.set('create_dialog_open', true);
    case CLOSE_SURVEY_CREATE_DIALOG:
      return state.set('create_dialog_open', false);
    case ADD_MEMBER_TO_PERMITTED_FOR_ID:
      return state
        .set('permitted_putting', true)
        .delete('permitted_putting_failed');
    case ADD_MEMBER_TO_PERMITTED_FOR_ID_COMMIT:
      return state.delete('permitted_putting').updateIn(
        ['data', action.meta.id, 'members_permitted'],
        members =>
          members
            ? members
                .push(action.payload.member_id)
                .toSet()
                .toList()
            : members
      );
    case ADD_MEMBER_TO_PERMITTED_FOR_ID_ROLLBACK:
      return state
        .set('permitted_putting_failed', true)
        .delete('permitted_putting');
    case REMOVE_MEMBER_FROM_PERMITTED_FOR_ID:
      return state
        .set('permitted_deleting', true)
        .delete('permitted_deleting_failed');
    case REMOVE_MEMBER_FROM_PERMITTED_FOR_ID_COMMIT:
      return state
        .delete('permitted_deleting')
        .updateIn(
          ['data', action.meta.id, 'members_permitted'],
          members =>
            members
              ? members.filter(member => member !== action.payload.member_id)
              : members
        );
    case REMOVE_MEMBER_FROM_PERMITTED_FOR_ID_ROLLBACK:
      return state
        .set('permitted_deleting_failed', true)
        .delete('permitted_deleting');
    default:
      return state;
  }
}

export default SurveysReducer;
