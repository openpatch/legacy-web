/*
 *
 * Formats reducer
 *
 */

import { fromJS } from 'immutable';
import {
  DEFAULT_ACTION,
  FETCH_FORMATS,
  FETCH_FORMATS_COMMIT,
  FETCH_FORMATS_ROLLBACK,
  SET_ACTIVE
} from '../constants/formats';

const initialState = fromJS({
  data: []
});

function FormatsReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;
    case FETCH_FORMATS:
      return state.set('fetching', true).delete('fetching_failed');
    case SET_ACTIVE:
      return state.set('active', action.payload.label);
    case FETCH_FORMATS_COMMIT:
      return state
        .delete('fetching')
        .set('data', fromJS(action.payload.formats));
    case FETCH_FORMATS_ROLLBACK:
      return state.delete('fetching').set('fetching_failed', true);
    default:
      return state;
  }
}

export default FormatsReducer;
