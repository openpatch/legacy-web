/*
 *
 * Tests reducer
 *
 */

import { List, fromJS } from 'immutable';
import FileSaver from 'file-saver';
import {
  DEFAULT_ACTION,
  FETCH_TESTS,
  FETCH_TESTS_COMMIT,
  FETCH_TESTS_ROLLBACK,
  FETCH_TEST,
  FETCH_TEST_COMMIT,
  FETCH_TEST_ROLLBACK,
  FETCH_FAVORITE_TESTS,
  FETCH_FAVORITE_TESTS_COMMIT,
  FETCH_FAVORITE_TESTS_ROLLBACK,
  POST_TEST,
  POST_TEST_COMMIT,
  POST_TEST_ROLLBACK,
  PUT_TEST,
  PUT_TEST_COMMIT,
  PUT_TEST_ROLLBACK,
  DELETE_TEST,
  DELETE_TEST_COMMIT,
  DELETE_TEST_ROLLBACK,
  SET_FILTER,
  SET_EDIT_DATA,
  SET_ACTIVE,
  FETCH_EDIT_DATA,
  FETCH_EDIT_DATA_COMMIT,
  FETCH_EDIT_DATA_ROLLBACK,
  RESET_EDIT_DATA,
  ADD_MEMBER_TO_PERMITTED_FOR_ID,
  ADD_MEMBER_TO_PERMITTED_FOR_ID_COMMIT,
  ADD_MEMBER_TO_PERMITTED_FOR_ID_ROLLBACK,
  REMOVE_MEMBER_FROM_PERMITTED_FOR_ID,
  REMOVE_MEMBER_FROM_PERMITTED_FOR_ID_COMMIT,
  REMOVE_MEMBER_FROM_PERMITTED_FOR_ID_ROLLBACK,
  FETCH_RESULTS_FOR_ID,
  FETCH_RESULTS_FOR_ID_COMMIT,
  FETCH_RESULTS_FOR_ID_ROLLBACK,
  FETCH_RESULTS_FOR_SURVEY_ID,
  FETCH_RESULTS_FOR_SURVEY_ID_COMMIT,
  FETCH_RESULTS_FOR_SURVEY_ID_ROLLBACK,
  SET_RESULTS_FILTER_BY_SURVEY_IDS,
  DELETE_RESULTS_FOR_SURVEY_ID,
  DELETE_RESULTS_FOR_SURVEY_ID_COMMIT,
  DELETE_RESULTS_FOR_SURVEY_ID_ROLLBACK,
  FETCH_TEST_FOR_SURVEY_ID,
  FETCH_TEST_FOR_SURVEY_ID_COMMIT,
  FETCH_TEST_FOR_SURVEY_ID_ROLLBACK,
  SET_SELECTED_RESULTS,
  DELETE_SELECTED_RESULTS,
  DELETE_SELECTED_RESULTS_COMMIT,
  DELETE_SELECTED_RESULTS_ROLLBACK,
  SET_ACTIVE_RESULT,
  FETCH_RESULT,
  FETCH_RESULT_COMMIT,
  FETCH_RESULT_ROLLBACK,
  EXPORT_RESULTS_FOR_TEST_ID,
  EXPORT_RESULTS_FOR_TEST_ID_COMMIT,
  EXPORT_RESULTS_FOR_TEST_ID_ROLLBACK,
  EXPORT_TEST,
  EXPORT_TEST_COMMIT,
  EXPORT_TEST_ROLLBACK
} from '../constants/tests';

const initialState = fromJS({
  data: {},
  results: {},
  results_filter: {},
  results_selected: [],
  edit: {
    item_groups: [],
    name: ''
  }
});

function TestsReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;
    case SET_ACTIVE:
      return state.set('active', Number(action.payload.active));
    case FETCH_TESTS:
    case FETCH_FAVORITE_TESTS:
      return state.set('fetching').delete('fetching_failed');
    case SET_FILTER:
      return state.set('filter', action.payload.filter);
    case FETCH_TESTS_COMMIT:
    case FETCH_FAVORITE_TESTS_COMMIT:
      let data = fromJS({});
      const tests = action.payload.tests;
      tests.forEach(test => (data = data.set(test.id, fromJS(test))));
      return state.set('data', data).delete('fetching');
    case FETCH_TESTS_ROLLBACK:
    case FETCH_FAVORITE_TESTS_ROLLBACK:
      return state.delete('fetching').set('fetching_failed');
    case FETCH_TEST:
    case FETCH_TEST_FOR_SURVEY_ID:
      return state.set('fetching', true).delete('fetching_failed');
    case FETCH_TEST_COMMIT:
    case FETCH_TEST_FOR_SURVEY_ID_COMMIT:
      const test = action.payload.test;
      return state.delete('fetching').setIn(['data', test.id], fromJS(test));
    case FETCH_TEST_ROLLBACK:
    case FETCH_TEST_FOR_SURVEY_ID_ROLLBACK:
      return state.delete('fetching').set('fetching_failed', true);
    case POST_TEST:
      return state.set('posting', true).delete('posting_failed');
    case POST_TEST_COMMIT:
      return state
        .delete('posting')
        .setIn(['edit', 'id'], action.payload.test_id);
    case POST_TEST_ROLLBACK:
      return state.delete('posting').set('posting_failed', true);
    case PUT_TEST:
      return state.set('putting', true).delete('putting_failed');
    case PUT_TEST_COMMIT:
      return state.delete('putting');
    case PUT_TEST_ROLLBACK:
      return state.delete('putting').set('putting_failed', true);
    case DELETE_TEST:
      return state.set('deleting', action.payload.id).delete('deleting_failed');
    case DELETE_TEST_COMMIT:
      return state.delete('deleting').deleteIn(['data', action.meta.id]);
    case DELETE_TEST_ROLLBACK:
      return state.set('deleting_failed', true).delete('deleting');
    case FETCH_EDIT_DATA:
      return state.set('fetching', true);
    case FETCH_EDIT_DATA_COMMIT:
      return state
        .delete('fetching')
        .set('edit', fromJS(action.payload.test).delete('member'));
    case FETCH_EDIT_DATA_ROLLBACK:
      return state.delete('fetching').set('edit', initialState.get('edit'));
    case SET_EDIT_DATA:
      return state.set('edit', fromJS(action.payload.data));
    case RESET_EDIT_DATA:
      return state.set('edit', initialState.get('edit'));
    case ADD_MEMBER_TO_PERMITTED_FOR_ID:
      return state
        .set('permitted_putting', true)
        .delete('permitted_putting_failed');
    case ADD_MEMBER_TO_PERMITTED_FOR_ID_COMMIT:
      return state.delete('permitted_putting').updateIn(
        ['data', action.meta.id, 'members_permitted'],
        members =>
          members
            ? members
                .push(action.payload.member_id)
                .toSet()
                .toList()
            : members
      );
    case ADD_MEMBER_TO_PERMITTED_FOR_ID_ROLLBACK:
      return state
        .set('permitted_putting_failed', true)
        .delete('permitted_putting');
    case REMOVE_MEMBER_FROM_PERMITTED_FOR_ID:
      return state
        .set('permitted_deleting', true)
        .delete('permitted_deleting_failed');
    case REMOVE_MEMBER_FROM_PERMITTED_FOR_ID_COMMIT:
      return state
        .delete('permitted_deleting')
        .updateIn(
          ['data', action.meta.id, 'members_permitted'],
          members =>
            members
              ? members.filter(member => member !== action.payload.member_id)
              : members
        );
    case REMOVE_MEMBER_FROM_PERMITTED_FOR_ID_ROLLBACK:
      return state
        .set('permitted_deleting_failed', true)
        .delete('permitted_deleting');
    case SET_RESULTS_FILTER_BY_SURVEY_IDS:
      return state.setIn(
        ['results_filter', 'survey_ids'],
        fromJS(action.payload.surveyIds)
      );
    case FETCH_RESULT:
    case FETCH_RESULTS_FOR_ID:
    case FETCH_RESULTS_FOR_SURVEY_ID:
      return state.set('fetching', true).delete('fetching_failed');
    case FETCH_RESULTS_FOR_ID_COMMIT:
    case FETCH_RESULTS_FOR_SURVEY_ID_COMMIT:
      let results = state
        .get('results')
        .filter(result => result.get('survey_id') !== action.meta.surveyId);
      const testResults = action.payload.test_results;
      testResults.forEach(
        testResult => (results = results.set(testResult.id, fromJS(testResult)))
      );
      return state.delete('fetching').set('results', results);
    case FETCH_RESULT_COMMIT:
      return state
        .setIn(
          ['results', action.payload.test_result.id],
          fromJS(action.payload.test_result)
        )
        .delete('fetching');
    case FETCH_RESULT_ROLLBACK:
    case FETCH_RESULTS_FOR_ID_ROLLBACK:
    case FETCH_RESULTS_FOR_SURVEY_ID_ROLLBACK:
      return state.delete('fetching').set('fetching_failed', true);
    case DELETE_SELECTED_RESULTS:
    case DELETE_RESULTS_FOR_SURVEY_ID:
      return state.set('deleting', true).delete('deleting_failed');
    case DELETE_RESULTS_FOR_SURVEY_ID_COMMIT:
      return state
        .delete('deleting')
        .update('results', results =>
          results.filter(
            result => result.get('survey_id') !== action.meta.surveyId
          )
        );
    case DELETE_SELECTED_RESULTS_COMMIT:
      return state
        .delete('deleting')
        .set('results_selected', new List([]))
        .update('results', results =>
          results.filter(
            result => !state.get('results_selected').includes(result.get('id'))
          )
        );
    case DELETE_SELECTED_RESULTS_ROLLBACK:
    case DELETE_RESULTS_FOR_SURVEY_ID_ROLLBACK:
      return state.set('deleting_failed', true).delete('deleting');
    case SET_SELECTED_RESULTS:
      return state.set('results_selected', fromJS(action.payload.ids));
    case SET_ACTIVE_RESULT:
      return state.set('active_result', Number(action.payload.id));
    case EXPORT_TEST:
    case EXPORT_RESULTS_FOR_TEST_ID:
      return state.set('downloading', true).delete('downloading_failed');
    case EXPORT_TEST_ROLLBACK:
    case EXPORT_RESULTS_FOR_TEST_ID_ROLLBACK:
      return state.set('downloading_failed', true).delete('downloading');
    case EXPORT_TEST_COMMIT:
    case EXPORT_RESULTS_FOR_TEST_ID_COMMIT:
      FileSaver.saveAs(
        new Blob([
          action.meta.format === 'json'
            ? JSON.stringify(action.payload, null, 2)
            : action.payload
        ]),
        `export_test_${action.meta.id}.${action.meta.format}`
      );
      return state.delete('downloading');
    default:
      return state;
  }
}

export default TestsReducer;
