import axios from 'axios';

/**
 * Create custom axios object for sending the auth token on every api call and
 * increase the timeout.
 */
const api = axios.create({
  baseURL: `${process.env.REACT_APP_API_URL}`,
  timeout: 120000,
  headers: {
    'Content-Type': 'application/json',
    Pragma: 'no-cache'
  }
});

api.interceptors.request.use(
  config => {
    // add access token to the auth header
    const loginURL = `v1/members/login`;
    if (config.url !== loginURL) {
      config.auth = {
        username: localStorage.getItem('access_token'),
        password: 'not-needed'
      };
    }
    return config;
  },
  error => {
    return Promise.reject(error);
  }
);

export default api;
