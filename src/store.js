// TODO remove
import 'regenerator-runtime/runtime';

import { createStore, compose, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import { createLogger } from 'redux-logger';
import createHistory from 'history/createHashHistory';
import { routerMiddleware } from 'react-router-redux';
import { offline } from '@redux-offline/redux-offline';
import offlineConfig from '@redux-offline/redux-offline/lib/defaults';
import immutableTransform from 'redux-persist-transform-immutable';
import reducers from './reducers';
import api from './api';

/**
 * Create history for using router middleware.
 */
const history = createHistory();

const middlewares = [thunkMiddleware, routerMiddleware(history)];

if (process.env.NODE_ENV === 'development') {
  /**
   * LoggerMiddleware to output all actions to the console.
   */
  const loggerMiddleware = createLogger({
    collapsed: true
  });
  middlewares.push(loggerMiddleware);
}

/**
 * Add autoRehydrate to the enhancers for auto loading persistent redux
 * storage.
 */
const enhancers = [applyMiddleware(...middlewares)];

const composeEnhancers =
  process.env.NODE_ENV !== 'production' &&
  typeof window === 'object' &&
  window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
    ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
    : compose;
/* eslint-enable */

/**
 * Create an initial store.
 */
const initialState = {};

const store = persistCallback =>
  createStore(
    reducers,
    initialState,
    composeEnhancers(
      ...enhancers,
      offline({
        ...offlineConfig,
        discard: (error, action) => {
          const { response } = error;
          if (!response) {
            return false;
          }
          if (
            response.status === 401 &&
            response.request &&
            response.request.responseURL !==
              `${process.env.REACT_APP_API_URL}/v1/members/logout`
          ) {
            return api({
              url: 'v1/members/token',
              data: {
                refresh_token: localStorage.getItem('refresh_token')
              },
              method: 'POST'
            })
              .then(res => {
                if (res.status === 403) {
                  return true;
                } else {
                  localStorage.setItem('access_token', res.data.access_token);
                  return false;
                }
              })
              .catch(() => true);
          } else {
            return response.status >= 400 && response.status < 500;
          }
        },
        effect: (effect, action) => {
          return api(effect).then(res => {
            if (res.status === 200) {
              return res.data;
            }
            return false;
          });
        },
        retry: (action, retries) => {
          const decaySchedule = [1000, 1000 * 5];
          return decaySchedule[retries] || null;
        },
        persistCallback,
        persistOptions: {
          blacklist: ['assessment', 'admin'],
          transforms: [immutableTransform()]
        }
      }),
      createStore => (reducer, preloadedState, enhancer) =>
        enhancer(createStore)(reducer, preloadedState)
    )
  );
export { store, history };

export default store;
