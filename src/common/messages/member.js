/**
 *
 * Member messages
 *
 * This contains all the text for the member domain.
 *
 */

import { defineMessages } from 'react-intl';

export default defineMessages({
  username: {
    id: 'commoop.common.containers.Login.username',
    defaultMessage: 'Username'
  },
  password: {
    id: 'commoop.common.containers.Login.password',
    defaultMessage: 'Password'
  }
});
