/**
 * 
 * Notifications messages
 * 
 * This contains all the text for the Notifications domain.
 * 
 */

import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'commoop.common.Notifications.domain',
    defaultMessage: 'Notifications'
  }
});
