/*
 *
 * Member reducer
 *
 */

import { fromJS } from 'immutable';

import {
  DEFAULT_ACTION,
  LOGIN,
  LOGIN_COMMIT,
  LOGIN_ROLLBACK,
  TOGGLE_FAVORITE,
  TOGGLE_FAVORITE_ROLLBACK
} from '../constants/member';

const initialState = fromJS({});

function MemberReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;
    case LOGIN:
      return state.set('logging_in', true).delete('logging_in_failed');
    case LOGIN_COMMIT:
      localStorage.setItem('refresh_token', action.payload.refresh_token);
      localStorage.setItem('access_token', action.payload.access_token);
      return state.delete('logging_in').merge(fromJS(action.payload.me));
    case LOGIN_ROLLBACK:
      return state.delete('logging_in').set('logging_in_failed', true);
    case TOGGLE_FAVORITE:
      let type = action.payload.type;
      let id = action.payload.id;
      let key = `${type}_favorized`;
      return state.update(
        key,
        list =>
          list.includes(id) ? list.remove(list.indexOf(id)) : list.push(id)
      );
    case TOGGLE_FAVORITE_ROLLBACK:
      type = action.meta.type;
      id = action.meta.id;
      key = `${type}_favorized`;
      return state.update(
        key,
        list =>
          list.includes(id) ? list.remove(list.indexOf(id)) : list.push(id)
      );
    default:
      return state;
  }
}

export default MemberReducer;
