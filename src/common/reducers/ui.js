/*
 *
 * UI reducer
 *
 */

import { fromJS } from 'immutable';
import { TOGGLE_OPEN, TOGGLE_IMPRINT_PRIVACY_OPEN } from '../constants/ui';

const initialState = fromJS({
  open: false,
  imprint_privacy_open: false
});

function UIReducer(state = initialState, action) {
  switch (action.type) {
    case TOGGLE_OPEN:
      return state.update('open', open => !open);
    case TOGGLE_IMPRINT_PRIVACY_OPEN:
      return state.update('imprint_privacy_open', open => !open);
    default:
      return state;
  }
}

export default UIReducer;
