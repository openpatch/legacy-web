/*
 *
 * Notifications reducer
 *
 */

import { fromJS } from 'immutable';
import {
  DEFAULT_ACTION,
  ADD_NOTIFICATION,
  REMOVE_NOTIFICATION,
  CLEAR_NOTIFICATIONS
} from '../constants/notifications';

const initialState = fromJS([]);

function NotificationsReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;
    case ADD_NOTIFICATION:
      return state.push(fromJS(action.payload.notification));
    case REMOVE_NOTIFICATION:
      return state.shift();
    case CLEAR_NOTIFICATIONS:
      return initialState;
    default:
      return state;
  }
}

export default NotificationsReducer;
