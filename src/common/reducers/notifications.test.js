import { fromJS } from 'immutable';
import reducer from './notifications';

import * as constants from '../constants/notifications';

const mockState = fromJS([{ message: '1' }, { message: '2' }]);

describe('notifications reducer', () => {
  it('returns the initial state', () => {
    expect(reducer(undefined, {})).toEqual(fromJS([]));
  });

  it('should handle DEFAULT_ACTION', () => {
    expect(
      reducer(mockState, {
        type: constants.DEFAULT_ACTION
      })
    ).toEqual(mockState);
  });

  it('should handle ADD_NOTIFICATION', () => {
    expect(
      reducer(mockState, {
        type: constants.ADD_NOTIFICATION,
        payload: {
          notification: {
            message: 'hi'
          }
        }
      })
    ).toEqual(mockState.push(fromJS({ message: 'hi' })));
  });

  it('should handle REMOVE_NOTIFICATION', () => {
    expect(
      reducer(mockState, {
        type: constants.REMOVE_NOTIFICATION
      })
    ).toEqual(mockState.shift());
  });

  it('should handle CLEAR_NOTIFICATIONS', () => {
    expect(
      reducer(mockState, {
        type: constants.CLEAR_NOTIFICATIONS
      })
    ).toEqual(fromJS([]));
  });
});
