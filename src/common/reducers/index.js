import { combineReducers } from 'redux-immutable';

import language from './language';
import member from './member';
import ui from './ui';
import notifications from './notifications';

const commonReducers = combineReducers({
  language,
  member,
  ui,
  notifications
});

export default commonReducers;
