/*
 *
 * DrawerLayout
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { withStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import Hidden from '@material-ui/core/Hidden';
import Divider from '@material-ui/core/Divider';

import { makeOpenSelector } from '../selectors/ui';

import { toggleOpen } from '../actions/ui';

const drawerWidth = 240;

const styles = theme => ({
  root: {
    backgroundColor: theme.palette.background.default,
    display: 'flex'
  },
  content: {
    width: `100%`,
    [theme.breakpoints.up('md')]: {
      width: `calc(100% - ${drawerWidth}px)`
    }
  },
  drawerHeader: theme.mixins.toolbar,
  drawerPaper: {
    width: 250,
    [theme.breakpoints.up('md')]: {
      width: drawerWidth,
      position: 'relative',
      height: '100%'
    }
  }
});

export class DrawerLayout extends React.Component {
  state = {
    height: window.innerHeight
  };

  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    open: PropTypes.bool,
    drawerHeader: PropTypes.oneOfType([PropTypes.element, PropTypes.string]),
    drawerContent: PropTypes.oneOfType([PropTypes.element, PropTypes.string])
  };

  onResize = () => {
    this.setState({
      height: window.innerHeight
    });
  };

  componentDidMount() {
    window.addEventListener('resize', this.onResize);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.onResize);
  }

  render() {
    const {
      classes,
      theme,
      drawerHeader,
      drawerContent,
      children,
      open
    } = this.props;

    const drawer = (
      <div>
        {drawerHeader ? (
          <div className={classes.drawerHeader}>{drawerHeader}</div>
        ) : null}
        <Divider />
        {drawerContent}
      </div>
    );

    return (
      <div className={classes.root}>
        <Hidden mdUp implementation="css">
          <Drawer
            variant="temporary"
            anchor={theme.direction === 'rtl' ? 'right' : 'left'}
            open={open}
            classes={{
              paper: classes.drawerPaper
            }}
            onClose={this.props.toggleOpen}
            ModalProps={{
              keepMounted: true // Better open performance on mobile.
            }}
          >
            {drawer}
          </Drawer>
        </Hidden>
        <Hidden smDown implementation="css">
          <Drawer
            variant="permanent"
            open
            classes={{
              paper: classes.drawerPaper
            }}
            style={{
              height: this.state.height
            }}
          >
            {drawer}
          </Drawer>
        </Hidden>
        <main className={classes.content}>{children}</main>
      </div>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  open: makeOpenSelector()
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    toggleOpen: () => dispatch(toggleOpen())
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(
  withStyles(styles, { withTheme: true })(DrawerLayout)
);
