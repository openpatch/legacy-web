import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import { IntlProvider } from 'react-intl';

import { makeLocaleSelector } from '../selectors/language';

const LanguageProvider = props => (
  <IntlProvider
    locale={props.locale}
    key={props.locale}
    messages={props.messages[props.locale]}
  >
    {props.children}
  </IntlProvider>
);

LanguageProvider.propTypes = {
  locale: PropTypes.string
};

LanguageProvider.defaultProps = {
  locale: 'en'
};

const mapStateToProps = createSelector(makeLocaleSelector(), locale => ({
  locale
}));

export default connect(mapStateToProps)(LanguageProvider);
