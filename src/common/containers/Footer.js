import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { injectIntl } from 'react-intl';
import { withStyles } from '@material-ui/core/styles';
import commonMessages from '../messages/common';
import { toggleImprintPrivacyOpen } from '../actions/ui';

const styles = theme => ({
  footer: {
    bottom: 0,
    left: 0,
    right: 0,
    display: 'flex',
    background: '#002a51',
    fontSize: 13,
    color: 'white',
    fontFamily: 'Roboto',
    padding: 16
  },
  footerMenu: {
    flex: 1,
    display: 'flex',
    padding: '0 16px',
    justifyContent: 'center'
  },
  footerCopyright: {
    display: 'flex',
    justifyContent: 'center',
    flex: 1
  },
  footerContact: {
    display: 'flex',
    justifyContent: 'center',
    flex: 1
  }
});

export class Footer extends React.Component {
  static propTypes = {
    classes: PropTypes.object.isRequired,
    intl: PropTypes.object.isRequired,
    dispatch: PropTypes.func.isRequired,
    position: PropTypes.string
  };

  static defaultProps = {
    position: 'absolute'
  };
  render() {
    const { intl, classes, dispatch, position } = this.props;
    return (
      <footer
        className={classes.footer}
        style={{
          position
        }}
      >
        <div className={classes.footerCopyright}>
          Copyright © 2017 University of Duisburg-Essen
        </div>
        <div className={classes.footerMenu}>
          <button onClick={() => dispatch(toggleImprintPrivacyOpen())}>
            {intl.formatMessage(commonMessages.imprintPrivacy)}
          </button>
        </div>

        <div className={classes.footerContact} />
      </footer>
    );
  }
}

export default connect()(withStyles(styles)(injectIntl(Footer)));
