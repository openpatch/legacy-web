/*
 *
 * AppBar
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import MuiAppBar from '@material-ui/core/AppBar';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import Toolbar from '@material-ui/core/Toolbar';
import MenuIcon from '@material-ui/icons/Menu';

import { toggleOpen } from '../actions/ui';

const styles = theme => ({
  wrapper: {
    width: '100%',
    position: 'relative',
    margin: '0 auto'
  },
  navIconHide: {
    [theme.breakpoints.up('md')]: {
      display: 'none'
    }
  },
  flex: {
    flex: 1,
    userSelect: 'none',
    cursor: 'default'
  },
  fab: {
    position: 'absolute',
    right: theme.spacing.unit,
    bottom: -28
  }
});

export class AppBar extends React.PureComponent {
  static propTypes = {
    classes: PropTypes.object.isRequired,
    toggleOpen: PropTypes.func.isRequired,
    title: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
    bg: PropTypes.string,
    fab: PropTypes.element,
    tabs: PropTypes.element
  };

  static defaultProps = {
    title: 'COMMOOP',
    bg: null,
    fab: null,
    tabs: null
  };
  render() {
    const {
      bg,
      fab,
      classes,
      toggleOpen,
      title,
      children,
      tabs,
      ...props
    } = this.props;
    return (
      <MuiAppBar position="static" style={{ backgroundColor: bg }} {...props}>
        <div className={classes.wrapper}>
          <Toolbar
            style={{
              marginRight: fab && !tabs ? 28 : 0
            }}
          >
            <IconButton
              color="inherit"
              aria-label="open drawer"
              onClick={toggleOpen}
              className={classes.navIconHide}
            >
              <MenuIcon />
            </IconButton>
            <Typography
              variant="title"
              color="inherit"
              className={classes.flex}
              noWrap
            >
              {title}
            </Typography>
            {children}
          </Toolbar>
          {tabs}
          {fab && <div className={classes.fab}>{fab}</div>}
        </div>
      </MuiAppBar>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return {
    toggleOpen: () => dispatch(toggleOpen())
  };
}

export default connect(null, mapDispatchToProps)(withStyles(styles)(AppBar));
