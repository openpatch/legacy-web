/*
 *
 * WithAuthorization
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import Login from '../components/Login';

import { roleSelector, loginSelector } from '../selectors/member';

import { login } from '../actions/member';

/**
 * HOC that Handles whether or not the user is allowed to see the page.
 * @param {array} allowedRoles - user roles that are allowed to see the page.
 * @returns {Component}
 */
function Authorization(allowedRoles) {
  return WrappedComponent => {
    class WithAuthorization extends React.PureComponent {
      static propTypes = {
        role: PropTypes.string.isRequired,
        status: PropTypes.object.isRequired,
        login: PropTypes.func.isRequired
      };

      render() {
        const { role, status, login, ...props } = this.props;
        if (allowedRoles.includes(role)) {
          return <WrappedComponent {...props} />;
        } else {
          return <Login {...status} login={login} />;
        }
      }
    }

    const mapStateToProps = createStructuredSelector({
      role: roleSelector,
      status: loginSelector
    });

    const mapDispatchToProps = dispatch => ({
      login: (username, password) => dispatch(login(username, password))
    });

    return connect(mapStateToProps, mapDispatchToProps)(WithAuthorization);
  };
}

const AdminRole = Authorization(['admin']);

const AuthorRole = Authorization(['author', 'admin']);

const DataAnalystRole = Authorization(['data_analyst', 'author', 'admin']);

const UserRole = Authorization(['user']);

export { AdminRole, AuthorRole, DataAnalystRole, UserRole };

export default Authorization;
