import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { injectIntl } from 'react-intl';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import commonMessages from '../messages/common';
import Markdown from '../components/Markdown';
import FullScreenDialog from '../components/FullScreenDialog';
import { makeImprintPrivacyOpenSelector } from '../selectors/ui';
import { toggleImprintPrivacyOpen } from '../actions/ui';

const styles = theme => ({
  body: {
    padding: theme.spacing.unit
  }
});

export class ImprintPrivacyDialog extends React.Component {
  static propTypes = {
    intl: PropTypes.object.isRequired,
    classes: PropTypes.object.isRequired
  };
  render() {
    const { classes, intl, open, toggleOpen } = this.props;
    return (
      <FullScreenDialog open={open} onClose={toggleOpen}>
        <div className={classes.body}>
          <Markdown
            source={intl.formatMessage(commonMessages.imprintPrivacyMarkdown)}
          />
        </div>
      </FullScreenDialog>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  open: makeImprintPrivacyOpenSelector()
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    toggleOpen: () => dispatch(toggleImprintPrivacyOpen())
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(injectIntl(withStyles(styles)(ImprintPrivacyDialog)));
