/*
 *
 * App
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { MuiThemeProvider } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import { Provider } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import { Route, Redirect, Switch } from 'react-router-dom';
import { ConnectedRouter } from 'react-router-redux';
import CircularProgress from '@material-ui/core/CircularProgress';
import Loadable from 'react-loadable';
import 'typeface-roboto';

import { logout } from '../actions/member';

import { history } from '../../store';
import { translationMessages } from '../../i18n';
import LanguageProvider from './LanguageProvider';
import api from '../../api';
import { DataAnalystRole } from './Authorization';
import NotificationsProvider from './NotificationsProvider';
import Loading from '../components/Loading';
import ImprintPrivacy from './ImprintPrivacy';

const Admin = Loadable({
  loader: () => import('../../admin'),
  loading: Loading
});
const Assessment = Loadable({
  loader: () => import('../../assessment'),
  loading: Loading
});

const styles = theme => ({
  loading: {
    position: 'absolute',
    top: '50%',
    left: '50%'
  }
});

export class App extends React.Component {
  state = {
    rehydrated: false
  };

  static propTypes = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired,
    store: PropTypes.func.isRequired
  };

  constructor(props) {
    super(props);
    const store = props.store(this.persistCallback);
    this.store = store;

    api.interceptors.response.use(
      function(response) {
        // Do something with response data
        return response;
      },
      function(error) {
        // Do something with response error
        const request = error.response ? error.response.request : {};
        if (
          request.status === 403 &&
          request.responseURL ===
            `${process.env.REACT_APP_API_URL}/v1/members/token`
        ) {
          store.dispatch(logout());
        }
        return Promise.reject(error);
      }
    );
  }

  persistCallback = () => {
    this.setState({
      rehydrated: true
    });
  };

  render() {
    const { classes, theme } = this.props;
    const { rehydrated } = this.state;
    return (
      <React.Fragment>
        <CssBaseline />
        <Provider store={this.store}>
          {rehydrated ? (
            <MuiThemeProvider theme={theme}>
              <LanguageProvider messages={translationMessages}>
                <ConnectedRouter history={history}>
                  <React.Fragment>
                    <NotificationsProvider />
                    <Switch>
                      <Route path="/admin" component={DataAnalystRole(Admin)} />
                      <Route path="/assessment" component={Assessment} />
                      <Route
                        path="/"
                        component={() => <Redirect to="/assessment" />}
                      />
                    </Switch>
                    <ImprintPrivacy />
                  </React.Fragment>
                </ConnectedRouter>
              </LanguageProvider>
            </MuiThemeProvider>
          ) : (
            <CircularProgress className={classes.loading} />
          )}
        </Provider>
      </React.Fragment>
    );
  }
}

export default withStyles(styles)(App);
