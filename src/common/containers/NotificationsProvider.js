/*
 *
 * NotificationsProvider
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { List } from 'immutable';

import makeNotificationsSelector from '../selectors/notifications';

import Notification from '../components/Notification';
import {
  removeNotification,
  clearNotifications
} from '../actions/notifications';

export class NotificationsProvider extends React.Component {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    notifications: PropTypes.instanceOf(List).isRequired
  };

  state = {
    transition: undefined,
    open: false
  };

  componentWillMount() {
    this.props.dispatch(clearNotifications());
  }

  componentWillUpdate = nextProps => {
    const { notifications } = nextProps;
    if (notifications.size > 0 && !this.state.open) {
      this.setState({
        open: true
      });
    }
  };

  onClose = transition => {
    this.setState({
      open: false
    });
  };

  onExited = () => {
    this.props.dispatch(removeNotification());
  };

  render() {
    const { notifications } = this.props;
    let activeNotification = notifications.first();
    if (activeNotification) {
      return (
        <Notification
          {...activeNotification.toJS()}
          open={this.state.open}
          transition={this.state.transition}
          onExited={this.onExited}
          onClose={this.onClose}
        />
      );
    }
    return null;
  }
}

const mapStateToProps = createStructuredSelector({
  notifications: makeNotificationsSelector()
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(
  NotificationsProvider
);
