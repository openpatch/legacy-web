import React from 'react';
import TextField from '@material-ui/core/TextField';

class PureTextField extends React.PureComponent {
  render() {
    return <TextField {...this.props} />;
  }
}

export default PureTextField;
