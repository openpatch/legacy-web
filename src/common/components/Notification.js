/**
 *
 * Notification
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import Snackbar from '@material-ui/core/Snackbar';
import Button from '@material-ui/core/Button';

class Notification extends React.PureComponent {
  static propTypes = {
    actions: PropTypes.arrayOf(
      PropTypes.shape({
        label: PropTypes.string,
        callback: PropTypes.func
      })
    ),
    message: PropTypes.string,
    onClose: PropTypes.func,
    autoHideDuration: PropTypes.number,
    anchorOrigin: PropTypes.shape({
      vertical: PropTypes.string,
      horizontal: PropTypes.string
    }),
    key: PropTypes.string
  };

  static defaultProps = {
    actions: undefined,
    message: '',
    key: 'notification-provider',
    onClose: () => {},
    anchorOrigin: {
      vertical: 'bottom',
      horizontal: 'center'
    }
  };

  render() {
    const { actions, ...props } = this.props;
    return (
      <Snackbar
        {...props}
        action={
          actions
            ? actions.map(action => (
                <Button
                  size="small"
                  color="secondary"
                  key={action.label}
                  onClick={action.callback}
                >
                  {action.label}
                </Button>
              ))
            : undefined
        }
      />
    );
  }
}

export default Notification;
