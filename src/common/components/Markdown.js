/**
 *
 * Markdown
 *
 */

import React from 'react';
import ReactMarkdown from 'react-markdown';
import 'highlight.js/styles/github.css';

import CodeBlock from './CodeBlock';

class Markdown extends React.PureComponent {
  static defaultProps = {
    renderers: {}
  };
  render() {
    const { renderers, ...props } = this.props;
    return (
      <ReactMarkdown
        {...props}
        renderers={{
          code: CodeBlock,
          ...renderers
        }}
      />
    );
  }
}

export default Markdown;
