/**
 *
 * NotSupported
 *
 */

import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import ErrorIcon from '@material-ui/icons/Error';
import Typography from '@material-ui/core/Typography';

const styles = theme => ({
  container: {
    width: '100%',
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    color: theme.palette.error.main
  }
});

class NotSupported extends React.Component {
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.container}>
        <ErrorIcon style={{ fontSize: 40 }} />
        <Typography variant="caption">Currently not supported</Typography>
      </div>
    );
  }
}

export default withStyles(styles)(NotSupported);
