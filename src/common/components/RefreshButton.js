/**
 *
 * RefreshButton
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import RefreshIcon from '@material-ui/icons/Refresh';

const styles = theme => ({
  '@keyframes spin': {
    from: {
      transform: 'rotate(0deg)'
    },
    to: {
      transform: 'rotate(360deg)'
    }
  },
  spinning: {
    animationName: 'spin',
    animationDuration: '1s',
    animationIterationCount: 'infinite'
  }
});

class RefreshButton extends React.Component {
  static propTypes = {
    classes: PropTypes.object,
    refreshing: PropTypes.bool,
    onClick: PropTypes.func
  };

  static defaultProps = {
    refreshing: false,
    onClick: () => {}
  };

  render() {
    const { classes, refreshing, onClick, ...props } = this.props;
    return (
      <IconButton onClick={onClick} {...props}>
        <RefreshIcon className={refreshing ? classes.spinning : null} />
      </IconButton>
    );
  }
}

export default withStyles(styles)(RefreshButton);
