import React from 'react';
import PropTypes from 'prop-types';
import CircularProgress from '@material-ui/core/CircularProgress';
import Fade from '@material-ui/core/Fade';
import Typography from '@material-ui/core/Typography';

class Loading extends React.Component {
  static propTypes = {
    delay: PropTypes.string
  };

  static defaultProps = {
    delay: '800ms'
  };
  render() {
    const { delay } = this.props;
    return (
      <div
        style={{
          width: '100%',
          height: '100%',
          display: 'flex',
          alignItems: 'center',
          flexDirection: 'column',
          justifyContent: 'center'
        }}
      >
        <Fade
          in={true}
          style={{
            transitionDelay: delay
          }}
          unmountOnExit
        >
          <CircularProgress />
        </Fade>
        <Typography variant="caption">Loading...</Typography>
      </div>
    );
  }
}

export default Loading;
