import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { injectIntl } from 'react-intl';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';

import commonMessages from '../messages/common';

const styles = theme => ({
  content: {
    fontFamily: 'Roboto'
  }
});

class ConfirmDialog extends React.Component {
  static propTypes = {
    intl: PropTypes.object.isRequired,
    classes: PropTypes.object.isRequired,
    title: PropTypes.string.isRequired,
    content: PropTypes.node,
    open: PropTypes.bool,
    onOk: PropTypes.func.isRequired,
    onCancel: PropTypes.func.isRequired
  };

  static defaultProps = {
    open: false
  };

  render() {
    const { classes, title, intl, content, onOk, onCancel, open } = this.props;
    return (
      <Dialog
        disableBackdropClick
        disableEscapeKeyDown
        aria-labelledby="confirmation-dialog-title"
        open={open}
      >
        <DialogTitle id="confirmation-dialog-title">{title}</DialogTitle>
        <DialogContent className={classes.content}>{content}</DialogContent>
        <DialogActions>
          <Button onClick={onCancel} color="primary">
            {intl.formatMessage(commonMessages.cancel)}
          </Button>
          <Button onClick={onOk} color="primary">
            {intl.formatMessage(commonMessages.ok)}
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

export default withStyles(styles)(injectIntl(ConfirmDialog));
