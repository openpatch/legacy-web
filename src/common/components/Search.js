/**
*
* Search
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import SearchIcon from '@material-ui/icons/Search';

const styles = theme => ({
  background: {
    position: 'relative',
    background: 'rgba(255,255,255,0.15)',
    borderRadius: 2,
    boxSizing: 'border-box'
  },
  icon: {
    width: 72,
    height: '100%',
    display: 'flex',
    position: 'absolute',
    alignItems: 'center',
    pointerEvents: 'none',
    justifyContent: 'center'
  },
  wrapper: {
    position: 'relative',
    display: 'inline-block'
  },
  input: {
    width: 200,
    transition: 'width 300ms cubic-bezier(0.4, 0, 0.2, 1) 0ms',
    padding: '8px 8px 8px 72px',
    background: 'none',
    color: 'white',
    position: 'relative',
    border: 0,
    margin: 0,
    outline: 0,
    verticalAlign: 'top',
    boxSizing: 'border-box',
    '&:focus': {
      width: 350
    },
    '&::placeholder': {
      color: 'white'
    }
  }
});

const Search = ({ classes, onChange, value, placeholder }) => {
  return (
    <div className={classes.background}>
      <div className={classes.icon}>
        <SearchIcon size={24} />
      </div>
      <span className={classes.wrapper}>
        <input
          className={classes.input}
          onChange={onChange}
          value={value}
          placeholder={placeholder}
        />
      </span>
    </div>
  );
};

Search.propTypes = {
  classes: PropTypes.object.isRequired,
  onChange: PropTypes.func,
  value: PropTypes.string,
  placeholder: PropTypes.string
};

Search.defaultProps = {
  onChange: () => {},
  value: '',
  placeholder: 'Search ...'
};

export default withStyles(styles)(Search);
