/**
 *
 * ShareButton
 *
 */

import React from 'react';
import IconButton from '@material-ui/core/IconButton';
import PersonAddIcon from '@material-ui/icons/PersonAdd';
import Tooltip from '@material-ui/core/Tooltip';

class ShareButton extends React.PureComponent {
  render() {
    return (
      <Tooltip title="Edit Permissions">
        <IconButton color="inherit" {...this.props}>
          <PersonAddIcon />
        </IconButton>
      </Tooltip>
    );
  }
}

export default ShareButton;
