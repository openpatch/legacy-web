import React from 'react';
import PropTypes from 'prop-types';

class Ranking extends React.Component {
  static propTypes = {
    onChange: PropTypes.func,
    content: PropTypes.shape({
      fragments: PropTypes.arrayOf(PropTypes.string)
    }).isRequired,
    value: PropTypes.number
  };

  static defaultProps = {
    onChange: () => {},
    edit: false
  };

  onChange = value => {
    this.props.onChange(value);
  };

  render() {
    return <div>Ranking</div>;
  }
}

export default Ranking;
