import React from 'react';
import { storiesOf } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';
import { withKnobs } from '@storybook/addon-knobs/react';
import { action } from '@storybook/addon-actions';

import ResponseField from './ResponseField';

storiesOf('common/components/ResponseField', module)
  .addDecorator((story, context) => withInfo({})(story)(context))
  .addDecorator(withKnobs)
  .add('text', () => (
    <ResponseField
      type="text"
      content={{
        multiline: true,
        type: 'text',
        rows: 10
      }}
    />
  ));
