import React from 'react';
import PropTypes from 'prop-types';

import PureTextField from '../PureTextField';

class Text extends React.Component {
  static propTypes = {
    onChange: PropTypes.func,
    input: PropTypes.shape({
      multiline: PropTypes.bool,
      type: PropTypes.oneOf(['number', 'text', 'email', 'password']),
      defaultValue: PropTypes.string,
      rows: PropTypes.number
    }),
    value: PropTypes.string
  };

  static defaultProps = {
    onChange: () => {},
    edit: false,
    input: {},
    content: {},
    value: ''
  };

  onChange = e => {
    const value = e.currentTarget.value;
    this.props.onChange(value);
  };

  render() {
    const { input, value } = this.props;
    return (
      <PureTextField
        {...input}
        fullWidth
        value={value}
        onChange={this.onChange}
      />
    );
  }
}

export default Text;
