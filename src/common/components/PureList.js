/**
 *
 * PureList
 *
 */

import React from 'react';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';

export class PureListItemText extends React.PureComponent {
  render() {
    return <ListItemText {...this.props} />;
  }
}

export class PureListItemIcon extends React.PureComponent {
  render() {
    return <ListItemIcon {...this.props} />;
  }
}

export class PureListItem extends React.PureComponent {
  render() {
    return <ListItem {...this.props} />;
  }
}

export class PureList extends React.PureComponent {
  render() {
    return <List {...this.props} />;
  }
}

export default PureList;
