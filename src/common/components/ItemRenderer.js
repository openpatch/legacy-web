import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Markdown from './Markdown';
import Typography from '@material-ui/core/Typography';
import { Map } from 'immutable';
import Paper from '@material-ui/core/Paper';
import { injectIntl } from 'react-intl';

import commonMessages from '../messages/common';

import formats from '../../formats';
import ResponseField from './ResponseField/ResponseField';
import StickyNote from './StickyNote';

const styles = theme => ({
  container: {
    maxWidth: 1280,
    minWidth: 800,
    margin: '0 auto',
    position: 'relative'
  },
  assignment: {
    marginTop: theme.spacing.unit,
    marginBottom: theme.spacing.unit * 4
  },
  assignmentHeader: {
    padding: theme.spacing.unit * 2,
    backgroundColor: theme.palette.tertiary[100]
  },
  assignmentBody: {
    padding: theme.spacing.unit * 2
  },
  item: {
    padding: theme.spacing.unit * 2,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    marginBottom: theme.spacing.unit * 4,
    marginTop: theme.spacing.unit * 4,
    boxSizing: 'border-box'
  },
  response: {
    marginBottom: theme.spacing.unit * 4,
    marginTop: theme.spacing.unit * 4
  }
});

export const constants = {
  CHANGE_NOTE: 'commoop.ItemRenderer.CHANGE_NOTE',
  CHANGE_RESPONSE: 'commoop.ItemRenderer.CHANGE_RESPONSE'
};

const actions = {
  changeNote: note => ({ type: constants.CHANGE_NOTE, payload: { note } }),
  changeResponse: (response, id) => ({
    type: constants.CHANGE_RESPONSE,
    payload: { response, id }
  })
};

export const itemReducer = (state = new Map({}), action, reducer) => {
  switch (action.type) {
    case constants.CHANGE_NOTE:
      return state.set('note', action.payload.note);
    case constants.CHANGE_RESPONSE:
      return state.setIn(
        ['responses', action.payload.id],
        action.payload.response
      );
    default:
      return reducer(state, action);
  }
};

class ItemRenderer extends React.Component {
  static propTypes = {
    classes: PropTypes.object.isRequired,
    intl: PropTypes.object.isRequired,
    item: PropTypes.instanceOf(Map).isRequired,
    state: PropTypes.instanceOf(Map),
    hidden: PropTypes.bool,
    dispatch: PropTypes.func,
    onNoteChange: PropTypes.func,
    onResponseChange: PropTypes.func
  };

  static defaultProps = {
    state: new Map({}),
    hidden: false,
    dispatch: () => {},
    onNoteChange: () => {},
    onResponseChange: () => {}
  };

  onNoteChange = e => {
    this.props.onNoteChange(e.target.value);
    this.props.dispatch(actions.changeNote(e.target.value));
  };

  onResponseChange = (value, id) => {
    this.props.onResponseChange(value, id);
    this.props.dispatch(actions.changeResponse(value, id));
  };

  render() {
    const { intl, item, classes, dispatch, state, hidden } = this.props;
    const { Component } = formats[item.getIn(['format', 'type'])];

    if (!Component) {
      return <div>{intl.formatMessage(commonMessages.unsupported)}</div>;
    }

    return (
      <div
        className={classes.container}
        style={{ display: !hidden ? 'block' : 'none' }}
      >
        {item.get('assignment') && (
          <Paper className={classes.assignment}>
            <div className={classes.assignmentHeader}>
              <Typography>
                {intl.formatMessage(commonMessages.assignment)}
              </Typography>
            </div>
            <div className={classes.assignmentBody}>
              <Markdown source={item.get('assignment')} />
            </div>
          </Paper>
        )}
        {item.getIn(['content', 'public']) && (
          <Paper className={classes.item}>
            <Component
              dispatch={dispatch}
              {...state.toJS()}
              content={item.getIn(['content', 'public'], new Map({})).toJS()}
            />
          </Paper>
        )}
        {item.get('responses') &&
          item
            .get('responses', new Map({}))
            .map((response, i) => (
              <Paper key={i} className={classes.response}>
                <ResponseField
                  {...response.toJS()}
                  value={state.get('responses', new Map({})).toJS()[i]}
                  onChange={value => this.onResponseChange(value, i)}
                />
              </Paper>
            ))
            .toList()}
        {item.get('allow_note') && (
          <StickyNote
            title={intl.formatMessage(commonMessages.personalNote)}
            value={state.get('note', '')}
            x={state.getIn(['note_position', 'x'], 0)}
            y={state.getIn(['note_position', 'y'], 0)}
            onValueChange={this.onNoteChange}
          />
        )}
      </div>
    );
  }
}

export default withStyles(styles)(injectIntl(ItemRenderer));
