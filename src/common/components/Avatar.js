/**
 *
 * Avatar
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import MuiAvatar from '@material-ui/core/Avatar';

const roleColors = {
  admin: '#2ecc71',
  author: '#3498db',
  data_analyst: '#8e44ad',
  user: '#e67e22',
  default: '#AEAEAE'
};

const getInitials = name => {
  const upperName = name.toUpperCase();
  const parts = upperName.split(' ');
  if (parts.length >= 2) {
    return parts[0][0] + parts[1][0];
  } else {
    return upperName[0] + upperName[1];
  }
};

class Avatar extends React.PureComponent {
  static propTypes = {
    src: PropTypes.string,
    name: PropTypes.string,
    role: PropTypes.string,
    size: PropTypes.number,
    onClick: PropTypes.func
  };

  static defaultProps = {
    name: 'NA',
    role: 'default',
    size: 36,
    onClick: () => {}
  };

  render() {
    const { src, name, role, size, onClick } = this.props;
    if (!src) {
      const color = roleColors[role];
      const initials = getInitials(name);
      return (
        <MuiAvatar
          onClick={onClick}
          style={{
            height: size,
            width: size,
            backgroundColor: color,
            fontSize: size / 2
          }}
        >
          {initials}
        </MuiAvatar>
      );
    } else {
      return (
        <MuiAvatar
          src={src}
          alt={name}
          onClick={onClick}
          style={{
            height: size,
            width: size
          }}
        />
      );
    }
  }
}

export default Avatar;
