/**
 *
 * SaveButton
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import SaveIcon from '@material-ui/icons/Save';
import SendIcon from '@material-ui/icons/Send';

const styles = theme => ({
  saved: {
    color: theme.palette.secondary.contrastText,
    backgroundColor: theme.palette.secondary.main
  },
  error: {
    color: theme.palette.error.contrastText,
    backgroundColor: theme.palette.error.main
  }
});

class SaveButton extends React.Component {
  static propTypes = {
    classes: PropTypes.object.isRequired,
    status: PropTypes.oneOf(['saved', 'error', 'new']),
    fab: PropTypes.bool,
    onClick: PropTypes.func
  };

  static defaultProps = {
    fab: false,
    onClick: () => {}
  };

  render() {
    const { classes, status, fab, onClick } = this.props;
    return (
      <Button
        variant={fab ? 'fab' : null}
        className={status === 'saved' ? classes.saved : classes.error}
        onClick={onClick}
      >
        {status === 'new' ? <SaveIcon /> : <SendIcon />}
      </Button>
    );
  }
}

export default withStyles(styles)(SaveButton);
