/*
 *
 * Login
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import { withStyles } from '@material-ui/core/styles';
import green from '@material-ui/core/colors/green';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Paper from '@material-ui/core/Paper';
import CircularProgress from '@material-ui/core/CircularProgress';
import Typography from '@material-ui/core/Typography';

// @TODO move into public folder
import ddiLogo from './ddi-logo.png';

import memberMessages from '../messages/member';

const styles = theme => ({
  root: {
    position: 'absolute',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    height: '100%',
    backgroundColor: theme.palette.primary[500],
    width: '100%',
    justifyContent: 'center'
  },
  form: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    padding: theme.spacing.unit * 2
  },
  wrapper: {
    margin: theme.spacing.unit,
    position: 'relative'
  },
  inputs: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit
  },
  buttonProgress: {
    color: green[500],
    position: 'absolute',
    top: '50%',
    left: '50%',
    marginTop: -12,
    marginLeft: -12
  },
  logo: {
    width: 300,
    margin: theme.spacing.unit * 2
  }
});

export class Login extends React.Component {
  state = {
    username: '',
    password: ''
  };

  static propTypes = {
    login: PropTypes.func.isRequired,
    loggingInFailed: PropTypes.bool,
    loggingIn: PropTypes.bool,
    classes: PropTypes.object.isRequired
  };

  static defaultProps = {
    loggingIn: false,
    loggingInFailed: false
  };

  componentDidMount() {}

  onUsernameChange = e => {
    this.setState({
      username: e.currentTarget.value
    });
  };

  onPasswordChange = e => {
    this.setState({
      password: e.currentTarget.value
    });
  };

  onSubmit = e => {
    e.preventDefault();
    const { login } = this.props;
    login(this.state.username, this.state.password);
  };

  render() {
    const { loggingInFailed, loggingIn, classes } = this.props;
    return (
      <form onSubmit={this.onSubmit}>
        <div className={classes.root}>
          <Paper className={classes.form}>
            <Typography type="headline">COMMOOP - Admin</Typography>
            <TextField
              id="username"
              label={<FormattedMessage {...memberMessages.username} />}
              className={classes.inputs}
              name="username"
              onChange={this.onUsernameChange}
              value={this.state.username}
              error={loggingInFailed}
              margin="normal"
            />
            <TextField
              id="password"
              label={<FormattedMessage {...memberMessages.password} />}
              className={classes.inputs}
              onChange={this.onPasswordChange}
              name="password"
              value={this.state.password}
              type="password"
              error={loggingInFailed}
              margin="normal"
            />
            <div className={classes.wrapper}>
              <Button variant="contained" type="submit">
                Login
              </Button>
              {loggingIn && (
                <CircularProgress
                  size={24}
                  className={classes.buttonProgress}
                />
              )}
            </div>
          </Paper>
          <div className={classes.logo}>
            <a
              href="https://www.ddi.wiwi.uni-due.de/"
              target="_blank"
              rel="noopener noreferrer"
            >
              <img
                width="100%"
                height="auto"
                src={ddiLogo}
                alt="Didaktik der Informatik"
              />
            </a>
          </div>
        </div>
      </form>
    );
  }
}

export default withStyles(styles)(Login);
