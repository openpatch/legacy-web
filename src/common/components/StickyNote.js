import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import PureTextField from './PureTextField';
import Typography from '@material-ui/core/Typography';

const styles = theme => ({
  root: {
    position: 'fixed',
    width: 300,
    minHeight: 150
  },
  header: {
    padding: theme.spacing.unit * 2,
    backgroundColor: theme.palette.secondary[100],
    userSelect: 'none'
  },
  body: {
    padding: theme.spacing.unit * 2
  }
});

class StickyNote extends React.Component {
  static propTypes = {
    classes: PropTypes.object.isRequired,
    title: PropTypes.string,
    value: PropTypes.string,
    onValueChange: PropTypes.func,
    onPositionChange: PropTypes.func
  };

  static defaultProps = {
    title: '',
    value: '',
    onValueChange: () => {},
    onPositionChange: () => {}
  };

  state = {
    x: window.innerWidth - 310,
    y: window.innerHeight - 160
  };

  activeMove = e => {
    window.addEventListener('mousemove', this.move);
    window.addEventListener('mouseup', this.deactivateMove);
  };

  deactivateMove = e => {
    window.removeEventListener('mousemove', this.move);
    window.removeEventListener('mouseup', this.deactivateMove);
  };

  move = e => {
    const x = e.clientX;
    const y = e.clientY;
    this.setState({
      x,
      y
    });
  };

  render() {
    const { classes, title, value, onValueChange } = this.props;
    return (
      <Paper
        className={classes.root}
        style={{
          top: this.state.y,
          left: this.state.x
        }}
      >
        <div className={classes.header} onMouseDown={this.activeMove}>
          <Typography>{title}</Typography>
        </div>
        <div className={classes.body}>
          <PureTextField
            multiline
            fullWidth
            value={value}
            onChange={onValueChange}
            margin="normal"
          />
        </div>
      </Paper>
    );
  }
}

export default withStyles(styles)(StickyNote);
