/**
 *
 * VisualizationButton
 *
 */

import React from 'react';
import IconButton from '@material-ui/core/IconButton';
import VisualizationIcon from '@material-ui/icons/BlurOn';
import Tooltip from '@material-ui/core/Tooltip';

class VisualizationButton extends React.Component {
  render() {
    return (
      <Tooltip title="Show Visualization">
        <IconButton color="inherit" {...this.props}>
          <VisualizationIcon />{' '}
        </IconButton>
      </Tooltip>
    );
  }
}

export default VisualizationButton;
