/**
 *
 * AddButton
 *
 */

import React from 'react';
import IconButton from '@material-ui/core/IconButton';
import Add from '@material-ui/icons/Add';

const AddButton = props => {
  return (
    <IconButton color="inherit" {...props}>
      <Add />
    </IconButton>
  );
};

export default AddButton;
