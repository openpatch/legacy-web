import React from 'react';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';

class LabeledSwitch extends React.PureComponent {
  render() {
    const { label, ...props } = this.props;
    return <FormControlLabel control={<Switch {...props} />} label={label} />;
  }
}

export default LabeledSwitch;
