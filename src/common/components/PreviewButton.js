/**
 *
 * PreviewButton
 *
 */

import React from 'react';
import IconButton from '@material-ui/core/IconButton';
import VisibilityIcon from '@material-ui/icons/Visibility';
import Tooltip from '@material-ui/core/Tooltip';

class PreviewButton extends React.PureComponent {
  render() {
    return (
      <Tooltip title="Show Preview">
        <IconButton color="inherit" {...this.props}>
          <VisibilityIcon />
        </IconButton>
      </Tooltip>
    );
  }
}

export default PreviewButton;
