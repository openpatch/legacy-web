import Survey from '@material-ui/icons/Event';
import Test from '@material-ui/icons/Class';
import Format from '@material-ui/icons/Extension';
import Permissions from '@material-ui/icons/PersonAdd';

export { Survey, Test, Format, Permissions };
