/**
 *
 * CodeEditor
 *
 */

import React from 'react';
import AceEditor from 'react-ace';

const languages = ['java', 'markdown'];

const themes = ['github'];

languages.forEach(lang => {
  require(`brace/mode/${lang}`);
  require(`brace/snippets/${lang}`);
});

themes.forEach(theme => {
  require(`brace/theme/${theme}`);
});

class CodeEditor extends React.PureComponent {
  static defaultProps = {
    mode: 'java',
    theme: 'github'
  };

  render() {
    return <AceEditor {...this.props} />;
  }
}

export default CodeEditor;
