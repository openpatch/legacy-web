/**
 *
 * InfoButton
 *
 */

import React from 'react';
import IconButton from '@material-ui/core/IconButton';
import InfoIcon from '@material-ui/icons/InfoOutlined';
import Tooltip from '@material-ui/core/Tooltip';

class InfoButton extends React.PureComponent {
  render() {
    return (
      <Tooltip title="Show Info">
        <IconButton color="inherit" {...this.props}>
          <InfoIcon />
        </IconButton>
      </Tooltip>
    );
  }
}

export default InfoButton;
