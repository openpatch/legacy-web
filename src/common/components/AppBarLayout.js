import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  wrapper: {
    display: 'flex',
    width: '100%',
    flexDirection: 'column',
    height: '100vh'
  },
  content: {
    flex: 1,
    padding: theme.spacing.unit * 2,
    overflow: 'auto'
  }
});

const AppBarLayout = ({ appBar, children, classes, className }) => (
  <div className={classes.wrapper}>
    {appBar}
    <div className={classes.content}>
      <div
        className={className}
        style={{
          width: '100%',
          margin: '0 auto',
          marginBottom: 16
        }}
      >
        {children}
      </div>
    </div>
  </div>
);

AppBarLayout.propTypes = {
  appBar: PropTypes.element,
  classes: PropTypes.object.isRequired,
  className: PropTypes.string
};

export default withStyles(styles)(AppBarLayout);
