import React from 'react';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  appBarSpacer: {
    marginTop: 64,
    [theme.breakpoints.up('md')]: {
      marginTop: 58
    }
  }
});

const AppBarSpacer = ({ classes }) => <div className={classes.appBarSpacer} />;

export default withStyles(styles)(AppBarSpacer);
