/**
 *
 * CodeBlock
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import hljs from 'highlight.js/lib/highlight';

import java from 'highlight.js/lib/languages/java';

['java'].forEach(langName => {
  // Using require() here because import() support hasn't landed in Webpack yet
  const langModule = require(`highlight.js/lib/languages/${langName}`);
  hljs.registerLanguage(langName, langModule);
});

class CodeBlock extends React.PureComponent {
  static defaultProps = {
    language: null,
    value: null
  };

  static propTypes = {
    value: PropTypes.string,
    language: PropTypes.string
  };

  constructor(props) {
    super(props);
    this.setRef = this.setRef.bind(this);
  }

  setRef(el) {
    this.codeEl = el;
  }

  componentDidMount() {
    if (this.props.language) {
      this.highlightCode();
    }
  }

  componentDidUpdate() {
    if (this.props.language) {
      this.highlightCode();
    }
  }

  highlightCode() {
    hljs.highlightBlock(this.codeEl);
  }

  render() {
    const { language, value, ...props } = this.props;
    return (
      <pre>
        <code ref={this.setRef} className={language} {...props}>
          {value}
        </code>
      </pre>
    );
  }
}

export default CodeBlock;
