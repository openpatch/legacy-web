/**
 *
 * StatisticSummaryTable
 *
 */

import React from 'react';
import { Map } from 'immutable';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';

const round = value => {
  value = Math.round(value * 100) / 100;
  if (isNaN(value)) {
    return '';
  }
  return value;
};

export const generateRowData = (data = new Map({}), key) =>
  new Map({
    min: data.get(`${key}_min`),
    max: data.get(`${key}_max`),
    median: data.get(`${key}_median`),
    standard_deviation: data.get(`${key}_standard_deviation`),
    variance: data.get(`${key}_variance`),
    n: data.get(`${key}_n`)
  });

export const StatisticSummaryTable = ({ rows }) => (
  <Table>
    <TableHead>
      <TableRow>
        <TableCell>Metric</TableCell>
        <TableCell>Min</TableCell>
        <TableCell>Max</TableCell>
        <TableCell>Median</TableCell>
        <TableCell>SD</TableCell>
        <TableCell>Variance</TableCell>
        <TableCell>N</TableCell>
      </TableRow>
    </TableHead>
    <TableBody>
      {rows
        .map((value = new Map({}), key) => (
          <TableRow key={key}>
            <TableCell>{key}</TableCell>
            <TableCell>{round(value.get('min'))}</TableCell>
            <TableCell>{round(value.get('max'))}</TableCell>
            <TableCell>{round(value.get('median'))}</TableCell>
            <TableCell>{round(value.get('standard_deviation'))}</TableCell>
            <TableCell>{round(value.get('variance'))}</TableCell>
            <TableCell>{value.get('n')}</TableCell>
          </TableRow>
        ))
        .toList()}
    </TableBody>
  </Table>
);

export default StatisticSummaryTable;
