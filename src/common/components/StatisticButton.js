/**
 *
 * StatisticButton
 *
 */

import React from 'react';
import IconButton from '@material-ui/core/IconButton';
import StatisticIcon from '@material-ui/icons/BarChart';
import Tooltip from '@material-ui/core/Tooltip';

class StatisticButton extends React.Component {
  render() {
    return (
      <Tooltip title="Show Statistic">
        <IconButton color="inherit" {...this.props}>
          <StatisticIcon />{' '}
        </IconButton>
      </Tooltip>
    );
  }
}

export default StatisticButton;
