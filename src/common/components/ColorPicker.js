import React from 'react';
import PropTypes from 'prop-types';
import { CirclePicker } from 'react-color';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import IconButton from '@material-ui/core/IconButton';
import ColorLensIcon from '@material-ui/icons/ColorLens';

class SketchExample extends React.Component {
  static propTypes = {
    onChange: PropTypes.func
  };

  static defaultProps = {
    onChange: () => {}
  };
  state = {
    open: false
  };

  handleClickOpen = () => {
    this.setState({ open: true });
  };
  handleClose = () => {
    this.setState({ open: false });
  };

  handleChange = color => {
    this.handleClose();
    this.props.onChange(color.hex);
  };

  render() {
    return (
      <div>
        <IconButton
          style={{ color: this.props.color }}
          onClick={this.handleClickOpen}
        >
          <ColorLensIcon />
        </IconButton>
        <Dialog
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="color-picker-title"
        >
          <DialogTitle id="color-picker-title">Color Picker</DialogTitle>
          <DialogContent>
            <CirclePicker
              color={this.state.color}
              onChange={this.handleChange}
            />
          </DialogContent>
        </Dialog>
      </div>
    );
  }
}

export default SketchExample;
