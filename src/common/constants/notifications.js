/**
 *
 * Notifications constants
 *
 */

export const DEFAULT_ACTION = 'commoop.common.notifications.DEFAULT_ACTION';

export const ADD_NOTIFICATION = 'commoop.common.notifications.ADD_NOTIFICATION';

export const REMOVE_NOTIFICATION =
  'commoop.common.notifications.REMOVE_NOTIFICATION';

export const CLEAR_NOTIFICATIONS =
  'commoop.common.notifications.CLEAR_NOTIFICATIONS';
