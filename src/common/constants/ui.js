/**
 *
 * UI constants
 *
 */

export const TOGGLE_OPEN = 'commoop.common.ui.TOGGLE_OPEN';

export const TOGGLE_IMPRINT_PRIVACY_OPEN =
  'commoop.common.ui.TOGGLE_IMPRINT_PRIVACY_OPEN';
