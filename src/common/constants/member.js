/**
 *
 * Member constants
 *
 */

export const DEFAULT_ACTION = 'commoop.common.member.DEFAULT_ACTION';

export const LOGIN = 'commoop.common.member.LOGIN';

export const LOGIN_COMMIT = 'commoop.common.member.LOGIN_COMMIT';

export const LOGIN_ROLLBACK = 'commoop.common.member.LOGIN_ROLLBACK';

export const LOGOUT = 'commoop.common.member.LOGOUT';

export const TOGGLE_FAVORITE = 'commoop.common.containers.TOGGLE_FAVORITE';

export const TOGGLE_FAVORITE_ROLLBACK =
  'commoop.common.containers.TOGGLE_FAVORITE_ROLLBACK';

export const TOGGLE_FAVORITE_COMMIT =
  'commoop.common.containers.TOGGLE_FAVORITE_COMMIT';
