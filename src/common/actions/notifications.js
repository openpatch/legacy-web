/**
 *
 * Notifications actions
 *
 */

import {
  DEFAULT_ACTION,
  ADD_NOTIFICATION,
  REMOVE_NOTIFICATION,
  CLEAR_NOTIFICATIONS
} from '../constants/notifications';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION
  };
}

export function addNotification(notification) {
  return {
    type: ADD_NOTIFICATION,
    payload: {
      notification
    }
  };
}

export function removeNotification() {
  return {
    type: REMOVE_NOTIFICATION
  };
}

export function clearNotifications() {
  return {
    type: CLEAR_NOTIFICATIONS
  };
}
