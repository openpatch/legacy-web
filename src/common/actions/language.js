/*
 *
 * Language actions
 *
 */

import { CHANGE_LOCALE } from '../constants/language';

export function changeLocale(languageLocale) {
  return {
    type: CHANGE_LOCALE,
    locale: languageLocale
  };
}
