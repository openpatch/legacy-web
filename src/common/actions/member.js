/**
 *
 * Member actions
 *
 */

import {
  DEFAULT_ACTION,
  LOGIN,
  LOGIN_COMMIT,
  LOGIN_ROLLBACK,
  LOGOUT,
  TOGGLE_FAVORITE,
  TOGGLE_FAVORITE_COMMIT,
  TOGGLE_FAVORITE_ROLLBACK
} from '../constants/member';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION
  };
}

export function toggleFavorite(type, id) {
  return {
    type: TOGGLE_FAVORITE,
    payload: {
      type,
      id
    },
    meta: {
      offline: {
        effect: {
          url: `v1/members/me/favorites`,
          method: 'PATCH',
          data: {
            type,
            id
          }
        },
        commit: {
          type: TOGGLE_FAVORITE_COMMIT
        },
        rollback: {
          type: TOGGLE_FAVORITE_ROLLBACK,
          meta: {
            type,
            id
          }
        }
      }
    }
  };
}

export function logout() {
  return {
    type: LOGOUT,
    meta: {
      offline: {
        effect: {
          url: 'v1/members/logout',
          method: 'GET'
        }
      }
    }
  };
}

export function login(username, password) {
  return {
    type: LOGIN,
    payload: {
      username,
      password
    },
    meta: {
      offline: {
        effect: {
          url: 'v1/members/login',
          method: 'GET',
          auth: { username, password }
        },
        commit: {
          type: LOGIN_COMMIT,
          meta: {
            username
          }
        },
        rollback: {
          type: LOGIN_ROLLBACK,
          meta: {
            username
          }
        }
      }
    }
  };
}
