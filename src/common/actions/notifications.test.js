import * as actions from './notifications';

describe('notifications actions', () => {
  it('Default Action', () => {
    expect(actions.defaultAction()).toMatchSnapshot();
  });

  it('should create add notification action', () => {
    expect(actions.addNotification({ message: 'hi' })).toMatchSnapshot();
  });

  it('should create remove notification action', () => {
    expect(actions.removeNotification()).toMatchSnapshot();
  });

  it('should create clear notifications action', () => {
    expect(actions.clearNotifications()).toMatchSnapshot();
  });
});
