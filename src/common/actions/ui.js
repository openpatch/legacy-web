/**
 *
 * UI actions
 *
 */

import { TOGGLE_OPEN, TOGGLE_IMPRINT_PRIVACY_OPEN } from '../constants/ui';

export function toggleOpen() {
  return {
    type: TOGGLE_OPEN
  };
}

export function toggleImprintPrivacyOpen() {
  return {
    type: TOGGLE_IMPRINT_PRIVACY_OPEN
  };
}
