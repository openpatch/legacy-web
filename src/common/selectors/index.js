import * as language from './language';
import * as member from './member';
import * as ui from './ui';
import * as notifications from './notifications';

export default {
  language,
  member,
  ui,
  notifications
};
