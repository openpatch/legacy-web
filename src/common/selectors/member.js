import { createSelector } from 'reselect';
import { dataSelector as itemsDataSelector } from '../../admin/selectors/items';
import { dataSelector as testsDataSelector } from '../../admin/selectors/tests';
import { dataSelector as surveysDataSelector } from '../../admin/selectors/surveys';

/**
 * Direct selector to the Member state domain
 */
const MemberSelector = state => state.common.get('member');

/**
 * Other specific selectors
 */

const favoriteItemsSelector = createSelector(MemberSelector, substate =>
  substate.get('items_favorized')
);

const favoriteTestsSelector = createSelector(MemberSelector, substate =>
  substate.get('tests_favorized')
);

const favoriteSurveysSelector = createSelector(MemberSelector, substate =>
  substate.get('surveys_favorized')
);

const roleSelector = createSelector(MemberSelector, substate =>
  substate.get('role')
);

export const idSelector = createSelector(MemberSelector, substate =>
  substate.get('id')
);

const usernameSelector = createSelector(MemberSelector, substate =>
  substate.get('username')
);

const loginSelector = createSelector(MemberSelector, substate => ({
  role: substate.get('role'),
  loggingIn: substate.get('logging_in'),
  loggingInFailed: substate.get('logging_in_failed')
}));

const makeFavoriteItemsDataSelector = () =>
  createSelector([favoriteItemsSelector, itemsDataSelector], (itemIds, data) =>
    data.filter(value => itemIds.includes(value.get('id'))).toList()
  );

export const makeFavoriteTestsDataSelector = () =>
  createSelector([favoriteTestsSelector, testsDataSelector], (testIds, data) =>
    data.filter(value => testIds.includes(value.get('id'))).toList()
  );

export const makeFavoriteSurveysDataSelector = () =>
  createSelector(
    [favoriteSurveysSelector, surveysDataSelector],
    (surveyIds, data) =>
      data.filter(value => surveyIds.includes(value.get('id'))).toList()
  );

/**
 * Default selector used by Member
 */

export const makeMemberSelector = () =>
  createSelector(MemberSelector, substate => substate);

export default makeMemberSelector;
export {
  MemberSelector,
  favoriteItemsSelector,
  favoriteSurveysSelector,
  favoriteTestsSelector,
  roleSelector,
  usernameSelector,
  loginSelector,
  makeFavoriteItemsDataSelector
};
