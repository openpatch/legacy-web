import { fromJS } from 'immutable';
import makeNotificationsSelector from './notifications';

describe('notifications selectors', () => {
  it('Expect to have unit tests specified', () => {
    const selector = makeNotificationsSelector();
    const state = {
      nothingOfRelevance: [],
      common: fromJS({
        notifications: { test: 'hi' }
      })
    };
    expect(selector(state)).toEqual(
      fromJS({
        test: 'hi'
      })
    );
  });
});
