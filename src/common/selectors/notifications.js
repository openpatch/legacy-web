import { createSelector } from 'reselect';

/**
 * Direct selector to the Notifications state domain
 */
const NotificationsSelector = state => state.common.get('notifications');

/**
 * Other specific selectors
 */

/**
 * Default selector used by Notifications
 */

const makeNotificationsSelector = () =>
  createSelector(NotificationsSelector, substate => substate);

export default makeNotificationsSelector;
export { NotificationsSelector };
