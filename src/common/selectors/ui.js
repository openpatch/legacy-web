import { createSelector } from 'reselect';

/**
 * Direct selector to the UI state domain
 */
export const UISelector = state => state.common.get('ui');

/**
 * Sharable selectors
 */

export const makeOpenSelector = () =>
  createSelector(UISelector, substate => substate.get('open'));

export const makeImprintPrivacyOpenSelector = () =>
  createSelector(UISelector, substate => substate.get('imprint_privacy_open'));
/**
 * Default selector used by UI
 */

export const makeUISelector = () =>
  createSelector(UISelector, substate => substate);

export default makeUISelector;
