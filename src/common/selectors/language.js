import { createSelector } from 'reselect';

/**
 * Direct selector to the languageToggle state domain
 */
const selectLanguage = state => state.common.get('language');

/**
 * Select the language locale
 */

const makeLocaleSelector = () =>
  createSelector(selectLanguage, languageState => languageState.get('locale'));

export { selectLanguage, makeLocaleSelector };
