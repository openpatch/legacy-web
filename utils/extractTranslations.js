const i18n = require('../src/i18n');
const transform = require('babel-core').transform;
const nodeGlob = require('glob');
const fs = require('fs');

const presets = [require.resolve('babel-preset-react-app')];
const plugins = ['react-intl'];

const FILES_TO_PARSE = 'src/**/!(*.test).js';
const locales = i18n.appLocales;
const DEFAULT_LOCALE = i18n.DEFAULT_LOCALE;

const response = transform('code', {
  presets: [...presets],
  plugins: [...plugins]
});

const glob = pattern =>
  new Promise((resolve, reject) => {
    nodeGlob(
      pattern,
      (error, value) => (error ? reject(error) : resolve(value))
    );
  });

const readFile = fileName =>
  new Promise((resolve, reject) => {
    fs.readFile(
      fileName,
      (error, value) => (error ? reject(error) : resolve(value))
    );
  });

const writeFile = (fileName, data) =>
  new Promise((resolve, reject) => {
    fs.writeFile(
      fileName,
      data,
      (error, value) => (error ? reject(error) : resolve(value))
    );
  });

// Store exisiting translations into memory
const existingLocaleMappings = [];
const localeMappings = [];

for (const locale of locales) {
  existingLocaleMappings[locale] = {};
  localeMappings[locale] = {};

  const translationFileName = `src/translations/${locale}.json`;
  try {
    const messages = JSON.parse(fs.readFileSync(translationFileName));
    const messageKeys = Object.keys(messages);
    for (const messageKey of messageKeys) {
      existingLocaleMappings[locale][messageKey] = messages[messageKey];
    }
  } catch (error) {
    if (error.code != 'ENOENT') {
      process.stderr.write(
        `There was an error loading this translation file: ${translationFileName}
                \n${error}`
      );
    }
  }
}

const extractFromFile = async fileName => {
  try {
    const code = await readFile(fileName);
    // use babel plugin to extract instances where react-intl is used
    const { metadata: result } = await transform(code, { presets, plugins });
    for (const message of result['react-intl'].messages) {
      for (const locale of locales) {
        const existingLocaleMapping =
          existingLocaleMappings[locale][message.id];
        const newMsg = locale == DEFAULT_LOCALE ? message.defaultMessage : '';
        localeMappings[locale][message.id] = existingLocaleMapping
          ? existingLocaleMapping
          : newMsg;
      }
    }
  } catch (error) {
    // process.stderr.write(`Error transforming file: ${fileName}\n${error}`);
  }
};

(async function main() {
  const files = await glob(FILES_TO_PARSE);
  await Promise.all(files.map(fileName => extractFromFile(fileName)));
  for (const locale of locales) {
    const translationFileName = `src/translations/${locale}.json`;

    try {
      let messages = {};
      Object.keys(localeMappings[locale]).sort().forEach(key => {
        messages[key] = localeMappings[locale][key];
      });
      const prettified = `${JSON.stringify(messages, null, 2)}\n`;
      await writeFile(translationFileName, prettified);
      console.log(`${locale}: Done`);
    } catch (error) {
      console.log(error);
    }
  }
  process.exit();
})();
