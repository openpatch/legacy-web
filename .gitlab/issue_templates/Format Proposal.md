Please read this!

Before opening a new issue, make sure to search for keywords in the issues
filtered by the "format proposal" label and verify the issue you're about
to submit isn't a duplicate.

Please remove this notice if you're confident your issue isn't a duplicate.

------

### Description

(Include problem, use cases, benefits, and/or goals)

### Proposal

- How does the new format work?
- Which competency could be tested?
- (Optionally include a paper-prototype)

### Links / references

- References and links to paper, which cover something similar or this format is based on

### Format checklist

Make sure these are completed before closing the issue,
with a link to the relevant commit.

- [ ] Define content-structure
- [ ] Define solution-structure
- [ ] Implement user view
- [ ] Implement admin view
- [ ] Implement unit tests
- [ ] Write documentation
- [ ] Write some test items
- [ ] Make pull request

/label ~"format proposal"

