Please read this!

Before opening a new issue, make sure to search for keywords in the issues
filtered by the "feature proposal" label and verify the issue you're about
to submit isn't a duplicate.

Please remove this notice if you're confident your issue isn't a duplicate.

------

### Description

(Include problem, use cases, benefits, and/or goals)

### Proposal

### Links / references

#### Overview

What is it?
Why should someone use this feature?
What is the underlying problem?
How do you use this feature?

#### Use cases

Who is this for? Provide one or more use cases.

### Feature checklist

Make sure these are completed before closing the issue,
with a link to the relevant commit.

- [ ] Implement feature
- [ ] Implement unit tests
- [ ] Write documentation
- [ ] Make pull request

/label ~"feature proposal"
