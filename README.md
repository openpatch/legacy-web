# COMMOOP

[![pipeline status](https://git.uni-due.de/commoop/commoop-web/badges/master/pipeline.svg)](https://git.uni-due.de/commoop/commoop-web/commits/master)
[![coverage report](https://git.uni-due.de/commoop/commoop-web/badges/master/coverage.svg)](https://git.uni-due.de/commoop/commoop-web/commits/master)

# Quick start

1. Install [yarn](https://yarnpkg.com).
1. Clone this repo using `git clone https://git.uni-due.de/commoop/commoop-web.git`.
1. Run `yarn install` to install dependencies.
1. Set the environment variable `REACT_APP_API_URL` to the url of the [api server](https://git.uni-due.de/commoop/commoop-api).
1. Run `yarn start` to start the app.
1. Visit [http://localhost:3000](http://localhost:3000).

# Contributing

There are many ways to contribute to the COMMOOP project. See [Contributing](CONTRIBUTING.md) for more details.

# Documentation

* [Introduction](docs/general/introduction.md): An introduction for newcomers to commoop.
* [Overview](docs/general): A short overview of the included tools.
* [Command](docs/general/commands.md): A overview of all available commands.
* [Testing](docs/testing): How to write tests.
* [Styling](docs/css): How to work with the CSS tooling.
* [JS](docs/js): Overview of the core libraries which we use.
