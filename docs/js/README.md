# Javascript
We use ES6 through out this projects. When you have used Javascript in the past, please read this article to keep up with the current standard. [Javascript an introduction to es6](https://medium.com/sons-of-javascript/javascript-an-introduction-to-es6-1819d0d89a0f)

## Table of Contents

- [i18n](i18n.md)
- [Immutable JS](immutablejs.md)
- [Redux](redux.md)
- [Reselect](reselect.md)