# `i18n`

`react-intl` is a library to manage internationalization and pluralization support
for your react application. This involves multi-language support for both the static text but also things like variable numbers, words or names that change with application state. `react-intl` provides an incredible amount of mature facility to preform these very tasks.

The complete `react-intl` docs can be found here:

https://github.com/yahoo/react-intl/wiki

## Extracting i18n JSON files
You can extract all i18n language within each component by running the following command:

```
yarn extract
```

This will extract all language strings into i18n JSON files in `src/translations`.

## Adding a Language
You can add a language by running the plop command:

```
yarn plop
```

And then select the language option. Afterwards enter the two character i18n standard language specifier. This will add in the necessary JSON language file and import statements for the language.