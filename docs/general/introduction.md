# Introduction

The [`README.md`](../../README.md) gives you adequate information on how to clone, install dependencies and launch the COMMOOP web-app.

Once you have done that, this document is intended to give you as taste of how everything works. It still assumes basic knowledge of [React](https://facebook.github.io/react/), [Redux](http://redux.js.org) and [React Router.](https://reacttraining.com/react-router/).

## Tech Stack

Here's a curated list of packages that you should have knowledge of, when working with this project. However, the best way to have a complete list of dependencies is to see [`package.json`](../../package.json)

### Core

* [ ] [React](https://facebook.github.io/react/)
* [ ] [React Router](https://github.com/ReactTraining/react-router)
* [ ] [Redux](http://redux.js.org/)
* [ ] [Reselect](https://github.com/reactjs/reselect)
* [ ] [ImmutableJS](https://facebook.github.io/immutable-js/)
* [ ] [Material UI](https://github.com/mui-org/material-ui)
* [ ] [React Intl](https://github.com/yahoo/react-intl)

### Testing

* [ ] [Jest](http://facebook.github.io/jest/)

### Auto Formatting

* [ ] [Prettier](https://github.com/prettier/prettier)

## Project Structure

### docs

As the name suggest, this folder contains the documentation of this project.

### public

This folder contains public assets which can be used in our project. See [create-react-app](https://github.com/facebookincubator/create-react-app/blob/master/packages/react-scripts/template/README.md#using-the-public-folder) for more information.

### src

We use the [container/component architecture](https://medium.com/@dan_abramov/smart-and-dumb-components-7ca2f9a7c7d0). `containers/` contains React components which are connected to the redux store. `components/` contains dumb React components which depend on containers for data. **Container components care about how things work, while components care about how things look**. We added `formats/` to contain all formats which are supported in our project. You can use `yarn plop` to add a new format.

### templates

These are files which are used by [`plop`](https://plopjs.com/) to generate new formats based on a template.

### utils

Are internal scripts which could not be executed as-is in the web browser.

## Basic Flow

You can launch the project by running `yarn start`. To fully understand its inner workings, you will have to understand multiple technologies and how they interact. Form this point, we are going into an overdrive of implementation details.

### How does the project starts?

We used [`create-react-app`](https://github.com/facebookincubator/create-react-app) to boilerplate our project, therefore the projects starts with the `src/public/index.html`(../../src/public/index.html). React will render our project into the `div#root`.

`create-react-app` will internally use webpack to translate our source files into small javascript files which could be executed from web browsers.

#### `src/index.js`

This is the entry point of our project. It will render our main container `App`. Webpack will access the entire project from this file. Because `App` is the entry point of our project, it contains things which you will not see anywhere else in the project:

* `<Provider />` connects our project to the redux `store`
* `<MuiThemeProvider />` passes the defined `theme` to all components which are using `withStyles`. Both come form `material-ui`
* `<LanguageProvider />` provides language translation support to our project
* `<ConnectedRouter />` will have information of our routes and will pass it to the Redux store

You can also see a higher order component (hoc) `Loadable` which is heavily used throughout our project. This component wraps a given component to support code splitting. The passed component will only be loaded when the code is executed. Therefore webpack will create a separate javascript file for each component which is loaded with `Loadable`. We have done this to increase loading times.

#### Redux

Redux is the core of our project. So you should complete this checklist:

* [ ] Understand the [motivation behind Redux](http://redux.js.org/docs/introduction/Motivation.html)
* [ ] Understand the [three principles of Redux](http://redux.js.org/docs/introduction/ThreePrinciples.html)
* [ ] Take a look at a [simple example](http://redux.js.org/docs/basics/ExampleTodoList.html)

The Redux `store` is the heart of our project. Check out [`store.js`](../../src/store.js) to see how we have configured the store.

The store is created with the `createStore()` function, which accepts three parameters.

1. **Root reducer**: A master reducer combining all reducers.
1. **Initial state**: The initial state
1. **Middleware/enhancers**: Middlewares are third party libraries which intercept each redux action dispatched to the redux store and then do stuff.

In our project we ware using three such middlewares.

1. [**Thunk Middleware**](https://github.com/gaearon/redux-thunk): This is used for async function.
1. [**Logger Middleware**](https://github.com/evgenyrodionov/redux-logger): When an action is dispatch it will print the previous and the next state of the store in the browser console.
1. [**Router Middleware**](https://github.com/reactjs/react-router-redux): Keeps our routes in sync with the redux `store`.

#### Reselect

Reselect is a library used for slicing our redux state and providing only the relevant sub-tree to a react component. It has three key features:

1. **Computation**: While performing a search operation, reselect will filter the original array and return only matching usernames. Redux state does not have to store a separate array of filtered usernames.
1. **Memoization**: A selector will not compute a new result unless one of its arguments change. That means, if you are repeating the same search once again, reselect will not filter the array over and over. It will just return the previously computed, and subsequently cached, result. Reselect compares the old and the new arguments and then decides whether to compute again or return the cached result.
1. **Composability**: You can combine multiple selectors. For example, one selector can filter usernames according to a search key and another selector can filter the already filtered array according to gender. One more selector can further filter according to age. You combine these selectors by using `createSelector()`
