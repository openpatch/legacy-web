# Command Line Commands

## Development

```
yarn start
```

Starts the development server running on `http://localhost:3000`

## Storybook

```
yarn storybook
```

[Storybook](https://storybook.js.org/) can be used to develop React components in isolation. It is also used for displaying and documenting all components and containers which are used in this project.

## Plop (Generators)

```
yarn plop
```

Allows to auto-generate boilerplate code for common parts of our project, specifically `component`, `container`, `format`, `domain` and `language`.

## Building

```
yarn build
```

Prepares our project for deployment. Optimizes and minifies all files, piping them to the `build` folder. These files could then be uploaded to our production server.

## Testing

```
yarn test
```

Tests our project with unit test specified in the `**/tests/*.test.js` or just `**/*.test.js` files. Jest will start in watch mode and will therefore re-run tests whenever a file changes. The `test` command allows an optional `-- [string]` argument to filter the test run by Jest. For example:

```
# Run only the tests related to parsonsPuzzle
yarn test -- parsonsPuzzle
```

## Extract Language Messages

```
yarn extract
```

Will extract all Messages which are found by [`babel-plugin-react-intl`](https://github.com/yahoo/babel-plugin-react-intl) and pipes them to the `src/translations/` folder. Already existing messages will not be overwritten.
