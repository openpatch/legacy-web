# Testing

Testing our project is a vital part of keeping the code quality high. So there is one simple rule **no merge without tests**. Every piece of code you write must also have a suitable test, otherwise it will not be accepted.

## Table of Contents

* [Snapshot Testing](snapshot-testing.md)
* [Unit Testing](unit-testing.md)
* [Stories](stories.md)

## How to Test?

To test your code do the following:

1. Start Jest in watch mode with `yarn test` and keep it running
1. Add `*.test.js` files directly next to your code. They could also be in a subdirectory `tests/*.test.js`.
1. Write your unit and snapshot tests in those files.
1. Take a look at the output and see if all tests pass!
