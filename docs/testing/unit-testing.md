# Unit Testing

Unit testing is the practice of testing the smallest possible units of our code. We run our tests and automatically verify that our functions do the thing we expect them to do.

This project uses the [Jest](https://facebook.github.io/jest/) test framework to run tests and make assertions. You `describe` a unit of your code, `it` and `expect` it to do the correct thing.

## Example

Lets assume we have the following piece of code in a file named `padLeft.js`:

```javascript
// padLeft.js

export function padLeft(string, pad) {
  while (string.length < pad) {
    string += pad;
  }
  return string;
}
```

We are going to add a second file called `padLeft.test.js` with our unit tests inside.

```javascript
import { padLeft } from './padLeft';

describe('padLeft()', () => {
  it('pads string', () => {
    expect(padLeft('hallo', 2)).toEqual('  hallo');
  });
  it('pads number', () => {
    expect(padLeft(3, 2)).toEqual('  3');
  });
});
```

Jest will show this output when running the test:

```
padLeft()
    ✓ pads string
    ✕ pads number
```

Ok we see that we have an bug in our implementation. When someone passes a number to the function it will count up, so let's see how we can fix it.

```javascript
// padLeft.js

export function padLeft(string, pad) {
  if (typeof string === 'number') {
    string = string.toString();
  }
  while (string.length < pad) {
    string += pad;
  }
  return string;
}
```

Now Jest will show this output when running the test:

```
padLeft()
    ✓ pads string
    ✓ pads number
```

As you see it can sometimes be better to think and write the test cases first and them implement the function.
