# Snapshot Testing

Snapshot test are a very useful tool whenever you want to make sure your UI does not change unexpectedly.

Imagine you make a screenshot of a UI component. When you change something in the code base, the test runner will make another screenshot and will compare it to the first one. The test will fail if the two images do not match. This is a indicator that either you have broken something or that you made some the screenshots needs to be updated to the new version of the UI component.

## Example

Let's assume we have a WarningButton component. Then a simple snapshot tests might look like this:

```javascript
// WarningButton.js
import { Button } from 'gestyled';

const WarningButton = props => <Button bg="coral" />;

export default WarningButton

```

```javascript
// WarningButton.test.js
import React from 'react';
// this is need for testing our components which relay on being embedded in a
// ThemeProvider and a LanguageProvider
import createComponentForTest from '../../utils/createComponentForTest'
import WarningButton from './WarningButton';

describe('<Button />', () => {
    test('renders correctly', () => {
        const component = createComponentForTest(<WarningButton />);
        const tree = component.toJSON();
        expect(tree).toMatchSnapshot();
    });
});
```

When the test is run the first time Jest will create an initial snapshot automatically. Now let's assume we do not like the coral color of our WarningButton anymore and change it to 'crimson'. Now our test will fail and Jest will print the difference between the new snapshot and the old one. We will review it and when you are sure that it was intended then we could update the snapshot by pressing `u`.

We can also emulate functions calls. For a more detailed explanation see [Testing React Apps](https://facebook.github.io/jest/docs/en/tutorial-react.html#content).