# CSS

This project uses [`styled-components`](https://www.styled-components.com/) allowing to write CSS in JavaScript, removing the mapping between styles and components.

We also use [`gestyled`](http://projects.barkmin.eu/gestyled/) which is a library based on `styled-components`. These components should be used in our project to keep a consistent style.

So there is no need to write `*.css` files.