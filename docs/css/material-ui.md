# Material UI

[`material-ui`](https://github.com/mui-org/material-ui) is a component library which is based on Google's [Material Design](https://material.io/guidelines/). It should be used to keep a consistent style through out our project.
