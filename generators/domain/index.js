/**
 * Domain Generator
 */

module.exports = {
  description: 'Add a domain',
  prompts: [
    {
      type: 'list',
      name: 'section',
      message: 'For which section should it be?',
      default: 'common',
      choices: () => ['admin', 'common', 'user']
    },
    {
      type: 'input',
      name: 'name',
      message: 'What should it be called?',
      default: 'language'
    }
  ],
  actions: data => {
    const actions = [];
    // Messages
    actions.push({
      type: 'add',
      path: '../src/{{section}}/messages/{{camelCase name}}.js',
      templateFile: './domain/messages.js.hbs',
      abortOnFail: true
    });

    actions.push({
      type: 'modify',
      path: '../src/{{section}}/messages/index.js',
      pattern: /(.*from\s'.\/[a-zA-Z]*';\n)(?!.*from\s'.\/[a-zA-Z]*';)/g,
      templateFile: './domain/add-import.hbs',
      abortOnFail: true
    });
    actions.push({
      type: 'modify',
      path: '../src/{{section}}/messages/index.js',
      pattern: /(\s[a-z\-A-Z,]+)\n(?!.*\s[a-z\-A-Z,]+)/g,
      templateFile: './domain/add.hbs',
      abortOnFail: true
    });

    // Actions
    actions.push({
      type: 'add',
      path: '../src/{{section}}/actions/{{camelCase name}}.js',
      templateFile: './domain/actions.js.hbs',
      abortOnFail: true
    });
    actions.push({
      type: 'add',
      path: '../src/{{section}}/actions/{{camelCase name}}.test.js',
      templateFile: './domain/actions.test.js.hbs',
      abortOnFail: true
    });

    actions.push({
      type: 'modify',
      path: '../src/{{section}}/actions/index.js',
      pattern: /(.*from\s'.\/[a-zA-Z]*';\n)(?!.*from\s'.\/[a-zA-Z]*';)/g,
      templateFile: './domain/add-import.hbs',
      abortOnFail: true
    });
    actions.push({
      type: 'modify',
      path: '../src/{{section}}/actions/index.js',
      pattern: /(\s[a-z\-A-Z,]+)\n(?!.*\s[a-z\-A-Z,]+)/g,
      templateFile: './domain/add.hbs',
      abortOnFail: true
    });

    // Constants
    actions.push({
      type: 'add',
      path: '../src/{{section}}/constants/{{camelCase name}}.js',
      templateFile: './domain/constants.js.hbs',
      abortOnFail: true
    });

    actions.push({
      type: 'modify',
      path: '../src/{{section}}/constants/index.js',
      pattern: /(.*from\s'.\/[a-zA-Z]*';\n)(?!.*from\s'.\/[a-zA-Z]*';)/g,
      templateFile: './domain/add-import.hbs',
      abortOnFail: true
    });
    actions.push({
      type: 'modify',
      path: '../src/{{section}}/constants/index.js',
      pattern: /(\s[a-z\-A-Z,]+)\n(?!.*\s[a-z\-A-Z,]+)/g,
      templateFile: './domain/add.hbs',
      abortOnFail: true
    });

    // Selectors
    actions.push({
      type: 'add',
      path: '../src/{{section}}/selectors/{{camelCase name}}.js',
      templateFile: './domain/selectors.js.hbs',
      abortOnFail: true
    });
    actions.push({
      type: 'add',
      path: '../src/{{section}}/selectors/{{camelCase name}}.test.js',
      templateFile: './domain/selectors.test.js.hbs',
      abortOnFail: true
    });

    actions.push({
      type: 'modify',
      path: '../src/{{section}}/selectors/index.js',
      pattern: /(.*from\s'.\/[a-zA-Z]*';\n)(?!.*from\s'.\/[a-zA-Z]*';)/g,
      templateFile: './domain/add-import.hbs',
      abortOnFail: true
    });
    actions.push({
      type: 'modify',
      path: '../src/{{section}}/selectors/index.js',
      pattern: /(\s[a-z\-A-Z,]+)\n(?!.*\s[a-z\-A-Z,]+)/g,
      templateFile: './domain/add.hbs',
      abortOnFail: true
    });

    // Reducer
    actions.push({
      type: 'add',
      path: '../src/{{section}}/reducers/{{camelCase name}}.js',
      templateFile: './domain/reducer.js.hbs',
      abortOnFail: true
    });
    actions.push({
      type: 'add',
      path: '../src/{{section}}/reducers/{{camelCase name}}.test.js',
      templateFile: './domain/reducer.test.js.hbs',
      abortOnFail: true
    });

    actions.push({
      type: 'modify',
      path: '../src/{{section}}/reducers/index.js',
      pattern: /(.*from\s'.\/[a-zA-Z]*';\n)(?!.*from\s'.\/[a-zA-Z]*';)/g,
      templateFile: './domain/add-reducer-import.hbs',
      abortOnFail: true
    });
    actions.push({
      type: 'modify',
      path: '../src/{{section}}/reducers/index.js',
      pattern: /(\s[a-z\-A-Z,]+)\n(?!.*\s[a-z\-A-Z,]+)/g,
      templateFile: './domain/add-reducer.hbs',
      abortOnFail: true
    });

    return actions;
  }
};
