/**
 * Component Generator
 */

module.exports = {
  description: 'Add an unconnected component',
  prompts: [
    {
      type: 'list',
      name: 'section',
      message: 'For which section should it be?',
      default: 'common',
      choices: () => ['admin', 'common', 'user']
    },
    {
      type: 'confirm',
      name: 'wantMessages',
      default: true,
      message: 'Do you want i18n messages (i.e. will this component use text)?'
    },
    {
      type: 'list',
      name: 'domain',
      message: 'Which domain should be used?',
      default: 'items',
      when: answers => answers.wantMessages,
      choices: answers => {
        switch (answers.section) {
          case 'admin':
            return ['formats', 'items', 'members', 'surveys', 'tests'];
          case 'common':
            return ['language', 'member', 'ui'];
          case 'user':
            return ['assessment', 'landing'];
          default:
            return [];
        }
      }
    },
    {
      type: 'list',
      name: 'type',
      message: 'Select the type of component',
      default: 'Stateless Function',
      choices: () => ['Stateless Function', 'ES6 Class (Pure)', 'ES6 Class']
    },
    {
      type: 'input',
      name: 'name',
      message: 'What should it be called?',
      default: 'Button'
    }
  ],
  actions: data => {
    // Generate index.js and index.test.js
    let componentTemplate;

    switch (data.type) {
      case 'ES6 Class': {
        componentTemplate = './component/es6.js.hbs';
        break;
      }
      case 'ES6 Class (Pure)': {
        componentTemplate = './component/es6.pure.js.hbs';
        break;
      }
      case 'Stateless Function': {
        componentTemplate = './component/stateless.js.hbs';
        break;
      }
      default: {
        componentTemplate = './component/es6.js.hbs';
      }
    }

    const actions = [
      {
        type: 'add',
        path: '../src/{{section}}/components/{{properCase name}}.js',
        templateFile: componentTemplate,
        abortOnFail: true
      },
      {
        type: 'add',
        path: '../src/{{section}}/components/{{properCase name}}.md',
        templateFile: './component/index.md.hbs',
        abortOnFail: true
      }
    ];

    return actions;
  }
};
