/**
 * Container Generator
 */

module.exports = {
  description: 'Add a container component',
  prompts: [
    {
      type: 'list',
      name: 'section',
      message: 'For which section should it be?',
      default: 'common',
      choices: () => ['admin', 'common', 'user']
    },
    {
      type: 'list',
      name: 'domain',
      message: 'Which domain should be used?',
      default: 'items',
      choices: answers => {
        switch (answers.section) {
          case 'admin':
            return ['formats', 'items', 'members', 'surveys', 'tests'];
          case 'common':
            return ['language', 'member', 'ui', 'notifications'];
          case 'user':
            return ['assessment', 'landing'];
          default:
            return [];
        }
      }
    },
    {
      type: 'input',
      name: 'name',
      message: 'What should it be called?',
      default: 'Form'
    },
    {
      type: 'list',
      name: 'component',
      message: 'Select a base component:',
      default: 'PureComponent',
      choices: () => ['PureComponent', 'Component']
    },
    {
      type: 'confirm',
      name: 'wantAppBar',
      default: true,
      message: 'Do you want an app bar for this container?'
    },
    {
      type: 'confirm',
      name: 'wantMessages',
      default: true,
      message: 'Do you want i18n messages (i.e. will this component use text)?'
    }
  ],
  actions: data => {
    // Generate index.js and index.test.js
    const actions = [
      {
        type: 'add',
        path: '../src/{{section}}/containers/{{properCase name}}.js',
        templateFile: './container/index.js.hbs',
        abortOnFail: true
      },
      {
        type: 'add',
        path: '../src/{{section}}/containers/{{properCase name}}.md',
        templateFile: './container/index.md.hbs',
        abortOnFail: true
      }
    ];
    return actions;
  }
};
