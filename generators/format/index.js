/**
 * Format Generator
 */
module.exports = {
  description: 'Add a format',
  prompts: [
    {
      type: 'input',
      name: 'name',
      message: 'What should it be called?',
      default: 'Questionnaire'
    }
  ],
  actions: data => {
    const actions = [
      {
        type: 'add',
        path: '../src/formats/{{ camelCase name}}/{{ properCase name }}.js',
        templateFile: './format/Component.js.hbs',
        abortOnFail: true
      },
      {
        type: 'add',
        path: '../src/formats/{{camelCase name}}/{{ properCase name }}.md',
        templateFile: './format/Component.md.hbs',
        abortOnFail: true
      },
      {
        type: 'add',
        path: '../src/formats/{{camelCase name}}/index.js',
        templateFile: './format/index.js.hbs',
        abortOnFail: true
      },
      {
        type: 'add',
        path: '../src/formats/{{camelCase name}}/messages.js',
        templateFile: './format/messages.js.hbs',
        abortOnFail: true
      },
      {
        type: 'add',
        path: '../src/formats/{{camelCase name}}/reducer.js',
        templateFile: './format/reducer.js.hbs',
        abortOnFail: true
      },
      {
        type: 'add',
        path: '../src/formats/{{camelCase name}}/reducer.test.js',
        templateFile: './format/reducer.test.js.hbs',
        abortOnFail: true
      },
      {
        type: 'add',
        path: '../src/formats/{{camelCase name}}/actions.js',
        templateFile: './format/actions.js.hbs',
        abortOnFail: true
      },
      {
        type: 'add',
        path: '../src/formats/{{camelCase name}}/actions.test.js',
        templateFile: './format/actions.test.js.hbs',
        abortOnFail: true
      },
      {
        type: 'add',
        path: '../src/formats/{{camelCase name}}/constants.js',
        templateFile: './format/constants.js.hbs',
        abortOnFail: true
      },
      {
        type: 'add',
        path: '../src/formats/{{camelCase name}}/Form.js',
        templateFile: './format/Form.js.hbs',
        abortOnFail: true
      },
      {
        type: 'add',
        path: '../src/formats/{{camelCase name}}/Form.md',
        templateFile: './format/Form.md.hbs',
        abortOnFail: true
      },
      {
        type: 'add',
        path: '../src/formats/{{camelCase name}}/selectors.js',
        templateFile: './format/selectors.js.hbs',
        abortOnFail: true
      },
      {
        type: 'add',
        path: '../src/formats/{{camelCase name}}/selectors.test.js',
        templateFile: './format/selectors.test.js.hbs',
        abortOnFail: true
      },
      {
        type: 'modify',
        path: '../src/formats/index.js',
        pattern: /(from\s'.\/[a-zA-Z]+';\n)(?!.*from\s'.\/[a-zA-Z]+';)/g,
        templateFile: './format/add-format-import.hbs'
      },
      {
        type: 'modify',
        path: '../src/formats/index.js',
        pattern: /(:\s[a-z\-A-Z,]+)\n(?!.*:\s[a-z\-A-Z,]+)/g,
        templateFile: './format/add-format.hbs'
      }
    ];
    return actions;
  }
};
