/**
 * generator/index.js
 *
 * Exports the generators so plop knows them
 */

const componentGenerator = require('./component/index.js');
const containerGenerator = require('./container/index.js');
const languageGenerator = require('./language/index.js');
const formatGenerator = require('./format/index.js');
const domainGenerator = require('./domain/index.js');

module.exports = plop => {
  plop.setGenerator('component', componentGenerator);
  plop.setGenerator('container', containerGenerator);
  plop.setGenerator('language', languageGenerator);
  plop.setGenerator('format', formatGenerator);
  plop.setGenerator('domain', domainGenerator);
  plop.addHelper('curly', (object, open) => (open ? '{' : '}'));
};
