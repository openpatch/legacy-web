# Contributing to COMMOOP Web

## Table of Contents

* [How Can I Contribute?](#how-can-i-contribute)
  * [Reporting Bugs](#reporting-bugs)
  * [Suggesting Enhancements](#suggesting-enhancements)
  * [Code Contribution](#code-contribution)
  * [Translation Contribution](#translation-contribution)
* [Styleguides](#styleguides)
  * [Git Commit Messages](#git-commit-messages)
  * [JavaScript Styleguide](#javascript-styleguide)
  * [Documentation Styleguide](#documentation-styleguide)
* [Additional Notes](#additional-notes)
  * [Issue and Merge Request Labels](#issue-and-merge-request-labels)

## How Can I Contribute?

### Reporting Bugs

This section guides you through submitting a bug report for COMMOOP Web. Following these guidelines helps future maintainers and other developers understand your report, reproduce the behavior, and find related reports.

Before creating bug reports, please check if this bug was not already reported. Therefore, visit the [issues page](https://git.uni-due.de/commoop/commoop-web/issues). When you find the same report you can add further detail in the comment section or just give a :thumbsup: to indicate that it also effects you.

#### How Do I Submit a Bug Report?

This is simple! Just create a new issue using the bug template. This template will guide you to submit a understandable bug report.

### Suggesting Enhancements

This is also very simple, because we have a template for this, too. But we distinguish between a **Feature Proposal** and a **Format Proposal**. A **Feature Proposal** is something that enhances the website. A **Format Propsal** is used when you what to propose a new format.

### Code Contribution

1. Select an issue from the [issue board](https://gitlab.com/commoop/commoop-web/issues) or write a new issue, before starting to code
1. Clone the repository, if not already done `git clone git@gitlab.com:commoop/commoop-web.git`
1. Checkout a new branch `git checkout -b {issue-number}-{issue-title}` e.g.: `git checkout -b 6-admin-login-page`
1. Use the generators if implementing a new component, container or format `yarn plop`
1. Commit and push to this branch as often as you like
1. Be sure to test your code
1. Be sure to document your code
1. Submit a [merge request](https://gitlab.com/commoop/commoop-web/merge_requests), when you are finished with the implementation

### Translation Contribution

1. Select an translation issue from the [issue board](https://gitlab.com/commoop/commoop-web/issues?label_name%5B%5D=translation) or write a new issue, before starting to translate
1. Clone the repository, if not already done `git clone git@gitlab.com:commoop/commoop-web.git`
1. Checkout a new branch `git checkout -b {issue-number}-{issue-title}` e.g.: `git checkout -b 10-support-french`
1. Use the generator if translating to a new language `npm plop language`
1. Commit and push to this branch as often as you like
1. Submit a [merge request](https://gitlab.com/commoop/commoop-web/merge_requests), when you are finished with the translation

## Styleguides

### Git Commit Messages

* Use the present tense ("Add feature" not "Added feature")
* Use the imperative mood ("Move cursor to..." not "Moves cursor to...")
* Limit the first line to 72 characters or less
* Reference issues and merge request liberally after the first line
* When only changing documentation, include `[ci skip]` in the commit description
* Consider starting the commit message with an applicable emoji:
  * :art: `:art:` when improving the format/structure of the code
  * :racehorse: `:racehorse:` when improving performance
  * :construction: `:construction:` when adding and/or extending tests
  * :memo: `:memo:` when writing docs
  * :bug: `:bug:` when fixing a bug
  * :fire: `:fire:` when removing code or files
  * :green_heart: `:green_heart:` when fixing the CI build
  * :sparkles: `:sparkles:` when add new features

### JavaScript Styleguide

When you commit your code [Prettier](https://github.com/prettier/prettier) transforms your code to match our Styleguide. So no need to bother.

### Documentation Styleguide

When documenting source code use [jsdoc](http://usejsdoc.org/) for regular javascript files and [prop-types](https://reactjs.org/docs/typechecking-with-proptypes.html) and [Storybook](https://storybook.js.org/) for React components.

## Additional Notes

### Issue and Pull Request Labels

This section lists the labels we use to help us track and manage issues and merge requests. Most labels are also used in other COMMOOP repositories.

| Label name         | search                                                                                       | Description                                      |
| ------------------ | -------------------------------------------------------------------------------------------- | ------------------------------------------------ |
| `bug`              | [:mag:](https://git.uni-due.de/commoop/commoop-web/issues?label_name%5B%5D=bug)              | Reports that are very likely to be bugs.         |
| `confirmed`        | [:mag:](https://git.uni-due.de/commoop/commoop-web/issues?label_name%5B%5D=confirmed)        | A bug which could be reproduced                  |
| `critical`         | [:mag:](https://git.uni-due.de/commoop/commoop-web/issues?label_name%5B%5D=critical)         | A critical bug which should be fixed immediately |
| `discussion`       | [:mag:](https://git.uni-due.de/commoop/commoop-web/issues?label_name%5B%5D=discussion)       | Something which needs some discussion            |
| `documentation`    | [:mag:](https://git.uni-due.de/commoop/commoop-web/issues?label_name%5B%5D=documentation)    | Related to any type of documentation             |
| `translation`      | [:mag:](https://git.uni-due.de/commoop/commoop-web/issues?label_name%5B%5D=translation)      | Related to any type of translation               |
| `enhancement`      | [:mag:](https://git.uni-due.de/commoop/commoop-web/issues?label_name%5B%5D=enhancement)      | Something to enhance the user experience         |
| `feature proposal` | [:mag:](https://git.uni-due.de/commoop/commoop-web/issues?label_name%5B%5D=feature+proposal) | Proposal of a new feature                        |
| `format proposal`  | [:mag:](https://git.uni-due.de/commoop/commoop-web/issues?label_name%5B%5D=format+proposal)  | Proposal of a new format                         |
| `support`          | [:mag:](https://git.uni-due.de/commoop/commoop-web/issues?label_name%5B%5D=support)          | Used for tracking support requests               |
